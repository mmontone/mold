/* gl3backend.c */
//#define GLEW_STATIC
//#include <GL/glew.h>
#ifdef __APPLE__
#   define GLFW_INCLUDE_GLCOREARB
#endif
//#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include "nanovg.h"
#define NANOVG_GL2_IMPLEMENTATION
#include "nanovg_gl.h"
