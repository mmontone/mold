/* gl3backend.c */
//#define GLEW_STATIC
//#include <GL/glew.h>
#ifdef __APPLE__
#   define GLFW_INCLUDE_GLCOREARB
#endif
//#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include "nanovg.h"
#define NANOVG_GL3_IMPLEMENTATION
#define MESA_GL_VERSION_OVERRIDE "3.2"
#include "nanovg_gl.h"
