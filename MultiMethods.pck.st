'From Cuis 6.0 [latest update: #5686] on 15 March 2023 at 11:39:22 pm'!
'Description Implementation of Clojure style Multi Methods.'!
!provides: 'MultiMethods' 1 7!
SystemOrganization addCategory: 'MultiMethods-Examples'!
SystemOrganization addCategory: 'MultiMethods-Tests'!
SystemOrganization addCategory: 'MultiMethods'!


!classDefinition: #MultiMethod category: 'MultiMethods'!
Object subclass: #MultiMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods'!
!classDefinition: 'MultiMethod class' category: 'MultiMethods'!
MultiMethod class
	instanceVariableNames: ''!

!classDefinition: #EncounterMethod category: 'MultiMethods-Examples'!
MultiMethod subclass: #EncounterMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'EncounterMethod class' category: 'MultiMethods-Examples'!
EncounterMethod class
	instanceVariableNames: ''!

!classDefinition: #BunnyEncountersLionMethod category: 'MultiMethods-Examples'!
EncounterMethod subclass: #BunnyEncountersLionMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'BunnyEncountersLionMethod class' category: 'MultiMethods-Examples'!
BunnyEncountersLionMethod class
	instanceVariableNames: ''!

!classDefinition: #LionEncountersBunnyMethod category: 'MultiMethods-Examples'!
EncounterMethod subclass: #LionEncountersBunnyMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'LionEncountersBunnyMethod class' category: 'MultiMethods-Examples'!
LionEncountersBunnyMethod class
	instanceVariableNames: ''!

!classDefinition: #ExtensibleVisitorMethod category: 'MultiMethods-Examples'!
MultiMethod subclass: #ExtensibleVisitorMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'ExtensibleVisitorMethod class' category: 'MultiMethods-Examples'!
ExtensibleVisitorMethod class
	instanceVariableNames: ''!

!classDefinition: #BoxedMorphExtensibleVisitorMethod category: 'MultiMethods-Examples'!
ExtensibleVisitorMethod subclass: #BoxedMorphExtensibleVisitorMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'BoxedMorphExtensibleVisitorMethod class' category: 'MultiMethods-Examples'!
BoxedMorphExtensibleVisitorMethod class
	instanceVariableNames: ''!

!classDefinition: #MorphExtensibleVisitorMethod category: 'MultiMethods-Examples'!
ExtensibleVisitorMethod subclass: #MorphExtensibleVisitorMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'MorphExtensibleVisitorMethod class' category: 'MultiMethods-Examples'!
MorphExtensibleVisitorMethod class
	instanceVariableNames: ''!

!classDefinition: #PasteUpMorphExtensibleVisitorMethod category: 'MultiMethods-Examples'!
ExtensibleVisitorMethod subclass: #PasteUpMorphExtensibleVisitorMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'PasteUpMorphExtensibleVisitorMethod class' category: 'MultiMethods-Examples'!
PasteUpMorphExtensibleVisitorMethod class
	instanceVariableNames: ''!

!classDefinition: #ShapeAreaMethod category: 'MultiMethods-Examples'!
MultiMethod subclass: #ShapeAreaMethod
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiMethods-Examples'!
!classDefinition: 'ShapeAreaMethod class' category: 'MultiMethods-Examples'!
ShapeAreaMethod class
	instanceVariableNames: ''!


!EncounterMethod commentStamp: 'MM 3/15/2023 23:01:19' prior: 0!
EncounterMethod valueWithArguments: nil

EncounterMethod valueWithArguments: {1}.
EncounterMethod valueWithArguments: {1. 2}.

EncounterMethod valueWithArguments: #(lion bunny).
EncounterMethod valueWithArguments: #(bunny lion).!

!ExtensibleVisitorMethod commentStamp: 'MM 3/15/2023 23:36:02' prior: 0!
ExtensibleVisitorMethod valueWithArguments: {Morph new}.
ExtensibleVisitorMethod valueWithArguments: {BoxedMorph new}.
ExtensibleVisitorMethod valueWithArguments: {LayoutMorph new}.
ExtensibleVisitorMethod valueWithArguments: {PasteUpMorph new}.
ExtensibleVisitorMethod valueWithArguments: {WorldMorph new}.!

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:19:05'!
appliesTo: arguments

	self test isSymbol ifTrue: 	[
		^ self dispatchValue perform: self test 
			with: arguments].
	self test isBlock ifTrue: [
		^ self test value: self dispatchValue value: arguments].
	self error: 'Invalid test' ! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:32:57'!
arguments

	^ self subclassResponsibility ! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:43:22'!
dispatch: arguments

	^ arguments
! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:51:09'!
dispatchValue

	^ self subclassResponsibility ! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:29:35'!
preferMethod: method1 or: method2
	
	^ self subclassResponsibility ! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:31:39'!
test
	^ #=! !

!MultiMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:23:00'!
valueWithArguments: arguments
	
	|methods applyableMethods dispatchedArguments|
	
	self assert: arguments isCollection.
	self assert: arguments size = self arguments size.
	
	methods := self allSubclasses.
	dispatchedArguments := self dispatch: arguments.
	
	applyableMethods := methods select: [:method |
		method appliesTo: dispatchedArguments].
	
	applyableMethods isEmpty ifTrue: [
		self error: 'No applyable method for: ', arguments asString].
	
	applyableMethods size = 1 
		ifTrue: [ |method|
			method := applyableMethods first.
			^ method valueWithArguments: arguments]
		ifFalse: [ | sortedMethods method |
			sortedMethods := applyableMethods sorted: [:m1 :m2 | self preferMethod: m1 or: m2].
			method := sortedMethods first.
			^ method valueWithArguments: arguments].! !

!EncounterMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:59:13'!
arguments

	^ #(anAnimal anotherAnimal)! !

!BunnyEncountersLionMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:59:20'!
dispatchValue

	^ #(bunny lion)! !

!BunnyEncountersLionMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:54:24'!
valueWithArguments: animals

	^ #runAway! !

!LionEncountersBunnyMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:59:25'!
dispatchValue

	^ #(lion bunny)! !

!LionEncountersBunnyMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:55:58'!
valueWithArguments: arguments

	^ #eat! !

!ExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:04:37'!
arguments
	^ #(visitedObject)! !

!ExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:29:38'!
preferMethod: method1 or: method2

	|x y|
	
	x := method1 dispatchValue.
	y := method2 dispatchValue.
	
	^ x inheritsFrom: y! !

!ExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:35:27'!
test
	^ [:dispatchValue :args | args first isKindOf: dispatchValue]! !

!BoxedMorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:29:10'!
dispatchValue

	^ BoxedMorph! !

!BoxedMorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:12:33'!
valueWithArguments: arguments

	^ #thingForBoxedMorph! !

!MorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:29:16'!
dispatchValue

	^ Morph! !

!MorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:12:10'!
valueWithArguments: arguments

	^ #thingForMorph! !

!PasteUpMorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:29:23'!
dispatchValue

	^ PasteUpMorph! !

!PasteUpMorphExtensibleVisitorMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 23:25:58'!
valueWithArguments: args

	^ #thingForPasteUpMorph! !

!ShapeAreaMethod class methodsFor: 'as yet unclassified' stamp: 'MM 3/15/2023 22:41:39'!
arguments

	^#(aShape. anotherShape)! !
