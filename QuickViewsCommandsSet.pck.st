'From Cuis 5.0 [latest update: #4528] on 15 February 2021 at 11:27:53 am'!
'Description Commander commands set for QuickViews.'!
!provides: 'QuickViewsCommandsSet' 1 2!
!requires: 'QuickViews' 1 30 nil!
!requires: 'Commander' 1 92 nil!
SystemOrganization addCategory: 'QuickViewsCommandsSet'!


!classDefinition: #QuickViewsCommandsSet category: 'QuickViewsCommandsSet'!
MethodsCommandsSet subclass: #QuickViewsCommandsSet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViewsCommandsSet'!
!classDefinition: 'QuickViewsCommandsSet class' category: 'QuickViewsCommandsSet'!
QuickViewsCommandsSet class
	instanceVariableNames: ''!


!QuickViewsCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:30:35'!
commandsSetName
	^ 'Quick views'! !

!QuickViewsCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:00:18'!
doItAndViewCommand
	<command>
	^ PluggableCommand new
		commandName: 'do it and view';
		commandCategory: 'evaluation';
		arguments: {CommandArgument new name: #string;
									type: String;
									yourself 	};
			description: 'Evaluate string and view the result (QuickViews)';
			runBlock: [:str | |result| 
						result _ Compiler evaluate: str.
						result isSequenceable 
							ifTrue: [
								(QuickList on: result)
									actions: {'Inspect' -> #inspect};
									openTitled: 'A collection'.]
							ifFalse: [result inspect]]! !

!QuickViewsCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:33:17'!
quickListCommand
	<command>
	^ PluggableCommand new
		commandName: 'quick list';
		commandCategory: 'QuickViews';
		arguments: {CommandArgument new name: #string;
									type: SequenceableCollection ;
									yourself 	};
			description: 'Visualize as list (QuickViews)';
			runBlock: [:list | 			(QuickList on: list)
									actions: {'Inspect' -> #inspect};
									openTitled: 'A list'.]! !
