'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 4 October 2019 at 1:11:46.302968 pm'!
'Description MetaBrowser extensions for Meld.'!
!provides: 'MeldMetaBrowser' 1 1!
!requires: 'Meld' 1 192 nil!
!requires: 'MetaBrowser' 1 42 nil!
!classDefinition: #BrowsableMeldClass category: #MeldMetaBrowser!
BrowsableObject subclass: #BrowsableMeldClass
	instanceVariableNames: 'class'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'BrowsableMeldClass class' category: #MeldMetaBrowser!
BrowsableMeldClass class
	instanceVariableNames: ''!

!classDefinition: #MeldBrowseObject category: #MeldMetaBrowser!
BrowseObject subclass: #MeldBrowseObject
	instanceVariableNames: 'meldSpec class'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'MeldBrowseObject class' category: #MeldMetaBrowser!
MeldBrowseObject class
	instanceVariableNames: ''!

!classDefinition: #FSMStatesAspect category: #MeldMetaBrowser!
BrowseObjectAspect subclass: #FSMStatesAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'FSMStatesAspect class' category: #MeldMetaBrowser!
FSMStatesAspect class
	instanceVariableNames: ''!

!classDefinition: #FSMTransitionsAspect category: #MeldMetaBrowser!
BrowseObjectAspect subclass: #FSMTransitionsAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'FSMTransitionsAspect class' category: #MeldMetaBrowser!
FSMTransitionsAspect class
	instanceVariableNames: ''!

!classDefinition: #MeldPropertiesAspect category: #MeldMetaBrowser!
BrowseObjectAspect subclass: #MeldPropertiesAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'MeldPropertiesAspect class' category: #MeldMetaBrowser!
MeldPropertiesAspect class
	instanceVariableNames: ''!

!classDefinition: #PetitParserRulesAspect category: #MeldMetaBrowser!
BrowseObjectAspect subclass: #PetitParserRulesAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'PetitParserRulesAspect class' category: #MeldMetaBrowser!
PetitParserRulesAspect class
	instanceVariableNames: ''!

!classDefinition: #MeldMetaBrowserSystem category: #MeldMetaBrowser!
STMetaBrowserSystem subclass: #MeldMetaBrowserSystem
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'MeldMetaBrowserSystem class' category: #MeldMetaBrowser!
MeldMetaBrowserSystem class
	instanceVariableNames: ''!

!classDefinition: #MeldPropertiesAspectPane category: #MeldMetaBrowser!
BrowseObjectAspectPane subclass: #MeldPropertiesAspectPane
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldMetaBrowser'!
!classDefinition: 'MeldPropertiesAspectPane class' category: #MeldMetaBrowser!
MeldPropertiesAspectPane class
	instanceVariableNames: ''!


!MeldMetaBrowserSystem commentStamp: '<historical>' prior: 0!
A MetaBrowser system organization that takes into account Meld classes. !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:58'!
acceptedContents

	^ class smalltalkClass meldClassSpec definition! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:34'!
browseObject

	^ MeldBrowseObject on: class! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 22:37'!
compile

	class meldClassSpec compile! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 20:43'!
conceptMenu: aBrowser

	^ {{#label -> 'browse compiled class (b)'. #action -> #browseMethodFull. #icon -> #editFindReplaceIcon.#target->self.#args->{aBrowser}}.
	     {#label -> 'browse meld class (l)'. #action -> #browseMeldClass. #targetSelector -> #model. #icon -> #editFindReplaceIcon.#target->self.#args->{aBrowser}}.
		{#label -> 'explore meld spec (I)'. #action -> #exploreMeldSpec. #targetSelector -> #model. #icon->#exploreIcon.#target->self.#args->{aBrowser}}.
		{#label -> 'compile'. #action -> #compileMeldClass. #targetSelector -> #model. #icon->#scriptIcon.#target->self.#args->{aBrowser}}.
		{#label -> 'store'. #action -> #storeMeldClass. #targetSelector -> #model. #icon -> #saveIcon.#target->self.#args->{aBrowser}}.
		#-.
		{#label->'remove'.#action->#removeClass. #targetSelector->#model. #icon->#deleteIcon.#target->self.#args->{aBrowser}}}! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 21:08'!
contentsSymbolQuints: aBrowser

	^ class meldClass contentsSymbolQuints: aBrowser! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:52'!
description

	^ class comment ! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/24/2018 20:55'!
explorerContents

	^ class meldClass explorerContentsFor: self smalltalkClass! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:51'!
initialize: aClass

	class _ aClass
! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2018 10:36'!
meldClass

	^ class meldClass! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:51'!
name

	^ class name! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:52'!
printOn: aStream

	class printOn: aStream! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:52'!
smalltalkClass

	^ class! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 22:43'!
smalltalkMetaclass

	^ class meldClass! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 22:36'!
spec

	^ class meldClassSpec ! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 22:39'!
store

	class meldClassSpec store! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2018 10:39'!
updateContents: aSymbol context: aBrowserWindow

	|messageName receiver|
	
	(#(documentation source) includes: aSymbol) ifTrue: [
		^ super updateContents: aSymbol context: aBrowserWindow].
		
	messageName := ('update', aSymbol asString capitalized, 'Contents:') asSymbol.
	receiver _ class meldClassSpec.
	^ (receiver respondsTo: messageName) 
		ifTrue: [
			receiver perform: messageName with: aBrowserWindow]
		ifFalse: [
			self error: (receiver printString, ' can''t handle contents: ', aSymbol asString)]! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2018 10:25'!
updateDocumentationContents: aBrowserWindow

 	|editor|
									
	editor := TextModelMorph
					textProvider: self
					textGetter: #description
					textSetter: #description:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!BrowsableMeldClass methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2018 10:26'!
updateSourceContents: aBrowserWindow

	| browser editor |
		
	browser _ aBrowserWindow model.
		
	editor _ browser selectedConcept concept editorMorph.
		
	editor morphExtent: 1500 @ 1000.
	
	aBrowserWindow setContent: editor! !

!BrowsableMeldClass class methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:51'!
on: aClass

	^ self new initialize: aClass! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:59'!
acceptedContents

	^ class meldClassSpec definition! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2018 22:27'!
aspects

	^ super aspects, class meldClass aspects! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 12:19'!
definitionMorphClass

	^ MeldTextModelMorph! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 12:17'!
definitionTemplate

	^ class template: 'Unclassified'! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 11/27/2018 22:15'!
description

	^ String streamContents: [:s |
		   s nextPutAll: class meldClass title;
		   newLine; newLine;
		   nextPutAll: class meldClass description]! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 18:11'!
editorMorphClass

	^ MeldTextModelMorph ! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:54'!
initialize: aClass

	class _ aClass! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 16:21'!
textStyler

	^ class meldClass definitionTextStyler ! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 12:11'!
title

	^ class title! !

!MeldBrowseObject methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 12:13'!
updateDefinitionContents: aBrowserWindow
	
	|morph|
	
	morph _ self definitionMorphClass
				textProvider: self
				textGetter: #definitionTemplate
				textSetter: #defineFrom:notifying:.
			
	"Hack to store the browser temporarily."
	morph setProperty: #browser toValue: aBrowserWindow model. 
	
	aBrowserWindow setContent: morph! !

!MeldBrowseObject class methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:54'!
on: aClass
	
	^ self new initialize: aClass! !

!FSMStatesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:48'!
description

	^ 'FSM States'! !

!FSMStatesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:48'!
name

	^ 'States'! !

!FSMTransitionsAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:48'!
description

	^ 'FSM Transitions'! !

!FSMTransitionsAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:48'!
name

	^ 'Transitions'! !

!MeldPropertiesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 20:46'!
buildBrowserPanesFor: aConcept browser: aBrowser
	
	^ MeldPropertiesAspectPane on: aConcept aspect: self! !

!MeldPropertiesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 20:43'!
description

	^ 'Properties'! !

!MeldPropertiesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 20:43'!
name

	^ 'Properties'! !

!PetitParserRulesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:50'!
description

	^ 'Parser rules'! !

!PetitParserRulesAspect methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 16:50'!
name

	^ 'Rules'! !

!MeldMetaBrowserSystem methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 14:00'!
collectClasses: aCollection

	^ aCollection collect: [:aClass |
		aClass isMeldClass 
			ifTrue: [BrowsableMeldClass on: aClass]
			ifFalse: [BrowsableClass on: aClass]]! !

!MeldMetaBrowserSystem methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2018 12:10'!
conceptTypes

	^ MeldClass allSubclasses collect: [:aClass | MeldBrowseObject on: aClass]! !

!MeldPropertiesAspectPane methodsFor: 'as yet unclassified' stamp: 'MM 11/23/2018 20:46'!
build

	|propertiesMorph|
	
	propertiesMorph _ MeldPropertiesEditorMorph on: concept smalltalkClass.
	
	self addMorphUseAll: propertiesMorph! !
