'From Cuis 6.0 [latest update: #5848] on 15 June 2023 at 8:37:58 pm'!
'Description Structured Editor Framework

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'StructurEd' 1 52!
!requires: 'IMGUI' 1 78 nil!
SystemOrganization addCategory: 'StructurEd-UI'!
SystemOrganization addCategory: 'StructurEd'!


!classDefinition: #SEDEditorMorph category: 'StructurEd-UI'!
IMMorph subclass: #SEDEditorMorph
	instanceVariableNames: 'widgetBuilder editor hotWidget activeWidget cursorChanged contentsChanged autoCompleter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDEditorMorph class' category: 'StructurEd-UI'!
SEDEditorMorph class
	instanceVariableNames: ''!

!classDefinition: #SEDEditorLayout category: 'StructurEd-UI'!
IMFlexRowLayout subclass: #SEDEditorLayout
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDEditorLayout class' category: 'StructurEd-UI'!
SEDEditorLayout class
	instanceVariableNames: ''!

!classDefinition: #SEDNodeWidget category: 'StructurEd-UI'!
IMWidget subclass: #SEDNodeWidget
	instanceVariableNames: 'node line editor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDNodeWidget class' category: 'StructurEd-UI'!
SEDNodeWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDBooleanWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDBooleanWidget
	instanceVariableNames: 'label'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDBooleanWidget class' category: 'StructurEd-UI'!
SEDBooleanWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDColorWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDColorWidget
	instanceVariableNames: 'palette'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDColorWidget class' category: 'StructurEd-UI'!
SEDColorWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDNumberWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDNumberWidget
	instanceVariableNames: 'label'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDNumberWidget class' category: 'StructurEd-UI'!
SEDNumberWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDSelectionWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDSelectionWidget
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDSelectionWidget class' category: 'StructurEd-UI'!
SEDSelectionWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDSwitcherWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDSwitcherWidget
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDSwitcherWidget class' category: 'StructurEd-UI'!
SEDSwitcherWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDTextWidget category: 'StructurEd-UI'!
SEDNodeWidget subclass: #SEDTextWidget
	instanceVariableNames: 'font fontColor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDTextWidget class' category: 'StructurEd-UI'!
SEDTextWidget class
	instanceVariableNames: ''!

!classDefinition: #SEDEditorWidgetBuilder category: 'StructurEd-UI'!
Object subclass: #SEDEditorWidgetBuilder
	instanceVariableNames: 'editor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDEditorWidgetBuilder class' category: 'StructurEd-UI'!
SEDEditorWidgetBuilder class
	instanceVariableNames: ''!

!classDefinition: #SEDDefaultEditorWidgetBuilder category: 'StructurEd-UI'!
SEDEditorWidgetBuilder subclass: #SEDDefaultEditorWidgetBuilder
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd-UI'!
!classDefinition: 'SEDDefaultEditorWidgetBuilder class' category: 'StructurEd-UI'!
SEDDefaultEditorWidgetBuilder class
	instanceVariableNames: ''!

!classDefinition: #SEDEditor category: 'StructurEd'!
Object subclass: #SEDEditor
	instanceVariableNames: 'contents currentNode cursor selection contentsChanged lines'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDEditor class' category: 'StructurEd'!
SEDEditor class
	instanceVariableNames: ''!

!classDefinition: #SEDNode category: 'StructurEd'!
Object subclass: #SEDNode
	instanceVariableNames: 'start end selected line'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDNode class' category: 'StructurEd'!
SEDNode class
	instanceVariableNames: ''!

!classDefinition: #SEDBooleanNode category: 'StructurEd'!
SEDNode subclass: #SEDBooleanNode
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDBooleanNode class' category: 'StructurEd'!
SEDBooleanNode class
	instanceVariableNames: ''!

!classDefinition: #SEDColorNode category: 'StructurEd'!
SEDNode subclass: #SEDColorNode
	instanceVariableNames: 'color'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDColorNode class' category: 'StructurEd'!
SEDColorNode class
	instanceVariableNames: ''!

!classDefinition: #SEDNumberNode category: 'StructurEd'!
SEDNode subclass: #SEDNumberNode
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDNumberNode class' category: 'StructurEd'!
SEDNumberNode class
	instanceVariableNames: ''!

!classDefinition: #SEDSelectionNode category: 'StructurEd'!
SEDNode subclass: #SEDSelectionNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDSelectionNode class' category: 'StructurEd'!
SEDSelectionNode class
	instanceVariableNames: ''!

!classDefinition: #SEDTextNode category: 'StructurEd'!
SEDNode subclass: #SEDTextNode
	instanceVariableNames: 'text'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StructurEd'!
!classDefinition: 'SEDTextNode class' category: 'StructurEd'!
SEDTextNode class
	instanceVariableNames: ''!


!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
activeWidget
	"Answer the value of activeWidget"

	^ activeWidget! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
activeWidget: anObject
	"Set the value of activeWidget"

	activeWidget _ anObject! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/4/2019 10:51'!
cursorChanged
	"Answer the value of cursorChanged"

	^ cursorChanged! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/4/2019 10:51'!
cursorChanged: anObject
	"Set the value of cursorChanged"

	cursorChanged _ anObject! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
editor
	"Answer the value of editor"

	^ editor! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
editor: anObject
	"Set the value of editor"

	editor _ anObject! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
hotWidget
	"Answer the value of hotWidget"

	^ hotWidget! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2019 11:21'!
hotWidget: anObject
	"Set the value of hotWidget"

	hotWidget _ anObject! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:55'!
widgetBuilder
	"Answer the value of widgetBuilder"

	^ widgetBuilder ifNil: [widgetBuilder _ SEDDefaultEditorWidgetBuilder new
													editor: self;
													yourself]! !

!SEDEditorMorph methodsFor: 'accessing' stamp: 'MM 6/28/2019 00:35'!
widgetBuilder: anObject
	"Set the value of widgetBuilder"

	widgetBuilder _ anObject! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:49:43'!
browseNode: aNode
	BrowserWindow fullOnClass: aNode class selector: nil! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 14:30'!
contentsChanged
	^ contentsChanged! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/22/2020 09:29:08'!
initialize: anEditor
	editor _ anEditor.
	hotWidget _ nil.
	activeWidget _ nil.
	cursorChanged _ false.
	contentsChanged _ false.
	autoCompleter _ self initializeAutoCompleter.
	editor when: #contentsChanged send: #updateNodes to: self.! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/22/2020 09:32:38'!
initializeAutoCompleter
	editor autoCompleterClass ifNotNil: [:acClass |
		autoCompleter _  acClass withModel: editor.
		autoCompleter textMorph: self].
	^ autoCompleter
	
	! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:43:43'!
menuForNode: aNode

	| menu smalltalkMenu|
	
	smalltalkMenu := MenuMorph new defaultTarget: aNode.
	smalltalkMenu add: 'explore' action: #explore.
	smalltalkMenu add: 'inspect' action: #inspect.
	smalltalkMenu add: 'browse' target: self action: #browseNode: argument: aNode.
	
	menu := MenuMorph new defaultTarget: self.
	menu add:  'edit properties' target: aNode  action: #editProperties.
	menu add: 'remove' target: editor action: #removeElement: argument: aNode.
	aNode buildMenu: menu.
	menu addLine.
	menu add: 'Smalltalk' subMenu: smalltalkMenu.	
	
	^ menu ! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 14:31'!
updateNodes

	contentsChanged _ true! !

!SEDEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/8/2019 11:12'!
widgetFor: aNode
	^ "(aNode accept: self widgetBuilder)"
		(aNode accept: self widgetBuilder default: [SEDTextWidget on: aNode])
		editor: editor;
		yourself.! !

!SEDEditorMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2019 21:23'!
collectTextNodes: aString
	^ (aString subStrings: ' ') collect: [:s |
		SEDTextNode text: s].! !

!SEDEditorMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:36'!
on: anEditor
	^ self new initialize: anEditor! !

!SEDEditorMorph class methodsFor: 'examples' stamp: 'MM 6/28/2019 09:34'!
example1

	|contents editor|
	
	contents _ {SEDTextNode text: 'Edit the boolean: '.
				SEDBooleanNode value: false.
				SEDTextNode text: ' and this number: '.
				SEDNumberNode value: 10.
				SEDTextNode text: ' and this color: '.
				SEDColorNode color: Color white}.
				
	editor _ SEDEditor contents: contents.
	(SEDEditorMorph on: editor) openInWindow! !

!SEDEditorMorph class methodsFor: 'examples' stamp: 'MM 7/3/2019 18:02'!
example2

	|contents editor|
	
	contents _ OrderedCollection new.
	
	contents addAll: (self collectTextNodes: 'Edit the boolean: ');
			add: (SEDBooleanNode value: false);
			addAll: (self collectTextNodes: ' and this number: ');
			add: (SEDNumberNode value: 10);
			addAll: (self collectTextNodes: ' and this color: ');
			add: (SEDColorNode color: Color white).
				
	editor _ SEDEditor contents: contents.
	^ (SEDEditorMorph on: editor) openInWindow; yourself
	
	! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:37:45'!
afterDrawOn: aCanvas
	super afterDrawOn: aCanvas.
	self isHovered ifTrue: [
		container hotWidget: self].
	
	self isContextPressed ifTrue: [
		self doContextAction.
		self eventHandled: #contextPressed.
		^ self].
	
	container mouseEvent ifNotNil: [:ev | ev shiftPressed ifTrue: [
		self drag: self node with: [:dragCanvas|
			(self new: IMLabel)
				label: self contents;
				position: container mousePosition + 10;
				backgroundColor: Color blue;
				color: Color white;
				fitContents;
				drawOn: dragCanvas].
			]].	
	
	self whenDraggingOver: [:anObject | anObject isKindOf: SEDNode] do: [:aNode |
		aCanvas frameRectangle: self bounds borderWidth: 1 color: Color blue].
	
	self whenDropping: [:anObject| anObject isKindOf: SEDNode] do: [:aNode |
		Transcript show: 'Drop: '; show: aNode; cr.
		editor replaceNode: self node withNode: aNode]! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 14:19'!
beforeDrawOn: aCanvas

	self hasCursor ifTrue: [
		container keystroke ifNotNil: [:event |
			self processKeyStroke: event]].! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:17'!
cursor
	"Answer the value of cursor"

	^ editor cursor - node start! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:40:03'!
doContextAction
	^ self openMenu! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 14:29'!
hasCursor
	|cursor|
	container contentsChanged ifTrue: [^false].
	container cursorChanged ifTrue: [^false].
	cursor _ editor cursor.
	^ (node start <= cursor) & (cursor <= node end)! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:22'!
initialize: aNode
	node _ aNode! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:31:55'!
isContextPressed
	^ (container wasEventHandled: #contextPressed) not and: [container mouseButton2 = #down] and: [self containsPoint: container mousePosition ]! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:06'!
measure
	^ self subclassResponsibility ! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:50'!
moveCursorDown
	editor moveCursorDown.
	container cursorChanged: true.! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:51'!
moveCursorLeft
	editor moveCursorLeft.
	container cursorChanged: true.! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:51'!
moveCursorRight
	editor moveCursorRight.
	container cursorChanged: true.! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:51'!
moveCursorUp
	editor moveCursorUp.
	container cursorChanged: true.! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:40:42'!
openMenu
	|menu|
	
	menu _ container menuForNode: self node.
	
	menu popUpInWorld: container world! !

!SEDNodeWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/10/2023 13:53:07'!
processKeyStroke: aKeyStrokeEvent

	Transcript show: self node; show: ' processing keystroke';cr.
	"arrow left"
	aKeyStrokeEvent isArrowLeft ifTrue: [
		self moveCursorLeft.
		^ true].
	
	"arrow right"
	aKeyStrokeEvent isArrowRight ifTrue: [
		self moveCursorRight.
		^ true].
	
	"arrow up"
	aKeyStrokeEvent isArrowUp ifTrue: [
		self moveCursorUp.
		^ true].
	
	"arrow down"
	aKeyStrokeEvent isArrowDown ifTrue: [
		self moveCursorDown.
		^ true].
	
	^ false! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/30/2019 23:04'!
editor
	"Answer the value of editor"

	^ editor! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/30/2019 23:04'!
editor: anObject
	"Set the value of editor"

	editor _ anObject! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/28/2019 09:09'!
line
	"Answer the value of line"

	^ line! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/14/2020 01:38:47'!
line: anObject
	"Set the value of line"
	self assert: anObject isNil not.
	line _ anObject! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/28/2019 09:09'!
node
	"Answer the value of node"

	^ node! !

!SEDNodeWidget methodsFor: 'accessing' stamp: 'MM 6/28/2019 09:09'!
node: anObject
	"Set the value of node"

	node _ anObject! !

!SEDNodeWidget class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:21'!
on: aNode
	^ self new initialize: aNode! !

!SEDBooleanWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:56'!
drawWidgetOn: aCanvas
	label position: self position;
		drawOn: aCanvas.
		
	label isPressed ifTrue: [
		node value: node value not].! !

!SEDBooleanWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:31'!
measure
	
	node value 
		ifTrue: [
			label _ (self new: IMLabel)
				color: Color white;
				backgroundColor: Color green;
				label: 'True';
				fitContents;
				yourself]
		ifFalse: [
			label _ (self new: IMLabel)
				color: Color white;
				backgroundColor: Color red;
				label: 'False';
				fitContents;
				yourself
			].
		self extent: label extent.
		^ extent! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:17'!
close
	self setState: false! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:36'!
drawWidgetOn: aCanvas

	aCanvas fillRectangle: self bounds color: self node value.
	
	self isPressed ifTrue: [self toggle].
	
	self isOpened 
		ifTrue: [
			self popup: [:popupCanvas | |x y |
				
				x _ position x + 15.
				y _ position y + 15.
				
				1 to: 3 do: [:i |
					1 to: 3 do: [:j | |color cw|
						color _ palette at: (i + ((j - 1) * 3)).
						cw _ (self new: IMLabel)
									backgroundColor: color;
									label: '';
									extent: 15@15;
									position: x@y;
									drawOn: popupCanvas;
									yourself.
							
						cw isPressed ifTrue: [
							self node color: color.
							self close].
						
						x _ x + 15].
					
					x _ position x + 15.
					y _ y + 15]]]! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:17'!
initState
	^ false "opened state"! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:35'!
initialize
	super initialize.
	extent _ 15 @ 15.
	palette _ {Color white. Color blue. Color red. 
	                  Color yellow. Color orange. Color black.
	                  Color purple. Color green. Color brown}! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:17'!
isOpened
	^ self getState == true! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:07'!
measure
	^ self extent! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:17'!
open

	self setState: true! !

!SEDColorWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:18'!
toggle

	self isOpened ifTrue: [self close] ifFalse: [self open]! !

!SEDNumberWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:59'!
drawWidgetOn: aCanvas

	self isHovered ifFalse: [
		label color: Color black; backgroundColor: nil].
	
	label position: self position;
		 drawOn: aCanvas.
		
	label isPressed ifTrue: [
		node value: node value + 1]! !

!SEDNumberWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:33'!
measure
	
	label _ (self new: IMLabel)
				color: Color white;
				backgroundColor: Color blue;
				label: node value asString;
				fitContents;
				yourself.
	self extent: label extent.
	^ extent! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 14:58:57'!
afterDrawOn: aCanvas
	super afterDrawOn: aCanvas.
	self hasCursor ifTrue: [
		self drawTextCursorOn: aCanvas].
	self measure.! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 15:13:50'!
beforeDrawOn: aCanvas
	super beforeDrawOn: aCanvas.
		
	self measure.
	
	self isPressed ifTrue: [ |pos|
		pos _ self characterIndexAtPoint: container mousePosition.
		editor cursor:  node start + pos]! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/14/2020 01:41:39'!
characterIndexAtPoint: aPoint

	| ln block f cts|
	
	f _ self fontToUse.
	cts _ self contents.
	
	ln _ TextLine 
		start: 1
		stop: cts size
		internalSpaces: 0
		paddingWidth: 0.
	ln
		rectangle: self bounds;
		lineHeight: f lineSpacing baseline: f ascent.
		
	Transcript show: self bounds; cr.
		
	block _ (CharacterBlockScanner new text: 
			(cts asText font: f))
		characterBlockAtPoint: aPoint index: nil
		in: ln.
		
	Transcript show: block stringIndex; cr.

	^ block stringIndex! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/8/2019 11:08'!
contents
	^ node contents! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/14/2020 14:28:42'!
defaultFont
	^ FontFamily defaultFamilyAndPointSize! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/3/2019 19:35'!
defaultFontColor
	^ Color black! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 14:42'!
deleteMyself
	editor deleteNode: node! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 10:33'!
displayTextCursorAtX: x top: top bottom: bottom emphasis: emphasis on: aCanvas
	| textCursorColor x1 isBold isItalic x0 h w halfW r d |
	
	"Transcript show: 'Cursor atPos: '; show: {x. top. bottom}; cr."
	isBold _ emphasis allMask: 1.
	isItalic _ emphasis allMask: 2.
	textCursorColor _ Theme current textCursor.
	h _ bottom - top.
	w _ isBold
		ifTrue: [ h // 25 + 2 ]
		ifFalse: [ h // 30 + 1 ].
	halfW _ w // 2.
	isItalic
		ifTrue: [	
			"Keep tweaking if needed!!"
			d _ isBold ifTrue: [ 3 ] ifFalse: [ h // 24].
			x0 _ x- (h*5//24) + d.
			x1 _ x + d ]
		ifFalse: [
			x0 _ x.
			x1 _ x].
	x0 < halfW ifTrue: [
		x1 _ x1 - x0 + halfW.
		x0 _ halfW ].
	"r _ extent x-halfW-1.
	r < x1 ifTrue: [
		x0 _ x0 + r - x1.
		x1 _ r ]."
	"textCursorRect _ x0-halfW-1@ top corner: x1+halfW+1+1 @ bottom."
	"Transcript show: 'Line: '; show: {x0 + halfW@bottom. x1+halfW@(top + w)};cr."
	aCanvas
		line: x0+halfW@bottom to: x1+halfW@(top+w)
		width: w color: textCursorColor! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 4/4/2020 17:20:06'!
drawTextCursorOn: aCanvas

	| bottom x|
	
	bottom _ self fontToUse lineSpacing.
	x _ self fontToUse widthOfString: self contents from: 1 to: self cursor.
	self displayTextCursorAtX: (position x + x) top: position y bottom: (position y + bottom) emphasis: 0 on: aCanvas! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 15:13:41'!
drawWidgetOn: aCanvas
	
	|contents|
	
	contents _ self contents.	
	
	aCanvas drawString: contents at: position font: self fontToUse color: self fontColor.! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:11'!
fitContents
	extent  _ self measureContents ! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/3/2019 19:35'!
fontColor
	^ fontColor ifNil: [self defaultFontColor]! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/3/2019 19:35'!
fontToUse
	^ font ifNil: [self defaultFont]! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2019 23:30'!
insertCharacter: aCharacter
	editor insertCharacter: aCharacter! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:51'!
measure
	extent _ self measureContents! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 4/4/2020 17:20:13'!
measureContents
	| f |
	f _ self fontToUse.
	^ (f widthOfString: self contents)  @ f lineSpacing! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:13'!
node: aTextNode
	super node: aTextNode.
	self fitContents.! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 6/15/2023 20:25:23'!
processKeyStroke: aKeyStrokeEvent

	|char|

	(super processKeyStroke: aKeyStrokeEvent)
		ifTrue: [^ true].
		
	"backspace"
	aKeyStrokeEvent isBackspace ifTrue: [
		self removeCharacterLeft.
		^ true].
	
	"delete myself"
	aKeyStrokeEvent isDelete and: [
		aKeyStrokeEvent shiftPressed ] :: ifTrue: [
		self deleteMyself.
		^ true].
	
	"delete"
	aKeyStrokeEvent isDelete ifTrue: [
		self removeCharacterRight.
		^ true].	
	
	"otherwise, insert character at cursor position"
	char := container keystroke keyCharacter asCharacter. "Prevent utf8CodePoint"
	self insertCharacter: char.
	^ true! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 11:39'!
removeCharacterLeft
	editor removeCharacterLeft! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 13:38'!
removeCharacterRight
	editor removeCharacterRight! !

!SEDTextWidget methodsFor: 'as yet unclassified' stamp: 'MM 7/6/2019 11:37'!
replaceContents: aString
	editor replaceSubstringFrom: node start to: node end  with: aString ! !

!SEDTextWidget methodsFor: 'accessing' stamp: 'MM 7/3/2019 19:30'!
font
	"Answer the value of font"

	^ font! !

!SEDTextWidget methodsFor: 'accessing' stamp: 'MM 7/3/2019 19:30'!
font: anObject
	"Set the value of font"

	font _ anObject! !

!SEDTextWidget methodsFor: 'accessing' stamp: 'MM 7/3/2019 19:30'!
fontColor: anObject
	"Set the value of fontColor"

	fontColor _ anObject! !

!SEDEditorWidgetBuilder methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:56'!
editor
	"Answer the value of editor"

	^ editor! !

!SEDEditorWidgetBuilder methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:56'!
editor: anObject
	"Set the value of editor"

	editor _ anObject! !

!SEDDefaultEditorWidgetBuilder methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:23'!
visitBooleanNode: aBooleanNode
	^ SEDBooleanWidget on: aBooleanNode! !

!SEDDefaultEditorWidgetBuilder methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:23'!
visitColorNode: aColorNode
	^ SEDColorWidget on: aColorNode! !

!SEDDefaultEditorWidgetBuilder methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:23'!
visitNumberNode: aNode
	^ SEDNumberWidget on: aNode! !

!SEDDefaultEditorWidgetBuilder methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:22'!
visitTextNode: aTextNode
	^ SEDTextWidget on: aTextNode! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/22/2020 09:33:14'!
autoCompleterClass
	^ nil! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:35'!
calculateCurrentNode
	^ contents detect: [:aNode | (aNode start <= cursor) & (cursor <= aNode end)]! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:49'!
contents
	^ contents! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 12:11'!
contents: aCollection
	contents _ aCollection.
	self updateNodesLines .
	contentsChanged _ true! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 11:55'!
contentsChanged
	^ contentsChanged! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:29'!
currentNode
	^ currentNode ifNil: [currentNode _ self calculateCurrentNode]! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2019 23:05'!
cursorChanged
	^ false! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 12:07'!
groupNodesByLine: aCollection

	|currentLine rows row|
	
	aCollection ifEmpty: [^ OrderedCollection new].
	
	rows _ OrderedCollection new.
	row _ OrderedCollection new.
	
	currentLine _ aCollection first line.
	
	aCollection do: [:aNode |
		aNode line = currentLine ifFalse: [
			currentLine _ aNode line.
			rows add: row.
			row _ OrderedCollection new.].
		row add: aNode].
	rows add: row.
	^ rows! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:31'!
initialize
	cursor _ 1.
	contentsChanged _ true
	! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:29'!
moveCursorDown

	|currLine|
	
	currLine _ self currentLine.
	
	(self lines size = currLine) ifTrue: [^self].
	
	self cursor:  (self lines at: currLine + 1) first start
! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:27'!
moveCursorLeft
	self cursor: ((cursor - 1) max: 1)! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/6/2019 10:52'!
moveCursorRight
	contents detect: [:aNode | (aNode start <= (cursor + 1)) & ((cursor + 1) <= aNode end)] 
			ifFound: [:found | self cursor: cursor + 1] 
			ifNone: [] 
	! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:27'!
moveCursorToEndOfLine: aNumber
	self cursor: (lines at: aNumber) last end! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:39'!
moveCursorUp

	|currLine|

	currLine _ self currentLine.
	
	(currLine <= 1) ifTrue: [^self].
	
	self cursor: (self lines at: currLine - 1) first start
! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/5/2019 23:30'!
updateCurrentNode

	^ currentNode _ self calculateCurrentNode.! !

!SEDEditor methodsFor: 'as yet unclassified' stamp: 'MM 7/4/2019 12:07'!
updateNodesLines
	lines _ self groupNodesByLine: contents! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/4/2019 11:19'!
contentsChanged: anObject
	"Set the value of contentsChanged"

	contentsChanged _ anObject.
	contentsChanged ifTrue: [
		self triggerEvent: #contentsChanged]! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/5/2019 23:38'!
currentLine
	"Answer the value of currentLine"

	^ self currentNode line! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:38'!
cursor
	"Answer the value of cursor"

	^ cursor! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/5/2019 23:31'!
cursor: anObject
	"Set the value of cursor"

	cursor _ anObject.
	self updateCurrentNode.
	! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/4/2019 12:07'!
lines
	"Answer the value of lines"

	^ lines! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/4/2019 12:07'!
lines: anObject
	"Set the value of lines"

	lines _ anObject! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:38'!
selection
	"Answer the value of selection"

	^ selection! !

!SEDEditor methodsFor: 'accessing' stamp: 'MM 7/3/2019 21:38'!
selection: anObject
	"Set the value of selection"

	selection _ anObject! !

!SEDEditor methodsFor: 'text-model' stamp: 'MM 6/22/2020 09:48:34'!
actualContents
	^ contents! !

!SEDEditor methodsFor: 'text-model' stamp: 'MM 6/22/2020 09:47:20'!
hasSelection
	^ false! !

!SEDEditor methodsFor: 'text-model' stamp: 'MM 6/22/2020 09:48:06'!
startIndex
	^ cursor! !

!SEDEditor methodsFor: 'text-model' stamp: 'MM 6/22/2020 09:50:33'!
textSize
	^ self actualContents size! !

!SEDEditor class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:05'!
contents: aCollection
	^ self new contents: aCollection; yourself! !

!SEDNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:16'!
accept: aVisitor
	^ self subclassResponsibility ! !

!SEDNode methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2020 23:45:04'!
buildMenu: aMenu
	"Add menu items specific of the node to aMenu"! !

!SEDNode methodsFor: 'as yet unclassified' stamp: 'MM 6/10/2023 13:34:57'!
line

	^ line! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 7/1/2019 14:19'!
end
	"Answer the value of end"

	^ end! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 7/1/2019 14:19'!
end: anObject
	"Set the value of end"

	end _ anObject! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 6/27/2019 23:58'!
selected
	"Answer the value of selected"

	^ selected! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 6/27/2019 23:58'!
selected: anObject
	"Set the value of selected"

	selected _ anObject! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 7/1/2019 14:19'!
start
	"Answer the value of start"

	^ start! !

!SEDNode methodsFor: 'accessing' stamp: 'MM 7/1/2019 14:19'!
start: anObject
	"Set the value of start"

	start _ anObject! !

!SEDBooleanNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:17'!
accept: aVisitor
	^ aVisitor visitBooleanNode: self! !

!SEDBooleanNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:01'!
value
	^ value! !

!SEDBooleanNode methodsFor: 'accessing' stamp: 'MM 6/28/2019 00:01'!
value: anObject
	"Set the value of value"

	value _ anObject! !

!SEDBooleanNode class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:03'!
value: aBoolean
	^ self new value: aBoolean; yourself ! !

!SEDColorNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:17'!
accept: aVisitor
	^ aVisitor visitColorNode: self! !

!SEDColorNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 09:10'!
value
	^ color! !

!SEDColorNode methodsFor: 'accessing' stamp: 'MM 6/28/2019 00:02'!
color
	"Answer the value of color"

	^ color! !

!SEDColorNode methodsFor: 'accessing' stamp: 'MM 6/28/2019 00:02'!
color: anObject
	"Set the value of color"

	color _ anObject! !

!SEDColorNode class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:02'!
color: aColor
	^ self new color: aColor; yourself ! !

!SEDNumberNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:17'!
accept: aVisitor
	^ aVisitor visitNumberNode: self! !

!SEDNumberNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:00'!
value
	^ value! !

!SEDNumberNode methodsFor: 'accessing' stamp: 'MM 6/28/2019 00:00'!
value: anObject
	"Set the value of value"

	value _ anObject! !

!SEDNumberNode class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:03'!
value: aNumber
	^ self new value: aNumber; yourself ! !

!SEDSelectionNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:17'!
accept: aVisitor
	^ aVisitor visitSelectionNode: self! !

!SEDTextNode methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:16'!
accept: aVisitor
	^ aVisitor visitTextNode: self! !

!SEDTextNode methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2019 23:08'!
contents
	^ text! !

!SEDTextNode methodsFor: 'accessing' stamp: 'MM 6/27/2019 23:57'!
text
	"Answer the value of text"

	^ text! !

!SEDTextNode methodsFor: 'accessing' stamp: 'MM 6/27/2019 23:57'!
text: anObject
	"Set the value of text"

	text _ anObject! !

!SEDTextNode class methodsFor: 'as yet unclassified' stamp: 'MM 6/27/2019 23:58'!
text: aString
	^ self new text: aString; yourself! !

!SEDTextNode class methodsFor: 'as yet unclassified' stamp: 'MM 6/28/2019 00:00'!
value: aString
	^ self text: aString! !
