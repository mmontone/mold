'From Cuis 6.0 [latest update: #5062] on 6 February 2022 at 7:27:28 pm'!
'Description '!
!provides: 'Utils' 1 12!
!requires: 'ExtensibleMenus' 1 0 nil!
SystemOrganization addCategory: 'Utils'!


!classDefinition: #FlatFileList category: 'Utils'!
FileList subclass: #FlatFileList
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Utils'!
!classDefinition: 'FlatFileList class' category: 'Utils'!
FlatFileList class
	instanceVariableNames: ''!

!classDefinition: #FlatFileListWindow category: 'Utils'!
FileListWindow subclass: #FlatFileListWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Utils'!
!classDefinition: 'FlatFileListWindow class' category: 'Utils'!
FlatFileListWindow class
	instanceVariableNames: ''!

!classDefinition: #IconsRepository category: 'Utils'!
Object subclass: #IconsRepository
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Utils'!
!classDefinition: 'IconsRepository class' category: 'Utils'!
IconsRepository class
	instanceVariableNames: ''!

!classDefinition: #DirectoryIconsRepository category: 'Utils'!
IconsRepository subclass: #DirectoryIconsRepository
	instanceVariableNames: 'directory iconsCache'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Utils'!
!classDefinition: 'DirectoryIconsRepository class' category: 'Utils'!
DirectoryIconsRepository class
	instanceVariableNames: ''!

!classDefinition: #MorphBuilderStream category: 'Utils'!
Object subclass: #MorphBuilderStream
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Utils'!
!classDefinition: 'MorphBuilderStream class' category: 'Utils'!
MorphBuilderStream class
	instanceVariableNames: ''!


!FlatFileList methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 12:16:04'!
currentDirectorySelectedIndex
	^ 0! !

!FlatFileList methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 12:40:44'!
directory: aDirectoryEntry

	self assert: aDirectoryEntry isDirectory .
	^ super directory: aDirectoryEntry! !

!FlatFileList methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 12:50:14'!
initialDirectoryList

	| dirList |
	
	directory ifNotNil: [^ {directory parent}, directory directories].
	
	dirList _ DirectoryEntry roots collect: [ :each |
		each name ifNil: ['/']].
	
	dirList isEmpty ifTrue: [
		dirList _ Array with: directory].
	
	^dirList! !

!FlatFileList methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 12:46:00'!
selectedDirectoryIndex: anIndex
	currentDirectorySelected _ self initialDirectoryList at: anIndex.
	self directory: currentDirectorySelected.
	brevityState := #FileList.
	self changed: #fileList.
	self acceptedContentsChanged.
	self changed: #currentDirectorySelected! !

!FlatFileListWindow methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 12:43:28'!
morphicDirectoryTreePane

	^(PluggableListMorph
		model: model
		listGetter: #initialDirectoryList
		indexGetter: #currentDirectorySelectedIndex
		indexSetter: #selectedDirectoryIndex:
		mainView: self
		menuGetter: #volumeMenu
		keystrokeAction: nil)
			autoDeselect: false;
			yourself! !

!FlatFileListWindow class methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:54:21'!
openFileList
	"
	FileListWindow openFileList
	"
	| win |
	
	win _ self open: (FlatFileList new directory: DirectoryEntry currentDirectory) explore label: nil.
	win model when: #updateButtonRow send: #updateButtonRow to: win.
	^ win! !

!IconsRepository methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:40:26'!
getIcon: iconName

	^ self subclassResponsibility! !

!DirectoryIconsRepository methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:44:05'!
getIcon: iconName

	^ iconsCache at: iconName ifAbsent: [ |icon|
		icon _ self readIcon: iconName.
		iconsCache at: icon put: icon.
		icon]! !

!DirectoryIconsRepository methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:48:30'!
iconPath: iconName
	
	^ directory // iconName! !

!DirectoryIconsRepository methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:55:22'!
initialize: aDirectory

	iconsCache _ Dictionary new.
	directory _ aDirectory! !

!DirectoryIconsRepository methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:44:25'!
readIcon: iconName

	^ ImageReadWriter formFromFileEntry: (self iconPath: iconName)! !

!DirectoryIconsRepository class methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:55:08'!
on: aDirectory

	^ self new initialize: aDirectory! !

!MorphBuilderStream methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:03:01'!
row: aBlock
	^ aBlock value: RowLayoutMorph new! !

!InspectorWindow methodsFor: '*Utils' stamp: 'MM 9/18/2021 20:11:29'!
grabSelectedKey

	 self activeHand grab: model selectedKey

	! !

!InspectorWindow methodsFor: '*Utils' stamp: 'MM 9/18/2021 20:15:18'!
grabSelection

	 self activeHand grab: model selection

	! !

!InspectorWindow class methodsFor: '*Utils' stamp: 'MM 9/18/2021 20:16:52'!
smalltalkInspectorMenuOptions

	^ `{
			{
				#itemGroup 	-> 		10.
				#itemOrder 		-> 		10.
				#label 			-> 		'grab'.
				#selector 		-> 		#grabSelection.
				#icon 			-> 		#haloGrabIcon
			} asDictionary.
		}`! !
