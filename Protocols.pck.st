'From Cuis6.3 [latest update: #6304] on 28 March 2024 at 1:26:18 pm'!
'Description '!
!provides: 'Protocols' 1 1!
SystemOrganization addCategory: #Protocols!


!classDefinition: #Protocol category: #Protocols!
Object subclass: #Protocol
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Protocols'!
!classDefinition: 'Protocol class' category: #Protocols!
Protocol class
	instanceVariableNames: ''!

!classDefinition: #IndexedCollectionProtocol category: #Protocols!
Protocol subclass: #IndexedCollectionProtocol
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Protocols'!
!classDefinition: 'IndexedCollectionProtocol class' category: #Protocols!
IndexedCollectionProtocol class
	instanceVariableNames: ''!

!classDefinition: #TableProtocol category: #Protocols!
Protocol subclass: #TableProtocol
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Protocols'!
!classDefinition: 'TableProtocol class' category: #Protocols!
TableProtocol class
	instanceVariableNames: ''!

!classDefinition: #TreeProtocol category: #Protocols!
Protocol subclass: #TreeProtocol
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Protocols'!
!classDefinition: 'TreeProtocol class' category: #Protocols!
TreeProtocol class
	instanceVariableNames: ''!


!IndexedCollectionProtocol commentStamp: 'MM 3/28/2024 13:21:12' prior: 0!
IndexedCollectionProtocol isImplementedBy: Dictionary new .
IndexedCollectionProtocol isImplementedBy: OrderedCollection new.
IndexedCollectionProtocol isImplementedBy: Object new.!

!Protocol class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:18:11'!
isImplementedBy: anObject

	^ self protocolMethods allSatisfy: [:selector | anObject respondsTo: selector]

	! !

!Protocol class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:06:30'!
protocolMethods

	^ self subclassResponsibility ! !

!IndexedCollectionProtocol class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:19:55'!
protocolMethods

	^ {#at:. #at:put:}! !

!TableProtocol class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:24:13'!
protocolMethods

	^ {#rows. #columns}! !

!TreeProtocol class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:26:13'!
protocolMethods

	^ {#children. #parent}! !
