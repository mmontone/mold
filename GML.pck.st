'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 24 November 2018 at 4:13:02.2491 pm'!
'Description Generic Modeling Language'!
!provides: 'GML' 1 11!
!requires: 'PetitParser' 1 1 nil!
!classDefinition: #GMLElement category: #GML!
Object subclass: #GMLElement
	instanceVariableNames: 'type id doc children options value args'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'GML'!
!classDefinition: 'GMLElement class' category: #GML!
GMLElement class
	instanceVariableNames: ''!

!classDefinition: #GMLGrammar category: #GML!
PPCompositeParser subclass: #GMLGrammar
	instanceVariableNames: 'elem idAndType elemOptions elemArgs elemDoc elemBody identStart identCont identifier idBeforeType typeBeforeId elemArgList propValue children elemChildrenBody elemValueBody elemOptionsList elemOption assignElemOption falseElemOption trueElemOption optValue assoc array boolean number string char arrayItems arrayItem lineComment id type booleanTrue booleanFalse'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'GML'!
!classDefinition: 'GMLGrammar class' category: #GML!
GMLGrammar class
	instanceVariableNames: ''!

!classDefinition: #GMLTextStyler category: #GML!
GMLGrammar subclass: #GMLTextStyler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'GML'!
!classDefinition: 'GMLTextStyler class' category: #GML!
GMLTextStyler class
	instanceVariableNames: ''!

!classDefinition: #GMLParser category: #GML!
PPCompositeParser subclass: #GMLParser
	instanceVariableNames: 'elem idAndType elemOptions elemArgs elemDoc elemBody identStart identCont identifier idBeforeType typeBeforeId elemArgList propValue children elemChildrenBody elemValueBody elemOptionsList elemOption assignElemOption falseElemOption trueElemOption optValue assoc array boolean number string char arrayItems arrayItem lineComment id type booleanTrue booleanFalse'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'GML'!
!classDefinition: 'GMLParser class' category: #GML!
GMLParser class
	instanceVariableNames: ''!

!classDefinition: #GMLParserTest category: #GML!
TestCase subclass: #GMLParserTest
	instanceVariableNames: 'parser'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'GML'!
!classDefinition: 'GMLParserTest class' category: #GML!
GMLParserTest class
	instanceVariableNames: ''!


!GMLGrammar commentStamp: '<historical>' prior: 0!
Generic Modeling Language parser.

Grammar:

gml <- (_ elem)*

elem <- id_and_type _
        (elem_options / elem_args)? _
        elemdoc? _
        elem_body? _
        elem_options? %make_elem

idchar <- [a-zA-Z0-9._%$#]
id     <- idchar+ %make_id

id_before_type <- id _ '::' _ id %make_id_before_type
type_before_id <- id _ id %make_type_before_id
id_only     <- id
id_and_type <- id_before_type /
               type_before_id /
               id_only

elem_args <- '(' _ elem_arglist? _ ')'
elem_arglist <- propvalue _ (',' propvalue)* %make_arglist

elem_value_body <- ('=' / ':') _ propvalue
elem_children_body <- '{' _ children _ '}' %make_children
children <- (elem _ ';' _)* 
elem_body <- elem_children_body / elem_value_body

elem_options <- '[' _ elem_options_list? _ ']' %make_elem_options
elem_options_list <-  elemoption (_ ',' _ elemoption)* %make_elem_options_list

elemoption <-  assign_elemoption /
               false_elemoption /
               true_elemoption 

assign_elemoption <- id _ '=' _ optvalue %make_assign_option
true_elemoption <- id
false_elemoption <- '!!' id %make_false_option

optvalue <- assoc / array / boolean / number / string / id
propvalue <- array / boolean / number / string / elem / id

string <- ['] (!!['] Char)* ['] %make_string /
          ["] (!!["] Char)* ["] %make_string

boolean <- 'true' / 'false'

number <- [0-9]+ %make_number

array <- '[' _ array_items? _ ']' %make_array
array_items <- array_item  (_ ',' _ array_item)*
array_item <- array / assoc / boolean / number / string / elem / id

assoc <- id '=' optvalue %make_assoc

elemdoc <- '<<' (!!'>>' Char)* '>>' %make_doc

line_comment <- '//' (!!EndOfLine .)* (EndOfLine / EndOfFile)

Char        <- '\\' [nrt'"\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3] [0-7] [0-7]
            /  '\\' [0-7] [0-7]?
            /  !!'\\' .

_           <- (Space / line_comment)*

Space       <- ' ' / '\t' / EndOfLine
EndOfLine   <- '\r\n' / '\n' / '\r'
EndOfFile   <- !!.!

!GMLParser commentStamp: 'MM 10/30/2018 17:09' prior: 0!
Generic Modeling Language parser.

Grammar:

gml <- (_ elem)*

elem <- id_and_type _
        (elem_options / elem_args)? _
        elemdoc? _
        elem_body? _
        elem_options? %make_elem

idchar <- [a-zA-Z0-9._%$#]
id     <- idchar+ %make_id

id_before_type <- id _ '::' _ id %make_id_before_type
type_before_id <- id _ id %make_type_before_id
id_only     <- id
id_and_type <- id_before_type /
               type_before_id /
               id_only

elem_args <- '(' _ elem_arglist? _ ')'
elem_arglist <- propvalue _ (',' propvalue)* %make_arglist

elem_value_body <- ('=' / ':') _ propvalue
elem_children_body <- '{' _ children _ '}' %make_children
children <- (elem _ ';' _)* 
elem_body <- elem_children_body / elem_value_body

elem_options <- '[' _ elem_options_list? _ ']' %make_elem_options
elem_options_list <-  elemoption (_ ',' _ elemoption)* %make_elem_options_list

elemoption <-  assign_elemoption /
               false_elemoption /
               true_elemoption 

assign_elemoption <- id _ '=' _ optvalue %make_assign_option
true_elemoption <- id
false_elemoption <- '!!' id %make_false_option

optvalue <- assoc / array / boolean / number / string / id
propvalue <- array / boolean / number / string / elem / id

string <- ['] (!!['] Char)* ['] %make_string /
          ["] (!!["] Char)* ["] %make_string

boolean <- 'true' / 'false'

number <- [0-9]+ %make_number

array <- '[' _ array_items? _ ']' %make_array
array_items <- array_item  (_ ',' _ array_item)*
array_item <- array / assoc / boolean / number / string / elem / id

assoc <- id '=' optvalue %make_assoc

elemdoc <- '<<' (!!'>>' Char)* '>>' %make_doc

line_comment <- '//' (!!EndOfLine .)* (EndOfLine / EndOfFile)

Char        <- '\\' [nrt'"\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3] [0-7] [0-7]
            /  '\\' [0-7] [0-7]?
            /  !!'\\' .

_           <- (Space / line_comment)*

Space       <- ' ' / '\t' / EndOfLine
EndOfLine   <- '\r\n' / '\n' / '\r'
EndOfFile   <- !!.!

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
args
	"Answer the value of args"

	^ args! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
args: anObject
	"Set the value of args"

	args _ anObject! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/12/2018 07:40'!
childWithId: anObject

	^ self childWithId: anObject ifNone: [self error: self printString, ' has no child ', anObject]! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/12/2018 07:37'!
childWithId: anObject ifNone: aBlock

	^ self children 
				detect: [:elem | elem id = anObject]
				ifNone: aBlock! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/16/2018 20:20'!
childWithId: anObject ifPresent: aBlock

	^ aBlock value: (self children 
				detect: [:elem | elem id = anObject]
				ifNone: [^ nil])! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
children
	"Answer the value of children"

	^ children! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
children: anObject
	"Set the value of children"

	children _ anObject! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
doc
	"Answer the value of doc"

	^ doc! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
doc: anObject
	"Set the value of doc"

	doc _ anObject! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
id
	"Answer the value of id"

	^ id! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
id: anObject
	"Set the value of id"

	id _ anObject! !

!GMLElement methodsFor: 'initialization' stamp: 'MM 11/9/2018 19:58'!
initialize

	options _ Dictionary new.
	args _ OrderedCollection new.! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/9/2018 19:41'!
optionAt: aSymbol
	
	^ options at: aSymbol! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/9/2018 19:41'!
optionAt: aSymbol ifAbsent: aBlock
	
	^ options at: aSymbol ifAbsent: aBlock! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/14/2018 10:41'!
optionAt: aSymbol ifPresent: aBlock
	
	^ options at: aSymbol ifPresent: aBlock! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 11/9/2018 19:42'!
optionAt: aSymbol put: anObject
	
	^ options at: aSymbol put: anObject! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
options
	"Answer the value of options"

	^ options! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
options: anObject
	"Set the value of options"

	options _ anObject! !

!GMLElement methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:46'!
printOn: aStream

	"Append to the argument, aStream, a sequence of characters that  
	identifies the receiver."

	| title |
	title _ self class name.
	aStream
		nextPutAll: (title first isVowel ifTrue: ['an '] ifFalse: ['a ']);
		nextPutAll: title.
	aStream nextPut: $[.
	self printTypeAndIdOn: aStream.
	aStream nextPut: $].
	aStream nextPutAll: '<', self identityHash  printString, '>'! !

!GMLElement methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:43'!
printTypeAndIdOn: aStream

	id ifNotNil: [
		aStream nextPutAll: id].
	
	type ifNotNil: [
		aStream nextPutAll: '::';
					nextPutAll: type]! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
type
	"Answer the value of type"

	^ type! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
type: anObject
	"Set the value of type"

	type _ anObject! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:36'!
value

	^ value! !

!GMLElement methodsFor: 'accessing' stamp: 'MM 10/30/2018 19:35'!
value: anObject
	"Set the value of value"

	value _ anObject! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
array

	"array <- '[' _ array_items? _ ']'"
	^ (self trim: '[' asParser) , arrayItems optional bind, (self trim: ']' asParser)! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
arrayItem

	"array_item <- array / assoc / boolean / number / string / elem / id"
	
	^ array / assoc / boolean / number / string / elem / identifier! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:28'!
arrayItems
	
	"array_items <- array_item _ ',' _ array_items / array_item"
	
	^ arrayItem, 
		((self trim: ',' asParser), arrayItem)  star! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:29'!
assignElemOption

	"assign_elemoption <- id _ '=' _ optvalue %make_assign_option"
	
	^ identifier bind, (self trim: '=' asParser), optValue bind ! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:29'!
assoc

	"assoc <- id '=' optvalue"
	
	^ identifier bind, '=' asParser, optValue bind ! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
boolean
	
	"boolean <- 'true' / 'false'"
	
	^ booleanTrue / booleanFalse! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
booleanFalse

	 ^ 'false' asParser ==> [:b | false]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
booleanTrue

	^ 'true' asParser ==> [:b | true]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
char

	"Char        <- '\\' [nrt'""\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3][0-7][0-7]
            /  '\\' [0-7][0-7]?
            /  !!'\\' ."

	^  #word asParser / $_ asParser / $' asParser / $" asParser! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:29'!
children

	"children <- (elem _ ';' _)* "
	
	^ (elem , 
	    (self trim: ';' asParser)) star ! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:30'!
elem

	"elem <- id_and_type _
        (elem_options / elem_args)? _
        elemdoc? _
        elem_body? _
        elem_options? "

	^ ((self trim: idAndType) bind,
	   (self trim: ( elemOptions / elemArgs )) optional bind,
	   (self trim: elemDoc) optional bind,
	   (self trim: elemBody) optional bind,
	   elemOptions optional bind) ! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:30'!
elemArgList

	"elem_arglist <- propvalue _ (',' _ propvalue)*"
	
	^ propValue, 
		(',' asParser trim, propValue) star! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:30'!
elemArgs

	"elem_args <- '(' _ elem_arglist? _ ')'"
	
	^ '(' asParser trim, elemArgList optional bind, ')' asParser trim! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
elemBody

	"elem_body <- elem_children_body / elem_value_body"
	
	^ elemChildrenBody / elemValueBody! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:31'!
elemChildrenBody

	"elem_children_body <- '{' _ children _ '}'"
	
	^ '{' asParser trim, children bind, '}' asParser trim
	! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:31'!
elemDoc

	"elemdoc <- '<<' (!!'>>' Char)* '>>' "
	
	^ ('<<' asParser, ('>>' asParser negate) star flatten bind, '>>' asParser)! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
elemOption

	"elemoption <-  assign_elemoption /
               false_elemoption /
               true_elemoption "

	^ assignElemOption / falseElemOption / trueElemOption! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:31'!
elemOptions

	"elem_options <- '[' _ elem_options_list? _ ']' "
	
	^ ('[' asParser trim, elemOptionsList optional bind , ']' asParser trim)! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:32'!
elemOptionsList

	"elem_options_list <-  elemoption (_ ',' _ elemoption)*"
	
	^ elemOption, 
		(',' asParser trim, elemOption) star! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:32'!
elemValueBody

	"elem_value_body <- ('=' / ':') _ propvalue"
	
	^ ('=' asParser / ':' asParser) trim,  propValue bind ! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
falseElemOption

	"false_elemoption <- '!!' id %make_false_option"
	
	^ ('!!' asParser, identifier bind) ~=> [:id | id -> false]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
id

	^ identifier! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
idAndType

	"id_and_type <- id_before_type /
               type_before_id /
               id_only"

	^ idBeforeType / typeBeforeId / (identifier ==> [:id | {id. nil}])! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
idBeforeType

	"id_before_type <- id _ '::' _ id %make_id_before_type"
	
	^ id bind, '::' asParser trim, type bind ~=> [:id :type | {id. type}]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
identCont
	"IdentCont   <- IdentStart / [0-9]"
	^ identStart / #digit asParser! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
identStart
	"IdentStart  <- [a-zA-Z_]"
	
	^ #letter asParser / $_ asParser! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
identifier
	"Identifier  <- IdentStart IdentCont* _"
	^(identStart, identCont star) flatten! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
lineComment

	"line_comment <- '//' (!!EndOfLine .)* (EndOfLine / EndOfFile)"
	^ '//' asParser, Character newLineCharacter asParser negate star flatten! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
number

	"number <- [0-9]+"
	
	^ #digit asParser plus flatten ==> [:s | s asNumber]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
optValue
		
	"optvalue <- assoc / array / boolean / number / string / id"
	
	^ assoc / array / boolean / number / string / identifier! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
propValue

	"propvalue <- array / boolean / number / string / elem / id"
	
	^ array / boolean / number / string / elem / identifier! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
start

	"gml <- (_ elem)*"
	
	"^ elem trim star end"
	
	^ elem end
! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
string

	"string <- ['] (!!['] Char)* ['] /
          [""] (!![""] Char)* [""]"

	^  ($' asParser, $' asParser negate star flatten bind,  $' asParser) ~=> [:x | x] /
	   ($" asParser, $" asParser negate star flatten bind, $" asParser ~=> [:x | x])! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
trim: aParser

	"Answer a new parser that consumes spaces before and after the receiving parser."
	
	^ aParser trim: (#space asParser / self lineComment)! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
trueElemOption

	"true_elemoption <- id"
	
	^ identifier ==> [:id | id -> true]! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
type

	^ identifier! !

!GMLGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:23'!
typeBeforeId

	"type_before_id <- id _ id"
	
	^ (type bind, (#blank asParser plus), id bind) ~=> [:type :id | {id. type}]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:18'!
booleanFalse

	^ super booleanFalse token ==> [:token | self style: token as: #false. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:18'!
booleanTrue

	^ super booleanTrue token ==> [:token | self style: token as: #true. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:34'!
elemDoc

	^ super elemDoc token ==> [:token | self style: token as: #comment. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:18'!
falseElemOption

	^ super falseElemOption token ==> [:token | self style: token as: #false. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:19'!
id

	^ super id token ==> [:token | self style: token as: #identifier. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:53'!
initialize

	self propertyAt: #ranges put: OrderedCollection new.! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:19'!
number

	^ super number token ==> [:token | self style: token as: #number. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:53'!
ranges

	^ self propertyAt: #ranges! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:19'!
string

	^ super string token ==> [:token | self style: token as: #string. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:09'!
style: aToken as: aSymbol
	
	(self propertyAt: #ranges) add: (SHRange start: aToken start end: aToken stop type: aSymbol)! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:57'!
styleTable

	^ self class styleTable! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:19'!
trueElemOption

	^ super trueElemOption token ==> [:token | self style: token as: #true. token]! !

!GMLTextStyler methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:19'!
type

	^ super type token ==> [:token | self style: token as: #type. token]! !

!GMLTextStyler class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:57'!
gmlStyleTable

	^ {{#identifier. #black. #bold}.
	     {#type. #blue. #bold}.
		{#true. #green}.
		{#false. #red}}! !

!GMLTextStyler class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:57'!
styleTable

	^ Theme current generateShoutConfig, self gmlStyleTable! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:34'!
array

	"array <- '[' _ array_items? _ ']'"
	^ (self trim: '[' asParser) , arrayItems optional bind, (self trim: ']' asParser)
		~=> [:arr | arr]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:41'!
arrayItem

	"array_item <- array / assoc / boolean / number / string / elem / id"
	
	^ array / assoc / boolean / number / string / elem / identifier! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:41'!
arrayItems
	
	"array_items <- array_item _ ',' _ array_items / array_item"
	
	|array|
	
	array _ OrderedCollection new.
	^ (arrayItem ==> [:x | array add: x], 
		((self trim: ',' asParser), (arrayItem ==> [:x | array add: x])) star)
	==> [:x | array asArray]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 21:06'!
assignElemOption

	"assign_elemoption <- id _ '=' _ optvalue %make_assign_option"
	
	^ identifier bind, (self trim: '=' asParser), optValue bind ~=> [:key :val | key -> val]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:33'!
assoc

	"assoc <- id '=' optvalue"
	
	^ identifier bind, '=' asParser, optValue bind ~=> [:key :val | key -> val]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:30'!
boolean
	
	"boolean <- 'true' / 'false'"
	
	^ booleanTrue / booleanFalse! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:31'!
booleanFalse

	 ^ 'false' asParser ==> [:b | false]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:31'!
booleanTrue

	^ 'true' asParser ==> [:b | true]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 11:07'!
char

	"Char        <- '\\' [nrt'""\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3][0-7][0-7]
            /  '\\' [0-7][0-7]?
            /  !!'\\' ."

	^  #word asParser / $_ asParser / $' asParser / $" asParser! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:39'!
children

	"children <- (elem _ ';' _)* "
	
	|children|
	children _ OrderedCollection new.
	
	^ (elem ==> [:elem | children add: elem] , 
	    (self trim: ';' asParser)) star ==> [:x | children]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 19:59'!
elem

	"elem <- id_and_type _
        (elem_options / elem_args)? _
        elemdoc? _
        elem_body? _
        elem_options? "

	^ ((self trim: idAndType) bind,
	   (self trim: ( elemOptions / elemArgs )) optional bind,
	   (self trim: elemDoc) optional bind,
	   (self trim: elemBody) optional bind,
	   elemOptions optional bind) 
	~=> [:idAndType :optsOrArgs :doc :body :moreOpts | |elem opts|
		   elem _ GMLElement new
			id: idAndType first;
			type: idAndType second;
			doc: doc;
			yourself.
			
			opts _ Dictionary new.
			
			(optsOrArgs isKindOf: Dictionary) ifTrue: [
				opts addAll: optsOrArgs].
			moreOpts ifNotNil: [opts addAll: moreOpts].
			elem options: opts.
			
			(optsOrArgs isKindOf: SequenceableCollection ) ifTrue: [elem args: optsOrArgs].
			
			body ifNotNil: [
				body first caseOf: {
					[#value] -> [elem value: body second].
					[#children] -> [elem children: body second]
				}].		
		elem]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:03'!
elemArgList

	"elem_arglist <- propvalue _ (',' _ propvalue)*"
	
	|args|
	
	args := OrderedCollection new.
	
	^ (propValue ==> [:x | args add: x], 
		(',' asParser trim, (propValue ==> [:x | args add: x])) star)
		==> [:x | args]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 21:11'!
elemArgs

	"elem_args <- '(' _ elem_arglist? _ ')'"
	
	^ '(' asParser trim, elemArgList optional bind, ')' asParser trim ~=> [:args | args]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 12:25'!
elemBody

	"elem_body <- elem_children_body / elem_value_body"
	
	^ elemChildrenBody / elemValueBody! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:29'!
elemChildrenBody

	"elem_children_body <- '{' _ children _ '}'"
	
	^ '{' asParser trim, children bind, '}' asParser trim 
	     ~=> [:children | {#children. children}]
	! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:20'!
elemDoc

	"elemdoc <- '<<' (!!'>>' Char)* '>>' "
	
	^ ('<<' asParser, ('>>' asParser negate) star flatten bind, '>>' asParser) ~=> [:doc | doc]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 13:52'!
elemOption

	"elemoption <-  assign_elemoption /
               false_elemoption /
               true_elemoption "

	^ assignElemOption / falseElemOption / trueElemOption! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 21:03'!
elemOptions

	"elem_options <- '[' _ elem_options_list? _ ']' "
	
	^ ('[' asParser trim, elemOptionsList optional bind , ']' asParser trim)
	  ~=> [:options | options]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 19:46'!
elemOptionsList

	"elem_options_list <-  elemoption (_ ',' _ elemoption)*"
	|options|
	
	options _ Dictionary new.
	
	^ (elemOption ==> [:opt | options at:opt key put: opt value], 
		(',' asParser trim, (elemOption ==> [:opt | options at: opt key put: opt value])) star)
		==> [:x | options]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:30'!
elemValueBody

	"elem_value_body <- ('=' / ':') _ propvalue"
	
	^ ('=' asParser / ':' asParser) trim,  propValue bind ~=> [:value | {#value. value}]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 21:08'!
falseElemOption

	"false_elemoption <- '!!' id %make_false_option"
	
	^ ('!!' asParser, identifier bind) ~=> [:id | id -> false]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:20'!
id

	^ identifier! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:14'!
idAndType

	"id_and_type <- id_before_type /
               type_before_id /
               id_only"

	^ idBeforeType / typeBeforeId / (identifier ==> [:id | {id. nil}])! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:20'!
idBeforeType

	"id_before_type <- id _ '::' _ id %make_id_before_type"
	
	^ id bind, '::' asParser trim, type bind ~=> [:id :type | {id. type}]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 11:24'!
identCont
	"IdentCont   <- IdentStart / [0-9]"
	^ identStart / #digit asParser! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 11:08'!
identStart
	"IdentStart  <- [a-zA-Z_]"
	
	^ #letter asParser / $_ asParser! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:26'!
identifier
	"Identifier  <- IdentStart IdentCont* _"
	^(identStart, identCont star) flatten! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 17:51'!
lineComment

	"line_comment <- '//' (!!EndOfLine .)* (EndOfLine / EndOfFile)"
	^ '//' asParser, Character newLineCharacter asParser negate star flatten! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 20:33'!
number

	"number <- [0-9]+"
	
	^ #digit asParser plus flatten ==> [:s | s asNumber]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 14:02'!
optValue
		
	"optvalue <- assoc / array / boolean / number / string / id"
	
	^ assoc / array / boolean / number / string / identifier! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 14:04'!
propValue

	"propvalue <- array / boolean / number / string / elem / id"
	
	^ array / boolean / number / string / elem / identifier! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 19:38'!
start

	"gml <- (_ elem)*"
	
	"^ elem trim star end"
	
	^ elem end
! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:30'!
string

	"string <- ['] (!!['] Char)* ['] /
          [""] (!![""] Char)* [""]"

	^  ($' asParser, $' asParser negate star flatten bind,  $' asParser) ~=> [:x | x] /
	   ($" asParser, $" asParser negate star flatten bind, $" asParser ~=> [:x | x])! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 17:54'!
trim: aParser

	"Answer a new parser that consumes spaces before and after the receiving parser."
	
	^ aParser trim: (#space asParser / self lineComment)! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 10/30/2018 21:08'!
trueElemOption

	"true_elemoption <- id"
	
	^ identifier ==> [:id | id -> true]! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:21'!
type

	^ identifier! !

!GMLParser methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:21'!
typeBeforeId

	"type_before_id <- id _ id"
	
	^ (type bind, (#blank asParser plus), id bind) ~=> [:type :id | {id. type}]! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 22:21'!
setUp
	parser _ GMLParser.! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:03'!
testArgs

	|elem|
	
	elem _ parser parse: 'foo(x, y)'.
	
	self assert: elem args size = 2.
	self assert: elem args first id = 'x'.
	self assert: elem args second id = 'y'! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:25'!
testAssoc

	|elem|
	
	elem _ parser parse: 'foo(x=y)'.
	self assert: elem args first id = 'x'.
	self assert: elem args first value id = 'y'! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:05'!
testChildren

	|elem|
	
	elem _ parser parse: 'foo {x; Bar baz;}'.
	
	self assert: elem children size = 2.
	self assert: elem children first id = 'x'.
	self assert: elem children second id = 'baz'.
	self assert: elem children second type = 'Bar'
	! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:17'!
testDoc

	|elem|
	
	elem _ parser parse: 'foo <<doc>>'.
	self assert: elem id = 'foo'.
	self assert: elem doc = 'doc'.
	
	elem _ parser parse: 'Form MyForm
	 <<foo>> 
	{
		field1 :: String <<field1 doc>>;	
	}'.
	
	self assert: elem doc = 'foo'.! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:17'!
testIdAndType

	|elem|
	
	elem _ parser parse: 'foo'.
	self assert: elem id = 'foo'.
	self assert: elem type isNil.
	
	elem _ parser parse: 'Foo bar'.
	self assert: elem type = 'Foo'.
	self assert: elem id = 'bar'.
	
	elem _ parser parse: 'field :: String'.
	self assert: elem type = 'String'.
	self assert: elem id = 'field'.! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 20:12'!
testIdentifier

	parser new identifier parse: 'foo'! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:34'!
testOptions

	|elem|
	
	elem _ parser parse: 'foo[x=2]'.
	self assert: (elem optionAt: 'x') = 2.
	
	elem _ parser parse: 'foo {} [x=2]'.
	self assert: (elem optionAt: 'x') = 2.
	
	elem _ parser parse: 'foo [trueOption]'.
	self assert: (elem optionAt: 'trueOption').
	
	elem _ parser parse: 'foo [!!falseOption]'.
	self assert: (elem optionAt: 'falseOption') not! !

!GMLParserTest methodsFor: 'as yet unclassified' stamp: 'MM 11/9/2018 21:07'!
testValueBody

	|elem|
	
	elem _ parser parse: 'foo = 22'.
	self assert: elem id = 'foo'.
	self assert: elem value = 22.
	
	elem _ parser parse: 'foo = ''bar'''.
	self assert: elem id = 'foo'.
	self assert: elem value = 'bar'.
	
	elem _ parser parse: 'foo = "bar"'.
	self assert: elem id = 'foo'.
	self assert: elem value = 'bar'.
	
	elem _ parser parse: 'foo = true'.
	self assert: elem id = 'foo'.
	self assert: elem value.
	
	elem _ parser parse: 'foo = false'.
	self assert: elem id = 'foo'.
	self assert: elem value not.
	
	elem _ parser parse: 'foo = [1,2,3]'.
	self assert: elem id = 'foo'.
	self assert: elem value = #(1 2 3)! !
