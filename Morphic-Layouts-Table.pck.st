'From Cuis6.3 [latest update: #6304] on 28 March 2024 at 4:41:44 pm'!
'Description Morph with a table layout.

TODO: implement a better algorithm: https://drafts.csswg.org/css3-tables-algorithms/Overview.src.htm'!
!provides: 'Morphic-Layouts-Table' 1 7!
SystemOrganization addCategory: #'Morphic-Layouts-Table'!


!classDefinition: #TableCellMorph category: #'Morphic-Layouts-Table'!
BoxedMorph subclass: #TableCellMorph
	instanceVariableNames: 'hAlignment vAlignment hResizing vResizing padding'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Morphic-Layouts-Table'!
!classDefinition: 'TableCellMorph class' category: #'Morphic-Layouts-Table'!
TableCellMorph class
	instanceVariableNames: ''!

!classDefinition: #TableLayoutMorph category: #'Morphic-Layouts-Table'!
BoxedMorph subclass: #TableLayoutMorph
	instanceVariableNames: 'rows cols'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Morphic-Layouts-Table'!
!classDefinition: 'TableLayoutMorph class' category: #'Morphic-Layouts-Table'!
TableLayoutMorph class
	instanceVariableNames: ''!


!TableCellMorph commentStamp: '<historical>' prior: 0!
A table cell. 

It's instances are added as submorphs to a TableLayoutMorph.!

!TableLayoutMorph commentStamp: '<historical>' prior: 0!
A morph that layouts in table form.

Its submorphs are expected to be instances of TableCellMorph.!

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:25'!
alignBottom

	vAlignment _ #bottom.
	self fixedHeight.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:25'!
alignLeft

	hAlignment _ #left.
	self fixedWidth.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:25'!
alignRight

	hAlignment _ #right.
	self fixedWidth.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:26'!
alignTop

	vAlignment _ #top.
	self fixedHeight.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:26'!
centerHorizontally

	hAlignment _ #center.
	self fixedWidth.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:26'!
centerVertically

	vAlignment _ #center.
	self fixedHeight.! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:11'!
contents

	^ self submorphs first! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 01:26'!
defaultBorderWidth

	^ 0! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 12:18'!
defaultColor
	
	^Color white! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:23'!
expandHorizontally

	hResizing _ #expand! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:24'!
expandVertically

	vResizing _ #expand! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:25'!
fixedHeight

	vResizing _ #fixed! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:24'!
fixedWidth

	hResizing _ #fixed! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:21'!
horizontalAlign: aSymbol

	hAlignment _ aSymbol! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:16'!
initialize
	super initialize.
	
	hAlignment _ #center.
	vAlignment _ #center.
	hResizing _ #expand.
	vResizing _ #expand.
	padding _ 0.

	! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:17'!
morph

	^ self submorphs first! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:16'!
padding: aNumber

	padding _ aNumber! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:21'!
verticalAlign: aSymbol

	vAlignment _ aSymbol! !

!TableCellMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 22:11'!
with: aMorph

	self removeAllMorphs.
	self addMorph: aMorph! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:45'!
alignSubmorphBottom

	| y |
	
	y _ self morphHeight -  padding - self morph morphHeight.
	self morph morphPosition: (self morph morphPosition x @ y) ! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:51'!
alignSubmorphLeft

	self morph morphPosition: (padding @ self morph morphPosition y) ! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:51'!
alignSubmorphRight

	| x |
	
	x _ self morphWidth -  padding - self morph morphWidth.
	self morph morphPosition: (x @ self morph morphPosition y ) ! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:42'!
alignSubmorphTop

	self morph morphPosition: (self morph morphPosition x @ padding) ! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:52'!
centerSubmorphHorizontally

	| x |
	
	x _ padding + ((self morphWidth - (padding * 2) - self morph morphWidth) / 2).
	
	self morph morphPosition: (x @ self morph morphPosition y)! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:48'!
centerSubmorphVertically

	| y |
	
	y _ padding + ((self morphHeight - (padding * 2) - self morph morphHeight) / 2).
	
	self morph morphPosition: (self morph morphPosition x @ y)! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:58'!
expandSubmorphHorizontally

	|x width |
	
	x _ padding.
		
	width _ self morphWidth - (padding * 2).
		
	self morph morphPosition: (x @ (self morph morphPosition y)).
	self morph morphExtent: (width @ (self morph morphHeight)).
	! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 22:58'!
expandSubmorphVertically

	|y height |
	
	y _ padding.
		
	height _ self morphHeight - (padding * 2).
		
	self morph morphPosition: ((self morph morphPosition x) @ y).
	self morph morphExtent: (self morph morphWidth @ height).
	! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 23:03'!
layoutSubmorphs

	(vResizing == #expand) ifTrue: [
		self expandSubmorphVertically]
	ifFalse: [
		vAlignment caseOf: {[#top] -> [self alignSubmorphTop].
	                                 [#center] -> [self centerSubmorphVertically].
	                                 [#bottom] -> [self alignSubmorphBottom]}.
	].
	
	(hResizing == #expand) ifTrue: [
		self expandSubmorphHorizontally ]
	ifFalse: [
		hAlignment caseOf: {[#left] -> [self alignSubmorphLeft].
	                                 [#center] -> [self centerSubmorphHorizontally].
	                                 [#right] -> [self alignSubmorphRight]}.
	]! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 23:36'!
minimumExtent

	^self minimumWidth @ (self minimumHeight)! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 23:40'!
minimumHeight

	(vResizing == #expand) ifTrue: [
		^ self morph minimumExtent y]
	ifFalse: [
		^ (padding * 2) + self morph morphHeight]! !

!TableCellMorph methodsFor: 'layout' stamp: 'MM 7/9/2018 23:37'!
minimumWidth

	(hResizing == #expand) ifTrue: [
		^ self morph minimumExtent x]
	ifFalse: [
		^ (padding * 2) + self morph morphWidth]! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:37:23'!
testAlign1

	"self testAlign1"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									alignTop;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 23:10'!
testAlign10

	"self testAlign10"
	
	|cell|
	
	cell _ TableCellMorph new with: (BorderedRectMorph new color: Color red;
																			yourself);
									alignLeft ;
									alignBottom;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 13:37:42'!
testAlign2

	"self testAlign2"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									alignTop;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:52:53'!
testAlign3

	"self testAlign3"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									alignBottom;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:53:08'!
testAlign4

	"self testAlign4"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									alignBottom;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:53:22'!
testAlign5

	"self testAlign5"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									centerHorizontally ;
									alignBottom;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:53:33'!
testAlign6

	"self testAlign6"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									alignRight ;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:53:44'!
testAlign7

	"self testAlign7"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									alignRight ;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:53:56'!
testAlign8

	"self testAlign8"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									alignRight ;
									alignBottom;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:54:07'!
testAlign9

	"self testAlign9"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									alignLeft ;
									alignBottom;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:54:19'!
testExpand1

	"self testExpand1"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									expandVertically ;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:54:30'!
testExpand2

	"self testExpand2"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									centerVertically;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:54:42'!
testExpand3

	"self testExpand3"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandVertically ;
									centerHorizontally;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:54:54'!
testPadding1

	"self testPadding1"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandVertically ;
									centerHorizontally;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableCellMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 14:55:00'!
testPadding2

	"self testPadding2"
	
	|cell|
	
	cell := TableCellMorph new with: (BoxedMorph new color: Color red;
																			yourself);
									expandHorizontally ;
									centerVertically;
									padding: 5;
									morphExtent: 100 @100;
									yourself.
									
	cell openInWorld
! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 01:33'!
addCell: aTableCellMorph

	self addMorphBack: aTableCellMorph! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 01:26'!
defaultBorderWidth
	^ 0! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 01:53'!
getCol: anIndex

	|rowSize col|
	
	rowSize _ self submorphs size / self rows.
	
	col _ Array new: self rows.
	
	1 to: self rows do: [:c |
		col at: c put: (self submorphs at: ((c - 1) * rowSize) + anIndex)].
	
	^col  	
		! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 16:33:55'!
getRow: anIndex

	|rowSize|
	
	rowSize := (self submorphs size / self rows) floor.
	^self submorphs copyFrom: ((anIndex - 1) * rowSize) + 1 count: rowSize    ! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 16:40:56'!
layoutSubmorphs

	|rowSize rowHeights colWidths x y cellIndex|
	
	rowSize := (self submorphs size / self rows) floor.
	rowHeights := Array new: self rows.
	colWidths := Array new: rowSize.
	
	1 to: self rows do: [:r | |row |
		row := self getRow: r.
		rowHeights at: r put: (row inject: 0 into: [:max :cell | max max: cell minimumExtent y])].
	
	1 to: rowSize do: [:c | |col|
		col := self getCol: c.
		colWidths at: c put: (col inject: 0 into: [:max :cell | max max: cell minimumExtent x])].
	
	x := self borderWidth.
	y := self borderWidth.
	cellIndex := 1.
	
	rowHeights do: [:height |
		colWidths do: [:width | |cell|
			cell := self submorphs at: cellIndex.
			cell morphPosition: (x @ y).
			cell morphExtent: (width @ height).
			x := x + width.
			cellIndex := cellIndex + 1].
		x := self borderWidth.
		y := y + height].
	
	"fit contents"
	self morphExtent: 0@0! !

!TableLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 16:35:34'!
minimumExtent

	|rowSize rowHeights colWidths width height|
	
	self submorphs size isZero ifTrue: [^super minimumExtent].
	
	rowSize := (self submorphs size / self rows) floor.
	rowHeights := Array new: self rows.
	colWidths := Array new: rowSize.
	
	1 to: self rows do: [:r | |row |
		row := self getRow: r.
		rowHeights at: r put: (row inject: 0 into: [:max :cell | max max: cell minimumExtent y])].
	
	1 to: rowSize do: [:c | |col|
		col := self getCol: c.
		colWidths at: c put: (col inject: 0 into: [:max :cell | max max: cell minimumExtent x])].
	
	width := colWidths inject: (self borderWidth * 2) into: [:v :w | v + w].
	height := rowHeights inject: (self borderWidth * 2) into: [:v :h | v+ h].
	
	^width @ height! !

!TableLayoutMorph methodsFor: 'accessing' stamp: 'MM 7/10/2018 01:50'!
cols
	"Answer the value of cols"

	^ cols ifNil: [self submorphs size / rows]! !

!TableLayoutMorph methodsFor: 'accessing' stamp: 'MM 7/9/2018 23:16'!
cols: anObject
	"Set the value of cols"

	cols _ anObject! !

!TableLayoutMorph methodsFor: 'accessing' stamp: 'MM 7/10/2018 01:49'!
rows
	"Answer the value of rows"

	^ rows ifNil: [self submorphs size / cols]! !

!TableLayoutMorph methodsFor: 'accessing' stamp: 'MM 7/9/2018 23:16'!
rows: anObject
	"Set the value of rows"

	rows _ anObject! !

!TableLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 23:16'!
cols: aNumber

	^ self new cols: aNumber; yourself
! !

!TableLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/9/2018 23:16'!
rows: aNumber
	
	^ self new rows: aNumber; yourself! !

!TableLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 12:16'!
test1
	
	"self test1"
	
	|table|
	
	table _ TableLayoutMorph new
				rows: 3;
				borderWidth: 0;
				morphExtent: 200 @ 200;
				yourself.
	
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Name:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color blue; yourself);
						alignTop;
						alignLeft;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Description:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color yellow; morphExtent: 200@30; yourself);
						alignTop;
						alignLeft;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Test:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color green; morphExtent: 20@50; yourself);
						alignRight;
						centerVertically;
						padding: 5;
						yourself).
						
	table openInWorld! !

!TableLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/10/2018 12:19'!
test2
	
	"self test2"
	
	|table|
	
	table _ TableLayoutMorph new
				rows: 3;
				borderWidth: 3;
				borderColor: Color blue;
				morphExtent: 200 @ 200;
				color: Color white;
				yourself.
	
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Name:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color blue; yourself);
						alignTop;
						alignLeft;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Description:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color yellow; morphExtent: 200@30; yourself);
						alignTop;
						alignLeft;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (LabelMorph contents: 'Test:');
						alignTop;
						alignLeft;
						borderWidth:0;
						padding: 5;
						yourself).
						
	table addCell: (TableCellMorph new
						with: (BoxedMorph new color: Color green; morphExtent: 20@50; yourself);
						alignRight;
						centerVertically;
						padding: 5;
						yourself).
						
	table openInWorld! !
