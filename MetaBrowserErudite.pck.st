'From Cuis 5.0 [latest update: #4520] on 7 January 2021 at 10:11:27 am'!
'Description Erudite extensions for MetaBrowser.'!
!provides: 'MetaBrowserErudite' 1 12!
!requires: 'Erudite' 1 144 nil!
!requires: 'MetaBrowser' 1 86 nil!
SystemOrganization addCategory: #MetaBrowserErudite!


!classDefinition: #EruditeBookBrowsableObject category: #MetaBrowserErudite!
BrowsableObject subclass: #EruditeBookBrowsableObject
	instanceVariableNames: 'smalltalkClass book'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaBrowserErudite'!
!classDefinition: 'EruditeBookBrowsableObject class' category: #MetaBrowserErudite!
EruditeBookBrowsableObject class
	instanceVariableNames: ''!

!classDefinition: #EruditeBookSectionBrowsableObject category: #MetaBrowserErudite!
BrowsableObject subclass: #EruditeBookSectionBrowsableObject
	instanceVariableNames: 'book section'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaBrowserErudite'!
!classDefinition: 'EruditeBookSectionBrowsableObject class' category: #MetaBrowserErudite!
EruditeBookSectionBrowsableObject class
	instanceVariableNames: ''!

!classDefinition: #EruditeBookSectionsAspect category: #MetaBrowserErudite!
BrowseObjectAspect subclass: #EruditeBookSectionsAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaBrowserErudite'!
!classDefinition: 'EruditeBookSectionsAspect class' category: #MetaBrowserErudite!
EruditeBookSectionsAspect class
	instanceVariableNames: ''!

!classDefinition: #EruditeBookSectionsAspectPane category: #MetaBrowserErudite!
BrowseObjectAspectPane subclass: #EruditeBookSectionsAspectPane
	instanceVariableNames: 'explorer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaBrowserErudite'!
!classDefinition: 'EruditeBookSectionsAspectPane class' category: #MetaBrowserErudite!
EruditeBookSectionsAspectPane class
	instanceVariableNames: ''!


!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2020 12:53:34'!
printOn: aStream

	smalltalkClass printOn: aStream! !

!EruditeBookSectionsAspect methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:53:35'!
name
	^ 'Sections'! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:03:32'!
acceptedContents
	^ smalltalkClass definition! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/29/2020 10:08:13'!
actions
	^ {{#action->#readBook.#icon->#inspectIcon.#description->'Open in book reader'}.
	    {#action->#editBook.#icon->#openIcon.#description->'Open in book editor'}}! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:52:27'!
aspects

	^ {BrowseObjectOverviewAspect new.
		EruditeBookSectionsAspect new.
		BrowseObjectMetaAspect new.
		BrowseObjectDocumentationAspect new}! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:30:09'!
browseObject
	^ self! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:04:15'!
contentsTypes: aBrowser

	^ {{#contents->#view. #action->#toggleContents:.
		#args->{#view}.#target->aBrowser.#title->'view'.#help->'view the rendered book'}.
		{#contents->#source. #action->#toggleContents:. 
		#args->{#source}.#target->aBrowser. #title->'source'. #help->'the textual source code as written'}.
		{#contents->#documentation. #target->aBrowser.#action->#toggleContents:.
		#args->{#documentation}.#title->'documentation'.#help->'the first comment in the method'}.
	}! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/27/2020 20:59:25'!
description
	^ smalltalkClass comment asString! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/29/2020 10:13:54'!
editBook
	EruditeBookEditorMorph open: self getBook! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/27/2020 21:03:08'!
editorMorph

	^ EruditeDocViewerMorph 
		textProvider: self 
		textGetter: #acceptedContents 
		textSetter: #contents:notifying:! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 11:27:39'!
getBook
	^ book ifNil: [smalltalkClass new]! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/27/2020 21:06:29'!
getStyledText: aDocument

	| erudite text |
		
	erudite _ EruditeMarkupParser parse: aDocument contents.
	
	"We transplant the images from document to styledDocument and back. TODO: this is not good!!!! FIX"
	erudite images: aDocument images.
	text _ MorphicEruditeDocRenderer render: erudite notifying: self.
	aDocument images: erudite images.
	^ text! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2020 13:05:27'!
icon
	^ Theme current addressBookIcon ! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2020 12:50:10'!
initialize: aClass
	smalltalkClass _ aClass! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/29/2020 10:14:04'!
readBook
	EruditeBookReaderMorph open: self getBook! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 11:28:01'!
renderedContents
	^ self getStyledText: self getBook sections first document! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:30:53'!
shouldStyle: text with: anSHTextStyler
	"This is a notification that anSHTextStyler is about to re-style its text.
	Answer true to allow styling to proceed, or false to veto the styling"

	^true! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:29:10'!
textStylerClassFor: aSymbol
	aSymbol = #acceptedContents ifTrue: [
		^ SHTextStylerST80].
	^ nil! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 08:57:38'!
updateDocumentationContents: aBrowserWindow

 	|editor|
									
	editor := MetaBrowserTextModelMorph
					textProvider: aBrowserWindow model selectedConcept
					textGetter: #description
					textSetter: #description:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 08:58:08'!
updateSourceContents: aBrowserWindow

	| browser editor |
		
	browser _ aBrowserWindow model.
		
	editor _ browser selectedConcept concept editorMorph.
		
	editor morphExtent: 1500 @ 1000.
	
	aBrowserWindow setContent: editor! !

!EruditeBookBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 09:03:04'!
updateViewContents: aBrowserWindow

 	|editor|
	
	 editor _ EruditeDocViewerMorph 
		textProvider: self 
		textGetter: #renderedContents 
		textSetter: #renderedContents:notifying:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!EruditeBookBrowsableObject class methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2020 12:49:56'!
on: aClass
	^ self new initialize: aClass! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:27:08'!
aspects
	^ #()! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:30:38'!
browseObject
	^ self! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 21:01:53'!
contents
	^ section ifNotNil: [section document contents] ifNil: ['']! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 21:03:29'!
contents: aString notifying: aRequestor
	section ifNotNil: [section contents: aString].
	^ true! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:26:40'!
getStyledText: aDocument

	| erudite text |
		
	erudite _ EruditeMarkupParser parse: aDocument contents.
	
	"We transplant the images from document to styledDocument and back. TODO: this is not good!!!! FIX"
	erudite images: aDocument images.
	text _ MorphicEruditeDocRenderer render: erudite notifying: self.
	aDocument images: erudite images.
	^ text! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:21:20'!
initialize: aBook section: aSection
	book _ aBook.
	section _ aSection! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 13:24:32'!
renderedContents
	^ section ifNotNil: [self getStyledText: section document] ifNil: ['']! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 21:01:21'!
updateSourceContents: aBrowserWindow

 	|editor|
	
	 editor _ EruditeDocViewerMorph 
		textProvider: self 
		textGetter: #contents
		textSetter: #contents:notifying:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!EruditeBookSectionBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:31:48'!
updateViewContents: aBrowserWindow

 	|editor|
	
	 editor _ EruditeDocViewerMorph 
		textProvider: self 
		textGetter: #renderedContents 
		textSetter: #renderedContents:notifying:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!EruditeBookSectionBrowsableObject class methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:20:49'!
book: aBook section: aSection
	^ self new initialize: aBook section: aSection! !

!EruditeBookSectionsAspect methodsFor: 'as yet unclassified' stamp: 'MM 12/29/2020 09:15:45'!
actions

	^ {{#target->self. #action->#newSection:. #label->'new section'. #icon->#newIcon.#help->'add a new section to the book'}.
	   {#target->self. #action->#moveSectionUp:. #label->'move section up'. #icon->#goUpIcon.#help->'move section up'}.
	   {#target->self. #action->#moveSectionDown:. #label->'move section down'. #icon->#goDownIcon.#help->'move section down'}}, 
		super actions! !

!EruditeBookSectionsAspect methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 11:57:15'!
buildBrowserPanesFor: aConcept browser: aBrowser

	^ EruditeBookSectionsAspectPane on: aConcept aspect: self! !

!EruditeBookSectionsAspectPane methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:16:28'!
build

	| explorerMorph |

	explorer _ EruditeBookExplorer on: concept getBook.
	explorer addDependent: self.
	explorerMorph _ HierarchicalListMorph
		model: explorer
		listGetter: #explorerList
		indexGetter: #getCurrentSelection
		indexSetter: #noteNewSelection:
		mainView: self
		menuGetter: #explorerMenu
		keystrokeAction: #explorerKey:from:.
	explorerMorph
		autoDeselect: true;
		color: Color white.
	
	self addMorphUseAll: explorerMorph.
		
	^ explorerMorph! !

!EruditeBookSectionsAspectPane methodsFor: 'as yet unclassified' stamp: 'MM 12/28/2020 12:31:09'!
update: aSymbol

	aSymbol == #getCurrentSelection ifTrue: [ |section|
		section _ explorer getCurrentSelection 	ifNotNil: [:sel | sel section].
		browser selectedSubConcept: (EruditeBookSectionBrowsableObject book: concept getBook section: section)]! !

!EruditeBook class methodsFor: '*MetaBrowserErudite' stamp: 'MM 1/7/2021 10:00:08'!
asBrowsableObject
	^ (self isSubclassOf: EruditeBook) 
		ifTrue: [ EruditeBookBrowsableObject on: self]
		ifFalse: [super asBrowsableObject]! !
