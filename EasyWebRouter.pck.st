'From Cuis 6.0 [latest update: #5062] on 7 February 2022 at 1:32:45 pm'!
'Description '!
!provides: 'EasyWebRouter' 1 9!
!requires: 'AXAnnouncements' 1 1 nil!
!requires: 'WebClient' 1 22 nil!
SystemOrganization addCategory: 'EasyWebRouter-Tests'!
SystemOrganization addCategory: 'EasyWebRouter-Examples'!
SystemOrganization addCategory: 'EasyWebRouter'!


!classDefinition: #EasyWebServerWatcherMorph category: 'EasyWebRouter'!
LayoutMorph subclass: #EasyWebServerWatcherMorph
	instanceVariableNames: 'webServer listSelection events'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'EasyWebServerWatcherMorph class' category: 'EasyWebRouter'!
EasyWebServerWatcherMorph class
	instanceVariableNames: ''!

!classDefinition: #EasyWebRequest category: 'EasyWebRouter'!
WebRequest subclass: #EasyWebRequest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'EasyWebRequest class' category: 'EasyWebRouter'!
EasyWebRequest class
	instanceVariableNames: ''!

!classDefinition: #EasyWebServer category: 'EasyWebRouter'!
WebServer subclass: #EasyWebServer
	instanceVariableNames: 'announcer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'EasyWebServer class' category: 'EasyWebRouter'!
EasyWebServer class
	instanceVariableNames: ''!

!classDefinition: #WebServerEvent category: 'EasyWebRouter'!
AXAnnouncement subclass: #WebServerEvent
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebServerEvent class' category: 'EasyWebRouter'!
WebServerEvent class
	instanceVariableNames: ''!

!classDefinition: #WebServerActionEvent category: 'EasyWebRouter'!
WebServerEvent subclass: #WebServerActionEvent
	instanceVariableNames: 'request action'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebServerActionEvent class' category: 'EasyWebRouter'!
WebServerActionEvent class
	instanceVariableNames: ''!

!classDefinition: #WebServerRequestEvent category: 'EasyWebRouter'!
WebServerEvent subclass: #WebServerRequestEvent
	instanceVariableNames: 'request'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebServerRequestEvent class' category: 'EasyWebRouter'!
WebServerRequestEvent class
	instanceVariableNames: ''!

!classDefinition: #WebServerResponseEvent category: 'EasyWebRouter'!
WebServerEvent subclass: #WebServerResponseEvent
	instanceVariableNames: 'response content'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebServerResponseEvent class' category: 'EasyWebRouter'!
WebServerResponseEvent class
	instanceVariableNames: ''!

!classDefinition: #WebRoute category: 'EasyWebRouter'!
Object subclass: #WebRoute
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebRoute class' category: 'EasyWebRouter'!
WebRoute class
	instanceVariableNames: ''!

!classDefinition: #FooWebRoute category: 'EasyWebRouter-Tests'!
WebRoute subclass: #FooWebRoute
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter-Tests'!
!classDefinition: 'FooWebRoute class' category: 'EasyWebRouter-Tests'!
FooWebRoute class
	instanceVariableNames: ''!

!classDefinition: #WebRouteInstance category: 'EasyWebRouter'!
WebRoute subclass: #WebRouteInstance
	instanceVariableNames: 'name path parameters httpMethod enabled trace debug description'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebRouteInstance class' category: 'EasyWebRouter'!
WebRouteInstance class
	instanceVariableNames: ''!

!classDefinition: #PluggableWebRoute category: 'EasyWebRouter'!
WebRouteInstance subclass: #PluggableWebRoute
	instanceVariableNames: 'handler'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'PluggableWebRoute class' category: 'EasyWebRouter'!
PluggableWebRoute class
	instanceVariableNames: ''!

!classDefinition: #WebRouteParameter category: 'EasyWebRouter'!
Object subclass: #WebRouteParameter
	instanceVariableNames: 'name type source'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EasyWebRouter'!
!classDefinition: 'WebRouteParameter class' category: 'EasyWebRouter'!
WebRouteParameter class
	instanceVariableNames: ''!


!EasyWebServerWatcherMorph commentStamp: '<historical>' prior: 0!
I watch an EasyWebServer events and show them in a list.!

!EasyWebServer commentStamp: '<historical>' prior: 0!
Unlike in WebServer, entryPoints is a Collection of WebRoutes.!

!WebServerEvent commentStamp: 'MM 9/16/2021 11:17:21' prior: 0!
A wrapper for web server events.!

!WebServerActionEvent commentStamp: '<historical>' prior: 0!
An action on an EasyWebServer.!

!WebServerActionEvent methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 13:08:04'!
printOn: aStream

	aStream nextPutAll: 'Action: '.
	action name ifNotNil: [
		aStream nextPutAll: action name; space].
	aStream
		nextPutAll: action path;
		space;
		nextPutAll: action httpMethod asString
	! !

!WebServerRequestEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:24:06'!
printOn: aStream
	aStream nextPutAll: 'Request: '; 
		nextPutAll: request url;
		space;
		nextPutAll: request method asString! !

!WebServerResponseEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:23:47'!
printOn: aStream

	aStream nextPutAll: 'Response: ';
		nextPutAll: response status asString.! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:09'!
name

	^ name! !

!EasyWebServerWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:06:57'!
buildMorph

	| eventsListMorph actionsRow |
	
	listSelection _ ListValueModel with: events.
	eventsListMorph _  PluggableListMorph new.
	listSelection setAsModelOf: eventsListMorph.
	
	self addMorphUseAll: eventsListMorph.
	
	actionsRow _ RowLayoutMorph new.
	actionsRow addMorph: (PluggableButtonMorph model: self action: #inspectEvent label: 'Inspect').
	self addMorph: actionsRow fixedHeight: 30.
	! !

!EasyWebServerWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:05:46'!
initialize: aWebServer

	webServer _ aWebServer.
	
	self registerEvents.
	self buildMorph.! !

!EasyWebServerWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:10:46'!
registerEvents

	webServer when: #request: send: #request:  to: self.
	webServer when: #action:request: send: #action:request: to: self.
	webServer when: #response:content: send: #response:content: to: self. ! !

!EasyWebServerWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 10:41:33'!
on: aWebServer

	^ self newColumn initialize: aWebServer! !

!EasyWebRequest methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 09:23:11'!
sendResponse: aResponse content: aString

	server announcer announce: (WebServerResponseEvent response: aResponse content: aString).

	^ super sendResponse: aResponse content: aString! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:47:31'!
addRoute: aWebRoute

	entryPoints add: aWebRoute! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:51:14'!
addService: aString action: anAction

	self addRoute: (PluggableWebRoute new
					handler: anAction;
					path: aString;
					httpMethod: 'GET';
					yourself) ! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:56:10'!
dispatchRequest: aRequest

	[announcer announce: (WebServerRequestEvent request: aRequest).
	
		super dispatchRequest: aRequest]
	on: Error do: [:err | self halt]! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:08:25'!
dispatchRequest: request url: dispatchUrl


	"Look up the handler for a given url and dispatch to it"

	| entryUrl action |
	
	"Handle TRACE requests right away"
	request method = 'TRACE' ifTrue:[
		^request send200Response: request asString contentType: 'message/http'.
	].

	"Look up the entry point for the request"
	action := nil.
	entryUrl := dispatchUrl asLowercase.
	(entryUrl beginsWith: '/') ifFalse:[entryUrl := '/', entryUrl].
	mutex critical:[action _ self matchActionForUrl: entryUrl request: request	].

	"Handle OPTIONS requests"
	request method = 'OPTIONS' ifTrue:[
		"HEAD, TRACE, and OPTIONS are always supported"
		request sendOptionsResponse: self builtinHttpMethods,
			(action ifNil:[self defaultHttpMethods] ifNotNil:[action first])
	].

	"Handle 404 not found"
	action ifNil:[^request send404Response].

	"Handle 405 method not allowed"
	"(request method = 'HEAD' 
		or:[(action at: 1) includes: request method]) ifFalse:[
			^request send405Response: self builtinHttpMethods, action first.
		]."

	^[self invokeAction: action request: request] 
		on: Error 
		do:[:ex|
			errorHandler 
				ifNil:[self handleError: ex request: request]
				ifNotNil:[errorHandler value: ex value: request]]! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 18:02:20'!
initialize

	super initialize.
	entryPoints _ OrderedCollection new.
	announcer _ AXPluggableAnnouncer on: self.
	self initializeRoutes.! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:02:53'!
initializeRoutes! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 09:22:43'!
invokeAction: action request: aRequest

	announcer announce: (WebServerActionEvent action: action request: aRequest).

	^ action handleRequest: aRequest webServer: self! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:36:30'!
matchActionForUrl: anURL request: aRequest

	^ entryPoints detect: [:action | action matches: anURL request: aRequest] ifNone: [nil]! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 10:37:21'!
newRequest
	"Answer a new request.
	Subclasses should override this method to use a different request class."

	^ (EasyWebRequest new)
		server: self;
		yourself! !

!EasyWebServer methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 23:31:19'!
routeAt: routeName

	^ entryPoints detect: [:ep | ep name = routeName]
! !

!EasyWebServer methodsFor: 'accessing' stamp: 'MM 9/17/2021 09:21:32'!
announcer
	"Answer the value of announcer"

	^ announcer! !

!EasyWebServer methodsFor: 'accessing' stamp: 'MM 9/17/2021 09:21:32'!
announcer: anObject
	"Set the value of announcer"

	announcer _ anObject! !

!EasyWebServer class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:28:56'!
eventsTriggered

	^ super eventsTriggered, {WebServerActionEvent}! !

!WebServerActionEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:32'!
action
	"Answer the value of action"

	^ action! !

!WebServerActionEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:32'!
action: anObject
	"Set the value of action"

	action _ anObject! !

!WebServerActionEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:32'!
request
	"Answer the value of request"

	^ request! !

!WebServerActionEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:32'!
request: anObject
	"Set the value of request"

	request _ anObject! !

!WebServerActionEvent class methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:35:08'!
action: anAction request: aRequest

	^ self new
		action: anAction;
		request: aRequest;
		yourself! !

!WebServerRequestEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:21'!
request
	"Answer the value of request"

	^ request! !

!WebServerRequestEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:21'!
request: anObject
	"Set the value of request"

	request _ anObject! !

!WebServerRequestEvent class methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:34:45'!
request: aRequest

	^ self new
		request: aRequest;
		yourself! !

!WebServerResponseEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:12'!
content
	"Answer the value of content"

	^ content! !

!WebServerResponseEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:12'!
content: anObject
	"Set the value of content"

	content _ anObject! !

!WebServerResponseEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:12'!
response
	"Answer the value of response"

	^ response! !

!WebServerResponseEvent methodsFor: 'accessing' stamp: 'MM 9/16/2021 11:18:12'!
response: anObject
	"Set the value of response"

	response _ anObject! !

!WebServerResponseEvent class methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 11:34:24'!
response: aResponse content: aString

	^ self new response: aResponse;
		content: aString;
		yourself! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 00:02:11'!
extractParametersFromRequest: aRequest

	|params|
	
	params _ Dictionary new.
	
	self extractPathParams: aRequest url into: params.
	
	self extractQueryParams: aRequest into: params.
	
	^ params! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 00:06:32'!
extractPathParams: anURL into: aDictionary

	! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 00:06:50'!
extractQueryParams: aRequest into: aDictionary! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:52:07'!
handleRequest: aRequest webServer: aWebServer

	^ self handleRequest: aRequest webServer: aWebServer parameters: (self extractParametersFromRequest: aRequest)! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:52:17'!
handleRequest: aRequest webServer: aWebServer parameters: params

	^ self subclassResponsibility ! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:40:34'!
handlerSelector

	^ self subclassResponsibility ! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:22:06'!
httpMethod
	^ self subclassResponsibility ! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:39:50'!
matches: anURL request: aRequest

	^ aRequest method asSymbol == self httpMethod asSymbol and: [self pathRegex matches: anURL]! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:35:37'!
middleware

	^ {}! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:22:10'!
parameters
	^ self subclassResponsibility ! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:22:01'!
path
	^ self subclassResponsibility ! !

!WebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 21:32:48'!
pathRegex

	^ (self path copyWithRegex: '{.*}' matchesReplacedWith: '(.*)') asRegex! !

!FooWebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:33:16'!
httpMethod
	^ #get! !

!FooWebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:29:27'!
parameters
	^ {WebRouteParameter name: #id type: #string source: #get}! !

!FooWebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:28:42'!
path
	^ '/foo/{id}'! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
debug
	"Answer the value of debug"

	^ debug! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
debug: anObject
	"Set the value of debug"

	debug _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:17'!
description

	^ description! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
enabled
	"Answer the value of enabled"

	^ enabled! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
enabled: anObject
	"Set the value of enabled"

	enabled _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 23:20:00'!
handleRequest: aRequest webServer: aWebServer parameters: params

	|handlerSelector|
	
	trace ifTrue: [Transcript show: 'Accessing: ', self name; newLine].
	debug ifTrue: [self halt].
	
	handlerSelector _ (self name, ':params:') asSymbol.
	^ aWebServer perform: handlerSelector with: aRequest with: params! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 21:30:38'!
httpMethod

	^ httpMethod! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
httpMethod: anObject
	"Set the value of httpMethod"

	httpMethod _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 21:30:25'!
parameters

	^ parameters! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
parameters: anObject
	"Set the value of parameters"

	parameters _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 21:30:15'!
path

	^ path! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
path: anObject
	"Set the value of path"

	path _ anObject! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
trace
	"Answer the value of trace"

	^ trace! !

!WebRouteInstance methodsFor: 'accessing' stamp: 'MM 9/11/2021 19:56:00'!
trace: anObject
	"Set the value of trace"

	trace _ anObject! !

!WebRouteInstance methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:53:02'!
initialize

	super initialize.
	debug _ false.
	enabled _ true.
	trace _ false.
	parameters  _ OrderedCollection new.
	httpMethod _ 'GET'! !

!WebRouteInstance class methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 19:57:37'!
metamodel

	|metamodel|
	
	metamodel _ MEMetaModel new.
	metamodel name: 'Web route';
		description: 'A web route handler.';
		instanceClass: WebRouteInstance.
	
	metamodel addAttribute: #name type: #string
		properties: [:attr |		attr description: 'The route name';
						category: #settings].
	metamodel addAttribute: #path type: #string
		properties: [:attr | attr description: 'The route path template';
						category: #settings].
	"metamodel addAttribute: #parameters type: (MEListAttributeType of: MEWebRouteParameter)	."
	metamodel addAttribute: #parameters type: #composition
		properties: [:attr |
					attr description: 'The route parameters'.
					attr type multiplicity: #many;
							associatedModel: WebRouteParameter].
	metamodel addAttribute: #httpMethod type: (MEChoiceAttributeType choices: {'GET'.'POST'.'DELETE'})
		properties: [:attr | attr description: 'The HTTP method';
						category: #settings].
	"metamodel addAttribute: #middleware type: (MEListAttributeType of: MEWebRouteMiddleware)
		properties: [:attr | attr description: 'Web route middleware']."
		
	metamodel addAttribute: #description type: #text
		properties: [:attr | attr description: 'A description of the route parameter.'].
		
	metamodel addAttribute: #enabled type: #boolean
		properties: [:attr | attr description: 'Whether the route is enabled or not';
						category: #settings].
					
	metamodel addAttribute: #debug type: #boolean
		properties: [:attr | attr description: 'When enabled, a debugger is triggered on entry';
						category: #debugging].
	metamodel addAttribute: #trace type: #boolean
		properties: [:attr | attr description: 'When enabled, the route call is logged to the Transcript';
						category: #debugging].

	metamodel propertyAt: #printer 
			put: [:aWebRoute | aWebRoute name, ': ', aWebRoute httpMethod, ' ', aWebRoute path].
	metamodel propertyAt: #printTemplate
			put: '{name}: {HTTP method} {path}'.
	metamodel action: 'compile' selector: #compile description: 'Compile the web route'.
		
		
	^ metamodel	! !

!PluggableWebRoute methodsFor: 'accessing' stamp: 'MM 9/17/2021 12:45:45'!
handler
	"Answer the value of handler"

	^ handler! !

!PluggableWebRoute methodsFor: 'accessing' stamp: 'MM 9/17/2021 12:45:45'!
handler: anObject
	"Set the value of handler"

	handler _ anObject! !

!PluggableWebRoute methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:46:50'!
handleRequest: aRequest webServer: aWebServer parameters: params

	^ handler value: aRequest value: params! !

!WebRouteParameter methodsFor: 'accessing' stamp: 'MM 9/7/2021 20:22:44'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!WebRouteParameter methodsFor: 'accessing' stamp: 'MM 9/7/2021 20:22:44'!
source
	"Answer the value of source"

	^ source! !

!WebRouteParameter methodsFor: 'accessing' stamp: 'MM 9/7/2021 20:22:44'!
source: anObject
	"Set the value of source"

	source _ anObject! !

!WebRouteParameter methodsFor: 'accessing' stamp: 'MM 9/7/2021 20:22:44'!
type
	"Answer the value of type"

	^ type! !

!WebRouteParameter methodsFor: 'accessing' stamp: 'MM 9/7/2021 20:22:44'!
type: anObject
	"Set the value of type"

	type _ anObject! !

!WebRouteParameter class methodsFor: 'as yet unclassified' stamp: 'MM 9/7/2021 20:30:25'!
name: paramName type: paramType source: paramSource

	^ self new name: paramName; 
			type: paramType;
			source: paramSource;
			yourself! !
