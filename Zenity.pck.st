'From Cuis 5.0 [latest update: #4743] on 11 September 2021 at 4:20:32 pm'!
'Description '!
!provides: 'Zenity' 1 4!
SystemOrganization addCategory: 'Zenity'!


!classDefinition: #ZenityDateInputMorph category: 'Zenity'!
LayoutMorph subclass: #ZenityDateInputMorph
	instanceVariableNames: 'date'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zenity'!
!classDefinition: 'ZenityDateInputMorph class' category: 'Zenity'!
ZenityDateInputMorph class
	instanceVariableNames: ''!

!classDefinition: #ZenityFileInputMorph category: 'Zenity'!
LayoutMorph subclass: #ZenityFileInputMorph
	instanceVariableNames: 'file'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zenity'!
!classDefinition: 'ZenityFileInputMorph class' category: 'Zenity'!
ZenityFileInputMorph class
	instanceVariableNames: ''!

!classDefinition: #ZenityColorChooserButtonMorph category: 'Zenity'!
PluggableButtonMorph subclass: #ZenityColorChooserButtonMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zenity'!
!classDefinition: 'ZenityColorChooserButtonMorph class' category: 'Zenity'!
ZenityColorChooserButtonMorph class
	instanceVariableNames: ''!

!classDefinition: #Zenity category: 'Zenity'!
Object subclass: #Zenity
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Zenity'!
!classDefinition: 'Zenity class' category: 'Zenity'!
Zenity class
	instanceVariableNames: ''!


!ZenityDateInputMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 16:19:21'!
initialize
	super initialize.
	date _ ''.
	
	self addMorphUseAll: (StringInputMorph on: (AspectAdaptor on: self aspect: #dateString)).
	self addMorph: (PluggableButtonMorph model: self action: #pickUpDate  label: '...' ) fixedWidth: 30. ! !

!ZenityDateInputMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 16:19:40'!
pickUpDate
	|selectedDate|
	
	selectedDate _ Zenity calendar .
	
	selectedDate ifNotNil: [
		self date: selectedDate]
	! !

!ZenityDateInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 16:18:08'!
date
	"Answer the value of file"

	^ date! !

!ZenityDateInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 16:18:21'!
date: anObject
	"Set the value of file"

	date _ anObject.
	
	self changed: date! !

!ZenityDateInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 16:18:40'!
dateString
	^ date asString! !

!ZenityDateInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 16:19:01'!
dateString: aString
	self date: aString asDate! !

!ZenityDateInputMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 16:17:32'!
new

	^ self newRow! !

!ZenityFileInputMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 14:30:20'!
initialize
	super initialize.
	file _ ''.
	
	self addMorphUseAll: (StringInputMorph on: (AspectAdaptor on: self aspect: #filePath)).
	self addMorph: (PluggableButtonMorph model: self action: #pickUpFile  label: '...' ) fixedWidth: 30. ! !

!ZenityFileInputMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 14:28:16'!
pickUpFile
	|selectedFile|
	
	selectedFile _ Zenity selectFile .
	
	selectedFile ifNotNil: [
		self file: selectedFile]
	! !

!ZenityFileInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 14:24:53'!
file
	"Answer the value of file"

	^ file! !

!ZenityFileInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 14:29:59'!
file: anObject
	"Set the value of file"

	file _ anObject.
	
	self changed: file! !

!ZenityFileInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 14:28:43'!
filePath
	^ file asString! !

!ZenityFileInputMorph methodsFor: 'accessing' stamp: 'MM 9/11/2021 14:29:07'!
filePath: aString
	self file: aString asFileEntry! !

!ZenityFileInputMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/11/2021 14:23:48'!
new

	^ self newRow! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:08:56'!
defaultColor

	^ Color white! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:26:20'!
initialize
	super initialize.
	
	self model: self defaultColor asValue! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:08:22'!
initialize: aModel

	self model: aModel.
	self color: self value.! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:23:18'!
model: anObject
	"Set my model and make me me a dependent of the given object."

	model ifNotNil: [model removeDependent: self].
	anObject addDependent: self.
	model _ anObject! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:19:14'!
performAction

	| c |
	
	c _ Zenity selectColor.
	
	c ifNotNil: [self value: c]! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:23:53'!
update: aSymbol

	self color: model value! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:08:41'!
value

	^ model value ifNil: [self defaultColor]! !

!ZenityColorChooserButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:14:24'!
value: aColor

	model value: aColor.
	! !

!ZenityColorChooserButtonMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:07:43'!
on: aModel
	^ self new initialize: aModel! !

!Zenity class methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:13:42'!
calendar
	
	| dateStr |	
	
	OSProcess waitForCommand: 'DISPLAY=:0 ', self zenityCommand, ' --calendar > /tmp/zenity-calendar'.
	
	dateStr _ '/tmp/zenity-calendar' asFileEntry textContents withBlanksTrimmed.
	
	dateStr ifEmpty: [^nil].
	
	^ Date readFrom: dateStr readStream! !

!Zenity class methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:12:39'!
selectColor

	| colorStr rgb rgbStr |
		
	OSProcess waitForCommand: 'DISPLAY=:0 ', self zenityCommand, ' --color-selection > /tmp/zenity-color'.
	
	colorStr _ '/tmp/zenity-color' asFileEntry textContents withBlanksTrimmed.
	
	colorStr withBlanksTrimmed ifEmpty: [^nil].
	
	rgbStr _ colorStr copyFrom: (colorStr indexOf: $() + 1 to: (colorStr indexOf: $)) - 1.
	
	rgb _ (rgbStr subStrings: #($,)) collect: [:s | s asNumber / 255].
	
	rgb size == 3 ifTrue: [
		^ Color perform: #r:g:b: withArguments: rgb]
		ifFalse: [
			^ Color perform: #r:g:b:alpha: withArguments: rgb]
			! !

!Zenity class methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 09:13:27'!
selectFile
	
	| fileStr |
	
	OSProcess waitForCommand: self zenityCommand, ' --file-selection > /tmp/zenity-file'.
	
	fileStr _ '/tmp/zenity-file' asFileEntry textContents withBlanksTrimmed.
	
	fileStr ifEmpty: [^ nil].
	
	^  fileStr asFileEntry
	! !

!Zenity class methodsFor: 'as yet unclassified' stamp: 'MM 9/10/2021 00:19:45'!
zenityCommand
	^ '/usr/bin/zenity'! !
