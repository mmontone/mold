'From Cuis 5.0 [latest update: #4743] on 5 September 2021 at 10:54:42 am'!
'Description PetitParser for PEG and other services.'!
!provides: 'PetitPeg' 1 13!
SystemOrganization addCategory: 'PetitPeg'!
SystemOrganization addCategory: 'PetitPeg-Tests'!


!classDefinition: #PPPegGrammar category: 'PetitPeg'!
PPCompositeParser subclass: #PPPegGrammar
	instanceVariableNames: 'definition identifier leftArrow expression grammar sequence slash prefix and not suffix primary question star plus open close literal class dot identStart identCont range char comment ruleName ruleReference quantifier'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PPPegGrammar class' category: 'PetitPeg'!
PPPegGrammar class
	instanceVariableNames: ''!

!classDefinition: #PPEPegGrammar category: 'PetitPeg'!
PPPegGrammar subclass: #PPEPegGrammar
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PPEPegGrammar class' category: 'PetitPeg'!
PPEPegGrammar class
	instanceVariableNames: ''!

!classDefinition: #PPPegAutoCompleteGrammar category: 'PetitPeg'!
PPPegGrammar subclass: #PPPegAutoCompleteGrammar
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PPPegAutoCompleteGrammar class' category: 'PetitPeg'!
PPPegAutoCompleteGrammar class
	instanceVariableNames: ''!

!classDefinition: #PPPegPetitParser category: 'PetitPeg'!
PPPegGrammar subclass: #PPPegPetitParser
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PPPegPetitParser class' category: 'PetitPeg'!
PPPegPetitParser class
	instanceVariableNames: ''!

!classDefinition: #PPRuleReference category: 'PetitPeg'!
PPParser subclass: #PPRuleReference
	instanceVariableNames: 'name'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PPRuleReference class' category: 'PetitPeg'!
PPRuleReference class
	instanceVariableNames: ''!

!classDefinition: #PetitParserVisitor category: 'PetitPeg'!
Object subclass: #PetitParserVisitor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PetitParserVisitor class' category: 'PetitPeg'!
PetitParserVisitor class
	instanceVariableNames: ''!

!classDefinition: #PetitParserSyntraxDiagramGenerator category: 'PetitPeg'!
PetitParserVisitor subclass: #PetitParserSyntraxDiagramGenerator
	instanceVariableNames: 'parser stream outputFilePath'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg'!
!classDefinition: 'PetitParserSyntraxDiagramGenerator class' category: 'PetitPeg'!
PetitParserSyntraxDiagramGenerator class
	instanceVariableNames: ''!

!classDefinition: #PPPegGrammarTest category: 'PetitPeg-Tests'!
ProtoObject subclass: #PPPegGrammarTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg-Tests'!
!classDefinition: 'PPPegGrammarTest class' category: 'PetitPeg-Tests'!
PPPegGrammarTest class
	instanceVariableNames: ''!

!classDefinition: #PPPegPetitParserTest category: 'PetitPeg-Tests'!
ProtoObject subclass: #PPPegPetitParserTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PetitPeg-Tests'!
!classDefinition: 'PPPegPetitParserTest class' category: 'PetitPeg-Tests'!
PPPegPetitParserTest class
	instanceVariableNames: ''!


!PPPegGrammar commentStamp: '<historical>' prior: 0!
The PEG grammar.

# Hierarchical syntax
Grammar     <- _ Definition+ EndOfFile
Definition  <- Identifier LEFTARROW Expression

Expression  <- Sequence (SLASH Sequence)*
Sequence    <- Prefix*
Prefix      <- (AND / NOT)? Suffix
Suffix      <- Primary (QUESTION / STAR / PLUS)?
Primary     <- Identifier !!LEFTARROW
            /  OPEN Expression CLOSE
            /  Literal / Class / DOT

# Lexical syntax
Identifier  <- IdentStart IdentCont* _
IdentStart  <- [a-zA-Z_]
IdentCont   <- IdentStart / [0-9]

Literal     <- ['] (!!['] Char)* ['] _
            /  ["] (!!["] Char)* ["] _
Class       <- '[' (!!']' Range)* ']' _
Range       <- Char '-' Char / Char
Char        <- '\\' [nrt'"\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3][0-7][0-7]
            /  '\\' [0-7][0-7]?
            /  !!'\\' .

LEFTARROW   <- '<-' _
SLASH       <- '/' _
AND         <- '&' _
NOT         <- '!!' _
QUESTION    <- '?' _
STAR        <- '*' _
PLUS        <- '+' _
OPEN        <- '(' _
CLOSE       <- ')' _
DOT         <- '.' _

_           <- (Space / Comment)*
Comment     <- '#' (!!EndOfLine .)* (EndOfLine/EndOfFile)
Space       <- ' ' / '\t' / EndOfLine
EndOfLine   <- '\r\n' / '\n' / '\r'
EndOfFile   <- !!.
!

!PPEPegGrammar commentStamp: '<historical>' prior: 0!
The extended PEG grammar.

Extensions:
  label:node Labelled nodes
  @{x=y} Node attributes syntax
  @attr Inline attribute syntax
  {x := foo} Action syntax
  ^{x y | x + y} Stack action syntax
  $ exp Push the matched text!

!PPPegPetitParser commentStamp: '<historical>' prior: 0!
Convert a PEG grammar to a Petit parser!

!PetitParserSyntraxDiagramGenerator commentStamp: 'MM 9/5/2021 10:53:49' prior: 0!
Generates a syntax diagram using Python syntrax package.

https://kevinpt.github.io/syntrax

Usage:

(PetitParserSyntraxDiagramGenerator on: aPetitParser) generate imageMorph
	openInWorld!

!PPPegGrammarTest commentStamp: '<historical>' prior: 0!
Test of PEG grammar!

!PPPegPetitParserTest commentStamp: '<historical>' prior: 0!
Test of PEG to PetitParser conversion!

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/13/2018 00:04'!
comment
	
	"Comment     <- '#' (!!EndOfLine .)* (EndOfLine/EndOfFile)"
	^ ($# asParser, Character newLineCharacter asParser negate star, (Character newLineCharacter asParser)) trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 09:22'!
definition
	"Definition  <- Identifier LEFTARROW Expression"
	
	^ comment, definition / (ruleName,  leftArrow trim, expression)! !

!PPRuleReference methodsFor: 'as yet unclassified' stamp: 'MM 10/26/2018 14:57'!
name
	^ name! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 10/20/2018 18:25'!
parserClass
	^ PPPegGrammar.! !

!PPPegPetitParserTest methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 23:55'!
parserClass
	^ PPPegPetitParser.! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:03'!
and
	
	^ $& asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:38'!
char

	"Char        <- '\\' [nrt'""\[\]\\]
            /  '\\' 'u' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' 'U' [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f] [0-9A-Fa-f]
            /  '\\' [0-3][0-7][0-7]
            /  '\\' [0-7][0-7]?
            /  !!'\\' ."

	^  #word asParser / $_ asParser / $' asParser / $" asParser! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:04'!
class
	"Class       <- '[' (!!']' Range)* ']' _"
	^ $[ asParser, ($] asParser not, range) star, $] asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:04'!
close
	
	^ $) asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:04'!
dot
	
	^ $. asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 19:32'!
expression
	"Expression  <- Sequence (SLASH Sequence)*"
	
	^ sequence, (slash, sequence) star! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:04'!
grammar
	"Grammar     <- _ Definition+ EndOfFile"
	^ definition plus! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 19:12'!
identCont
	"IdentCont   <- IdentStart / [0-9]"
	^ identStart / #digit asParser! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:21'!
identStart
	"IdentStart  <- [a-zA-Z_]"
	
	^ #letter asParser / $_ asParser
	
	
	! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2018 23:30'!
identifier
	"Identifier  <- IdentStart IdentCont* _"
	^ ((identStart, identCont star) trim flatten)
		propertyAt: #completer put: #('identifier');
		yourself! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2018 23:46'!
leftArrow

	^ (('<-' asParser)
		propertyAt: #completer put: #('<-');
		yourself) trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 10/26/2018 17:15'!
literal
	"Literal     <- ['] (!!['] Char)* ['] _
            /  [""] (!![""] Char)* [""] _"

	^ ($' asParser, ($' asParser not, char) star,  $' asParser) trim flatten /
	   ($" asParser, ($" asParser not, char) star, $" asParser) trim flatten! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:06'!
not
	
	^ $!! asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:06'!
open
	
	^ $( asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:06'!
plus
	
	^ $+ asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:07'!
prefix

	"Prefix      <- (AND / NOT)? Suffix"
	
	^ (and / not) optional, suffix! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 09:56'!
primary
	"Primary     <- Identifier !!LEFTARROW
            /  OPEN Expression CLOSE
            /  Literal / Class / DOT"

	^ ((ruleReference, leftArrow not) ==> [:array | array first]) /
	   ((open,  expression,  close) ==> [:array | array second]) /
	   literal / class / dot! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:40'!
quantifier

	^ question / star / plus! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:07'!
question
	
	^ $? asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 19:36'!
range
	"Range       <- Char '-' Char / Char"
	
	^ (self char, $- asParser, self char) / self char! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 09:22'!
ruleName

	^ identifier! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:41'!
ruleReference

	^ identifier! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:12'!
sequence
	
	"Sequence    <- Prefix*"
	
	^prefix plus! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:07'!
slash
	
	^ $/ asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:08'!
star
	
	^ $* asParser trim! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:06'!
start

	^ grammar end! !

!PPPegGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:40'!
suffix
	"Suffix      <- Primary (QUESTION / STAR / PLUS)?"
	
	^ primary,  quantifier optional! !

!PPPegAutoCompleteGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:43'!
primary

	^ super primary
		propertyAt: #completer put: #('rule' '(' 'literal' '[range]' '.');
		yourself! !

!PPPegAutoCompleteGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:40'!
quantifier

	^ super quantifier 
		propertyAt: #completer put: #completeQuantifier;
		yourself! !

!PPPegAutoCompleteGrammar methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2018 11:41'!
ruleReference

	^ super ruleReference
		propertyAt: #completer put: #completeRule;
		yourself! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/26/2018 15:28'!
expression
	
	^ super expression ==> [:array |	|parser|
		parser _ array first.
		array second do: [:p |
			parser _ parser / p second].
		parser]! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/26/2018 14:57'!
identifier
	^ super identifier ==> [ :id |
		PPRuleReference rule: id withBlanksTrimmed asSymbol ].! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/26/2018 17:23'!
literal

	^ super literal ==> [:literal |
		|lit|
		lit _ literal withBlanksTrimmed.
		(lit copyFrom: 2 to: (lit size - 1)) asParser]! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 21:42'!
prefix

	^ super prefix ==> [:array | |andOrNot suffix|
		andOrNot _ array first.
		suffix _ array second.
		andOrNot caseOf: {
			[$&] -> [suffix and].
			[$!!] -> [suffix not].
		} otherwise:[ suffix ]]! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 20:05'!
sequence

	^ super sequence ==> [:array | PPSequenceParser withAll: array ]! !

!PPPegPetitParser methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 22:13'!
suffix

	^ super suffix ==> [:suffix |
		suffix second caseOf: {
			[$*] -> [suffix first star].
			[$+] -> [suffix first plus].
			[$?] -> [suffix first optional]}
		otherwise: [suffix first]]! !

!PPRuleReference methodsFor: 'accessing' stamp: 'MM 10/23/2018 23:49'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!PPRuleReference class methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 23:50'!
rule: aString

	^ self new name: aString; yourself! !

!PetitParserVisitor methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 13:55'!
visit: anObject

	|visitSelector|
	visitSelector _ ('visit', anObject class name asString, ':') asSymbol. 
	^ self perform: visitSelector with: anObject! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:43'!
generate

	self generateSyntraxSpec.
	self invokeSyntrax.
	! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 16:28'!
generateSyntraxSpec
	self visit: parser.
	self syntraxFilePath fileContents: stream contents.! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:47'!
imageMorph

	^ ImageMorph new image: self syntraxForm! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 13:58'!
initialize: aParser

	parser _ aParser.
	stream _ WriteStream on: String new.! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:42:14'!
invokeSyntrax

	|process command|
	
	command _ self syntraxCommand.
	
	process _ OSProcess waitForCommand: command.
	
	process exitStatus isZero ifFalse: [
		self error: 'Command failed: ', command]! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:48'!
outputFilePath

	^ outputFilePath ifNil: ['/tmp/ppdiagram.png']! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 16:16'!
syntraxCommand

	^ self syntraxCommandPath, ' -i ',  self syntraxFilePath  , ' -o ', self outputFilePath ! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:42:54'!
syntraxCommandPath

	^ '/home/marian/.local/bin/syntrax'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:20'!
syntraxFilePath

	^ '/tmp/ppdiagram.syntrax'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:41:16'!
syntraxForm

	^ ImageReadWriter formFromFileNamed: self outputFilePath.! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:36:07'!
visitPPActionParser: aParser

	^ self visit: aParser children first! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 16:01'!
visitPPChoiceParser: aParser
	
	stream nextPutAll: 'choice('.
	self visit: aParser children first.
	aParser children allButFirst do: [:parser |
		stream nextPutAll: ', '.
		self visit: parser].
	stream nextPut: $).! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:36:28'!
visitPPDelegateParser: aParser

	^ self visit: aParser children first! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:36:38'!
visitPPFlattenParser: aParser

	^ self visit: aParser children first! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:40:45'!
visitPPLiteralObjectParser: aParser

	stream nextPut: $';
			nextPutAll: aParser literal asString;
			nextPut: $'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 16:31'!
visitPPLiteralSequenceParser: aParser

	stream nextPut: $';
			nextPutAll: aParser literal asString;
			nextPut: $'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 9/5/2021 10:52:56'!
visitPPNotParser: aParser

	"TODO: this is wrong. We need to find the correct way of representing PEG negation semantics in syntax diagrams."
	stream nextPutAll: 'line(''not'' , '.
	self visit: aParser children first.
	stream nextPut: $).! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:15'!
visitPPOptionalParser: aParser

	stream nextPutAll: 'opt('.
	self visit: aParser children first.
	stream nextPut: $).! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:07'!
visitPPPossessiveRepeatingParser: aParser

	stream nextPutAll: 'loop('.
	self visit: aParser children first.
	stream nextPutAll: ', None)'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:39:37'!
visitPPPredicateObjectParser: aParser

	stream nextPut: $';
			nextPutAll: aParser message;
			nextPut: $'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 1/15/2021 12:39:34'!
visitPPPredicateParser: aParser

	stream nextPut: $';
			nextPutAll: aParser message;
			nextPut: $'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:04'!
visitPPRuleReference: aRuleReference

	stream nextPut: $';
			nextPutAll: aRuleReference name asString;
			nextPut: $'! !

!PetitParserSyntraxDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:03'!
visitPPSequenceParser: aParser

	"A line() creates a series of nodes arranged horizontally from left to right.

	line('[', 'foo', ',', '/bar', ']')"
	
	stream nextPutAll: 'line('.
	self visit: aParser children first.
	aParser children allButFirst do: [:parser |
		stream nextPutAll: ', '.
		self visit: parser].
	stream nextPut: $).! !

!PetitParserSyntraxDiagramGenerator class methodsFor: 'as yet unclassified' stamp: 'MM 11/10/2018 14:46'!
on: aParser

	^ self new initialize: aParser! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/13/2018 00:02'!
testComment
	self parse: '# this is a comment
'
		rule: #comment! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:19'!
testDefinition1
	self parse: 'Grammar     <- _ Definition+ EndOfFile'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:22'!
testDefinition2
	self parse: 'Expression  <- Sequence (SLASH Sequence)*'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:22'!
testDefinition3

	self parse: 'Primary     <- Identifier !!LEFTARROW
            /  OPEN Expression CLOSE
            /  Literal / Class / DOT'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:23'!
testDefinition4
	self parse: 'IdentStart  <- [a-zA-Z_]'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:26'!
testDefinition5
	self parse: 'Literal     <- [''] (!![''] Char)* [''] _
            /  ["] (!!["] Char)* ["] _'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/13/2018 00:04'!
testDefinition6
	self parse: '# lalal
   # asdfaddf
	Literal     <- [''] (!![''] Char)* [''] _
            /  ["] (!!["] Char)* ["] _'
		rule: #definition! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:16'!
testExpression1
	self parse: '''foo'''
		rule: #expression! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:16'!
testExpression2
	self parse: '''foo'' ''bar'''
		rule: #expression! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:16'!
testExpression3
	self parse: '''foo'' ''bar'' / ''baz'''
		rule: #expression! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:17'!
testExpression4
	self fail: '''foo'' ''bar'' | ''baz'''
		rule: #expression! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:44'!
testGrammar
	self parse: 'Grammar     <- _ Definition+ EndOfFile
Definition  <- Identifier LEFTARROW Expression'
	rule: #grammar! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 22:56'!
testIdentifier1
	self parse: 'foo'
		rule: #identifier
	! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:20'!
testIdentifier2
	self parse: '_'
		rule: #identifier
	! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:10'!
testLiteral1
	self parse: '''foo'''
		rule: #literal! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:11'!
testLiteral2
	self fail: '''foo'
		rule: #literal! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:15'!
testPrimary1
	self parse: '''foo'''
		rule: #primary! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:15'!
testPrimary2
	self parse: '(''foo'')'
		rule: #primary! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:14'!
testPrimary3
	self parse: '.'
		rule: #primary! !

!PPPegGrammarTest methodsFor: 'as yet unclassified' stamp: 'MM 9/12/2018 23:18'!
testPrimary4
	self parse: 'foo'
		rule: #primary! !

!PPPegPetitParserTest methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 21:31'!
testIdentifier

	|res|
	
	res _ self parse: 'foo'
		rule: #identifier.
		
	self assert: res isSymbol! !

!PPPegPetitParserTest methodsFor: 'as yet unclassified' stamp: 'MM 10/23/2018 23:48'!
testLiteral

	|res|
	
	res _ self parse: '"foo"'
		rule: #literal.
		
	! !
