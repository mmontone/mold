'From Cuis 6.0 [latest update: #5083] on 23 February 2022 at 12:39:37 pm'!
'Description Extensions for grabbing objects from the Smalltalk World.'!
!provides: 'ObjectGrabber' 1 5!
SystemOrganization addCategory: 'ObjectGrabber'!


!classDefinition: #ObjectDraggerMorph category: 'ObjectGrabber'!
LabelMorph subclass: #ObjectDraggerMorph
	instanceVariableNames: 'model'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ObjectGrabber'!
!classDefinition: 'ObjectDraggerMorph class' category: 'ObjectGrabber'!
ObjectDraggerMorph class
	instanceVariableNames: ''!

!classDefinition: #ObjectGrabberServices category: 'ObjectGrabber'!
Object subclass: #ObjectGrabberServices
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ObjectGrabber'!
!classDefinition: 'ObjectGrabberServices class' category: 'ObjectGrabber'!
ObjectGrabberServices class
	instanceVariableNames: ''!


!ObjectDraggerMorph commentStamp: '<historical>' prior: 0!
I'm a Morph that holds an object and displays its name in a label.!

!ObjectDraggerMorph methodsFor: 'accessing' stamp: 'MM 9/20/2021 18:35:34'!
model
	"Answer the value of model"

	^ model value! !

!ObjectDraggerMorph methodsFor: 'accessing' stamp: 'MM 2/23/2022 12:28:00'!
model: anObject
	"Set the value of model"

	model _ anObject asValue! !

!ObjectDraggerMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 18:51:50'!
acceptDroppingMorph: aMorph event: evt
	"This message is sent when a morph is dropped onto a morph that has agreed to accept the dropped morph by responding 'true' to the wantsDroppedMorph:event: message. 
	
	We start holding the new object contained in aMorph."
	
	|draggedObject|
	
	draggedObject _ aMorph
			valueOfProperty: #dropSelectorArgument
			ifAbsent: [self error: 'aMorph is missing dropSelectorArgument property'].
	
	model value: draggedObject.
	
	contents _ model value printString.
	
	self redrawNeeded! !

!ObjectDraggerMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 18:47:05'!
allowsMorphDrop

	^ true! !

!ObjectDraggerMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/23/2022 12:28:13'!
draggedObject

	^ model value! !

!ObjectDraggerMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 21:36:13'!
drawOn: aCanvas

	aCanvas fillRectangle: ((0@0) extent: self morphExtent) color: Color lightYellow.
	super drawOn: aCanvas! !

!ObjectDraggerMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 18:52:22'!
initialize: aModel

	"aModel can be an object, or a ValueModel."
	
	model _ aModel asValue.
	contents _ aModel value printString.
	self fitContents.! !

!ObjectDraggerMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 18:32:07'!
on: anObject
	
	"anObject can be a plain object, or a ValueModel."
	
	^ self new initialize: anObject! !

!ObjectGrabberServices class methodsFor: 'as yet unclassified' stamp: 'MM 2/23/2022 12:38:24'!
buildGrabMenuFor: anObject

	| menu |
	
	menu _ MenuMorph new.
	menu addTitle: 'Grab'.
		
	anObject grabTargets do: [:obj |
				menu add: (obj printString, ' (', obj class name, ')') 
					target: [obj grab]
					action: #value 
					argumentList: {}].
				
	^ menu! !

!ObjectGrabberServices class methodsFor: 'as yet unclassified' stamp: 'MM 1/29/2022 13:40:19'!
initialize

	self installGrabHalo! !

!ObjectGrabberServices class methodsFor: 'as yet unclassified' stamp: 'MM 1/29/2022 13:46:10'!
installGrabHalo

	|spec|
	
	spec _ 
	HaloSpec new 
		horizontalPlacement: #leftCenter
		verticalPlacement: #bottom
		color: Color purple
		iconSymbol: #haloGrabIcon
		addHandleSelector: #addGrabObjectHandle: 
		hoverHelp: 'Grab'.
	Preferences haloSpecifications.	
	Preferences parameters at: #HaloSpecs
		put: ((Preferences parameters at: #HaloSpecs), {spec}).! !

!ObjectGrabberServices class methodsFor: 'as yet unclassified' stamp: 'MM 1/29/2022 13:48:16'!
openGrabMenuFor: anObject

	(self buildGrabMenuFor: anObject) popUpInWorld! !

!ObjectGrabberServices class methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:58:10'!
smalltalkEditorMenuOptions
	
	^`{
			{
				#itemGroup 		-> 		20.
				#itemOrder 		-> 		70.
				#label 			-> 	'Grab it'.
				#selector 		-> 		#grabIt.
				#icon 			-> 		#haloGrabIcon
			} asDictionary.
		}`! !

!Object methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 14:07:00'!
grab

	self activeHand grab: self! !

!Object methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 14:04:07'!
grabTargets
	^ {self}! !

!CodePackageList methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 14:04:57'!
grabTargets
	^ self selection 
		ifNotNil: [:sel | super grabTargets, {sel}]
		ifNil: [super grabTargets]! !

!SmalltalkEditor methodsFor: '*ObjectGrabber' stamp: 'MM 9/18/2021 20:09:26'!
grabIt
	
	| answer |
	answer _ self
		evaluateSelectionAndDo: [ :result | self activeHand grab: result] ifFail: [] profiled: false.! !

!HandMorph methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 20:21:14'!
grab: anObject

	"Grab anObject using an ObjectHolderMorph."
 
	| sm |
			
	sm _ ObjectDraggerMorph on: anObject.
	sm setProperty: #dragSource toValue: self.
	sm setProperty: #dropSelectorArgument toValue: anObject.
	
	self attachMorphBeside: sm! !

!HaloMorph methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 13:47:00'!
addGrabObjectHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #openGrabObjectMenu! !

!HaloMorph methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 13:47:35'!
openGrabObjectMenu

	ObjectGrabberServices openGrabMenuFor: target! !

!CodePackageListWindow methodsFor: '*ObjectGrabber' stamp: 'MM 1/29/2022 14:09:15'!
grabTargets
	^ model selection 
		ifNotNil: [:sel | super grabTargets, {sel}]
		ifNil: [super grabTargets]! !

!FileListWindow methodsFor: '*ObjectGrabber' stamp: 'MM 2/23/2022 12:36:55'!
grabTargets

	| targets |
	
	targets _ super grabTargets asOrderedCollection.
	model selectedFileEntry ifNotNil: [:fileEntry |
		fileEntry name = 'nil' ifFalse: [
			targets add: fileEntry]].
	model currentDirectorySelected ifNotNil: [:wrapper |
		targets add: wrapper item].
	
	^ targets! !
ObjectGrabberServices initialize!
