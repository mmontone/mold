'From Cuis 5.0 [latest update: #4220] on 19 June 2020 at 9:07:09 pm'!
'Description '!
!provides: 'OpenGLMorphic' 1 2!
!requires: 'OpenGL' 1 3 nil!
SystemOrganization addCategory: #OpenGLMorphic!


!classDefinition: #OpenGLMorph category: #OpenGLMorphic!
BoxedMorph subclass: #OpenGLMorph
	instanceVariableNames: 'ogl'
	classVariableNames: ''
	poolDictionaries: 'OpenGLConstants GLExtConstants'
	category: 'OpenGLMorphic'!
!classDefinition: 'OpenGLMorph class' category: #OpenGLMorphic!
OpenGLMorph class
	instanceVariableNames: ''!

!classDefinition: #OpenGLRectangleMorphExample category: #OpenGLMorphic!
OpenGLMorph subclass: #OpenGLRectangleMorphExample
	instanceVariableNames: 'font status'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'OpenGLMorphic'!
!classDefinition: 'OpenGLRectangleMorphExample class' category: #OpenGLMorphic!
OpenGLRectangleMorphExample class
	instanceVariableNames: ''!

!classDefinition: #Simple3DEditor category: #OpenGLMorphic!
OpenGLMorph subclass: #Simple3DEditor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'OpenGLMorphic'!
!classDefinition: 'Simple3DEditor class' category: #OpenGLMorphic!
Simple3DEditor class
	instanceVariableNames: ''!


!OpenGLMorph commentStamp: '<historical>' prior: 0!
Morph that renders on OpenGL!

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:43:46'!
aboutToBeGrabbedBy: aHand
	ogl destroy. ogl _ nil.
	^ super aboutToBeGrabbedBy: aHand.
	! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:03:28'!
delete
	super delete.
	"Should we release OpenGL handle here?"
	ogl destroy.
	ogl _ nil! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:05:57'!
drawOn: aCanvas
	
	(owner isKindOf: HandMorph) ifTrue: [^self]. "We are being grabbed"
		
	self oglHandle.
	ogl beginFrame.
	
	self render.
	
	ogl endFrame.
	ogl swapBuffers.! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:17:13'!
initializeOGL
	^ OpenGL newIn: self morphBoundsInWorld ! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 18:50:27'!
oglHandle
	^ ogl ifNil: [ogl _ self initializeOGL]! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:04:20'!
render
	self subclassResponsibility ! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:04:53'!
replaceOGLHandle
	ogl ifNotNil: [ogl destroy. ogl _ nil]! !

!OpenGLMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:04:46'!
someSubmorphPositionOrExtentChanged
	self replaceOGLHandle.! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 18:32:08'!
font
	^ font ifNil:[font _  FontFamily defaultFamilyAndPointSize]! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:00:34'!
initialize
	super initialize.
	self morphExtent: 400@400.
	status _ #running! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:06:15'!
render
		
		ogl glDisable: GLDepthTest.	"for the simple example only"
		ogl glDisable: GLLighting.		"for the simple example only"

		ogl glClearColor: 1.0 with: 1.0 with: 1.0 with: 1.0.
		ogl glClear: GLColorBufferBit.

		ogl glRotatef: 5.0 with: 0.0 with: 0.0 with: 1.0.
		ogl glColor3f: 1.0 with: 0.0 with: 0.0.

		ogl glBegin: GLPolygon.
			ogl glVertex2f: -0.7 with: -0.7.
			ogl glVertex2f:  0.7 with: -0.7.
			ogl glVertex2f:  0.7 with:  0.7.
			ogl glVertex2f: -0.7 with:  0.7.
		ogl glEnd.

	"--- here is the 2d overlay setup ---"

		ogl glMatrixMode: GLProjection.
		ogl glPushMatrix.
		ogl glLoadIdentity.
		ogl glMatrixMode: GLModelview.
		ogl glPushMatrix.
		ogl glLoadIdentity.
		ogl glTranslated: -1 with: 1 with: 0.0.
		ogl glScaled: (2.0 / self morphWidth) with: (-2.0 / self morphHeight) with: 1.0.
		ogl glDisable: GLDepthTest.
		ogl glEnable: GLBlend.
		ogl glBlendFunc: GLOne with: GLOneMinusSrcAlpha.

		ogl glDisable: GLBlend.
		ogl glMatrixMode: GLModelview.
		ogl glPopMatrix.
		ogl glMatrixMode: GLProjection.
		ogl glPopMatrix.
		ogl glMatrixMode: GLModelview.! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 18:54:44'!
step
	self redrawNeeded! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 19:48:15'!
stepTime
	^ 1! !

!OpenGLRectangleMorphExample methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 18:30:52'!
wantsSteps
	^ true! !

!Simple3DEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:00:43'!
handlesMouseDown: aMouseButtonEvent

	^ true! !

!Simple3DEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:01:42'!
mouseButton1Down: aMouseButtonEvent localPosition: localEventPosition
	Transcript show: 'Mouse button 1 down'; newLine.! !

!Simple3DEditor methodsFor: 'as yet unclassified' stamp: 'MM 6/19/2020 21:06:32'!
render
		ogl glDisable: GLDepthTest.	"for the simple example only"
		ogl glDisable: GLLighting.		"for the simple example only"

		ogl glClearColor: 1.0 with: 1.0 with: 1.0 with: 1.0.
		ogl glClear: GLColorBufferBit.

		ogl glBegin: GLPolygon.
			ogl glVertex2f: -0.7 with: -0.7.
			ogl glVertex2f:  0.7 with: -0.7.
			ogl glVertex2f:  0.7 with:  0.7.
			ogl glVertex2f: -0.7 with:  0.7.
		ogl glEnd.

	"--- here is the 2d overlay setup ---"

		ogl glMatrixMode: GLProjection.
		ogl glPushMatrix.
		ogl glLoadIdentity.
		ogl glMatrixMode: GLModelview.
		ogl glPushMatrix.
		ogl glLoadIdentity.
		ogl glTranslated: -1 with: 1 with: 0.0.
		ogl glScaled: (2.0 / self morphWidth) with: (-2.0 / self morphHeight) with: 1.0.
		ogl glDisable: GLDepthTest.
		ogl glEnable: GLBlend.
		ogl glBlendFunc: GLOne with: GLOneMinusSrcAlpha.

		ogl glDisable: GLBlend.
		ogl glMatrixMode: GLModelview.
		ogl glPopMatrix.
		ogl glMatrixMode: GLProjection.
		ogl glPopMatrix.
		ogl glMatrixMode: GLModelview.! !
