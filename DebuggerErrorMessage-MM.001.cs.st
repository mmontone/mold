'From Cuis 6.0 [latest update: #5832] on 12 June 2023 at 12:12:57 pm'!

!DebuggerWindow class methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 21:17:57'!
open: model label: aString

	|  window |
	window := self new.
	aString ifNotNil: [ window setLabel: aString ].
	window
		model: model;
		buildMorphicWindow.
	window openInWorld.
	^window! !


!DebuggerWindow methodsFor: 'GUI building' stamp: 'MM 6/8/2023 21:21:55'!
buildMorphicWindow
	"Open a full morphic debugger with the given label"

	| bottomMorph |

	self receiverInspector doubleClickSelector: #inspectSelection.
	self contextVariableInspector doubleClickSelector: #inspectSelection.

	bottomMorph := LayoutMorph newRow.
	bottomMorph
		addMorph: self receiverInspector proportionalWidth: 0.2;
		addAdjusterAndMorph: self receiverInspectorText proportionalWidth: 0.3;
		addAdjusterAndMorph: self contextVariableInspector proportionalWidth: 0.2;
		addAdjusterAndMorph: self contextVariableInspectorText proportionalWidth: 0.3.
	self layoutMorph
		addMorph: ((TextModelMorph withText: self label asText)
					disableEditing; yourself) proportionalHeight: 0.1;
		addAdjusterAndMorph: self stackList proportionalHeight: 0.15;
		addAdjusterAndMorph: self buildLowerPanes proportionalHeight: 0.55;
		addAdjusterAndMorph: bottomMorph proportionalHeight: 0.2! !

