'From Cuis6.3 [latest update: #6304] on 28 March 2024 at 9:38:21 pm'!
'Description Presentation Based User Interface.

Author: Mariano Montone <marianomontone@gmail.com>

TODO: Implement presentations as a TextAttribute. Instead of using Morphs to display the presentations, we can print the evaluation results with a PresentationTextAttribute attached, that contains presentation information. Then, context menus should act upon that. This is potentially easier and more compatible with how Smalltalk evaluation works at the moment.'!
!provides: 'Presentations' 1 53!
!requires: 'StandardCommandsSet' 1 1 nil!
SystemOrganization addCategory: #Presentations!
SystemOrganization addCategory: #'Presentations-Workspace'!


!classDefinition: #PresentationsWorkspace category: #'Presentations-Workspace'!
Workspace subclass: #PresentationsWorkspace
	instanceVariableNames: 'charIndex'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations-Workspace'!
!classDefinition: 'PresentationsWorkspace class' category: #'Presentations-Workspace'!
PresentationsWorkspace class
	instanceVariableNames: ''!

!classDefinition: #PresentationsWriteStream category: #Presentations!
WriteStream subclass: #PresentationsWriteStream
	instanceVariableNames: 'presentationBuilder'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsWriteStream class' category: #Presentations!
PresentationsWriteStream class
	instanceVariableNames: ''!

!classDefinition: #PresentationsSmalltalkEditor category: #'Presentations-Workspace'!
SmalltalkEditor subclass: #PresentationsSmalltalkEditor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations-Workspace'!
!classDefinition: 'PresentationsSmalltalkEditor class' category: #'Presentations-Workspace'!
PresentationsSmalltalkEditor class
	instanceVariableNames: ''!

!classDefinition: #PresentationTextAction category: #Presentations!
TextAction subclass: #PresentationTextAction
	instanceVariableNames: 'model'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationTextAction class' category: #Presentations!
PresentationTextAction class
	instanceVariableNames: ''!

!classDefinition: #PresentationTextAnchor category: #Presentations!
TextAnchor subclass: #PresentationTextAnchor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationTextAnchor class' category: #Presentations!
PresentationTextAnchor class
	instanceVariableNames: ''!

!classDefinition: #PresentationsHandMorph category: #Presentations!
HandMorph subclass: #PresentationsHandMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsHandMorph class' category: #Presentations!
PresentationsHandMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsWorldMorph category: #Presentations!
PasteUpMorph subclass: #PresentationsWorldMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsWorldMorph class' category: #Presentations!
PresentationsWorldMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsCommandBar category: #Presentations!
LayoutMorph subclass: #PresentationsCommandBar
	instanceVariableNames: 'currentCommand mode'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsCommandBar class' category: #Presentations!
PresentationsCommandBar class
	instanceVariableNames: ''!

!classDefinition: #PresentationsInnerListMorph category: #Presentations!
InnerListMorph subclass: #PresentationsInnerListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsInnerListMorph class' category: #Presentations!
PresentationsInnerListMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsInnerTextMorph category: #Presentations!
InnerTextMorph subclass: #PresentationsInnerTextMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsInnerTextMorph class' category: #Presentations!
PresentationsInnerTextMorph class
	instanceVariableNames: ''!

!classDefinition: #NamePresentationMorph category: #Presentations!
LabelMorph subclass: #NamePresentationMorph
	instanceVariableNames: 'model printer hovered binding highlightMorphs'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'NamePresentationMorph class' category: #Presentations!
NamePresentationMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsPluggableListMorph category: #Presentations!
PluggableListMorph subclass: #PresentationsPluggableListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsPluggableListMorph class' category: #Presentations!
PresentationsPluggableListMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsTextModelMorph category: #Presentations!
TextModelMorph subclass: #PresentationsTextModelMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsTextModelMorph class' category: #Presentations!
PresentationsTextModelMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationsWorkspaceWindow category: #'Presentations-Workspace'!
WorkspaceWindow subclass: #PresentationsWorkspaceWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations-Workspace'!
!classDefinition: 'PresentationsWorkspaceWindow class' category: #'Presentations-Workspace'!
PresentationsWorkspaceWindow class
	instanceVariableNames: ''!

!classDefinition: #PresentationWrapperMorph category: #Presentations!
Morph subclass: #PresentationWrapperMorph
	instanceVariableNames: 'model currentMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationWrapperMorph class' category: #Presentations!
PresentationWrapperMorph class
	instanceVariableNames: ''!

!classDefinition: #PresentationAspect category: #Presentations!
Object subclass: #PresentationAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationAspect class' category: #Presentations!
PresentationAspect class
	instanceVariableNames: ''!

!classDefinition: #PresentationBuilder category: #Presentations!
Object subclass: #PresentationBuilder
	instanceVariableNames: 'builders precedenceList'
	classVariableNames: 'Instance'
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationBuilder class' category: #Presentations!
PresentationBuilder class
	instanceVariableNames: ''!

!classDefinition: #DefaultPresentationBuilder category: #Presentations!
PresentationBuilder subclass: #DefaultPresentationBuilder
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'DefaultPresentationBuilder class' category: #Presentations!
DefaultPresentationBuilder class
	instanceVariableNames: ''!

!classDefinition: #PresentationCommand category: #Presentations!
Object subclass: #PresentationCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationCommand class' category: #Presentations!
PresentationCommand class
	instanceVariableNames: ''!

!classDefinition: #PluggablePresentationCommand category: #Presentations!
PresentationCommand subclass: #PluggablePresentationCommand
	instanceVariableNames: 'name description arguments action category'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PluggablePresentationCommand class' category: #Presentations!
PluggablePresentationCommand class
	instanceVariableNames: ''!

!classDefinition: #PresentationsInteractionState category: #Presentations!
Object subclass: #PresentationsInteractionState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsInteractionState class' category: #Presentations!
PresentationsInteractionState class
	instanceVariableNames: ''!

!classDefinition: #PickPresentationArgumentState category: #Presentations!
PresentationsInteractionState subclass: #PickPresentationArgumentState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PickPresentationArgumentState class' category: #Presentations!
PickPresentationArgumentState class
	instanceVariableNames: ''!

!classDefinition: #RunPresentationCommandState category: #Presentations!
PresentationsInteractionState subclass: #RunPresentationCommandState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'RunPresentationCommandState class' category: #Presentations!
RunPresentationCommandState class
	instanceVariableNames: ''!

!classDefinition: #SelectPresentationCommandState category: #Presentations!
PresentationsInteractionState subclass: #SelectPresentationCommandState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'SelectPresentationCommandState class' category: #Presentations!
SelectPresentationCommandState class
	instanceVariableNames: ''!

!classDefinition: #PresentationsWorldState category: #Presentations!
ProtoObject subclass: #PresentationsWorldState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations'!
!classDefinition: 'PresentationsWorldState class' category: #Presentations!
PresentationsWorldState class
	instanceVariableNames: ''!

!classDefinition: #PresentationsWorkspaceCommandSet category: #'Presentations-Workspace'!
ProtoObject subclass: #PresentationsWorkspaceCommandSet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Presentations-Workspace'!
!classDefinition: 'PresentationsWorkspaceCommandSet class' category: #'Presentations-Workspace'!
PresentationsWorkspaceCommandSet class
	instanceVariableNames: ''!


!PresentationsWorkspace commentStamp: '<historical>' prior: 0!
PresentationsWorkspace openLabel: 'Presentations Workspace'!

!PresentationsWriteStream commentStamp: 'MM 2/1/2021 23:39:03' prior: 0!
|s|

s _ PresentationsWriteStream new.

s nextPutAll: 'Hello'.
s nextPut: 22.

s contents edit!

!PresentationsSmalltalkEditor commentStamp: '<historical>' prior: 0!
Smalltalk editor with Presentations awareness.!

!PresentationsHandMorph commentStamp: '<historical>' prior: 0!
I'm in charge of dispatching Presentations aware events.!

!PresentationsWorldMorph commentStamp: '<historical>' prior: 0!
The main world morph that implements support for a Presentation Based User Inteface.!

!PresentationsCommandBar commentStamp: '<historical>' prior: 0!
Command and status bar for Presentations mode.!

!PresentationsPluggableListMorph commentStamp: 'MM 12/16/2020 18:36:06' prior: 0!
A PluggableListMorph where its elements are displayed as presentations.!

!PresentationWrapperMorph commentStamp: '<historical>' prior: 0!
I'm a wrapper morph for an object presentations.

I'm in charge of:
- Draw presentation activation.
- Show presentation context menu.
- Presentations context awareness.
- Morph presentation switching.
- Events interception and redispatch for the embedded morph.!

!PresentationAspect commentStamp: '<historical>' prior: 0!
A PresentationAspect is an aspect of a model object.

I'm used to inform the set of "services" a model object provides.

It has a name, a description, the type of thing provided, and a selector with which to obtain the data from the model object.!

!PresentationBuilder commentStamp: 'MM 12/14/2020 09:55:47' prior: 0!
DefaultPresentationBuilder instance
	presentationFor: Object new.
	
DefaultPresentationBuilder instance
	presentationFor: Theme current closeIcon.!

!PresentationCommand commentStamp: '<historical>' prior: 0!
I represent a command in the presentation based user interface.

A command supports the goal of separating an application's user interface from its underlying functionality. In particular, commands separate the notion of an operation from the details of how the operation is invoked by the user.

A command can be invoked in different ways. It is independent of the type of user interaction.

Can be invoked from a mouse action, or a command bar, etc.

It has a name, a description, a set of typed arguments, and implements a #value method for its execution.!

!PresentationsWorldState commentStamp: '<historical>' prior: 0!
The Presentations world state.

Start with PresentationsWorldState startPresentationsWorld.!

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 19:17:07'!
bindingTo: anObject

	"Returns the name of the binding that points to anObject"
	
	"(bindings keyForIdentity: anObject) ifNotNil: [:bindingName |
		^ bindingName].
	^ nil"
	
	^ (bindings associations detect: [:association | association value == anObject] 
		ifNone: [^ nil]) key! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 11:45:27'!
buildPresentationFor: anObject
	^ self presentationBuilder presentationFor: anObject! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 10:30:56'!
editorClass
	^ PresentationsSmalltalkEditor ! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 21:35:44'!
getBindingFor: anObject
	"^ self nameForObject: anObject"
	charIndex := charIndex + 1.
	^ (Character codePoint: charIndex - 1) asString ! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 13:13:20'!
getPresentationTextFor: anObject
	
	|bindingName presentation|
	
	"Create a binding to presentation first"
	bindingName _ self getBindingFor: anObject.
	bindings at: bindingName put: anObject.
	
	presentation _ self buildPresentationFor: anObject.
	"attach binding to the presentation too"
	presentation binding: bindingName.
	
	^  Text string: bindingName attribute: (TextAnchor new anchoredFormOrMorph: presentation)! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 13:01:41'!
initialize
	super initialize.
	charIndex _ 131.! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 19:17:53'!
insertReferenceToPresentation: anObject at: aPosition

	| bindingName presentationMorph |
	
	bindingName _ (self bindingTo: anObject) ifNil: [^ self].
	
	presentationMorph _ DefaultPresentationBuilder instance presentationFor: anObject.
	self actualContents: self actualContents, (Text string: bindingName attribute: (TextAnchor new anchoredFormOrMorph: presentationMorph)).! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:21:32'!
openLabel: aString 
	"Create a standard system view of the model, me, and open it."
	| win |
	win _ PresentationsWorkspaceWindow editText: self label: aString wrap: true.
	self changed: #actualContents.
	^win! !

!PresentationsWorkspace methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 11:45:40'!
presentationBuilder
	^ DefaultPresentationBuilder instance! !

!PresentationsWriteStream methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 23:40:49'!
nextPut: anObject
	
	|presentation text|
	
	anObject isCharacter ifTrue: [^super nextPut: anObject].
	
	presentation _ self presentationBuilder presentationFor: anObject.
	text _ Text string: '*' attribute: (TextAnchor new anchoredFormOrMorph: presentation).
	collection _ collection, text.
	position _ position + 1.
	^ self! !

!PresentationsWriteStream methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 23:37:16'!
presentationBuilder
	^ presentationBuilder ifNil: [presentationBuilder _ DefaultPresentationBuilder instance]! !

!PresentationsWriteStream class methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 23:32:20'!
new
	^ self with: Text new! !

!PresentationsSmalltalkEditor methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:30:17'!
getMenu
	^ self halt! !

!PresentationsSmalltalkEditor methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:41:47'!
mouseButton2Down: aMouseButtonEvent localPosition: localEventPosition
	"Handle right click on text"
	"Normal TextEditors ignore right click on text, and don't give a chance to text attributes to act on it."
	
	|b|
	
	b _ textComposition characterBlockAtPoint: localEventPosition.

	(textComposition rightClickAt: localEventPosition) ifTrue: [
		markBlock _ b.
		pointBlock _ b.
		aMouseButtonEvent hand releaseKeyboardFocus: self.
		^ self ].
	! !

!PresentationsSmalltalkEditor methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:38:34'!
printIt
	"Treat the current text selection as an expression; evaluate it. Insert the 
	description of the result of evaluation after the selection and then make 
	this description the new text selection."
	
	self printItMorphed! !

!PresentationsSmalltalkEditor methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 11:41:35'!
printItMorphed
	"Treat the current text selection as an expression; evaluate it. Insert the 
	description of the result of evaluation after the selection and then make 
	this description the new text selection."
	
	|text|
	
	self
		evaluateSelectionAndDo: [ :result |
			
			text _ model getPresentationTextFor: result.
				
			self afterSelectionInsertAndSelect: ((' ', text, ' ') initialFontFrom: emphasisHere)]
		ifFail: [ morph flash ]
		profiled: false! !

!PresentationsSmalltalkEditor methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:37:59'!
printItStyled
	"Treat the current text selection as an expression; evaluate it. Insert the 
	description of the result of evaluation after the selection and then make 
	this description the new text selection."
	
	|text|
	
	self
		evaluateSelectionAndDo: [ :result |
			
			text _ Text string: result printText attributes: {PresentationTextAction model: result}.
				
			self afterSelectionInsertAndSelect: ((' ', text, ' ') initialFontFrom: emphasisHere)]
		ifFail: [ morph flash ]
		profiled: false! !

!PresentationTextAction methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:10:27'!
actOnClickFor: anObject
	"Note: evalString gets evaluated IN THE CONTEXT OF anObject
	 -- meaning that self and all instVars are accessible"
	"Compiler evaluate: evalString for: anObject logged: false."
	model inspect.
	^ true ! !

!PresentationTextAction methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 14:27:17'!
actOnRightClickFor: anObject
	! !

!PresentationTextAction methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:42:35'!
actOnRightClickFor: aModel in: aTextComposition at: clickPoint editor: editor
	self openMenu.
	^ true! !

!PresentationTextAction methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 14:29:10'!
mayActOnRightClick
	^ true! !

!PresentationTextAction methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:54:57'!
openMenu

	|menu|
	
	menu _ MenuMorph new
		defaultTarget: model.
	
	model presentationCommands do: [:cmd |
		"TODO: consider using a command icon too. And a keyboard shortcut?"
		(menu add: cmd name
		target: cmd
		action: #value:
		argumentList: {model})
			setBalloonText: cmd description].
	
	^ menu popUpInWorld ! !

!PresentationTextAction methodsFor: 'accessing' stamp: 'MM 2/8/2020 14:32:51'!
model
	"Answer the value of model"

	^ model! !

!PresentationTextAction methodsFor: 'accessing' stamp: 'MM 2/8/2020 14:32:51'!
model: anObject
	"Set the value of model"

	model _ anObject! !

!PresentationTextAction class methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 14:32:40'!
model: anObject
	^ self new model: anObject! !

!PresentationTextAnchor methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 12:39:32'!
mayBeExtended
	^ true! !

!PresentationsHandMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2020 22:52:20'!
startEventDispatch: aMorphicEvent
	"Transcript show: 'Event dispatch'; newLine."
	^ super startEventDispatch: aMorphicEvent! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 15:26:09'!
menu: titleString
	"Create a menu with the given title, ready for filling"

	| menu |
	(menu _ MenuMorph entitled: titleString) 
		defaultTarget: self; 
		addStayUpIcons.
	^ menu! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2020 22:50:06'!
newWorld
	"
[
	UISupervisor stopUIProcess.
	UISupervisor spawnNewMorphicProcessFor: PresentationsWorldMorph newWorld
] fork.
	"
	| w ws |
	w _ self new.
	ws _ PresentationsWorldState new.
	w worldState: ws.
	w morphPosition: `0@0` extent: Display extent.
	ws setCanvas: Display getCanvas.
	w borderWidth: 0.
	ws handsDo: [ :h |
		h privateOwner: w ].
	^w! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 15:24:17'!
openPresentationsMenu
	"Build and show the preferences menu for the world."

	self presentationsWorldMenu popUpInWorld: self runningWorld! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 15:28:06'!
openWorkspace
	PresentationsWorkspace openLabel: 'Presentations Workspace'! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/17/2020 09:11:46'!
presentationsWorldMenu
	"Build the Presentations menu for the world."

	^ (self menu: 'Presentations...')
		addItemsFromDictionaries: `{
			{
				#label 			-> 		'Workspace'.
				#selector 		-> 		#openWorkspace.
				#icon 			-> 		#terminalIcon.
				#balloonText 	-> 		'Open a workspace with presentations support'
			} asDictionary.
		}`! !

!PresentationsWorldMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 15:23:19'!
worldMenuOptions

	^`{{
		#itemGroup 		-> 		10.
		#itemOrder 		-> 		50.
		#label 			-> 		'Presentations...'.
		#object             ->    PresentationsWorldMorph .
		#selector 		-> 		#openPresentationsMenu.
		#icon 			-> 		#worldIcon.
		#balloonText 	-> 		'Presentations oriented user interface'.
	} asDictionary.
	}`! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 13:09:55'!
acceptDroppingMorph: aMorph event: evt
	"|charBlock|
	
	charBlock _ textComposition characterBlockAtPoint: evt eventPosition.
	editor insertAndSelect: (Text withForm: aMorph) at: charBlock stringIndex"
	"Set cursor position"
	"editor mouseButton1Down: MouseButtonEvent new localPosition: evt eventPosition. "
	editor addString: (Text string: aMorph binding attribute: (TextAnchor new anchoredFormOrMorph: aMorph))
	
	! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 21:46:05'!
allowsMorphDrop
	^ true! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 20:48:10'!
allowsSubmorphDrag
	^ true! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:34:03'!
mouseButton2Activity
	self owner mouseButton2Activity! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:40:08'!
mouseButton2Down: aMouseButtonEvent localPosition: localEventPosition
	self handleInteraction: [ editor mouseButton2Down: aMouseButtonEvent localPosition: localEventPosition ].! !

!PresentationsInnerTextMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/16/2020 18:28:50'!
wantsDroppedMorph: aMorph event: evt
	"Return true if the receiver wishes to accept the given morph, which is being dropped by a hand in response to the given event. Note that for a successful drop operation both parties need to agree. The symmetric check is done automatically via aMorph wantsToBeDroppedInto: self."
	^ aMorph isKindOf: NamePresentationMorph ! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/15/2020 13:10:18'!
binding
	"Answer the value of binding"

	^ binding! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/15/2020 13:10:18'!
binding: anObject
	"Set the value of binding"

	binding _ anObject! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/13/2020 21:24:38'!
contents
	^ printer ifNil: [model asString]
		ifNotNil: [printer isSymbol 
				ifTrue: [model perform: printer]
				ifFalse: [printer value: model]]! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 2/1/2021 19:12:25'!
highlightMorphs
	"Answer the value of highlightMorphs"

	^ highlightMorphs! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 2/1/2021 19:12:25'!
highlightMorphs: anObject
	"Set the value of highlightMorphs"

	highlightMorphs _ anObject! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/15/2020 13:10:18'!
hovered
	"Answer the value of hovered"

	^ hovered! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/15/2020 13:10:18'!
hovered: anObject
	"Set the value of hovered"

	hovered _ anObject! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/13/2020 20:33:00'!
model
	"Answer the value of model"

	^ model! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/13/2020 20:33:00'!
model: anObject
	"Set the value of model"

	model _ anObject! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/13/2020 21:23:57'!
printer
	"Answer the value of printer"

	^ printer! !

!NamePresentationMorph methodsFor: 'accessing' stamp: 'MM 12/13/2020 21:23:57'!
printer: anObject
	"Set the value of printer"

	printer _ anObject! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/16/2020 18:28:50'!
aboutToBeGrabbedBy: aHand
	"The receiver is being grabbed by a hand.
	Perform necessary adjustments (if any) and return the actual morph
	that should be added to the hand.
	Answer nil to reject the drag."

	^ "LabelMorph contents: model printString"
	  (NamePresentationMorph on: model)
		binding: self binding;
		yourself! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 10:33:53'!
acceptDroppingMorph: aMorph event: evt
	"This message is sent when a morph is dropped onto a morph that has agreed to accept the dropped morph by responding 'true' to the wantsDroppedMorph:event: message. This default implementation just adds the given morph to the receiver."
	
	"This is not working here. Seems it has to do with Morphic events/rendering workflow and the menu doesn't popup. How to fix??"
	"(CommandsSet commandsMenuFor: {aMorph model. model}) popUpInWorld: self runningWorld.
	aMorph delete."
	
	"I'm trying to run later using an alarm. Hack."
	self addAlarm: #droppedMorph: withArguments: {aMorph}  after: 100. ! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 10:13:01'!
allowsMorphDrop
	^ true! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 10:28:02'!
drawOn: aCanvas
	aCanvas
		drawString: (self contents ifNil: [ '' ])
		at: self morphTopLeft 
		font: self fontToUse
		color: self color.
	hovered ifTrue: [
		aCanvas frameRectangle: self morphLocalBounds borderWidth: 1 color: Color blue	]! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/15/2020 10:33:23'!
droppedMorph: aMorph
	(CommandsSet commandsMenuFor: {aMorph model. model}) popUpInWorld: self runningWorld.
	aMorph delete.! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:21:20'!
handlesMouseDown: aMouseButtonEvent
	"Do I want to receive mouseDown events (mouseDown:, mouseMove:, mouseUp:)?"
	^true! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:21:35'!
handlesMouseOver: evt
	"Do I want to receive mouseEnter: and mouseLeave: when the button is up and the hand is empty?" 
	^true! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 19:12:54'!
initialize: anObject
	model _ anObject.
	hovered _ false.
	highlightMorphs _ true.
	self fitContents.! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 10:29:14'!
measureContents
	| f |
	f _ self fontToUse.
	^((f widthOfString: self contents) max: 3)  @ (f lineSpacing - 2)! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 22:00:13'!
mouseButton1Down: aMouseButtonEvent localPosition: localEventPosition

	aMouseButtonEvent hand 
		waitForClicksOrDragOrSimulatedMouseButton2: self
		event: aMouseButtonEvent
		clkSel: #click:localPosition:
		clkNHalf: nil
		dblClkSel: #doubleClick:localPosition:
		dblClkNHalfSel: nil
		tripleClkSel: nil
		dragSel: #dragEvent:localPosition:! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 20:41:05'!
mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition
	self openMenu.! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:22:35'!
mouseButton2Down: aMouseButtonEvent localPosition: localEventPosition
	self openMenu! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 19:21:16'!
mouseEnter: aMouseMoveEvent
	hovered _ true.
	highlightMorphs ifTrue: [
		self runningWorld allMorphsDo: [:morph |
			((morph == self) not and: [		morph presentedObject == self presentedObject]) 
				ifTrue: [morph highlighted: true]]].
	self redrawNeeded ! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 19:21:43'!
mouseLeave: aMouseMoveEvent
	hovered _ false.
	highlightMorphs ifTrue: [
		self runningWorld allMorphsDo: [:morph |
			((morph == self) not and: [morph presentedObject == self presentedObject]) 
				ifTrue: [morph highlighted: false]]].
	self redrawNeeded ! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 13:55:18'!
openMenu
	(CommandsSet commandsMenuFor: {model}) popUpInWorld.! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 13:56:00'!
presentedObject
	^ model! !

!NamePresentationMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/16/2020 18:28:50'!
wantsDroppedMorph: aMorph event: evt
	"Return true if the receiver wishes to accept the given morph, which is being dropped by a hand in response to the given event. Note that for a successful drop operation both parties need to agree. The symmetric check is done automatically via aMorph wantsToBeDroppedInto: self."
	^ aMorph isKindOf: NamePresentationMorph ! !

!NamePresentationMorph class methodsFor: 'as yet unclassified' stamp: 'MM 12/13/2020 21:36:51'!
on: anObject
	^ self new initialize: anObject! !

!PresentationsTextModelMorph methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 20:46:13'!
allowsSubmorphDrag
	"Answer whether our morphs can just be grabbed with the hand, instead of requiring the use of the halo. Redefined to answer true."

	^ true! !

!PresentationsTextModelMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:32:20'!
innerMorphClass
	^PresentationsInnerTextMorph! !

!PresentationsTextModelMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:17:50'!
mouseButton2Activity
	^ self halt.! !

!PresentationsTextModelMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:37:52'!
mouseButton2Down: aMouseButtonEvent localPosition: localEventPosition

	| eventPositionLocalToScroller |
	scroller inspect.
	eventPositionLocalToScroller _ localEventPosition - scroller morphPosition.
	scroller mouseButton1Down: aMouseButtonEvent localPosition: eventPositionLocalToScroller.
	! !

!PresentationsWorkspaceWindow class methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:23:00'!
editText: aTextModel label: aLabelString wrap: aBoolean
	| window |
	window _ self new model: aTextModel.
	window setLabel: aLabelString.
	window layoutMorph
		addMorph: ((PresentationsTextModelMorph withModel: aTextModel)
			wrapFlag: aBoolean)
		proportionalHeight: 1.
	^ window openInWorld! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:44:39'!
buildPrecedenceList
	
	|counts|
	
	counts _ Dictionary new.
		
	builders do: [:builder |
		counts at: builder put: 0.
		builders do: [:builder2 |
			(builder isKindOf:builder2) ifTrue: [
				counts at: builder put: (counts at: builder) + 1]	]].
	^ precedenceList _ counts keysSortedSafely.! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:49:18'!
builders
	^ builders! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:43:27'!
collectBuilders
	builders _ OrderedCollection new.
	self class selectors do: [:selector |
		(selector asString beginsWith: 'build') ifTrue: [
			builders add: (self perform: selector) asDictionary]].
	^ builders! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:46:56'!
initializeBuilders
	self collectBuilders.
	self buildPrecedenceList.! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:49:29'!
precedenceList
	^ precedenceList! !

!PresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:53:09'!
presentationFor: anObject
	precedenceList do: [:entry |
		(anObject isKindOf: (entry at: #class)) ifTrue: [
			^ (entry at: #builder) value: anObject	]]! !

!PresentationBuilder class methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:49:05'!
initialize
	Instance _ nil! !

!PresentationBuilder class methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:48:10'!
instance
	^ Instance ifNil: [Instance _ self new initializeBuilders; yourself]! !

!DefaultPresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/14/2020 09:06:45'!
buildForm
	^ {#class -> Form. #builder -> [:form | ImageMorph new image: form; yourself]}! !

!DefaultPresentationBuilder methodsFor: 'as yet unclassified' stamp: 'MM 12/16/2020 18:28:50'!
buildObject
	^ {#class -> Object. 
	    #builder -> [:obj | NamePresentationMorph on: obj] }! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:49:20'!
arguments
	^ #()! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/10/2020 10:34:38'!
canUndo
	^ false! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/10/2020 09:33:48'!
category
	^ self subclassResponsibility ! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:48:56'!
description
	^ self subclassResponsibility! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/10/2020 15:19:36'!
isRequired
	^ true! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:48:48'!
name
	^ self subclassResponsibility! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/10/2020 10:34:29'!
undo
	^self subclassResponsibility ! !

!PresentationCommand methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2020 15:49:04'!
value
	^ self subclassResponsibility ! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:25'!
action
	"Answer the value of action"

	^ action! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:25'!
action: anObject
	"Set the value of action"

	action _ anObject! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:25'!
arguments: anObject
	"Set the value of arguments"

	arguments _ anObject! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/10/2020 09:33:56'!
category: anObject
	"Set the value of category"

	category _ anObject! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:51:34'!
description
	^ description! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:25'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:35'!
name
	^ name! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:50:25'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!PluggablePresentationCommand methodsFor: 'accessing' stamp: 'MM 2/8/2020 15:51:18'!
value: anObject
	^ action isSymbol 
		ifTrue: [anObject perform: action]
		ifFalse: [action value: anObject]! !

!PresentationsWorldState methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2020 22:34:45'!
initialize

	activeHand _ PresentationsHandMorph new.
	hands _ { activeHand }.
	damageRecorder _ DamageRecorder new.
	stepList _ Heap sortBlock: self stepListSortBlock.
	alarms _ Heap sortBlock: self alarmSortBlock.
	lastAlarmTime _ 0.
	drawingFailingMorphs _ WeakIdentitySet new.
	pause _ 20.
	lastCycleTime _ Time localMillisecondClock.
	lastCycleHadAnyEvent _ false! !

!PresentationsWorldState class methodsFor: 'as yet unclassified' stamp: 'MM 2/9/2020 17:25:22'!
startPresentationsWorld
	"PresentationsWorldState startPresentationsWorld"
	[UISupervisor stopUIProcess.
	UISupervisor spawnNewMorphicProcessFor: PresentationsWorldMorph newWorld] fork.! !

!PresentationsWorldState class methodsFor: 'as yet unclassified' stamp: 'MM 2/9/2020 17:25:29'!
stopPresentationsWorld
	"PresentationsWorldState stopPresentationsWorld"
	[UISupervisor stopUIProcess.
	UISupervisor spawnNewMorphicProcessFor: PasteUpMorph newWorld] fork.! !

!PresentationsWorkspaceCommandSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:30:35'!
commandsSetName
	^ 'Workspace commands'! !

!Object methodsFor: '*Presentations' stamp: 'MM 2/8/2020 14:37:09'!
presentationAspects
	^ {}! !

!Object methodsFor: '*Presentations' stamp: 'MM 2/8/2020 15:59:50'!
presentationCommands
	^ {PluggablePresentationCommand new
			name: 'inspect';
			description: 'Inspect object';
			action: #inspect.
	   PluggablePresentationCommand new
			name: 'browse class';
			description: 'Browse the class of the object';
			action: [:obj | BrowserWindow fullOnClass: obj class]}! !

!SystemDictionary methodsFor: '*Presentations' stamp: 'MM 2/10/2020 09:26:57'!
presentationCommands
	"Returns global presentations commands"
	^ {}! !

!TextAttribute methodsFor: '*Presentations' stamp: 'MM 2/8/2020 12:49:46'!
mayActOnRightClick
	^ false! !

!Morph methodsFor: '*Presentations' stamp: 'MM 3/28/2024 21:37:23'!
presentedObject

	^ nil! !

!PluggableMorph methodsFor: '*Presentations' stamp: 'MM 12/13/2020 19:52:55'!
presentedObject
	^ model! !

!TextComposition methodsFor: '*Presentations' stamp: 'MM 2/8/2020 12:48:24'!
rightClickAt: clickPoint
	"Give sensitive text a chance to fire.  Display flash: (100@100 extent: 100@100)."
	| startBlock action target range boxes box t |
	action _ false.
	startBlock _ self characterBlockAtPoint: clickPoint.
	t _ model actualContents.
	(t attributesAt: startBlock stringIndex) do: [ :att | 
		att mayActOnRightClick ifTrue: [
				(target _ model) ifNil: [ target _ editor morph].
				range _ t rangeOf: att startingAt: startBlock stringIndex.
				boxes _ self selectionRectsFrom: (self characterBlockForIndex: range first) 
							to: (self characterBlockForIndex: range last+1).
				box _ boxes detect: [ :each | each containsPoint: clickPoint] ifNone: nil.
				box ifNotNil: [
					box _ editor morph morphBoundsInWorld.
					editor morph allOwnersDo: [ :m | box _ box intersect: (m morphBoundsInWorld) ].
					Utilities
						awaitMouseUpIn: box
						repeating: nil
						ifSucceed: [(att actOnRightClickFor: target in: self at: clickPoint editor: editor) ifTrue: [action _ true]].
				]]].
	^ action! !
PresentationBuilder initialize!
