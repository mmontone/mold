'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 2 July 2016 at 12:23:05.986003 pm'!
!classDefinition: #OneLineEditorMorph category: #'Morphic-Widgets-Extras'!
BoxedMorph subclass: #OneLineEditorMorph
	instanceVariableNames: 'font emphasis contents editor showTextCursor pauseBlinking textCursorRect keyboardFocusWatcher crAction'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Morphic-Widgets-Extras'!
!OneLineEditorMorph commentStamp: '<historical>' prior: 0!
A plain text editor for Morphic. Handles only one line. Does not handle fonts/styles, alignment, Smalltalk utilities and any other advanced stuff in TextModelMorph. Just a simple text editor.

Can optionally include a crAction: a zero argument closure, to be evaluated on Cr keystroke.!


!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 4/15/2014 09:23'!
baseFont

	font ifNil: [ font _ AbstractFont default ].
	^font! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 3/16/2011 10:34'!
contents
	^contents! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 3/16/2011 10:41'!
contents: newContents 
	contents _ (newContents is: #Text)
		ifTrue: [
			emphasis := newContents emphasisAt: 1.
			 newContents string]
		ifFalse: [
			contents = newContents ifTrue: [^self].	"no substantive change"
			newContents].
	editor _ nil.
	self fitContents.
	self redrawNeeded! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 1/5/2013 14:23'!
crAction
	"Answer the optional Cr action"
	^crAction! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 1/5/2013 14:24'!
crAction: aBlock
	
	crAction := aBlock! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 3/6/2015 08:54'!
disableEdition
	self setProperty: #disablesEdition toValue: true.
	self stopBlinking! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 12/4/2011 22:25'!
editor
	"Return my current editor, or install a new one."
	editor ifNil: [ self installEditor ].
	^editor! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 1/1/2015 21:16'!
fitContents
	"Measures contents later at #minimumExtent"
	self morphExtent: 0@0! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 11/5/2008 13:18'!
fontToUse

	^ (emphasis isNil or: [emphasis = 0]) 
		ifTrue: [ self baseFont ]
		ifFalse: [ self baseFont emphasized: emphasis ]! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'jmv 3/17/2011 07:58'!
keyboardFocusWatcher: aMorph
	"We are usually used as a part of a bigger morph.
	Usually, that morph would be interested in us changing keyboard focus.
	An alternative implementation would be to define a new type of event, but:
		- I (jmv) prefer explicit message sends to registering in events.
		- There are too many evens already defined. It would be good to reduce that."
	keyboardFocusWatcher _ aMorph! !

!OneLineEditorMorph methodsFor: 'accessing' stamp: 'MM 7/2/2016 12:21'!
measureContents
	| f |

	f _ self fontToUse.
	^((f widthOfString: self contents) max: 3)  @ f height.! !


!OneLineEditorMorph methodsFor: 'drawing' stamp: 'MM 7/2/2016 12:21'!
characterIndexAtPoint: aPoint

	| line block f |
	f _ self fontToUse.
	
	line _ TextLine 
		start: 1
		stop: self contents size
		internalSpaces: 0
		paddingWidth: 0.
	line
		rectangle: (0@0 extent: extent);
		lineHeight: f height baseline: f ascent.
		
	block _ (CharacterBlockScanner new text: 
			(self contents asText font: font))
		characterBlockAtPoint: aPoint index: nil
		in: line.

	^ block stringIndex! !

!OneLineEditorMorph methodsFor: 'drawing' stamp: 'jmv 10/16/2013 22:37'!
displayTextCursorAtX: x top: top bottom: bottom emphasis: emphasis on: aCanvas
	| textCursorColor x1 isBold isItalic x0 h w halfW r d |
	isBold _ emphasis allMask: 1.
	isItalic _ emphasis allMask: 2.
	textCursorColor _ Theme current textCursor.
	h _ bottom - top.
	w _ isBold
		ifTrue: [ h // 25 + 2 ]
		ifFalse: [ h // 30 + 1 ].
	halfW _ w // 2.
	isItalic
		ifTrue: [	
			"Keep tweaking if needed!!"
			d _ isBold ifTrue: [ 3 ] ifFalse: [ h // 24].
			x0 _ x- (h*5//24) + d.
			x1 _ x + d ]
		ifFalse: [
			x0 _ x.
			x1 _ x].
	x0 < halfW ifTrue: [
		x1 _ x1 - x0 + halfW.
		x0 _ halfW ].
	r _ extent x-halfW-1.
	r < x1 ifTrue: [
		x0 _ x0 + r - x1.
		x1 _ r ].
	textCursorRect _ x0-halfW-1@ top corner: x1+halfW+1+1 @ bottom.
	aCanvas
		line: x0+halfW@bottom to: x1+halfW@(top+w)
		width: w color: textCursorColor! !

!OneLineEditorMorph methodsFor: 'drawing' stamp: 'MM 7/2/2016 12:20'!
drawOn: aCanvas
	self hasSelection ifTrue: [ self drawSelectionOn: aCanvas ].
	self hasTextCursor ifTrue: [ self drawTextCursorOn: aCanvas ].
	aCanvas
		drawString: self contents
		at: 0@0
		font: self fontToUse
		color: color! !

!OneLineEditorMorph methodsFor: 'drawing' stamp: 'MM 7/2/2016 12:20'!
drawSelectionOn: aCanvas
	| rightX leftX bottom |

	bottom _ self baseFont height.
	leftX _ self fontToUse widthOfString: self contents from: 1 to: editor startIndex-1.
	leftX _ leftX min: extent x.
	rightX _ self fontToUse widthOfString: self contents from: 1 to: editor stopIndex-1.
	rightX _ rightX min: extent x.

	aCanvas
		fillRectangle: (leftX @ 0 corner: rightX @ bottom)
		color: (Theme current textHighlightFocused: self hasKeyboardFocus)! !

!OneLineEditorMorph methodsFor: 'drawing' stamp: 'MM 7/2/2016 12:20'!
drawTextCursorOn: aCanvas
	|  bottom x |

	showTextCursor ifTrue: [
		bottom _ self baseFont height.
		x _ self fontToUse widthOfString: self contents from: 1 to: editor startIndex-1.
		self displayTextCursorAtX: x top: 0 bottom: bottom emphasis: emphasis on: aCanvas ]! !


!OneLineEditorMorph methodsFor: 'events' stamp: 'jmv 8/21/2012 08:43'!
clickAndHalf: aMouseButtonEvent localPosition: localEventPosition
	self handleInteraction: [ self editor clickAndHalf ]! !

!OneLineEditorMorph methodsFor: 'events' stamp: 'MM 5/7/2016 11:15'!
keyStroke: aKeyboardEvent
	"Handle a keystroke event."

	(Theme current keyStroke: aKeyboardEvent morph: self)
		ifTrue: [^ self].

	(self focusKeyboardFor: aKeyboardEvent)
		ifTrue: [ ^ self ].

	"Return - check for special action
	Note: Code below assumes that this was some
	input field reacting on Return."
	aKeyboardEvent isReturnKey ifTrue: [
		crAction ifNotNil: [
			"Break the keyboard focus so that the receiver can be safely deleted."
			"Is this needed at all? (jmv)"
			"aKeyboardEvent hand newKeyboardFocus: nil."
			^crAction value ]].

	self pauseBlinking.
	self handleInteraction: [ self editor processKeyStroke: aKeyboardEvent ].
	self updateFromContents.
	^ super keyStroke: aKeyboardEvent  "sends to keyStroke event handler, if any"! !

!OneLineEditorMorph methodsFor: 'events' stamp: 'jmv 1/14/2013 22:27'!
mouseButton1Down: aMouseButtonEvent localPosition: localEventPosition
	"Make this TextMorph be the keyboard input focus, if it isn't already,
		and repond to the text selection gesture."

	aMouseButtonEvent hand newKeyboardFocus: self.

	self handleInteraction: [
		self editor mouseDown: aMouseButtonEvent index: (self characterIndexAtPoint: localEventPosition) ].

	aMouseButtonEvent hand
		waitForClicksOrDragOrSimulatedMouseButton2: self
		event: aMouseButtonEvent
		clkSel: nil
		clkNHalf: #clickAndHalf:localPosition:
		dblClkSel: nil
		dblClkNHalfSel: nil
		tripleClkSel: nil! !

!OneLineEditorMorph methodsFor: 'events' stamp: 'jmv 1/14/2013 23:16'!
mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition

	super mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition.
	self pauseBlinking
! !

!OneLineEditorMorph methodsFor: 'events' stamp: 'jmv 8/20/2012 18:07'!
mouseEnter: event
	super mouseEnter: event.
	Preferences focusFollowsMouse
		ifTrue: [ event hand newKeyboardFocus: self ]! !

!OneLineEditorMorph methodsFor: 'events' stamp: 'jmv 8/21/2012 13:20'!
mouseMove: aMouseMoveEvent localPosition: localEventPosition

	aMouseMoveEvent mouseButton1Pressed ifFalse: [ ^ self ].
	self handleInteraction: [
		self editor mouseMove: aMouseMoveEvent index: (self characterIndexAtPoint: localEventPosition) ]! !


!OneLineEditorMorph methodsFor: 'initialization' stamp: 'cbr 12/3/2010 23:29'!
defaultColor
	"answer the default color/fill style for the receiver"
	^ Theme current text! !

!OneLineEditorMorph methodsFor: 'initialization' stamp: 'jmv 11/5/2008 12:15'!
initWithContents: aString font: aFont emphasis: emphasisCode 
	self initialize.
	
	font _ aFont.
	emphasis _ emphasisCode.
	self contents: aString! !

!OneLineEditorMorph methodsFor: 'initialization' stamp: 'jmv 10/16/2013 22:19'!
initialize
	super initialize.
	contents _ ''.
	font _ nil.
	emphasis _ 0.
	showTextCursor _ false. "Should never be nil"! !


!OneLineEditorMorph methodsFor: 'event handling testing' stamp: 'jmv 3/6/2015 08:53'!
disablesEdition

	^self hasProperty: #disablesEdition! !

!OneLineEditorMorph methodsFor: 'event handling testing' stamp: 'jmv 8/20/2012 18:52'!
handlesKeyboard

	^self visible! !

!OneLineEditorMorph methodsFor: 'event handling testing' stamp: 'jmv 8/20/2012 18:52'!
handlesMouseDown: aMouseButtonEvent
	^ true! !

!OneLineEditorMorph methodsFor: 'event handling testing' stamp: 'jmv 8/20/2012 18:52'!
handlesMouseOver: evt
	"implements #mouseEnter: and/or #mouseLeave:"
	^true! !


!OneLineEditorMorph methodsFor: 'events-processing' stamp: 'jmv 12/28/2011 22:45'!
focusKeyboardFor: aKeyboardEvent
	"If aKeyboardEvent tab or shift-tab use it to navigate keyboard focus.
	ctrl key ignored."
	aKeyboardEvent keyValue = 9
		ifTrue: [
			aKeyboardEvent shiftPressed
				ifFalse: [ aKeyboardEvent hand keyboardFocusNext ]
				ifTrue: [ aKeyboardEvent hand keyboardFocusPrevious ].
			^ true ].
	^super focusKeyboardFor: aKeyboardEvent! !

!OneLineEditorMorph methodsFor: 'events-processing' stamp: 'KenD 10/29/2015 20:43'!
fontPreferenceChanged

	font := nil.
	self baseFont.! !

!OneLineEditorMorph methodsFor: 'events-processing' stamp: 'jmv 3/6/2015 08:59'!
keyboardFocusChange: aBoolean 
	aBoolean
		ifTrue: [
			"A hand is wanting to send us characters..."
			editor ifNil: [ self editor ].	"Forces install"
			self selectAll.
			self showsBlinkingCursor ifTrue: [
				self startBlinking ]]
		ifFalse: [
			self stopBlinking.
			keyboardFocusWatcher ifNotNil: [
				keyboardFocusWatcher lostFocus: self ]].
	self redrawNeeded! !


!OneLineEditorMorph methodsFor: 'editing' stamp: 'MM 7/2/2016 12:20'!
handleInteraction: interactionBlock
	"Perform the changes in interactionBlock, noting any change in selection
	and possibly a change in the size of the composition"

	"Also couple the editor to Morphic keyboard events"

	| oldEditor oldContents |
	oldEditor _ editor.
	oldContents _ self contents.
	interactionBlock value.
	oldContents == self contents 
		ifTrue: [ "this will not work if the composition changed"
			editor _ oldEditor	"since it may have been changed while in block"].
	self redrawNeeded! !


!OneLineEditorMorph methodsFor: 'testing' stamp: 'jmv 11/5/2008 12:41'!
hasSelection

	^editor notNil and: [editor hasSelection]! !

!OneLineEditorMorph methodsFor: 'testing' stamp: 'jmv 10/16/2013 22:18'!
hasTextCursor

	^ self hasKeyboardFocus and: [editor notNil and: [editor hasSelection not]]! !


!OneLineEditorMorph methodsFor: 'unaccepted edits' stamp: 'jmv 3/14/2011 09:21'!
hasUnacceptedEdits: aBoolean
	"Set the hasUnacceptedEdits flag to the given value. "
	
	self flag: #jmv.
	"Not used in this morph, as it doesn't do accept / cancel"
	self redrawNeeded! !


!OneLineEditorMorph methodsFor: 'private' stamp: 'MM 7/2/2016 12:21'!
installEditor
	"Install an editor for my contents.  This constitutes 'hasFocus'.
	If priorEditor is not nil, then initialize the new editor from its state.
	We may want to rework this so it actually uses the prior editor."

	editor _ SimpleEditor new morph: self.
	editor changeString: self contents.
	self redrawNeeded.
	^editor! !

!OneLineEditorMorph methodsFor: 'private' stamp: 'MM 7/2/2016 12:20'!
updateFromContents
	self contents: self editor string! !


!OneLineEditorMorph methodsFor: 'geometry' stamp: 'jmv 3/6/2015 08:52'!
minimumExtent

	^ self measureContents! !


!OneLineEditorMorph methodsFor: 'blink cursor' stamp: 'jmv 7/18/2014 14:53'!
onBlinkCursor
	"Blink the cursor"
	showTextCursor _ showTextCursor not | pauseBlinking.
	pauseBlinking _ false.
	textCursorRect ifNotNil: [ :r | self invalidateLocalRect: r]! !

!OneLineEditorMorph methodsFor: 'blink cursor' stamp: 'jmv 3/6/2015 08:59'!
pauseBlinking
	"Show a solid cursor (non blinking) for a short while"
	pauseBlinking _ true.
	self showsBlinkingCursor ifTrue: [
		"Show cursor right now if needed"
		showTextCursor ifFalse: [
			showTextCursor _ true.
			textCursorRect ifNotNil: [ :r | self invalidateLocalRect: r ]]]! !

!OneLineEditorMorph methodsFor: 'blink cursor' stamp: 'jmv 3/6/2015 08:56'!
showsBlinkingCursor

	^self handlesKeyboard and: [ self disablesEdition not ]! !

!OneLineEditorMorph methodsFor: 'blink cursor' stamp: 'jmv 2/2/2014 22:20'!
startBlinking
	"And show the cursor"
	pauseBlinking _ true.
	"Start blinking in a short while"
	showTextCursor _ true.
	self startStepping: #onBlinkCursor stepTime: 500! !

!OneLineEditorMorph methodsFor: 'blink cursor' stamp: 'jmv 7/18/2014 14:53'!
stopBlinking
	"And do not show cursor anymore."
	self stopStepping: #onBlinkCursor.
	"Hide cursor right now if needed"
	showTextCursor ifTrue: [
		showTextCursor _ false.
		textCursorRect ifNotNil: [ :r | self invalidateLocalRect: r ]]! !


!OneLineEditorMorph methodsFor: 'typing/selecting keys' stamp: 'jmv 3/16/2011 10:45'!
selectAll
	self editor selectAll.
	self redrawNeeded! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OneLineEditorMorph class' category: #'Morphic-Widgets-Extras'!
OneLineEditorMorph class
	instanceVariableNames: ''!

!OneLineEditorMorph class methodsFor: 'instance creation' stamp: 'jmv 3/16/2011 10:25'!
contents: aString
	" 'LabelMorph contents: str' is faster than 'LabelMorph new contents: str' 
	(OneLineEditorMorph contents: 'Some string') openInWorld
	"
	^ self contents: aString font: nil! !

!OneLineEditorMorph class methodsFor: 'instance creation' stamp: 'jmv 11/4/2008 12:47'!
contents: aString font: aFont
	^ self basicNew initWithContents: aString font: aFont emphasis: 0! !

!OneLineEditorMorph class methodsFor: 'instance creation' stamp: 'jmv 11/4/2008 12:47'!
contents: aString font: aFont emphasis: emphasisCode
	^ self basicNew initWithContents: aString font: aFont emphasis: emphasisCode! !

!OneLineEditorMorph class methodsFor: 'instance creation' stamp: 'jmv 4/3/2011 22:35'!
new
	^self contents: 'some string'! !
