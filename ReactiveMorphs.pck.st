'From Cuis 6.0 [latest update: #5840] on 9 June 2023 at 11:37:40 pm'!
'Description Reactive and declarative Morphic interfaces.

Related libraries: SolidJs, VobyJs.

See: https://dev.to/ryansolid/building-a-reactive-library-from-scratch-1i0p
See: https://indepth.dev/posts/1289/solidjs-reactivity-to-rendering.'!
!provides: 'ReactiveMorphs' 1 96!
SystemOrganization addCategory: 'ReactiveMorphs-Tests'!
SystemOrganization addCategory: 'ReactiveMorphs'!


!classDefinition: #RxCollection category: 'ReactiveMorphs'!
SequenceableCollection subclass: #RxCollection
	instanceVariableNames: 'collection eventHandlers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxCollection class' category: 'ReactiveMorphs'!
RxCollection class
	instanceVariableNames: ''!

!classDefinition: #RxComponentMorph category: 'ReactiveMorphs'!
BoxedMorph subclass: #RxComponentMorph
	instanceVariableNames: 'currentMorph currentComponent componentsChain'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxComponentMorph class' category: 'ReactiveMorphs'!
RxComponentMorph class
	instanceVariableNames: ''!

!classDefinition: #RxContainerMorph category: 'ReactiveMorphs'!
BoxedMorph subclass: #RxContainerMorph
	instanceVariableNames: 'morph fitContents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxContainerMorph class' category: 'ReactiveMorphs'!
RxContainerMorph class
	instanceVariableNames: ''!

!classDefinition: #RxMorph category: 'ReactiveMorphs'!
RxContainerMorph subclass: #RxMorph
	instanceVariableNames: 'morphBuilder rxValue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxMorph class' category: 'ReactiveMorphs'!
RxMorph class
	instanceVariableNames: ''!

!classDefinition: #ReactiveMorphsTestCase category: 'ReactiveMorphs-Tests'!
TestCase subclass: #ReactiveMorphsTestCase
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs-Tests'!
!classDefinition: 'ReactiveMorphsTestCase class' category: 'ReactiveMorphs-Tests'!
ReactiveMorphsTestCase class
	instanceVariableNames: ''!

!classDefinition: #RxAbstractValue category: 'ReactiveMorphs'!
Object subclass: #RxAbstractValue
	instanceVariableNames: ''
	classVariableNames: 'RxDependants'
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxAbstractValue class' category: 'ReactiveMorphs'!
RxAbstractValue class
	instanceVariableNames: ''!

!classDefinition: #RxAspect category: 'ReactiveMorphs'!
RxAbstractValue subclass: #RxAspect
	instanceVariableNames: 'model getter setter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxAspect class' category: 'ReactiveMorphs'!
RxAspect class
	instanceVariableNames: ''!

!classDefinition: #RxFormula category: 'ReactiveMorphs'!
RxAbstractValue subclass: #RxFormula
	instanceVariableNames: 'vars block'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxFormula class' category: 'ReactiveMorphs'!
RxFormula class
	instanceVariableNames: ''!

!classDefinition: #RxImplicitFormula category: 'ReactiveMorphs'!
RxAbstractValue subclass: #RxImplicitFormula
	instanceVariableNames: 'vars block'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxImplicitFormula class' category: 'ReactiveMorphs'!
RxImplicitFormula class
	instanceVariableNames: ''!

!classDefinition: #RxValue category: 'ReactiveMorphs'!
RxAbstractValue subclass: #RxValue
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxValue class' category: 'ReactiveMorphs'!
RxValue class
	instanceVariableNames: ''!

!classDefinition: #RxComponent category: 'ReactiveMorphs'!
Object subclass: #RxComponent
	instanceVariableNames: 'containerMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxComponent class' category: 'ReactiveMorphs'!
RxComponent class
	instanceVariableNames: ''!

!classDefinition: #RxValueTransformer category: 'ReactiveMorphs'!
Object subclass: #RxValueTransformer
	instanceVariableNames: 'model getter setter currentValue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphs'!
!classDefinition: 'RxValueTransformer class' category: 'ReactiveMorphs'!
RxValueTransformer class
	instanceVariableNames: ''!


!RxFormula commentStamp: '<historical>' prior: 0!
|x y adder|
x _ RxValue with: 22.
y _ RxValue with: 44.
adder _ RxFormula with: {x. y} do: [:a :b | a + b].
adder value.

"then ..."
x value: 55.
adder value.


Example2:
-------

x  RxValue with: 0.
y  RxValue with: 1.

adder  RxValue formula: [x value + y value].

effect  RxValue formula: [Transcript show: 'Adder: '; show: adder value; newLine].

adder value.

x value: 22.
x value: 33.
y value: 40.!

!RxImplicitFormula commentStamp: '<historical>' prior: 0!
|x y adder|
x _ RxValue with: 22.
y _ RxValue with: 44.
adder _ RxFormula with: {x. y} do: [:a :b | a + b].
adder value.

"then ..."
x value: 55.
adder value.


Example2:
-------

x  RxValue with: 0.
y  RxValue with: 1.

adder  RxValue formula: [x value + y value].

effect  RxValue formula: [Transcript show: 'Adder: '; show: adder value; newLine].

adder value.

x value: 22.
x value: 33.
y value: 40.!

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 16:42:12'!
add: anObject

	|res|
	
	res := collection add: anObject.
	
	self changed.
	
	^ res! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 12:40:42'!
add: aBlock to: aMorph key: aSelector

	|eventHandler|
	
	eventHandler := [self reconcile: aMorph with: aBlock key: aSelector		].
	
	eventHandlers add: eventHandler.

	self onChangeSend: #value to: eventHandler.
			
	self reconcile: aMorph with: aBlock key: aSelector.
	
	^ aMorph! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 6/6/2023 18:28:57'!
addEach: aBlock to: aMorph

	^ self add: aBlock to: aMorph key: #yourself! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 20:01:38'!
apply: aSelectorOrBlock

	^ RxValueTransformer on: self transformer: aSelectorOrBlock.! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 12:08:33'!
do: aBlock
	^ collection do: aBlock! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 23:16:45'!
ifEmpty: aBlock ifNotEmpty: otherBlock

	^ RxMorph on: (RxValueTransformer on: self transformer: #isEmpty)  
			with: [:val | val ifTrue: aBlock ifFalse: otherBlock]! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 12:26:26'!
initialize: aCollection

	collection := aCollection.
	eventHandlers := OrderedCollection new.! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 17:25:28'!
isEmpty

	^ collection isEmpty! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 12:51:57'!
reconcile: aMorph with: aBlock key: aSelector

	"Simple reconciliation algorithm."
	
	|submorphs|
	
	submorphs := aMorph submorphs.
	
	aMorph removeAllMorphs.
	
	collection do: [:item | |key|
		key := item perform: aSelector.
		submorphs detect: [:m | (m valueOfProperty: #rxKey) = key]
			ifFound: [:morph | aMorph addMorph: morph] 
			ifNone: [|itemMorph|
				itemMorph := aBlock value: item.
				itemMorph setProperty: #rxKey toValue: (item perform: aSelector).
				aMorph addMorph: itemMorph]].
	
	^ aMorph! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 17:14:47'!
remove: anObject

	|res|
	
	res := collection remove: anObject.
	
	self changed.
	
	^ res! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 20:02:44'!
removeAll
	collection removeAll.
	self changed.! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/30/2023 12:42:41'!
replaceAll: anObject with: otherObject

	collection replaceAll: anObject with: otherObject.
	self changed.! !

!RxCollection methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 16:50:27'!
value

	^ collection! !

!RxCollection class methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 16:47:08'!
new

	^ self basicNew initialize: OrderedCollection new; yourself! !

!RxCollection class methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 16:47:02'!
with: aCollection

	^ self basicNew initialize: aCollection; yourself! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:54:40'!
call: aComponent

	componentsChain addFirst: currentComponent.
	currentComponent := aComponent.
	aComponent containerMorph: self.	
	self updateCurrentMorph. ! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:20:11'!
fitContents
	currentMorph 
		ifNotNil: [self morphExtent: currentMorph morphExtent]
		ifNil: [self morphExtent: 0@0].! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:55:36'!
initialize: anRxComponent
	
	componentsChain := OrderedCollection new.
	currentComponent := anRxComponent.
	anRxComponent containerMorph: self.	
	self updateCurrentMorph.! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:20:11'!
layoutSubmorphs

	self fitContents.
	super layoutSubmorphs ! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:54:57'!
returnFrom: aComponent

	self assert: aComponent == currentComponent.
	
	currentComponent := componentsChain removeFirst.
	
	self updateCurrentMorph.
	

	! !

!RxComponentMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:52:09'!
updateCurrentMorph.

	currentMorph := currentComponent buildMorph.
	
	self removeAllMorphs.
	currentMorph ifNotNil: [self addMorph: currentMorph position: 0@0].
	self fitContents.! !

!RxComponentMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:20:31'!
on: anRxComponent

	^ self new initialize: anRxComponent! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 19:45:45'!
color
	^ self owner color! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 19:46:37'!
drawOn: aCanvas

	"draw nothing. I'm just a container of another morph."! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 23:33:34'!
fitContents
	fitContents 
		ifTrue: [
			morph 
				ifNotNil: [self morphExtent: morph morphExtent]
				ifNil: [self morphExtent: 0@0]]
		ifFalse: [morph morphExtent: self morphExtent]! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 23:29:34'!
fitContents: aBoolean

	fitContents := aBoolean! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 23:28:57'!
initialize
	super initialize.
	
	fitContents := true.! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:30:04'!
layoutSubmorphs
	self fitContents.
	super layoutSubmorphs ! !

!RxContainerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:31:43'!
setMorph: aMorph
	morph := aMorph.
	self removeAllMorphs.
	morph ifNotNil: [self addMorph: morph position: 0@0].
	self fitContents.! !

!RxMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 23:15:01'!
initialize: anRxValue reactor: aBlock

	rxValue := anRxValue.
	morphBuilder := aBlock.
	
	rxValue when: #changed: 	send: #update:			to: self.
	
	self updateCurrentMorph: rxValue value.! !

!RxMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 22:00:22'!
update: anObject
	self updateCurrentMorph: anObject! !

!RxMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:32:03'!
updateCurrentMorph: anObject

	self setMorph: (morphBuilder value: anObject)! !

!RxMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 22:02:51'!
on: anRxValue with: aBlock

	^ self new initialize: anRxValue reactor: aBlock! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:29:57'!
testFormula

	| x y adder changes |
	
	changes := 0.
	x := RxValue with: 22.
	y := RxValue with: 44.
	adder := RxFormula with: {x. y} do: [:a :b | a + b].
	self assert: adder value = (22 + 44).

	"then ..."
	x value: 55.
	
	self assert: adder value = (55 + 44).

	x := RxValue with: 0.
	y := RxValue with: 1.

	adder := RxValue formula: [x value + y value].

	RxValue formula: [Transcript show: 'Adder: '; show: adder value; newLine. changes := changes + 1].

	self assert: adder value = (0 + 1).

	x value: 22.
	
	self assert: adder value = (22 + 1).
	
	x value: 33.
	
	self assert: adder value = (33 + 1).
	
	y value: 40.
	
	self assert: adder value = (33 + 40).! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 17:48:50'!
testFormulaUpdates

	| x y adder changes |
	
	changes := 0.
	x := RxValue with: 22.
	y := RxValue with: 44.
	adder := RxFormula with: {x. y} do: [:a :b | a + b].
	adder onChangeSend: #value to: [changes := changes + 1].
	 
	self assert: adder value = (22 + 44).
	self assert: changes = 0.

	"then ..."
	x value: 55.
	self assert: changes = 1.
	self assert: adder value = (55 + 44).
	
	x value: 55.
	self assert: changes = 1.
	
	x value: 66.
	self assert: changes = 2.
	self assert: adder value = (66 + 44).! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 17:50:20'!
testImplicitFormulaUpdates

	| x y adder changes |
	
	changes := 0.
	x := RxValue with: 22.
	y := RxValue with: 44.
	adder := RxValue formula: [x value + y value].
	adder onChangeSend: #value to: [changes := changes + 1].
	 
	self assert: adder value = (22 + 44).
	self assert: changes = 0.

	"then ..."
	x value: 55.
	self assert: changes = 1.
	self assert: adder value = (55 + 44).
	
	x value: 55.
	self assert: changes = 1.
	
	y value: 66.
	self assert: changes = 2.
	self assert: adder value = (55 + 66).! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:32:53'!
testNestedFormulas! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 09:59:22'!
testValue

	| val changed |
	
	val := RxValue with: 'something'.
	changed := 0.
	
	self assert: val value = 'something'.
	
	val onChangeSend: #value to: [changed := changed + 1]. 
	
	val value: 22.
	
	self assert: changed = 1.
	
	val value: 22.
	
	self assert: changed = 1.
	
	val value: 33.
	
	self assert: changed = 2.! !

!ReactiveMorphsTestCase methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 20:03:25'!
testValueTransformer

	| collection isEmpty changes |
	
	changes := 0.
	collection := RxCollection new.
	isEmpty := collection apply: #isEmpty.
	isEmpty onChangeSend: #value to: [changes := changes + 1].
	 	
	self assert: changes = 0.
	self assert: isEmpty value.
	
	collection add: 1.
	self assert: changes = 1.
	self assert: isEmpty value not.
	
	collection add: 2.
	self assert: changes = 1. "There's no change. Still not empty."
	self assert: isEmpty value not.
	
	collection removeAll.
	self assert: changes = 2. "Changed from not empty to empty."
	self assert: isEmpty
! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:37:57'!
apply: aSelectorOrBlock

	^ RxValueTransformer on: self transformer: aSelectorOrBlock.! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:38:09'!
apply: aSelectorOrBlock unapply: back

	^ RxValueTransformer on: self getter: aSelectorOrBlock setter: back! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 23:33:17'!
ifIsFalse: aBlock ifIsTrue: otherBlock
	^ self reactiveMorph: [:val | val ifFalse: aBlock ifTrue: otherBlock]! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 23:33:32'!
ifIsTrue: aBlock
	^ self reactiveMorph: [:val | val ifTrue: aBlock]! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 23:33:42'!
ifIsTrue: aBlock ifIsFalse: otherBlock
	^ self reactiveMorph: [:val | val ifTrue: aBlock ifFalse: otherBlock]! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:38:46'!
reactiveMorph: aBlock

	^ RxMorph on: self with: aBlock! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:38:55'!
stringValue

	^ self apply: #asString! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:15:57'!
value

	^ self subclassResponsibility ! !

!RxAbstractValue methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:16:14'!
value: anObject

	^ self subclassResponsibility ! !

!RxAbstractValue class methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:27:04'!
initialize

	RxDependants := nil.! !

!RxAspect methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 19:34:20'!
initialize: aModel aspect: aSelector

	self initialize: aModel getter: aSelector setter: (Symbol intern: (aSelector asString, ':')).! !

!RxAspect methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:58:38'!
initialize: aModel getter: getSelector setter: setSelector

	model := aModel.
	getter := getSelector.
	setter := setSelector.
	
	model addDependent: self.! !

!RxAspect methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:58:52'!
update: anObject

	self value: anObject! !

!RxAspect methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:48:09'!
value

	^ getter isSymbol 
		ifTrue: [model perform: getter]
		ifFalse: [getter value: model]! !

!RxAspect methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:59:50'!
value: anObject

	setter isSymbol 
		ifTrue: [model perform: setter with: anObject]
		ifFalse: [setter value: model value: anObject].
	self changed: self value.! !

!RxAspect class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:41:01'!
on: aModel aspect: aSelector

	^ self new initialize: aModel aspect: aSelector! !

!RxAspect class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 11:45:49'!
on: aModel getter: getter setter: setter

	^ self new initialize: aModel getter: getter setter: setter! !

!RxFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 15:32:37'!
initialize: aVarOrCollection block: aBlock

	vars := aVarOrCollection isCollection
			ifTrue: [aVarOrCollection]
			ifFalse: [Array with: aVarOrCollection].
	block := aBlock.
	
	vars do: [:var |
		var onChangeSend: #update: to: self]. ! !

!RxFormula methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 20:27:51'!
update: anObject

	"One of the variables changed"
	
	self changed: self value.! !

!RxFormula methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 20:30:47'!
value

	^ block valueWithArguments: (vars collect: [:var | var value])! !

!RxFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:16:37'!
value: anObject

	self error: 'Cannot set value of a reactive formula'! !

!RxFormula class methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 22:48:40'!
from: aBlock

	^ self new initialize: Array new block: aBlock! !

!RxFormula class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 15:21:51'!
return: aBlock

	^ self new initialize: Array new block: aBlock! !

!RxFormula class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 15:32:03'!
with: aVarOrCollection do: aBlock

	^ self new initialize: aVarOrCollection block: aBlock! !

!RxImplicitFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:21:55'!
initialize: aVarOrCollection block: aBlock

	vars := aVarOrCollection isCollection
			ifTrue: [aVarOrCollection]
			ifFalse: [Array with: aVarOrCollection].
	block := aBlock.
	
	vars do: [:var |		var onChangeSend: #update: to: self]! !

!RxImplicitFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:25'!
update: anObject

	"One of the variables changed"
	
	self changed: self value.! !

!RxImplicitFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:43'!
value

	^ block value! !

!RxImplicitFormula methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:25'!
value: anObject

	self error: 'Cannot set value of a reactive formula'! !

!RxImplicitFormula class methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:25'!
from: aBlock

	^ self new initialize: Array new block: aBlock! !

!RxImplicitFormula class methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:25'!
return: aBlock

	^ self new initialize: Array new block: aBlock! !

!RxImplicitFormula class methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:18:25'!
with: aVarOrCollection do: aBlock

	^ self new initialize: aVarOrCollection block: aBlock! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 22:52:42'!
apply: aSelectorOrBlock unapply: back

	^ RxValueTransformer on: self getter: aSelectorOrBlock setter: back! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:19:22'!
initialize: aValue

	value := aValue.! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 22:06:41'!
reactiveMorph: aBlock

	^ RxMorph on: self with: aBlock! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 17:05:19'!
stringValue

	^ self apply: #asString! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:23:40'!
value
		
	RxDependants ifNotNil: [:deps | deps first add: self].
	
	^ value
		! !

!RxValue methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:14:54'!
value: anObject
	"Set the value of value"
	
	value == anObject ifFalse: [
		value := anObject.
		self changed: anObject]! !

!RxValue class methodsFor: 'as yet unclassified' stamp: 'MM 6/9/2023 16:26:40'!
formula: aBlock

	"Create a formula with automatic dependents."
	"aBlock is evaluated, and the formula is made dependent of the reactive values evaluated."

	|formula|
	
	RxDependants := RxDependants ifNil: [OrderedCollection new].
	RxDependants addFirst: OrderedCollection new.
	
	[aBlock value. "Extract vars"
	 formula := RxImplicitFormula with: RxDependants first do: aBlock]
		ensure: [RxDependants removeFirst.
				RxDependants := RxDependants ifEmpty: [nil]].
		
	^ formula! !

!RxValue class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 15:24:17'!
set: aSelector of: anObject with: aBlock

	^ RxFormula with: self do: [:val |
		anObject perform: aSelector with: (aBlock value: val)]! !

!RxValue class methodsFor: 'as yet unclassified' stamp: 'MM 6/8/2023 18:19:36'!
with: anObject

	^ self new initialize: anObject! !

!RxComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:34:32'!
buildMorph

	^ self subclassResponsibility ! !

!RxComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:34:00'!
call: anRxComponent

	containerMorph call: anRxComponent! !

!RxComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:59:03'!
morph

	^ RxComponentMorph on: self! !

!RxComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:58:50'!
openInWorld

	self morph openInWorld! !

!RxComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:56:06'!
return
	containerMorph returnFrom: self! !

!RxComponent methodsFor: 'accessing' stamp: 'MM 6/3/2023 13:42:59'!
containerMorph
	"Answer the value of containerMorph"

	^ containerMorph! !

!RxComponent methodsFor: 'accessing' stamp: 'MM 6/3/2023 13:42:59'!
containerMorph: anObject
	"Set the value of containerMorph"

	containerMorph := anObject! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 15:45:49'!
calculate

	^ getter isSymbol 
		ifTrue: [model value perform: getter]
		ifFalse: [getter value: model value]! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 19:34:36'!
initialize: aModel getter: aBlockOrSelector

	model := aModel.
	getter := aBlockOrSelector.
	aBlockOrSelector isSymbol ifTrue: [
		setter := Symbol intern: (getter asString, ':') ].
	currentValue := self calculate.
	aModel when: #changed:
		send: #update:
		to: self.! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 15:49:56'!
initialize: aModel getter: get setter: set 

	model := aModel.
	getter := get.
	setter := set.
	currentValue := self calculate.
	aModel when: #changed:
		send: #update:
		to: self.! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 17:06:28'!
update: anObject

	|newValue|
	
	newValue := self calculate.
	
	newValue == currentValue ifFalse: [
		currentValue := newValue.
		self changed: currentValue]! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 00:11:54'!
value

	^ currentValue! !

!RxValueTransformer methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 15:52:27'!
value: anObject
	setter 
		ifNotNil: [model value: (anObject perform: setter)]
		ifNil: [model value: anObject]! !

!RxValueTransformer class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 15:51:08'!
on: aModel getter: getter setter: setter

	^ self new initialize: aModel getter: getter setter: setter! !

!RxValueTransformer class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 15:50:22'!
on: aModel transformer: aBlockOrSelector

	^ self new initialize: aModel getter: aBlockOrSelector! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 5/31/2023 21:03:23'!
children: aMorphList

	aMorphList do: [:morph |
		self assert: (morph isKindOf: Morph).
		self addMorph: morph].
	
	^ self! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 6/5/2023 13:47:45'!
explore
	RxMorphExplorerMorph openOn: self! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 6/4/2023 23:37:52'!
onClick: aHandlerBlock

	self setProperty: #'handlesMouseDown:' toValue: true.
	self setProperty: #'mouseButton1Down:localPosition:' toValue: 
		[:event :localPosition | aHandlerBlock value: event].! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 5/30/2023 12:34:07'!
onKeyStroke: aHandlerBlock
	self setProperty: #handlesKeyboard toValue: true.
	self setProperty: #'keyStroke:' toValue: aHandlerBlock! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 6/6/2023 18:32:18'!
rxChildren: anRxCollection from: aBlock
	^ anRxCollection addEach: aBlock to: self ! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 6/3/2023 15:31:11'!
rxProp: aSelector is: anRxValue
	
	|formula|
	
	formula := RxFormula with: anRxValue do: [:value | self perform: aSelector with: value].
	
	"We need to store the formula, otherwise it gets garbage collected."
	"TODO: think of a better way."
	self setProperty:  formula toValue: formula. 
	
	^ self ! !

!Morph methodsFor: '*ReactiveMorphs' stamp: 'MM 6/3/2023 19:26:13'!
rxPropAdaptor: aSelector

	^ RxAspect on: self aspect: aSelector! !

!PluggableButtonMorph class methodsFor: '*ReactiveMorphs' stamp: 'MM 5/31/2023 21:07:40'!
label: aString action: aBlock
	^ self model: aBlock action: #value label: aString! !
RxAbstractValue initialize!
