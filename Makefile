image=../Cuis-Smalltalk-Dev/Cuis5.0-4619.image
vm=../../sqcogspur64linuxht/bin/squeak

build: squeak-vm
	$(vm) $(image) -s install.st

build-all: squeak-vm
	$(vm) $(image) -s install-all.st

squeak-vm:
	mkdir squeak-vm && tar -C ./squeak-vm -xzf squeak-vm.tar.gz
