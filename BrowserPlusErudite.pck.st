'From Cuis 5.0 [latest update: #3960] on 4 December 2019 at 12:17:50 pm'!
'Description BrowserPlus plugin that renders documentation using Erudite.'!
!provides: 'BrowserPlusErudite' 1 3!
!requires: 'BrowserPlus' 1 32 nil!
!requires: 'Erudite' 1 134 nil!
SystemOrganization addCategory: #BrowserPlusErudite!


!classDefinition: #EruditeBPPlugin category: #BrowserPlusErudite!
BrowserPlusPlugin subclass: #EruditeBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlusErudite'!
!classDefinition: 'EruditeBPPlugin class' category: #BrowserPlusErudite!
EruditeBPPlugin class
	instanceVariableNames: ''!


!EruditeBPPlugin commentStamp: 'MM 12/2/2019 19:44:01' prior: 0!
BrowserPlus plugin that renders documentation using Erudite.!

!EruditeBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 12/2/2019 22:12:24'!
classCommentText: aString forClass: aClass
	
	| doc |
	
	doc _ SmalltalkEruditeParser parse: aString.
	^ (MorphicEruditeDocRenderer on: doc) render! !

!EruditeBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2019 12:16:53'!
textStylerClassFor: textGetter model: aBrowser
	"See BrowserPlusPlugin>>textStylerClassFor:model:"
	textGetter = #acceptedContents ifFalse: [^ nil].
	aBrowser currentCompiledMethod  ifNotNil: [
		^ ClassMethodCommentTextStyler].
	^ nil
		! !

!EruditeBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 12/2/2019 22:09:15'!
isAbstract
	^ false! !

!EruditeBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 12/2/2019 22:10:01'!
pluginName
	^ 'Erudite'! !
