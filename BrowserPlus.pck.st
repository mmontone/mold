'From Cuis 5.0 [latest update: #4743] on 12 August 2021 at 12:09:59 pm'!
'Description Browser extensions for navigation and custom menus.

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'BrowserPlus' 1 42!
!requires: 'SourceCodeServices' 1 3 nil!
!requires: 'Morphic-Widgets-Extras' 1 11 nil!
!requires: 'Props' 1 48 nil!
!requires: 'Utils' 1 0 nil!
SystemOrganization addCategory: 'BrowserPlus'!


!classDefinition: #CountingCollection category: 'BrowserPlus'!
SortedCollection subclass: #CountingCollection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'CountingCollection class' category: 'BrowserPlus'!
CountingCollection class
	instanceVariableNames: ''!

!classDefinition: #BrowserPlus category: 'BrowserPlus'!
Browser subclass: #BrowserPlus
	instanceVariableNames: 'locationIndex navigationChain registerBrowse plugins installedPlugins options'
	classVariableNames: 'BrowsingAccess BrowsingHistory BrowsingUpdates DefaultPlugins'
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'BrowserPlus class' category: 'BrowserPlus'!
BrowserPlus class
	instanceVariableNames: ''!

!classDefinition: #BrowserWindowPlus category: 'BrowserPlus'!
BrowserWindow subclass: #BrowserWindowPlus
	instanceVariableNames: 'toolbar aproposInput iconsRepository'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'BrowserWindowPlus class' category: 'BrowserPlus'!
BrowserWindowPlus class
	instanceVariableNames: ''!

!classDefinition: #BrowserPlusPlugin category: 'BrowserPlus'!
Object subclass: #BrowserPlusPlugin
	instanceVariableNames: 'enabled'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'BrowserPlusPlugin class' category: 'BrowserPlus'!
BrowserPlusPlugin class
	instanceVariableNames: ''!

!classDefinition: #AutoCategorizerBPPlugin category: 'BrowserPlus'!
BrowserPlusPlugin subclass: #AutoCategorizerBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'AutoCategorizerBPPlugin class' category: 'BrowserPlus'!
AutoCategorizerBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #ExamplesBPPlugin category: 'BrowserPlus'!
BrowserPlusPlugin subclass: #ExamplesBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'ExamplesBPPlugin class' category: 'BrowserPlus'!
ExamplesBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #InitializerBPPlugin category: 'BrowserPlus'!
BrowserPlusPlugin subclass: #InitializerBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'InitializerBPPlugin class' category: 'BrowserPlus'!
InitializerBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #NavigationBPPlugin category: 'BrowserPlus'!
BrowserPlusPlugin subclass: #NavigationBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'NavigationBPPlugin class' category: 'BrowserPlus'!
NavigationBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #UndefinedFinderBPPlugin category: 'BrowserPlus'!
BrowserPlusPlugin subclass: #UndefinedFinderBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlus'!
!classDefinition: 'UndefinedFinderBPPlugin class' category: 'BrowserPlus'!
UndefinedFinderBPPlugin class
	instanceVariableNames: ''!


!BrowserPlus commentStamp: '<historical>' prior: 0!
Browser extension with navigation support and custom menus.

Configure with:

PropertiesBrowser openOn: BrowserPlus.!

!BrowserWindowPlus commentStamp: '<historical>' prior: 0!
Browser extension with navigation support and custom menus.

BrowserWindowPlus open!

!AutoCategorizerBPPlugin commentStamp: '<historical>' prior: 0!
BrowserPlus plugin that adds a method for AutoCategorization of classes.!

!ExamplesBPPlugin commentStamp: '<historical>' prior: 0!
BrowserPlus plugin for running example methods from the browser.!

!InitializerBPPlugin commentStamp: '<historical>' prior: 0!
BrowserPlus plugin that provides menues for evaluating initialization methods.!

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:46:59'!
add: anObject
	^ self add: anObject count: 1! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:46:48'!
add: anObject count: aNumber

	|x|
	x := self detect: [:elem  | elem first = anObject] ifNone: [].
	
	x ifNotNil:[
		x at: 1 put: x first + aNumber.
		self reSort]
	ifNil:[
		super add: {aNumber. anObject}]! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:45:29'!
detectCounting: aBlock ifNone: exceptionBlock 
	"Evaluate aBlock with each of the receiver's elements as the argument. 
	Answer the first element for which aBlock evaluates to true. If none 
	evaluate to true, then evaluate the argument, exceptionBlock."

	super do: [:each  | (aBlock value: each) ifTrue: [^each]].
	^exceptionBlock value! !

!CountingCollection methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:45:17'!
doCounting: aBlock
	^ super do: [:x | aBlock value: x second value: x first] ! !

!CountingCollection methodsFor: 'initialization' stamp: 'MarianoMontone 6/14/2017 18:36:39'!
initialize
	super initialize.
	sortBlock _ [:x :y | x first > y first]! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:37:40'!
browseTo: aLocation
	
	|class|
	
	class _ Smalltalk classNamed: aLocation first.
	
	self setClass: class selector: aLocation second! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:44:30'!
navigateBack
	
	locationIndex ifNil: [^ nil].
	(locationIndex >= self navigationChain size) ifTrue: [^nil].
	
	locationIndex := locationIndex + 1.
	
	self navigateTo: (self navigationChain at: locationIndex)! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 22:06:02'!
navigateForward
	
	locationIndex ifNil: [^ nil].
	(locationIndex <= 1) ifTrue: [^nil].
	
	locationIndex := locationIndex - 1.
	
	self navigateTo: (self navigationChain at: locationIndex)! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:30:13'!
navigateTo: aLocation
	
	|class|
	
	class _ Smalltalk classNamed: aLocation first.
	
	self withRegisterDisabled: [
		self setClass: class selector: aLocation second]! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 22:48:16'!
navigationChain
	^ navigationChain! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 10:34:56'!
registerNavigation

	|location|
	
	selectedMessage ifNotNil: [
	
		(locationIndex notNil and: [locationIndex > 1]) ifTrue: [
			navigationChain := navigationChain copyFrom: locationIndex to: navigationChain size].
	
		location  := {selectedClassName. selectedMessage}.
		
		navigationChain addFirst: location.
		locationIndex := 1
		]! !

!BrowserPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/14/2017 19:14:11'!
withRegisterDisabled: aBlock

	|regBrowse |
	
	regBrowse _ registerBrowse.
	registerBrowse := false.
	aBlock value.
	registerBrowse := regBrowse ! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 18:51:12'!
browsingAccess
	^ self class browsingAccess! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/13/2017 21:40:14'!
browsingHistory
	^ self class browsingHistory! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 09:50:54'!
browsingHistoryMaxSize
	^ self class browsingHistoryMaxSize! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:14:31'!
browsingUpdates
	^ self class browsingUpdates! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 10:33:03'!
messageListIndex: anInteger
	super messageListIndex: anInteger.
	
	registerBrowse ifTrue: [
	
		"Add to browsing history"
		self registerBrowsingHistory.
	
		"Add to navigation chain"
		self registerNavigation] ! !

!BrowserPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:14:40'!
registerBrowsingHistory
	
	|browsingHistory browsingAccess |
	
	browsingHistory _ self browsingHistory.
	browsingAccess _ self browsingAccess.

	selectedMessage ifNotNil: [ |location|
		
		location _ {selectedClassName. selectedMessage}.
		
		browsingAccess add: location.	

		(browsingHistory isEmpty not and: [location = browsingHistory first]) ifFalse: [
			browsingHistory addFirst: location.
	
			(browsingHistory size > self browsingHistoryMaxSize) ifTrue: [
				browsingHistory removeLast.	
			]]] ! !

!BrowserPlus methodsFor: 'message functions' stamp: 'MarianoMontone 6/14/2017 19:24:11'!
defineMessageFrom: aString notifying: aRequestor
	|selector|
		
	selector := super defineMessageFrom: aString notifying: aRequestor.
	
	selector ifNotNil: [ |location|
		location _ {self selectedClassOrMetaClass . selector}.
		self browsingUpdates addFirst: location].
	
	^ selector! !

!BrowserPlus methodsFor: 'initialization' stamp: 'MM 11/18/2019 08:14:17'!
initialize
	super initialize.
	
	locationIndex := nil.
	navigationChain := OrderedCollection new.
	registerBrowse := true.
	plugins _ BrowserPlus defaultPlugins.
	installedPlugins _ OrderedCollection new.
	options _ Dictionary new.! !

!BrowserPlus methodsFor: 'initialization' stamp: 'MM 11/16/2019 22:24:34'!
initializePlugins
	self plugins do: [:plugin |
		plugin installOn: self]! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/16/2019 21:39:55'!
addPlugin: aPlugin
	plugins add: aPlugin! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/17/2019 00:21:39'!
enabledPlugins
	^ installedPlugins select: [:p | p enabled]! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/16/2019 22:41:06'!
installPlugin: aPluginInstance
	installedPlugins add: aPluginInstance! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/16/2019 22:30:22'!
installedPlugins
	^ installedPlugins! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/16/2019 21:27:48'!
plugins
	^ plugins! !

!BrowserPlus methodsFor: 'plugins' stamp: 'MM 11/16/2019 21:28:29'!
plugins: aCollection
	plugins _ aCollection! !

!BrowserPlus methodsFor: 'properties' stamp: 'MM 8/12/2021 11:01:52'!
browserProperties
	^ {
		(AspectProperty on: self aspect: #toolBarEnabled)
				name: 'toolbar enabled';
				type: BooleanPropertyType new;
				default: true;
				category: 'toolbar';
				yourself.
		(AspectProperty on: self aspect: #showIcons)
				name: 'show icons';
				type: BooleanPropertyType new;
				default: true;
				category: 'user interface';
				yourself.	
		}! !

!BrowserPlus methodsFor: 'properties' stamp: 'MM 11/18/2019 08:07:37'!
properties
	
	^ self browserProperties,
		((installedPlugins collect: [:plugin |
			plugin properties]) flatten)! !

!BrowserPlus methodsFor: 'options' stamp: 'MM 8/12/2021 11:00:32'!
showIcons
	^ options at: #showIcons ifAbsent: [true]! !

!BrowserPlus methodsFor: 'options' stamp: 'MM 8/12/2021 11:00:20'!
showIcons: aBoolean
	options at: #showIcons put: aBoolean.
	self changed: #showIcons! !

!BrowserPlus methodsFor: 'options' stamp: 'MM 11/18/2019 08:13:43'!
toolBarEnabled
	^ options at: #toolBarEnabled ifAbsent: [true]! !

!BrowserPlus methodsFor: 'options' stamp: 'MM 11/18/2019 08:19:13'!
toolBarEnabled: aBoolean
	options at: #toolBarEnabled put: aBoolean.
	self changed: #toolBarEnabled! !

!BrowserPlus methodsFor: 'as yet unclassified' stamp: 'MM 12/2/2019 22:08:14'!
classCommentText
	
	| comment theClass |
	comment _ super classCommentText asString.
	theClass _ self selectedClassOrMetaClass.
	self enabledPlugins do: [:plugin |
		comment _ plugin classCommentText: comment forClass: theClass].
	
	^ comment
			! !

!BrowserPlus methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2019 12:24:52'!
textStylerClassFor: textGetter
	self enabledPlugins do: [:plugin | |styler|
		styler _ plugin textStylerClassFor: textGetter model: self.
		styler ifNotNil: [^ styler] ].
	^ super textStylerClassFor: textGetter! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:23:26'!
addDefaultPlugin: aClass
	DefaultPlugins ifNil: [DefaultPlugins _ OrderedCollection new].
	(DefaultPlugins includes: aClass) ifFalse: [
		DefaultPlugins add: aClass]! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:51:22'!
browsingAccess
	BrowsingAccess ifNil: [
		BrowsingAccess := CountingCollection new].
	^BrowsingAccess! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/13/2017 21:39:35'!
browsingHistory
	BrowsingHistory ifNil: [
		BrowsingHistory := OrderedCollection new].
	^BrowsingHistory! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:52:54'!
browsingHistoryMaxSize
	^ 100! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:52:56'!
browsingUpdates
	BrowsingUpdates ifNil: [
		BrowsingUpdates := OrderedCollection new].
	^BrowsingUpdates! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:51:48'!
clearBrowsingAccess
	BrowsingAccess := CountingCollection new! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:48:01'!
clearBrowsingHistory
	BrowsingHistory := OrderedCollection new! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 18:53:16'!
clearBrowsingUpdates
	BrowsingUpdates := OrderedCollection new! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:38:22'!
defaultPlugins
	^ DefaultPlugins ifNil: [DefaultPlugins _ self initializeDefaultPlugins]! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:19:59'!
defaultPlugins: aCollection
	DefaultPlugins _ aCollection! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 12/5/2019 09:42:05'!
initializeDefaultPlugins
	"self initializeDefaultPlugins"
	^ DefaultPlugins _ BrowserPlusPlugin allInstallable ! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:44:31'!
isDefaultPluginEnabled: aPlugin
	^ DefaultPlugins includes: aPlugin! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:15:58'!
properties
	^ (BrowserPlusPlugin allInstallable collect: [:plugin |
		plugin properties]) flatten! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:43:49'!
toggleDefaultPlugin: aClass
	DefaultPlugins ifNil: [DefaultPlugins _ OrderedCollection new].
	(DefaultPlugins includes: aClass) ifTrue: [
		DefaultPlugins remove: aClass]
		ifFalse: [DefaultPlugins add: aClass]! !

!BrowserPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:49:27'!
toggleDefaultPlugin: aClass enabled: aBoolean
	DefaultPlugins ifNil: [DefaultPlugins _ OrderedCollection new].
	aBoolean ifTrue: [DefaultPlugins add: aClass]
				ifFalse: [DefaultPlugins remove: aClass]! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 13:13:36'!
apropos

	|text|
	
	text _ aproposInput contents.
	
	(text first isUppercase ) ifTrue: [
		"Apropos a class"
		self aproposClass: text	
	] ifFalse: [
		self aproposMethod: text].
	
	aproposInput contents: ''.! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 13:12:12'!
aproposClass: pattern

	"Search for a class by name."
	| foundClass classNames index toMatch exactMatch potentialClassNames |

	self okToChange ifFalse: [ ^self flash ].
	pattern isEmpty ifTrue: [^ self flash].
	toMatch _ (pattern copyWithout: $.) asLowercase withBlanksTrimmed.
	potentialClassNames _ model potentialClassNames asOrderedCollection.
	classNames _ (pattern last = $. or: [pattern last = $ ])
		ifTrue: [potentialClassNames select:
					[:nm |  nm asLowercase = toMatch]]
		ifFalse: [potentialClassNames select: 
					[:n | n includesSubstring: toMatch caseSensitive: false]].
	classNames isEmpty ifTrue: [^ self flash].
	exactMatch _ classNames detect: [ :each | each asLowercase = toMatch] ifNone: nil.

	index _ classNames size = 1
		ifTrue:	[1]
		ifFalse:	[exactMatch
			ifNil: [(PopUpMenu labelArray: classNames lines: #()) startUpMenu]
			ifNotNil: [classNames addFirst: exactMatch.
				(PopUpMenu labelArray: classNames lines: #(1)) startUpMenu]].
	index = 0 ifTrue: [^ self flash].
	foundClass _ Smalltalk at: (classNames at: index) asSymbol.
 	model selectCategoryForClass: foundClass.
	model selectClass: foundClass
! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2019 08:35:17'!
boxExtent
	"answer the extent to use in all the buttons. 
	 
	the label height is used to be proportional to the fonts preferences"
	"| e |
	e _ Preferences windowTitleFont lineSpacing.
	^e@e"
	^ 16@16! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:28:20'!
buildMorphicClassList

	^ (PluggableListMorphPlus
		model: model
		listGetter: #classList
		indexGetter: #classListIndex
		indexSetter: #classListIndex:
		mainView: self
		menuGetter: #classListMenu
		keystrokeAction: #classListKey:from:)
			defaultIcon: [:class | self classListIconFor: class];
			yourself! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:17:55'!
buildMorphicMessageCatList

	^ (PluggableListMorphPlus
		model: model
		listGetter: #messageCategoryList
		indexGetter: #messageCategoryListIndex
		indexSetter: #messageCategoryListIndex:
		mainView: self
		menuGetter: #messageCategoryMenu
		keystrokeAction: #messageCatListKey:from:)
		defaultIcon: [:cat | self catListIconFor: cat ];
		yourself! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 12:08:52'!
buildMorphicSystemCatList

	^ (PluggableListMorphPlus
		model: model
		listGetter: #systemCategoryList
		indexGetter: #systemCategoryListIndex
		indexSetter: #systemCategoryListIndex:
		mainView: self
		menuGetter: #systemCategoryMenu
		keystrokeAction: #systemCatListKey:from:)
			defaultIcon: [:cat | self systemCatListIconFor: cat];
			yourself! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:19:50'!
catListIconFor: aCategory

	^ self getIcon: 'category_obj.png'! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:27:52'!
classListIconFor: aClass

	^ self getIcon: 'class.png'! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:14:55'!
configure
	PropertiesBrowser openOn: model! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:54:19'!
getIcon: iconName

	^ iconsRepository getIcon: iconName! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:53:54'!
initialize

	|iconsDirectory|
	
	super initialize.
	
	iconsDirectory _ (CodePackage installedPackages at: 'BrowserPlus')
					fullFileName asFileEntry parent.
	iconsDirectory _ iconsDirectory / 'icons'			.
	iconsRepository _ DirectoryIconsRepository on: iconsDirectory.! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:41:46'!
model: aBrowser
	aBrowser ifNotNil: [aBrowser initializePlugins] .
	^ super model: aBrowser! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 12:09:07'!
systemCatListIconFor: aCategory

	^ self getIcon: 'package.png'! !

!BrowserWindowPlus methodsFor: 'as yet unclassified' stamp: 'MM 11/18/2019 08:18:45'!
update: aSymbol
	super update: aSymbol.
	aSymbol = #toolBarEnabled ifTrue: [
		toolbar visible: model toolBarEnabled]! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:26'!
browsingAccess
	^ model browsingAccess! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/13/2017 21:58:01'!
browsingHistory
	^ model browsingHistory! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:42'!
browsingUpdates
	^ model browsingUpdates! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 09:38:59'!
printBrowsingHistoryItem: bhItem

	^ bhItem first printString, '>>', bhItem second printString! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:35:30'!
showBrowsingAccess
	|menu choice|
	
	menu _ PopUpMenu labelArray: #('Most accessed' 'Recently updated').
	choice _ menu startUpMenu.
	
	choice = 1 ifTrue: [self showMostAccessed].
	choice = 2 ifTrue: [self showRecentlyUpdated]! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 10:37:53'!
showBrowsingHistory
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingHistory collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:27:01'!
showMostAccessed
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingAccess collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowPlus methodsFor: 'browsing' stamp: 'MarianoMontone 6/14/2017 19:28:55'!
showRecentlyUpdated
	
	| item menu |
	
	menu := SelectionMenu labelList: (self browsingUpdates collect:[:x | self printBrowsingHistoryItem: x]) selections: self browsingHistory .
	item := menu startUpMenu.
	
	item ifNotNil: [
		self model browseTo: item ]! !

!BrowserWindowPlus methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:22:43'!
buildWindowMenu
	
	| aMenu |

	aMenu _ MenuMorph new defaultTarget: self.

	aMenu 
		add: 'change title...' 			action: #relabel 						icon: #saveAsIcon;
		add: 'window color...' 			action: #setWindowColor 			icon: #graphicsIcon;
		add: 'configure ...' action: #configure icon: #preferencesIcon.
	
	aMenu
		addLine.
		
	self addWindowControlTo: aMenu.
	self addTileResizerMenuTo: aMenu.

	model enabledPlugins do: [:plugin |
		plugin buildWindowMenu: aMenu].	
	
	^ aMenu! !

!BrowserWindowPlus methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:22:48'!
classListMenu

	|menu|
	
	menu := super classListMenu.
	
	model selectedClass ifNotNil: [:class |
		class buildMenu: menu].
	
	model enabledPlugins do: [:plugin |
		plugin classListMenu: menu model: model].	
	
	^ menu! !

!BrowserWindowPlus methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:22:54'!
messageListMenu
	|menu|
	
	menu := super messageListMenu.
	
	model enabledPlugins do: [:plugin |
		plugin messageListMenu: menu model: model].
									
	^ menu! !

!BrowserWindowPlus methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:53:02'!
systemCategoryMenu
	|menu|
	
	menu _ super systemCategoryMenu.
	
	model enabledPlugins do: [:plugin |
		plugin systemCategoryMenu: menu model: model].
	
	^ menu! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MM 8/12/2021 10:40:33'!
buildMorphicMessageList
	"Build a morphic message list, with #messageList as its list-getter"

	^PluggableListMorphPlus
		model: model
		listGetter: #messageList
		indexGetter: #messageListIndex
		indexSetter: #messageListIndex:
		mainView: self
		menuGetter: #messageListMenu
		keystrokeAction: #messageListKey:from:
		"filterable: true"! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MarianoMontone 6/13/2017 23:25:34'!
createBackBox
	^(PluggableButtonMorph model: self action: #navigateBack)
		icon: (Theme current fetch: #('16x16' 'actions' 'go-previous'));
		iconName: #navigateBack;
		setBalloonText: 'navigate back';
		morphExtent: self boxExtent! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MarianoMontone 6/14/2017 12:30:08'!
createBrowsingAccessBox
	^(PluggableButtonMorph model: self action: #showBrowsingAccess)
		icon: (Theme current fetch: #('16x16' 'actions' 'appointment-new'));
		iconName: #browsingAccess;
		setBalloonText: 'show browsing access';
		morphExtent: self boxExtent! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MarianoMontone 6/14/2017 12:30:16'!
createBrowsingHistoryBox
	^(PluggableButtonMorph model: self action: #showBrowsingHistory)
		icon: (Theme current fetch: #('16x16' 'actions' 'address-book-new'));
		iconName: #browsingHistory;
		setBalloonText: 'show browsing history';
		morphExtent: self boxExtent! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MarianoMontone 6/13/2017 23:28:47'!
createForwardBox
	^(PluggableButtonMorph model: self action: #navigateForward)
		icon: (Theme current fetch: #('16x16' 'actions' 'go-next'));
		iconName: #navigateForward;
		setBalloonText: 'navigate forward';
		morphExtent: self boxExtent! !

!BrowserWindowPlus methodsFor: 'GUI building' stamp: 'MM 11/18/2019 08:05:48'!
layoutSubmorphs
	
	
	toolbar morphHeight: 30.
	
	"buttons morphPosition: (self morphExtent x - buttons morphWidth)@0."
	
	toolbar morphPosition: (self morphExtent x - 200)@0.	
	
	super layoutSubmorphs.! !

!BrowserWindowPlus methodsFor: 'initialization' stamp: 'MM 11/18/2019 08:24:03'!
buildMorphicWindow
	super buildMorphicWindow.
	self initializeToolBar.! !

!BrowserWindowPlus methodsFor: 'initialization' stamp: 'MM 1/7/2021 22:08:34'!
initializeToolBar

	toolbar := LayoutMorph newRow
					axisEdgeWeight: #rowRight;
					separation: 4@0;
					morphExtent: 200@16;
					yourself.
					
	aproposInput _ (OneLineEditorMorph contents: '')
							color: Color white;
							morphExtent: 50@20;
							crAction:[self apropos];
							yourself.
	
	toolbar addMorph: aproposInput.
	toolbar addMorph: self createBrowsingAccessBox .
	toolbar addMorph: self createBrowsingHistoryBox.
	toolbar addMorph: self createBackBox.
	toolbar addMorph: self createForwardBox.
	
	model installedPlugins do: [:plugin |
		plugin initializeToolBar: toolbar].
	
	self addMorph: toolbar position: 0@0! !

!BrowserWindowPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:25:54'!
navigateBack
	model navigateBack! !

!BrowserWindowPlus methodsFor: 'navigation' stamp: 'MarianoMontone 6/13/2017 23:26:26'!
navigateForward
	model navigateForward! !

!BrowserWindowPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:46:23'!
fullOnClass: aClass selector: aSelector
	"Open a new full browser set to class."

	| browser |
	browser _ BrowserPlus new.
	browser setClass: aClass selector: aSelector.
	self open: browser label: browser labelString! !

!BrowserWindowPlus class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:21:35'!
open
	^ self openBrowser! !

!BrowserWindowPlus class methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 6/14/2017 09:46:19'!
openBrowser
	| browser |
	browser _ BrowserPlus new.
	^ self open: browser label: browser defaultBrowserTitle! !

!BrowserWindowPlus class methodsFor: 'as yet unclassified' stamp: 'MM 8/12/2021 11:51:35'!
worldMenuForOpenGroup
	^ `{{#itemGroup -> 10.
		  #itemOrder -> 30.
		  #label -> 'BrowserPlus'.
		  #object -> BrowserWindowPlus.
		  #selector -> #openBrowser.
		  #icon 	-> 	#editFindReplaceIcon.
		  #balloonText -> 'A Smalltalk code browser with navigation extensions'
		} asDictionary}`! !

!BrowserPlusPlugin methodsFor: 'menu building' stamp: 'MM 11/16/2019 23:38:01'!
buildWindowMenu: aMenu
	"implement to add plugin specific menus to window menu"! !

!BrowserPlusPlugin methodsFor: 'menu building' stamp: 'MM 11/16/2019 23:09:41'!
classListMenu: aMenu model: aBrowser
	"Implement to extend the class list menu"! !

!BrowserPlusPlugin methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:00:53'!
messageListMenu: aMenu model: aBrowser
	"implement to extend message list menu"! !

!BrowserPlusPlugin methodsFor: 'menu building' stamp: 'MM 11/17/2019 00:53:23'!
systemCategoryMenu: aMenu model: aBrowser
	"Implement to extend the system category menu"! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 12/2/2019 22:07:35'!
classCommentText: aString forClass: aClass
	^ aString! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:20:55'!
initialize
	enabled _ true! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/18/2019 08:22:28'!
initializeToolBar: aLayoutMorph
	"Add buttons to extend the toolbar"! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:29:36'!
installOn: aBrowser
	aBrowser installPlugin: self! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:20:25'!
pluginName
	^ self class pluginName! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:20:14'!
properties
	^ {(AspectProperty on: self aspect: #enabled)
		name: self pluginName, ' enabled';
		type: BooleanPropertyType new;
		category: self pluginName}! !

!BrowserPlusPlugin methodsFor: 'as yet unclassified' stamp: 'MM 12/4/2019 12:08:00'!
textStylerClassFor: textGetter model: aBrowser
	"Overwrite this method to specify a text styler"
	^ nil! !

!BrowserPlusPlugin methodsFor: 'accessing' stamp: 'MM 11/17/2019 00:20:58'!
enabled
	"Answer the value of enabled"

	^ enabled! !

!BrowserPlusPlugin methodsFor: 'accessing' stamp: 'MM 11/17/2019 00:20:58'!
enabled: anObject
	"Set the value of enabled"

	enabled _ anObject! !

!BrowserPlusPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:16:04'!
allInstallable
	^ BrowserPlusPlugin allSubclasses select: [:class | class isAbstract not].! !

!BrowserPlusPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:33:49'!
description
	^ nil! !

!BrowserPlusPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 21:39:14'!
installOn: aBrowser
	^ self new installOn: aBrowser! !

!BrowserPlusPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:52:22'!
pluginName
	^ self subclassResponsibility ! !

!BrowserPlusPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:52:36'!
properties
	^ {(PluggableProperty getBlock: [BrowserPlus isDefaultPluginEnabled: self] setBlock: [:enabled | BrowserPlus toggleDefaultPlugin: self enabled: enabled])
		name: self pluginName, ' enabled';
		category: self pluginName;
		type: BooleanPropertyType new;
		yourself}
		! !

!AutoCategorizerBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 01:02:36'!
autoCategorize: aClass
	(self confirm: 'Autocategorize ', aClass name, '?')
		ifTrue: [AutoCategorizer categorize: aClass]
! !

!AutoCategorizerBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 21:36:31'!
classListMenu: aMenu model: aBrowser
	aBrowser selectedClass ifNotNil: [:aClass |
		aMenu add: 'categorize automatically' target: self action: #autoCategorize: argument: aClass].! !

!AutoCategorizerBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 21:37:47'!
isAbstract
	^ false! !

!AutoCategorizerBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 22:52:55'!
pluginName
	^ 'AutoCategorizer'! !

!ExamplesBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2019 16:21:12'!
messageListMenu: aMenu model: aBrowser

	aBrowser selectedMessageName ifNotNil: [:selector |
		(selector asString includesSubstring: 'Example' caseSensitive: false)
			ifTrue: [aMenu add: 'run example' target: self 
							action: #runExampleMethod:of: 
							argumentList: {selector. aBrowser selectedClass}]]! !

!ExamplesBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 23:59:21'!
runExampleMethod: aSelector of: aClass

	|result|
	
	result _ aClass perform: aSelector.
	(result isKindOf: Morph) 
		ifTrue: [result openInWorld]
		ifFalse: [result inspect]! !

!ExamplesBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 23:57:22'!
isAbstract
	^ false! !

!ExamplesBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:01:53'!
pluginName
	^ 'Examples'! !

!InitializerBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2019 16:24:47'!
classListMenu: aMenu model: aBrowser
	aBrowser selectedClassOrMetaClass ifNotNil: [:aClass |
		((aClass isKindOf: Metaclass) and: [aClass respondsTo: #initialize])
			ifTrue: [
				aMenu add: 'categorize automatically' target: aClass action: #initialize]]! !

!InitializerBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2019 16:20:40'!
messageListMenu: aMenu model: aBrowser

	aBrowser selectedMessageName ifNotNil: [:selector |
		(selector asString includesSubString: 'initialize') 
			ifTrue: [
				aMenu add: 'run ', selector asString target: aBrowser selectedClass
						action: selector]]! !

!InitializerBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2019 16:20:07'!
isAbstract
	^ false! !

!InitializerBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 12/5/2019 09:43:09'!
pluginName
	^ 'Initializer'! !

!NavigationBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 01:07:12'!
isAbstract
	^ false! !

!NavigationBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 01:07:04'!
pluginName
	^ 'Navigation'! !

!UndefinedFinderBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:58:09'!
browseUndefinedOfCategory: aCategory
	(UndefinedFinder runOnSystemCategory: aCategory) browse.! !

!UndefinedFinderBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:40:55'!
browseUndefinedOfClass: aClass
	(UndefinedFinder runOn: aClass) browse.! !

!UndefinedFinderBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:40:45'!
classListMenu: aMenu model: aBrowser
	aBrowser selectedClass ifNotNil: [:aClass |
		aMenu add: 'browse undefined references' target: self action: #browseUndefinedOfClass: argument: aClass].! !

!UndefinedFinderBPPlugin methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:59:09'!
systemCategoryMenu: aMenu model: aBrowser
	aBrowser selectedSystemCategoryName ifNotNil: [:aCategory |
		aMenu add: 'browse undefined references' target: self action: #browseUndefinedOfCategory: argument: aCategory].! !

!UndefinedFinderBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:33:31'!
description
	^ 'A BrowserPlus plugin that adds a menu for browsing undefined references in classes'! !

!UndefinedFinderBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:39:48'!
isAbstract
	^ false! !

!UndefinedFinderBPPlugin class methodsFor: 'as yet unclassified' stamp: 'MM 11/17/2019 00:40:01'!
pluginName
	^ 'Undefined finder'! !

!Class methodsFor: '*BrowserPlus' stamp: 'MM 10/1/2019 12:28'!
buildMenu: aMenu
	"Add class specific menu items to the class menu aMenu"! !

!TestCase class methodsFor: '*BrowserPlus' stamp: 'MM 10/1/2019 12:37'!
buildMenu: aMenu
	aMenu addLine.
	aMenu add: 'run test case' target: self selector: #runInSuite.
	^ aMenu! !
