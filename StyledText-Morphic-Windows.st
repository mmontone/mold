!classDefinition: #FancyDraggeableButtonMorph category: #'StyledText-Morphic-Windows'!
DraggeableButtonMorph subclass: #FancyDraggeableButtonMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!FancyDraggeableButtonMorph commentStamp: '<historical>' prior: 0!
for STE scrollbars!


!FancyDraggeableButtonMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2012 22:14'!
drawOn: aCanvas

	self drawSTELookOn: aCanvas! !

!FancyDraggeableButtonMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:16'!
drawSTELookOn: aCanvas

	aCanvas
		roundRect: (((0@0 extent: extent) insetBy: (owner morphBoundsInWorld isWide ifTrue: [0@4] ifFalse: [4@0])) )
		color: (Color black)
		radius: 4.
	aCanvas
		roundRect: (((-1@0 extent: extent) insetBy: (owner morphBoundsInWorld isWide ifTrue: [0@4] ifFalse: [5@0 corner: 4@0])) )
		color: (self isPressed ifTrue: [Color red] ifFalse: [Color gray: 0.86])
		radius: 4.! !

!FancyDraggeableButtonMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2012 22:37'!
iconColor

	^ self isPressed
		ifTrue: [ Color red ]
		ifFalse: [
			self mouseIsOver
				ifTrue: [ Color gray: 0.75 ]
				ifFalse: [ Color white ]].! !


!classDefinition: #FancyButtonMorph category: #'StyledText-Morphic-Windows'!
PluggableButtonMorph subclass: #FancyButtonMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!FancyButtonMorph commentStamp: '<historical>' prior: 0!
For STE drop down list and fancy scrollbars!


!FancyButtonMorph methodsFor: 'drawing' stamp: 'jmv 8/4/2014 08:39'!
drawOn: aCanvas
			
	self isRoundButton
		ifTrue: [
			aCanvas drawButtonIconFromCurrentMorph ifFalse: [
				self drawRoundGradientLookOn: aCanvas ]]
		ifFalse: [
			self drawSTELookOn: aCanvas.
			aCanvas drawButtonIconFromCurrentMorph ]! !

!FancyButtonMorph methodsFor: 'drawing' stamp: 'jmv 11/30/2014 18:51'!
drawSTELookOn: aCanvas

	aCanvas image: (BitBltCanvas steButtonForm: extent) at:0@0! !

!FancyButtonMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2012 22:37'!
iconColor

	^ self isPressed
		ifTrue: [ Color red ]
		ifFalse: [
			self mouseIsOver
				ifTrue: [ Color gray: 0.75 ]
				ifFalse: [ Color white ]].! !


!FancyButtonMorph methodsFor: 'scrollbar button' stamp: 'KenD 12/4/2015 21:19'!
updateDownButtonImage
	"update the receiver's as a downButton.  put a new image inside"

	icon _ BitBltCanvas buildArrowOfDirection: #down size: ScrollBar scrollbarThickness.
	"##FIXME@@ arrowWithGradientOfDirection: #down."
	actionSelector _ #scrollDown.
	self
		roundButtonStyle: false;
		redrawNeeded! !

!FancyButtonMorph methodsFor: 'scrollbar button' stamp: 'KenD 12/4/2015 21:16'!
updateLeftButtonImage
	"update the receiver's as a downButton.  put a new image inside"

	icon _ BitBltCanvas buildArrowOfDirection: #left size: ScrollBar scrollbarThickness. 
	"@@FIXME@@ arrowWithGradientOfDirection: #left."
	actionSelector _ #scrollUp.
	self
		roundButtonStyle: false;
		redrawNeeded! !

!FancyButtonMorph methodsFor: 'scrollbar button' stamp: 'KenD 12/4/2015 21:20'!
updateRightButtonImage
	"update the receiver's as a downButton.  put a new image inside"

	icon _ BitBltCanvas buildArrowOfDirection: #right size: ScrollBar scrollbarThickness.

	"##FIXME@@ arrowWithGradientOfDirection: #right."
	actionSelector _ #scrollDown.
	self
		roundButtonStyle: false;
		redrawNeeded! !

!FancyButtonMorph methodsFor: 'scrollbar button' stamp: 'KenD 12/4/2015 21:18'!
updateUpButtonImage
	"update the receiver's as a upButton. put a new image inside"

	icon _ BitBltCanvas buildArrowOfDirection: #up size: ScrollBar scrollbarThickness.. 
	"@@FIXME@@ arrowWithGradientOfDirection: #up"
	actionSelector _ #scrollUp.
	self
		roundButtonStyle: false;
		redrawNeeded! !


!classDefinition: #PluggableDropDownListMorph category: #'StyledText-Morphic-Windows'!
PluggableMorph subclass: #PluggableDropDownListMorph
	instanceVariableNames: 'listMorph getListSelector getIndexSelector setIndexSelector label downButton'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!PluggableDropDownListMorph commentStamp: '<historical>' prior: 0!
A widget that shows the current value, and can open the full list for user selection.!


!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 2/25/2013 15:21'!
basicOpenList
	| xtraWidth xtraHeight bounds |
	bounds _ self morphBoundsInWorld.
	listMorph _ PluggableActOnReturnKeyListMorph
		model: self
		listGetter: #getList
		indexGetter: #getIndex
		indexSetter: #setIndex:.
	listMorph
		color: Color white;
		morphWidth: self morphWidth;
		morphHeight: 4;
		borderWidth: 1;
		borderColor: (Color black alpha: 0.3);
		morphPosition: bounds bottomLeft;
		autoDeselect: false.
	self world addMorph: listMorph.
	listMorph updateList.
	xtraWidth _ listMorph hLeftoverScrollRange + 4.
	xtraWidth > 0 ifTrue: [
		listMorph morphWidth: listMorph morphWidth + xtraWidth ].
	xtraHeight _ listMorph vLeftoverScrollRange + 4.
	xtraHeight > 0 ifTrue: [
		listMorph morphHeight: (listMorph morphHeight + xtraHeight min: 100) ]! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 9/10/2010 15:31'!
closeList
	listMorph ifNotNil: [
		listMorph delete.
		listMorph _ nil ]! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 2/14/2013 12:41'!
openList
	self basicOpenList.
	self world activeHand newKeyboardFocus: listMorph! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 8/16/2010 16:01'!
openOrCloseList
	self isListOpen
		ifFalse: [ self openList ]
		ifTrue: [ self closeList ]! !


!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:29'!
drawBasicLookOn: aCanvas

	aCanvas
		fillRectangle: (0@0 extent: extent)
		color: color
		borderWidth: borderWidth
		borderStyleSymbol: #simple
		baseColorForBorder: borderColor.
	self drawLabelOn: aCanvas ! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:33'!
drawLabelOn: aCanvas 

	| f |
	f _ Preferences standardButtonFont.
	aCanvas drawString: label at: 0@(extent y // 2) + (8@ f height negated // 2) font: f color: Color black! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2012 22:19'!
drawOn: aCanvas

	"Theme current steButtons"
	true
		ifTrue: [ self drawSTELookOn: aCanvas ]
		ifFalse: [ self drawBasicLookOn: aCanvas ]! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:31'!
drawSTELookOn: aCanvas
	| gh |
"sin gradiente el borde!!"
	gh _ extent y-8 max: extent y//2.
	aCanvas
		roundRect: (0@0 extent: extent)
		color: (Color gray: 0.4)
		radius: 4
		gradientTop: 0.9
		gradientBottom: 0.6
		gradientHeight: gh.
	aCanvas
		roundRect: (1@1 extent: extent-2)
		color: (Color gray: 0.95)
		radius: 4
		gradientTop: 0.99
		gradientBottom: 0.96
		gradientHeight: gh.
	self drawLabelOn: aCanvas ! !


!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 9/9/2010 15:12'!
getIndex
	^model ifNil: [ 0 ] ifNotNil: [ model perform: getIndexSelector ]! !

!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 9/16/2009 13:45'!
getList
	^model perform: getListSelector! !

!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 6/3/2011 14:43'!
modelChanged
	self getLabel.
	self changed: self! !


!PluggableDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 4/6/2011 19:03'!
getLabel
	| i |
	i _ self getIndex.
	label _ i = 0
		ifTrue: [ '-none-' ]
		ifFalse: [ self getList at: i ].! !

!PluggableDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 9/10/2010 08:37'!
label
	^ label! !


!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 9/9/2010 14:25'!
handlesMouseDown: evt
	"So our #mouseDown: method is called"
	^ true! !

!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 9/9/2010 14:25'!
handlesMouseOver: anEvent
	"So our #mouseLeave: method is called"
	^ true! !

!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 1/27/2011 16:11'!
isListOpen
	^ listMorph notNil! !


!PluggableDropDownListMorph methodsFor: 'initialization' stamp: 'MM 4/29/2016 14:42'!
initialize
	| icon |
	super initialize.
	self color: Color white.
	self borderColor: Color black.
	self getLabel.
"	self extent: 120 @ 20."
	icon _ "Theme current steButtons" true
		ifFalse: [ BitBltCanvas arrowOfDirection: #down size: ScrollBar scrollbarThickness ]
		ifTrue: [ BitBltCanvas buildArrowOfDirection: #down size: ScrollBar scrollbarThickness].
			 "@@FIXME@@ arrowWithGradientOfDirection: #down"
	downButton _ PluggableButtonMorph new.
	downButton
		model: self;
		roundButtonStyle: false;
		icon: icon;
		actWhen: #buttonDown;
		action: #openOrCloseList.
	self addMorph: downButton.! !

!PluggableDropDownListMorph methodsFor: 'initialization' stamp: 'jmv 9/16/2009 11:29'!
model: anObject listGetter: getListSel indexGetter: getSelectionSel indexSetter: setSelectionSel

	self model: anObject.
	getListSelector _ getListSel.
	getIndexSelector _ getSelectionSel.
	setIndexSelector _ setSelectionSel.! !


!PluggableDropDownListMorph methodsFor: 'layout' stamp: 'jmv 12/20/2012 12:57'!
layoutSubmorphs
	| e innerBounds |
	"innerBounds _ self innerBounds".
	innerBounds _ self morphBoundsInWorld insetBy: borderWidth.
	e _ innerBounds height.
	downButton morphBoundsInWorld: (innerBounds bottomRight - e extent: e)! !


!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/20/2012 13:24'!
mouseLeave: evt
	super mouseLeave: evt.
	(listMorph isNil or: [ (listMorph morphBoundsInWorld containsPoint: evt eventPosition) not])
		ifTrue: [
			"Do the call even if the list is not there, as this also clears selection in subclass with entry field"
			self closeList ]! !

!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/27/2012 15:02'!
mouseLeaveList: evt
	(self morphBoundsInWorld containsPoint: evt eventPosition)
		ifFalse: [ self closeList ]! !

!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 6/3/2011 14:44'!
setIndex: index
	model perform: setIndexSelector with: index.
	"self changed: #getIndex." "No chance to actually see it, it is closed too quickly"
	self getLabel.
	self changed: self.
	self closeList! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PluggableDropDownListMorph class' category: #'StyledText-Morphic-Windows'!
PluggableDropDownListMorph class
	instanceVariableNames: ''!

!PluggableDropDownListMorph class methodsFor: 'instance creation' stamp: 'jmv 9/16/2009 11:30'!
model: anObject listGetter: getListSel indexGetter: getSelectionSel indexSetter: setSelectionSel

	^self new
		model: anObject
		listGetter: getListSel
		indexGetter: getSelectionSel
		indexSetter: setSelectionSel! !


!classDefinition: #PluggableFilteringDropDownListMorph category: #'StyledText-Morphic-Windows'!
PluggableDropDownListMorph subclass: #PluggableFilteringDropDownListMorph
	instanceVariableNames: 'editorMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!PluggableFilteringDropDownListMorph commentStamp: '<historical>' prior: 0!
A DropDownList that allows typing in, to filter visible items in the list.!


!PluggableFilteringDropDownListMorph methodsFor: 'private' stamp: 'jmv 12/20/2012 13:22'!
closeList
	"Also clear the selection in the entry field"
	(listMorph notNil and: [ listMorph hasKeyboardFocus ]) ifTrue: [
		self world activeHand newKeyboardFocus: editorMorph ].
	super closeList! !

!PluggableFilteringDropDownListMorph methodsFor: 'private' stamp: 'jmv 11/4/2010 15:42'!
openList

	super openList.
	editorMorph selectAll! !


!PluggableFilteringDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 9/10/2010 08:57'!
drawLabelOn: aCanvas
	"Not needed. Our label is a submorph"! !


!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/20/2010 15:00'!
editorClass
	^TextEditor! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/13/2012 10:41'!
keyStrokeInList: aKeyboardEvent
	"Send to the text anything but Up, Down, Return and Escape"
	(aKeyboardEvent isReturnKey or: [
		(#(27 30 31) includes: aKeyboardEvent keyValue)]) ifFalse: [
			editorMorph keyStroke: aKeyboardEvent ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/13/2012 10:59'!
keyStrokeInText: aKeyboardEvent

	"Handle Return and Escape separatedly"
	aKeyboardEvent isReturnKey ifTrue: [ ^self returnInEditor ].
	aKeyboardEvent keyValue = 27 ifTrue: [ ^self closeList ].

	"Send to the list only Up and Down,"
	self shouldOpenList
		ifTrue: [ self basicOpenList ]
		ifFalse: [
			self shouldCloseList ifTrue: [ self closeList ]].
	listMorph ifNotNil: [
		(#(30 31 ) includes: aKeyboardEvent keyValue)
			ifTrue: [ listMorph keyStroke: aKeyboardEvent ].
		listMorph verifyContents ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/29/2011 14:50'!
lostFocus: aMorph
	(self isListOpen and: [ listMorph hasKeyboardFocus not ]) ifTrue: [
		self closeList ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/16/2011 09:27'!
shouldCloseList
	"True if no matches to show, or if there is no  typed text"
	^self isListOpen and: [
		"Filter empty does not mean we shouldn't show the list. Show it without filtering!!"
		"self filter isEmpty or: [ "
			self getList isEmpty 
		"]"
	]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/16/2011 09:24'!
shouldOpenList
	"True if list not open, but it makes sense to show it, because there is more than one entry that matches the typed text. "
	^self isListOpen not and: [
		"Open full list if filter empty. Why not???"
		"self filter notEmpty and: [ "
			self getList notEmpty "
		]" 
	]! !


!PluggableFilteringDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 7/5/2011 09:02'!
filter
	^editorMorph contents withBlanksTrimmed! !

!PluggableFilteringDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 3/16/2011 17:28'!
getLabel
	super getLabel.
	editorMorph ifNotNil: [ editorMorph contents: label ]! !


!PluggableFilteringDropDownListMorph methodsFor: 'filtering' stamp: 'jmv 9/10/2010 15:56'!
filter: entry with: filter

	^filter, '*' match: entry! !


!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 4/6/2011 19:03'!
getIndex
	"answer the index in the filtered list"
	| i filter |
	i _ super getIndex.
	i = 0 ifFalse: [
		filter _ self filter.
		(filter notEmpty and: [ filter ~= '-none-' ]) ifTrue: [
			i _ self getList indexOf: ((model perform: getListSelector) at: i) ]].
	^i! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 8/10/2011 15:17'!
getList
	| answer filter |
	answer _ super getList.
	filter _ self filter.
	(filter notEmpty and: [ filter ~= '-none-' ]) ifTrue: [ | filtered |
		filtered _ (answer select: [ :str | self filter: str with: filter ]).
		(filtered includes: filter) ifFalse: [
			answer _ filtered ]].
	^answer! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 3/13/2012 10:26'!
returnInEditor
	| filter all selected |
	self isListOpen ifTrue: [
		^ listMorph returnPressed ].

	filter _ self filter.
	filter isEmpty ifTrue: [ ^ self ].
	all _ model perform: getListSelector.
	selected _ all
		detect: [ :any | self filter: any with: filter ]
		ifNone: [ ^ self ].
	^ super setIndex: (super getList indexOf: selected)! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 3/16/2011 09:10'!
setIndex: index
	| i filter |
	i _ index.
	filter _ self filter.
	filter notEmpty ifTrue: [
		i _ (model perform: getListSelector) indexOf: ( self getList at: i ) ].
	super setIndex: i! !


!PluggableFilteringDropDownListMorph methodsFor: 'initialization' stamp: 'jmv 12/27/2012 15:06'!
initialize
	| labelFont |
	super initialize.
	labelFont _ AbstractFont familyName: 'DejaVu' aroundPointSize: 10.
	editorMorph _ FilteringDDLEditorMorph contents: self label font: labelFont emphasis: 1.
	editorMorph keyboardFocusWatcher: self.
	self addMorph: editorMorph! !


!PluggableFilteringDropDownListMorph methodsFor: 'layout' stamp: 'jmv 12/20/2012 13:11'!
layoutSubmorphs
	| b innerBounds |
	super layoutSubmorphs.
	"innerBounds _ self innerBounds".
	innerBounds _ self morphBoundsInWorld insetBy: borderWidth.
	b _ innerBounds insetBy: 8@4.
	editorMorph morphBoundsInWorld: (b topLeft extent: b extent - (downButton morphWidth@0))! !


!classDefinition: #PluggableActOnReturnKeyListMorph category: #'StyledText-Morphic-Windows'!
PluggableListMorph subclass: #PluggableActOnReturnKeyListMorph
	instanceVariableNames: 'currentIndex'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!

!PluggableActOnReturnKeyListMorph methodsFor: 'model access' stamp: 'jmv 3/13/2012 10:57'!
changeModelSelection: anInteger
	"On regular PluggableListMorphs this method is called when a selection is made.
	But we don't want to update the model when the arrow keys are pressed, we'll wait for <Return>
	However we do act immediately on mouse click.
	
	Just store the current index."
	currentIndex _ anInteger.
	self selectionIndex: currentIndex! !

!PluggableActOnReturnKeyListMorph methodsFor: 'model access' stamp: 'jmv 3/13/2012 10:10'!
getCurrentSelectionIndex
	^currentIndex ifNil: [ super getCurrentSelectionIndex ]! !

!PluggableActOnReturnKeyListMorph methodsFor: 'model access' stamp: 'jmv 3/13/2012 10:10'!
realChangeModelSelection
	"Change the model's selected item index to be anInteger."
	setIndexSelector ifNotNil: [
		model
			perform: setIndexSelector
			with: currentIndex ].
	currentIndex _ nil! !


!PluggableActOnReturnKeyListMorph methodsFor: 'geometry' stamp: 'jmv 4/14/2013 22:00'!
clippingRect
	"Return the bounds to which any submorphs should be clipped if the property is set"
	"Should be a region, like our shadow"
	| r |
	r _ super clippingRect.
	^r origin extent: r extent - 4! !

!PluggableActOnReturnKeyListMorph methodsFor: 'geometry' stamp: 'jmv 2/25/2013 15:19'!
updateScrollBarsBounds
	
	| t |
	hideScrollBars ifTrue: [^self].
	t _ self scrollBarClass scrollbarThickness.
	scrollBar
		morphPosition: extent x - t - borderWidth - 4 @ borderWidth;
		morphExtent: t @ (self vScrollBarHeight-4).
	hScrollBar
		morphPosition: borderWidth @ (extent y - t - borderWidth - 4);
		morphExtent: self hScrollBarWidth - 4@t! !

!PluggableActOnReturnKeyListMorph methodsFor: 'geometry' stamp: 'jmv 2/25/2013 15:26'!
viewableExtent

	^self focusIndicatorExtent - (self xtraBorder * 2) - 4! !


!PluggableActOnReturnKeyListMorph methodsFor: 'drawing' stamp: 'jmv 2/25/2013 15:18'!
drawOn: aCanvas
	"We draw our shadow outside our strict bounds..."

	aCanvas
		roundRect: (4@4 extent: extent-4)
		color: (Color black alpha: 0.13)
		radius: 4.
	aCanvas
		roundRect: (0@0 extent: extent-4)
		color: (Color gray: 0.4)
		radius: 4.
	aCanvas
		roundRect: (1@1 extent: extent-2-4)
		color: (Color white)
		radius: 4! !


!PluggableActOnReturnKeyListMorph methodsFor: 'events' stamp: 'jmv 12/27/2012 15:02'!
keyStroke: event 

	event isReturnKey ifTrue: [
		self returnPressed.
		^self ].
	super keyStroke: event.
	
	"We know our model is a PluggableDropDownListMorph"
	model keyStrokeInList: event! !

!PluggableActOnReturnKeyListMorph methodsFor: 'events' stamp: 'jmv 2/14/2013 12:46'!
mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition
	"Do update model right away"
	super mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition.
	self realChangeModelSelection! !

!PluggableActOnReturnKeyListMorph methodsFor: 'events' stamp: 'jmv 12/27/2012 15:02'!
mouseLeave: evt
	"We know our model is a PluggableDropDownListMorph"
	model mouseLeaveList: evt! !

!PluggableActOnReturnKeyListMorph methodsFor: 'events' stamp: 'jmv 3/13/2012 10:10'!
returnPressed
	currentIndex
		ifNotNil: [ self realChangeModelSelection ]
		ifNil: [ self delete ]! !


!PluggableActOnReturnKeyListMorph methodsFor: 'initialization' stamp: 'MM 4/29/2016 14:41'!
scrollBarClass
	^ScrollBar! !


!PluggableActOnReturnKeyListMorph methodsFor: 'updating' stamp: 'jmv 3/13/2012 10:10'!
verifyContents
	super verifyContents.
	"ok???"
	self selectionIndex = 0 ifTrue: [
		self changeModelSelection: 1 ]! !


!classDefinition: #PluggableStyledTextMorph category: #'StyledText-Morphic-Windows'!
TextModelMorph subclass: #PluggableStyledTextMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!PluggableStyledTextMorph commentStamp: '<historical>' prior: 0!
To be used with StyledTextEditors!


!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 8/3/2011 17:04'!
characterStyleNamesAndShortcuts
	^model styleSet characterStyleNamesAndShortcuts! !

!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 11/16/2011 17:12'!
currentCharacterStyleIndex
	| cs |
	cs _ self textMorph editor currentCharacterStyleOrNil.
	cs ifNil: [ ^0 ].
	^(model styleSet characterStyleIndexOf: cs)! !

!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 10/16/2013 19:54'!
currentCharacterStyleIndex: index
	"This is a user command, and generates undo"
	(model styleSet characterStyleAt: index)
		ifNil: [
			self textMorph editor removeCharacterStyles ]
		ifNotNil: [ :style |
			style isNullStyle
				ifTrue: [ self textMorph editor removeCharacterStyles ]
				ifFalse: [ self textMorph editor applyAttribute: (CharacterStyleReference for: style) ]].
	self textMorph updateFromTextComposition! !

!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 11/16/2011 17:12'!
currentParagraphStyleIndex
	^model styleSet paragraphStyleIndexOf: self textMorph editor currentParagraphStyle! !

!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 10/16/2013 19:54'!
currentParagraphStyleIndex: index
	"This is a user command, and generates undo"
	| style |
	style _ model styleSet paragraphStyleAt: index.
	self textMorph editor applyAttribute: (ParagraphStyleReference for: style).
	self textMorph updateFromTextComposition! !

!PluggableStyledTextMorph methodsFor: 'services' stamp: 'jmv 8/3/2011 17:03'!
paragraphStyleNamesAndShortcuts
	^model styleSet paragraphStyleNamesAndShortcuts! !


!PluggableStyledTextMorph methodsFor: 'notifications' stamp: 'jmv 9/18/2009 14:06'!
possiblyChanged
	self triggerEvent: #possiblyChanged! !


!PluggableStyledTextMorph methodsFor: 'initialization' stamp: 'jmv 4/12/2012 22:06'!
scrollBarClass
	^FancyScrollBar! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PluggableStyledTextMorph class' category: #'StyledText-Morphic-Windows'!
PluggableStyledTextMorph class
	instanceVariableNames: ''!

!PluggableStyledTextMorph class methodsFor: 'class initialization' stamp: 'MM 4/29/2016 13:44'!
initialize
	"
	PluggableStyledTextMorph initialize
	"
	"TheWorldMenu addExtraOpenCommands: {
		{'Styled Text Editor' . {#theWorldMenu . #openFancierStyledTextEditor}. 'A window for composing styled text' }.
"		{'Styled Text Editor' . {#theWorldMenu . #openStyledTextEditor}. 'A window for composing styled text' }."
	}."
	Theme current class beCurrent! !

!PluggableStyledTextMorph class methodsFor: 'class initialization' stamp: 'jmv 8/13/2013 12:24'!
withModel: aStyledTextModel in: aLayoutMorph
	| topRow paragraphStyleList characterStyleList textMorph m topRowHeight labelFont topRowElementsWidth |
	textMorph _ self withModel: aStyledTextModel.
	textMorph
		borderWidth: 0;
		drawKeyboardFocusIndicator: false;
		wrapFlag: true.
	aLayoutMorph separation: 0.
	topRow _ ToolbarMorph newRow separation: 10@4.

	topRowHeight _ 32.
	topRow
		color: (Form 
			verticalGradient: topRowHeight
			topColor:  (Color r: 189 g: 214 b: 199 range: 255)
			bottomColor:  (Color r: 115 g: 134 b: 125 range: 255))..
"		borderWidth: topRowBorderWidth;"
"		borderColor: (Color r: 80 g: 80 b: 80 range: 255)."

	labelFont _ AbstractFont familyName: 'DejaVu' aroundPointSize: 10.
	m _ LabelMorph contents: 'Paragraph Style'.
	m font: labelFont emphasis: 1; color: Color white.
	topRowElementsWidth _ m morphWidth.
	topRow
		addMorph: m
		layoutSpec: (((LayoutSpec morphHeightFixedWidth: topRowElementsWidth) minorDirectionPadding: #center) minorDirectionPadding: #center).
	paragraphStyleList _ PluggableFilteringDropDownListMorph
			model: textMorph
			listGetter: #paragraphStyleNamesAndShortcuts
			indexGetter: #currentParagraphStyleIndex
			indexSetter: #currentParagraphStyleIndex:.
	paragraphStyleList borderWidth: 0"; height: ddlHeight".
	topRow
		addMorph: paragraphStyleList
		layoutSpec: (((LayoutSpec morphHeightFixedWidth: topRowElementsWidth+50) minorDirectionPadding: #center) minorDirectionPadding: #center).
	textMorph when: #possiblyChanged send: #modelChanged to: paragraphStyleList.

	m _ BoxedMorph new.
	m color: Color transparent.
	topRow addMorph: m layoutSpec: (LayoutSpec fixedWidth: 8).

	m _ LabelMorph contents: 'Character Style'.
	m font: labelFont emphasis: 1; color: Color white.
	topRow
		addMorph: m
		layoutSpec: (((LayoutSpec morphHeightFixedWidth: topRowElementsWidth) minorDirectionPadding: #center) minorDirectionPadding: #center).
	characterStyleList _ PluggableFilteringDropDownListMorph 
			model:textMorph
			listGetter: #characterStyleNamesAndShortcuts
			indexGetter: #currentCharacterStyleIndex
			indexSetter: #currentCharacterStyleIndex:.
	characterStyleList borderWidth: 0"; height: ddlHeight". 
	topRow
		addMorph: characterStyleList
		layoutSpec: (((LayoutSpec morphHeightFixedWidth: topRowElementsWidth+50) minorDirectionPadding: #center) minorDirectionPadding: #center).
	textMorph when: #possiblyChanged send: #modelChanged to: characterStyleList.

	aLayoutMorph
		addMorph: topRow layoutSpec: (LayoutSpec fixedHeight: topRowHeight);
		addMorph: textMorph layoutSpec: (LayoutSpec new).

	^aLayoutMorph! !


!classDefinition: #FancyScrollBar category: #'StyledText-Morphic-Windows'!
ScrollBar subclass: #FancyScrollBar
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!FancyScrollBar commentStamp: '<historical>' prior: 0!
A Fancy Scrollbar for STE.!


!FancyScrollBar methodsFor: 'initialization' stamp: 'jmv 4/12/2012 22:12'!
buttonClass
	^FancyButtonMorph! !

!FancyScrollBar methodsFor: 'initialization' stamp: 'jmv 4/12/2012 22:23'!
sliderClass
	^FancyDraggeableButtonMorph! !


!FancyScrollBar methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:19'!
drawOn: aCanvas

	aCanvas
		roundRect: (0@0 extent: extent)
		color: (Color gray: 0.4)
		radius: 4.
	aCanvas
		roundRect: (1@1 extent: extent-2)
		color: (Color gray: 0.95)
		radius: 4! !


!FancyScrollBar methodsFor: 'scrolling' stamp: 'jmv 3/5/2013 08:20'!
sliderGrabbedAt: handPositionRelativeToSlider

	grabPosition _ handPositionRelativeToSlider.
	sliderShadow
		morphBoundsInWorld: (slider morphBoundsInWorld insetBy: (extent x > extent y ifTrue: [0@3] ifFalse: [3@0]));
		show! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FancyScrollBar class' category: #'StyledText-Morphic-Windows'!
FancyScrollBar class
	instanceVariableNames: ''!

!FancyScrollBar class methodsFor: 'constants' stamp: 'jmv 8/13/2013 12:29'!
scrollbarThickness

	^super scrollbarThickness + 11! !


!classDefinition: #ToolbarMorph category: #'StyledText-Morphic-Windows'!
LayoutMorph subclass: #ToolbarMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!

!ToolbarMorph methodsFor: 'hacks' stamp: 'jmv 8/4/2014 08:37'!
adoptWidgetsColor: paneColor
	"We hold a Form in the color instance variable..."! !


!ToolbarMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2013 20:23'!
drawOn: aCanvas
	aCanvas
		fillRectangle: (0@0 extent: self morphExtent)
		tilingWith: color
		sourceRect: color boundingBox
		rule: Form paint! !


!classDefinition: #FilteringDDLEditorMorph category: #'StyledText-Morphic-Windows'!
OneLineEditorMorph subclass: #FilteringDDLEditorMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!

!FilteringDDLEditorMorph methodsFor: 'events' stamp: 'MM 4/29/2016 14:39'!
keyStroke: aKeyboardEvent
	"Handle a keystroke event."

	(self focusKeyboardFor: aKeyboardEvent)
		ifTrue: [ ^ self ].
		
	"(self closeWindowFor: aKeyboardEvent)
		ifTrue: [ ^ self ]."

	super keyStroke: aKeyboardEvent.

	"We know that our owner is a PluggableFilteringDropDownListMorph"
	owner keyStrokeInText: aKeyboardEvent! !

PluggableStyledTextMorph initialize!