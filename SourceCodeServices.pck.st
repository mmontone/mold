'From Cuis 5.0 [latest update: #3958] on 18 November 2019 at 9:22:33 am'!
'Description Services to process Smalltalk code.

Find undefined references to undeclared variables and not existent messages.'!
!provides: 'SourceCodeServices' 1 3!
SystemOrganization addCategory: #SourceCodeServices!


!classDefinition: #UndefinedFinder category: #SourceCodeServices!
ParseNodeVisitor subclass: #UndefinedFinder
	instanceVariableNames: 'methods undefined currentUndefined currentMethod'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SourceCodeServices'!
!classDefinition: 'UndefinedFinder class' category: #SourceCodeServices!
UndefinedFinder class
	instanceVariableNames: ''!

!classDefinition: #UndefinedFinderTest category: #SourceCodeServices!
TestCase subclass: #UndefinedFinderTest
	instanceVariableNames: 'foo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SourceCodeServices'!
!classDefinition: 'UndefinedFinderTest class' category: #SourceCodeServices!
UndefinedFinderTest class
	instanceVariableNames: ''!

!classDefinition: #AutoCategorizer category: #SourceCodeServices!
Object subclass: #AutoCategorizer
	instanceVariableNames: 'rules class'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SourceCodeServices'!
!classDefinition: 'AutoCategorizer class' category: #SourceCodeServices!
AutoCategorizer class
	instanceVariableNames: ''!


!UndefinedFinder commentStamp: '<historical>' prior: 0!
Finds undefined variables and messages.

Use:

(UndefinedFinder runOnSystemCategory: #SourceCodeServices) browse.

(UndefinedFinder runOn: UndefinedFinderTest) browse.

(UndefinedFinder runOnPackage: #Mold) browse.!

!AutoCategorizer commentStamp: '<historical>' prior: 0!
Categorizes the given class methods automatically.

Usage:

AutoCategorizer categorize: MyClass

How it works:

It goes through methods not categorized and tries to find a category for them (methods already categorized are untouched.)

First, sees if the method is categorized up the hierarchy chain, and uses that category.
Then, tries to categorize the methods applying the first matching rule in rules instVar.
Finally, if no category could be found, looks at the caller methods and extracts the category from them.!

!UndefinedFinder methodsFor: 'accessing' stamp: 'MM 11/16/2019 13:23:14'!
methods
	"Answer the value of methods"

	^ methods! !

!UndefinedFinder methodsFor: 'accessing' stamp: 'MM 11/16/2019 13:23:14'!
methods: anObject
	"Set the value of methods"

	methods _ anObject! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:14:09'!
browse
	MessageSetWindow open:  (MessageSet messageList: self messageList)! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:09:59'!
initialize
	undefined _ Dictionary new.
	currentUndefined _ OrderedCollection new! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:19:58'!
messageList
	^ undefined keys! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 16:55:17'!
run
	methods collect: [:compiledMethod | 
		currentUndefined _ OrderedCollection new.
		currentMethod _ compiledMethod methodReference.
		compiledMethod createMethodNode accept: self.
		currentUndefined ifNotEmpty: [
			undefined at: compiledMethod methodReference put: currentUndefined]].
	
	^ undefined! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 16:55:43'!
visitLiteralVariableNode: aNode
	aNode index ifNil: [
		aNode name first isUppercase ifTrue: [
			Smalltalk at: aNode name ifPresent: [:x |
				currentUndefined add: aNode]]
		ifFalse: [
			(currentMethod actualClass allInstVarNames includes: aNode name)
				ifFalse: [currentUndefined add: aNode]]]! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:10:38'!
visitMessageNode: aNode
	(Symbol hasInternedAndImplemented: aNode selectorSymbol)
		ifFalse: [currentUndefined add: aNode]! !

!UndefinedFinder methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:10:54'!
visitUndeclaredVariableNode: aNode
	currentUndefined add: aNode! !

!UndefinedFinder class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:13:39'!
runOn: aClass
	^ self new methods: aClass methodDict values; run; yourself! !

!UndefinedFinder class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:14:21'!
runOnPackage: aSymbol
	|categories methods |
	
	categories _ SystemOrganization categoriesMatching: aSymbol asString, '*'.
	methods _ OrderedCollection new.
	categories do: [:cat |
		(SystemOrganization classesAt: cat) do: [:class |
			methods addAll: class methodDict values]].
	
	^ self new
		methods: methods;
		run;
		yourself! !

!UndefinedFinder class methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 15:14:26'!
runOnSystemCategory: aSymbol
	^ self new
		methods: (((SystemOrganization classesAt: aSymbol)
			collect: [:class | class methodDict values]) inject: {} into: [:c :xs | c , xs]);
		run;
		yourself! !

!UndefinedFinderTest methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 13:47:30'!
undefMessage
	self foo! !

!UndefinedFinderTest methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2019 14:39:03'!
undefVar
	foo + bar
	! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:15:15'!
categorize
	self categorizeByInheritance.
	self categorizeByRules.
	self categorizeByCalls.! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:55:02'!
categorizeByCalls
	self uncategorizedClassMethods do: [:m | |calls cm|
		calls _ self localCallsOn: m.
		cm _ calls detect: [:im | self isClassified: im] ifNone: [nil].
		cm ifNotNil: [
			self classify: m under: cm category]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:34:39'!
categorizeByInheritance
	self uncategorizedClassMethods do: [:m | |chain cm|
		chain _ self methodHierarchyOf: m.
		cm _ chain detect: [:im | self isClassified: im] ifNone: [nil].
		cm ifNotNil: [
			self classify: m under: cm category]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 19:00:32'!
categorizeByRules
	self uncategorizedClassMethods do: [:m | |rule|
		rule _ self rules detect: [:r | r key value: m] ifNone: [nil].
		rule ifNotNil: [
			self classify: m under: rule value]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 17:30:04'!
classMethods
	^ class organization allMethodSelectors collect: [:selector |
		MethodReference class: class selector: selector] ! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2019 09:17:38'!
defaultRules
	^ OrderedCollection new
		add: [:m | m selector asString beginsWith: 'initialize'] -> #initialization;
		add: [:m | m selector asString beginsWith: 'print'] -> #printing;
		add: [:m | m selector asString beginsWith: 'draw'] -> #drawing;
		add: [:m | m selector asString beginsWith: 'is'] -> #testing;
		add: [:m | #(mouseButton1Activity. mouseButton2Activity) includes: m selector] -> #'event handling';
		yourself! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:55:13'!
localCallsOn: aMethod
	^ class allLocalCallsOn: aMethod selector! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:35:03'!
methodHierarchyOf: aMethod

	|list|
	
	list _ OrderedCollection new.
	
	class allSuperclasses reverseDo: [ :cl |
		(cl includesSelector: aMethod selector) ifTrue: [
			list addLast: (MethodReference class: cl selector: aMethod selector)]].
	
	^ list! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:08:12'!
rules
	^ rules ifNil: [rules _ self defaultRules]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:30:08'!
uncategorizedClassMethods
	^ self classMethods select: [:m | |cat|
		cat _ m category.
		cat isNil or: [cat = #'as yet unclassified']]! !

!AutoCategorizer methodsFor: 'initialization' stamp: 'MM 11/6/2019 17:55:09'!
initialize: aClass
	class _ aClass! !

!AutoCategorizer methodsFor: 'testing' stamp: 'MM 11/6/2019 18:32:01'!
isClassified: aMethod
	^ (aMethod category = nil or: [aMethod category = #'as yet unclassified']) not! !

!AutoCategorizer methodsFor: 'accessing method dictionary' stamp: 'MM 11/6/2019 18:47:58'!
classify: aMethod under: aCategory
	aMethod actualClass organization classify: aMethod selector under: aCategory.
	Transcript show: 'Classified '; show: aMethod; show: ' under: '; show: aCategory; newLine.! !

!AutoCategorizer class methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 17:06:48'!
categorize: aClass
	^ (self new initialize: aClass) categorize! !
