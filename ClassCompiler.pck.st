'From Cuis 5.0 [latest update: #4030] on 17 February 2020 at 2:38:58 pm'!
'Description Domain Specific Languages via the compilerClass protocol.

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'ClassCompiler' 1 4!
!requires: 'PetitParser' 1 3 nil!
SystemOrganization addCategory: #ClassCompiler!


!classDefinition: #ClassCompiler category: #ClassCompiler!
Compiler subclass: #ClassCompiler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompiler'!
!classDefinition: 'ClassCompiler class' category: #ClassCompiler!
ClassCompiler class
	instanceVariableNames: ''!

!classDefinition: #CCParser category: #ClassCompiler!
Object subclass: #CCParser
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompiler'!
!classDefinition: 'CCParser class' category: #ClassCompiler!
CCParser class
	instanceVariableNames: 'definition'!

!classDefinition: #CompiledClass category: #ClassCompiler!
Object subclass: #CompiledClass
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompiler'!
!classDefinition: 'CompiledClass class' category: #ClassCompiler!
CompiledClass class
	instanceVariableNames: ''!

!classDefinition: #DAO category: #ClassCompiler!
Object subclass: #DAO
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompiler'!
!classDefinition: 'DAO class' category: #ClassCompiler!
DAO class
	instanceVariableNames: ''!

!classDefinition: #SmalltalkClass category: #ClassCompiler!
Object subclass: #SmalltalkClass
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompiler'!
!classDefinition: 'SmalltalkClass class' category: #ClassCompiler!
SmalltalkClass class
	instanceVariableNames: ''!


!DAO commentStamp: '<historical>' prior: 0!
DAO named: Country
	extends: DAO
	fields: {{#id -> #type -> DBSerial. #dbName -> 'id'. #accessors -> true}.
	             {#ok -> #type -> DBBoolean. #accessors -> true. #default -> false. #description -> 'Set to true if ...'}}
	keys: {#id}
	tableName: 'countries'!

!SmalltalkClass commentStamp: '<historical>' prior: 0!
SmalltalkClass named: #MyClass
	extends: Object
	slots: {{#name -> {#type -> String. #public -> true. #default -> nil}}.
	           {#age -> {#type -> Integer. #public -> false}}.
	           {#color -> {#type -> Color. #public -> true. #default -> Color blue}},
	           {#style -> {#type -> String. #setter -> #style:.#getter -> #style. #description -> 'Use this for styling'}}.
	           {#foo -> {#slotClass -> TracedSlot. #description -> 'This slot value is traced'}}}
	category: 'ClassCompiler'.
	
!

!CCParser class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 09:34:47'!
compilerClass
	^ ClassCompiler! !

!CCParser class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 09:45:06'!
definition
	(self inheritsFrom: CCParser) ifFalse: [^ super definition].
	"If start is nil, then the class is not finalized. We have to return a normal definition. 
	The problem with this approach is that the incorrect, normal, definition is logged. "
	self start isNil ifTrue: [^ super definition].
	^ 'CCParser named: #{1} 
	extends: {2} 
	start: #{3} 
	category: ''{4}''' format: {self name.
				self superclass.
				self start.
				self category}! !

!ClassCompiler class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 10:32:22'!
doc
	"Idea to process and compile source code from class definitions:
	[^ lala foo: 'bar'] method methodNode block code.
	
	For example:
	
	PetitParser named: #MyParser
	    startRule: [myRule end].
	
	The source code of the [myRule end] can be accessed and code generated with it.
	Otherwise, just use a string of code as startRule input."! !

!CCParser class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 09:45:13'!
named: aSymbol extends: aClass start: startRule category: aCategory
	
	"This is an example of how it is possible to have special syntax for classy things definitions."
	"There's a problem with this, unfortunatly: the text used for initializing the class (CCParser), is lost, because it is not saved anywhere in ClassDescription.
	ClassDescription>>definition. Would it be possible to change that? 
	Perhaps implementing a ClassDescription subclass with an instance variable with description, and call a new ClassBuilder class for building.
	For the moment we've done it redifining #definition method in the superclass (see CCParser>>definition)"
	
	"Define the class"
	aClass subclass: aSymbol
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: aCategory.
		
	"Compile the start method"
	(Smalltalk at: aSymbol) class 
		compile: ('start
		   ^ #{1}' format: {startRule})
		classified: 'initialization'
		notifying: nil.
		
	! !

!CCParser class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 09:27:21'!
start
	^ nil! !

!CompiledClass class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 10:00:09'!
definitionFor: aSymbol extends: aClass compilerClass: compilerClass category: aCategory
	^ 'CompiledClass named: #{1}
	extends: {2}
	compilerClass: {3}
	category: ''''{4}''''' format: {aSymbol. aClass. compilerClass. aCategory}! !

!CompiledClass class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 09:51:06'!
named: aSymbol extends: aClass compilerClass: compilerClass category: aCategory
	
	"This is an example of how it is possible to have special syntax for classy things definitions."
	"There's a problem with this, unfortunatly: the text used for initializing the class (CCParser), is lost, because it is not saved anywhere in ClassDescription.
	ClassDescription>>definition. Would it be possible to change that? 
	Perhaps implementing a ClassDescription subclass with an instance variable with description, and call a new ClassBuilder class for building.
	For the moment we've done it redifining #definition method in the superclass (see CCParser>>definition)"
	
	|class|
	
	"Define the class"
	aClass subclass: aSymbol
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: aCategory.
		
	class _ (Smalltalk at: aSymbol) class.
		
	"Compile the compilerClass method"
	class
		compile: ('compilerClass
		   ^ {1}' format: {compilerClass})
		classified: 'initialization'
		notifying: nil.
		
	"Register the class definition"
	class
		compile: ('definition
		    ^''{1}''' format: {self definitionFor: aSymbol extends: aClass compilerClass: compilerClass category: aCategory})
		classified: 'initialization'
		notifying: nil! !
