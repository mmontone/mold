'From Cuis 5.0 [latest update: #5007] on 7 January 2022 at 9:42:34 am'!
'Description '!
!provides: 'CollectionExplorer' 1 0!
SystemOrganization addCategory: 'CollectionExplorer-FileSystem'!
SystemOrganization addCategory: 'CollectionExplorer-ODATA'!
SystemOrganization addCategory: 'CollectionExplorer-RESTAPI'!
SystemOrganization addCategory: 'CollectionExplorer-Database'!
SystemOrganization addCategory: 'CollectionExplorer'!


!classDefinition: #CollectionExplorerWindow category: 'CollectionExplorer'!
SystemWindow subclass: #CollectionExplorerWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer'!
!classDefinition: 'CollectionExplorerWindow class' category: 'CollectionExplorer'!
CollectionExplorerWindow class
	instanceVariableNames: ''!

!classDefinition: #CEFileSystemEntry category: 'CollectionExplorer-FileSystem'!
Object subclass: #CEFileSystemEntry
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-FileSystem'!
!classDefinition: 'CEFileSystemEntry class' category: 'CollectionExplorer-FileSystem'!
CEFileSystemEntry class
	instanceVariableNames: ''!

!classDefinition: #CEFileSystemDirectory category: 'CollectionExplorer-FileSystem'!
CEFileSystemEntry subclass: #CEFileSystemDirectory
	instanceVariableNames: 'directoryEntry'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-FileSystem'!
!classDefinition: 'CEFileSystemDirectory class' category: 'CollectionExplorer-FileSystem'!
CEFileSystemDirectory class
	instanceVariableNames: ''!

!classDefinition: #CEFileSystemFile category: 'CollectionExplorer-FileSystem'!
CEFileSystemEntry subclass: #CEFileSystemFile
	instanceVariableNames: 'fileEntry'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-FileSystem'!
!classDefinition: 'CEFileSystemFile class' category: 'CollectionExplorer-FileSystem'!
CEFileSystemFile class
	instanceVariableNames: ''!

!classDefinition: #ODATAEntity category: 'CollectionExplorer-ODATA'!
Object subclass: #ODATAEntity
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-ODATA'!
!classDefinition: 'ODATAEntity class' category: 'CollectionExplorer-ODATA'!
ODATAEntity class
	instanceVariableNames: ''!

!classDefinition: #ODATAEntitySet category: 'CollectionExplorer-ODATA'!
Object subclass: #ODATAEntitySet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-ODATA'!
!classDefinition: 'ODATAEntitySet class' category: 'CollectionExplorer-ODATA'!
ODATAEntitySet class
	instanceVariableNames: ''!

!classDefinition: #RESTAPIResource category: 'CollectionExplorer-RESTAPI'!
Object subclass: #RESTAPIResource
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-RESTAPI'!
!classDefinition: 'RESTAPIResource class' category: 'CollectionExplorer-RESTAPI'!
RESTAPIResource class
	instanceVariableNames: ''!

!classDefinition: #RESTAPICollection category: 'CollectionExplorer-RESTAPI'!
RESTAPIResource subclass: #RESTAPICollection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-RESTAPI'!
!classDefinition: 'RESTAPICollection class' category: 'CollectionExplorer-RESTAPI'!
RESTAPICollection class
	instanceVariableNames: ''!

!classDefinition: #RESTAPISingleResource category: 'CollectionExplorer-RESTAPI'!
RESTAPIResource subclass: #RESTAPISingleResource
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-RESTAPI'!
!classDefinition: 'RESTAPISingleResource class' category: 'CollectionExplorer-RESTAPI'!
RESTAPISingleResource class
	instanceVariableNames: ''!

!classDefinition: #CEDatabaseRow category: 'CollectionExplorer-Database'!
Object subclass: #CEDatabaseRow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-Database'!
!classDefinition: 'CEDatabaseRow class' category: 'CollectionExplorer-Database'!
CEDatabaseRow class
	instanceVariableNames: ''!

!classDefinition: #CEDatabaseTable category: 'CollectionExplorer-Database'!
Object subclass: #CEDatabaseTable
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer-Database'!
!classDefinition: 'CEDatabaseTable class' category: 'CollectionExplorer-Database'!
CEDatabaseTable class
	instanceVariableNames: ''!

!classDefinition: #CollectionExplorer category: 'CollectionExplorer'!
Object subclass: #CollectionExplorer
	instanceVariableNames: 'collection metamodel'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CollectionExplorer'!
!classDefinition: 'CollectionExplorer class' category: 'CollectionExplorer'!
CollectionExplorer class
	instanceVariableNames: ''!


!CEFileSystemEntry class methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 19:07:49'!
metamodel
	^ MEMetaModel new
		attribute: #name type: String;
		attribute: #mimeType type: String;
		attribute: #creationDate type: String;
		attribute: #modificationDate type: String;
		yourself! !

!CEFileSystemDirectory methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 19:00:18'!
metamodel
	^ self class metamodel! !

!CEFileSystemDirectory class methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 19:04:56'!
metamodel

	^ MEMetaModel new
		attribute: #name type: String;
		attribute: #elements type: #composition
			properties: [:attr |
				attr type associatedModel: CEFileSystemEntry;
					multiplicity: #many];
		yourself! !

!CollectionExplorer methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 18:55:59'!
initialize: aCollection metamodel: aMetaModel

	collection _ aCollection.
	metamodel _ aMetaModel.! !

!CollectionExplorer class methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 18:57:14'!
on: aCollection

	^ self on: aCollection metamodel: aCollection metamodel! !

!CollectionExplorer class methodsFor: 'as yet unclassified' stamp: 'MM 12/21/2021 18:55:36'!
on: aCollection metamodel: aMetaModel

	^ self new initialize: aCollection metamodel: aMetaModel! !
