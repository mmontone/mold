'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 18 September 2016 at 2:38:12.011232 pm'!

!SHTextStylerOMeta2 methodsFor: 'private' stamp: 'MM 9/18/2016 14:37'!
privateStyle
	| ranges |
	ranges := [
	useOMetaStyler := true.
	OMeta2ExtendedParser rangesFor: formattedText ]
		on: OM2Fail
		do: [ ].
 	ranges ifNil: [
		ranges := smalltalkFallbackStyler
			textModel: textModel;
			formattedAndStyledText;
			rangesIn: textModel actualContents setWorkspace: true.
		
		useOMetaStyler := false ].
	ranges ifNotNil: [
		self
			setAttributesIn: formattedText
			fromRanges: ranges
			in: nil ].! !
