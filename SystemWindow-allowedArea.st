'From Cuis 6.0 [latest update: #5832] on 5 June 2023 at 3:35:14 pm'!

!SystemWindow methodsFor: 'resize/collapse' stamp: 'MM 6/5/2023 15:34:37'!
allowedArea

	|allowedArea|
	
	allowedArea := Display boundingBox.
	
	self runningWorld taskbar ifNotNil: [ :tb |
			tb displayBounds ifNotNil: [ :r |
				allowedArea := (allowedArea areasOutside: r) first ]].
		
	^ allowedArea
	
	"^(RealEstateAgent maximumUsableAreaInWorld: self world) insetBy: Theme current fullScreenDeskMargin"! !
