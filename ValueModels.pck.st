'From Cuis 6.0 [latest update: #5090] on 26 February 2022 at 9:44:56 pm'!
'Description ValueModels for Cuis Smalltalk.

"Value Model" is the name of a framework in VisualWorks. It is implemented by the ValueModel hierarchy, but also has some related classes outside that hierarchy like SelectionInList and DependencyTransformer. ValueModel itself is an abstract class; you actually use instances of ValueModel''s concrete subclasses.

A ValueModel has two main characteristics:

    Its aspect is always value. This means that its getter message, setter message, and update aspect are value, value:, and #value respectively.
    It informs its dependents whenever its value changes. It has a standard mechanism for registering interest in the value, and those objects that have done so will be notified in a standard way.

This provides a simple, generic interface between any object (a value) and a value-based object (such as a visual widget that displays a single value). The value and the value-based object do not have to be customized for each other; the ValueModel does this by connecting the two and translating the interactions between them as necessary. Thus the value-based object neither knows nor cares where the value comes from or how to access it. The object is able to simply send value to its ValueModel, which in turn does whatever is necessary to obtain the value, then returns it.

If the value changes, it notifies the widget. This is the case even when multiple widgets share the same value; when one widget changes the value, it does not have to perform any notification because the ValueModel does so. In this way, the widget simply uses the value as it sees necessary and does not have to worry about what the consequences might be to anyone else who might also be using the value.

See http://c2.com/ppr/vmodels.html'!
!provides: 'ValueModels' 1 19!
SystemOrganization addCategory: 'ValueModels'!


!classDefinition: #ValueModel category: 'ValueModels'!
Object subclass: #ValueModel
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'ValueModel class' category: 'ValueModels'!
ValueModel class
	instanceVariableNames: ''!

!classDefinition: #AspectAdaptor category: 'ValueModels'!
ValueModel subclass: #AspectAdaptor
	instanceVariableNames: 'subject getter setter subjectSendsUpdates'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'AspectAdaptor class' category: 'ValueModels'!
AspectAdaptor class
	instanceVariableNames: ''!

!classDefinition: #ListValueModel category: 'ValueModels'!
ValueModel subclass: #ListValueModel
	instanceVariableNames: 'listIndex list'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'ListValueModel class' category: 'ValueModels'!
ListValueModel class
	instanceVariableNames: ''!

!classDefinition: #PluggableAdaptor category: 'ValueModels'!
ValueModel subclass: #PluggableAdaptor
	instanceVariableNames: 'getBlock setBlock updateBlock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'PluggableAdaptor class' category: 'ValueModels'!
PluggableAdaptor class
	instanceVariableNames: ''!

!classDefinition: #ValueHolder category: 'ValueModels'!
ValueModel subclass: #ValueHolder
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'ValueHolder class' category: 'ValueModels'!
ValueHolder class
	instanceVariableNames: ''!


!ValueModel commentStamp: '<historical>' prior: 0!
ValueModel is an abstract class whose subclasses provide the capability to adapt the protocol of a subject object to a simple <value> protocol. The intention is to allow general purpose objects, such as <views>, to interact with more specific objects, by providing an adapted layer between them. A client of a <valueModel> can set the value of the subject using a #value: message and similarly the value can be retrieved by sending #value:. 
An additional benefit is that <valueModel>s trigger a #value change notification whenever  the value is modified. This allows <valueModel>s to share data with several observers following the standard Observer pattern.  All <valueModel>s hold a <ComparisonPolicy> which determines when to trigger an update notification that the value has changed. By default, a change is notified when an attempt is made to set the value to a new one which is not equal to the existing value. 
 
The most commonly used subclass of ValueModel is ValueHolder whose instances wrap a subject object and use this as the value. !

!ListValueModel commentStamp: '<historical>' prior: 0!
ValueModel for lists.

Meant to be used in conjunction with PluggableListMorphs.

!

!PluggableAdaptor commentStamp: 'MM 2/26/2022 21:33:20' prior: 0!
I mediate between complex get/set behavior and the #value/#value: protocol used by ValueAdaptors.  The get/set behavior can be implemented by two blocks, or can be delegated to another object with messages such as #someProperty to get and #someProperty: to set.
!

!ValueHolder commentStamp: '<historical>' prior: 0!
I'm the simplest and most commonly used type of ValueModel. I will wrap the object within itself, thus giving the object ValueModel behavior. 

A ValueHolder wraps an object and notifies its dependants when that object is replaced. Since ValueHolders are commonly used, there is a short cut available for creating them. Sending #asValue to any object will answer that object wrapped inside a ValueHolder. !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/4/2021 13:26:11'!
asValue
	^ self! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/4/2021 13:26:17'!
asValueHolder
	^ self! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/4/2021 13:26:23'!
asValueModel
	^ self! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 18:42:56'!
is: aSymbol
	
	^ #ValueModel = aSymbol or: [ super is: aSymbol]! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 10:44'!
value
	^ self subclassResponsibility! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 10:44'!
value: anObject
	^ self subclassResponsibility ! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
getter
	"Answer the value of getter"

	^ getter! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
getter: anObject
	"Set the value of getter"

	getter _ anObject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
setter
	"Answer the value of setter"

	^ setter! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
setter: anObject
	"Set the value of setter"

	setter _ anObject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
subject
	"Answer the value of subject"

	^ subject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
subject: anObject
	"Set the value of subject"

	subject _ anObject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/29/2016 18:10'!
subjectSendsUpdates
	"Answer the value of subjectSendsUpdates"

	^ subjectSendsUpdates! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/29/2016 18:10'!
subjectSendsUpdates: anObject
	"Set the value of subjectSendsUpdates"

	subjectSendsUpdates _ anObject! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 9/9/2021 15:34:39'!
initialize: aSubject
	
	subject := aSubject.
	
	getter := #value.
	setter := #value:.
	subjectSendsUpdates := false.
	
	subject addDependent: self.! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 9/9/2021 15:36:35'!
update: aSymbol
	
	subjectSendsUpdates ifFalse: [
		self changed: self value]! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:04'!
value
	^ subject perform: getter! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/29/2016 18:09'!
value: anObject
	
	subject perform: setter with: anObject.
	
	subjectSendsUpdates ifFalse: [
		self changed: anObject]! !

!AspectAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:03'!
on: aSubject
	^ self new initialize: aSubject! !

!AspectAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 6/21/2019 19:21'!
on: anObject aspect: aSymbol
	
	|setter|
	
	setter _ (aSymbol asString, ':') asSymbol.
	
	^ self on: anObject
			getter: aSymbol
			setter: setter! !

!AspectAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:07'!
on: anObject getter: aSymbol setter: otherSymbol
	^ (self on: anObject) 
			getter: aSymbol;
			setter: otherSymbol;
			yourself! !

!ListValueModel methodsFor: 'accessing' stamp: 'MM 9/21/2021 00:10:15'!
getCurrentSelection

	listIndex ifNil: [^ nil].
	listIndex isZero ifTrue: [^ nil].
	^ self list at: listIndex! !

!ListValueModel methodsFor: 'accessing' stamp: 'MM 9/20/2021 23:58:14'!
list
	"Answer the value of list"

	^ list value! !

!ListValueModel methodsFor: 'accessing' stamp: 'MM 9/16/2021 10:47:12'!
listIndex
	"Answer the value of listIndex"

	^ listIndex! !

!ListValueModel methodsFor: 'accessing' stamp: 'MM 9/16/2021 10:49:59'!
listIndex: anObject
	"Set the value of listIndex"

	listIndex _ anObject.
	self changed: self getCurrentSelection.! !

!ListValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 20:45:32'!
initialize: aCollectionOrValueModel
	list _ aCollectionOrValueModel asValue.
	list onChangeSend: #changed to: self. 
	listIndex _ 0! !

!ListValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:06:19'!
listChanged
	
	"Send this message when our list changes."
	self changed: #list! !

!ListValueModel methodsFor: 'as yet unclassified' stamp: 'MM 9/16/2021 10:47:12'!
setAsModelOf: aPluggableListMorph

	aPluggableListMorph 	model: self 
					listGetter: #list 
					indexGetter: #listIndex 
					indexSetter: #listIndex: 
					mainView: nil 
					menuGetter: nil 
					keystrokeAction: nil! !

!ListValueModel class methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 20:45:00'!
with: aCollectionOrValueModel

	^ self new initialize: aCollectionOrValueModel! !

!ListValueModel class methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 23:55:37'!
with: aCollectionOrValueModel for: aPluggableListMorph
	
	|model|
	
	model _ self new initialize: aCollectionOrValueModel.
	
	model setAsModelOf: aPluggableListMorph.
	
	^ model! !

!ListValueModel class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 17:47:20'!
with: aCollectionOrValueModel initialize: aPluggableListMorph

	"This method returns aPluggableListMorph instead of the model.
	
	This is handy so that we can avoid having to use temporal variables for the list morph.
	
	Example:
	
	self addMorph: (SelectionInListModel with: (AspectAdaptor on: self aspect: #elements) initialize: PluggableListMorph new)."
	
	|model|
	
	model _ self new initialize: aCollectionOrValueModel.
	
	model setAsModelOf: aPluggableListMorph.
	
	^ aPluggableListMorph! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
getBlock
	"Answer the value of getBlock"

	^ getBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
getBlock: anObject
	"Set the value of getBlock"

	getBlock _ anObject! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
setBlock
	"Answer the value of setBlock"

	^ setBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
setBlock: anObject
	"Set the value of setBlock"

	setBlock _ anObject! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
updateBlock
	"Answer the value of updateBlock"

	^ updateBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
updateBlock: anObject
	"Set the value of updateBlock"

	updateBlock _ anObject! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:20'!
initialize
	updateBlock := [:value | true]! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:03'!
value
	^ getBlock value! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/31/2016 01:27'!
value: anObject
	^ (updateBlock value: anObject) ifTrue: [setBlock value: anObject. "self changed: anObject"]! !

!PluggableAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:06'!
getBlock: aBlock setBlock: anotherBlock
	
	^ self new initialize
			getBlock: aBlock;
			setBlock: anotherBlock;
			yourself.! !

!PluggableAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:06'!
getBlock: aBlock setBlock: anotherBlock updateBlock: updateBlock
	
	^ self new initialize
			getBlock: aBlock;
			setBlock: anotherBlock;
			updateBlock: updateBlock;
			yourself.! !

!ValueHolder methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 16:22'!
value
	^ value! !

!ValueHolder methodsFor: 'accessing' stamp: 'MM 9/3/2021 20:44:48'!
value: anObject
	"Set the value of value"
	
	value == anObject ifFalse: [
		value _ anObject.
		self changed: anObject]! !

!ValueHolder class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 16:23'!
value: anObject
	^ self new value: anObject! !

!Object methodsFor: '*ValueModels' stamp: 'MM 6/15/2019 13:41'!
asValue
	^ ValueHolder value: self! !

!Object methodsFor: '*ValueModels' stamp: 'MM 6/15/2019 13:41'!
asValueHolder
	^ ValueHolder value: self! !

!Object methodsFor: '*ValueModels' stamp: 'MM 6/15/2019 13:41'!
asValueModel
	^ ValueHolder value: self! !

!Object methodsFor: '*ValueModels' stamp: 'MM 9/4/2021 12:11:47'!
onChangeSend: aSelector to: aDependent

	^ self when: #changed: send: aSelector  to: aDependent! !
