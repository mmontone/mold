'From Cuis 5.0 [latest update: #4520] on 8 January 2021 at 12:56:03 pm'!
'Description '!
!provides: 'MetaBrowserFSM' 1 3!
SystemOrganization addCategory: 'MetaBrowserFSM'!


!classDefinition: #FSMBrowsableObject category: 'MetaBrowserFSM'!
BrowsableObject subclass: #FSMBrowsableObject
	instanceVariableNames: 'fsmClass'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaBrowserFSM'!
!classDefinition: 'FSMBrowsableObject class' category: 'MetaBrowserFSM'!
FSMBrowsableObject class
	instanceVariableNames: ''!


!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:31:14'!
printOn: aStream
	aStream nextPutAll: fsmClass printString! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 12:01:31'!
= anObject
	^ (anObject isKindOf: self class) and: [anObject fsmClass = fsmClass]! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 09:48:38'!
aspects
	
	^ {BrowseObjectOverviewAspect new.
		BrowseObjectMetaAspect new.
		BrowseObjectDocumentationAspect new}! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:43:16'!
browseObject
	^ self! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 12:09:04'!
contentsTypes: aBrowser

	^ {{#contents->#source. #action->#toggleContents:context:. 
		#target->self.#args->{#source. aBrowser}. #title->'source'. #help->'the textual source code as writen'}.
		{#contents->#diagram. #action->#toggleContents:context:. 
		#target->self.#args->{#diagram. aBrowser}. #title->'diagram'. #help->'FSM diagram'}.
	{#contents->#documentation. #target->self.#action->#toggleContents:context:.
		#args->{#documentation.aBrowser}.#title->'documentation'.#help->'documentation'}}! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:56:21'!
description
	^ 'A Finite State Machine'! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 12:01:44'!
fsmClass
	^ fsmClass! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:33:20'!
icon
	^ Theme current worldIcon! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:18:05'!
initialize: anFSMClass
	fsmClass _ anFSMClass! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:55:35'!
source
	^ fsmClass definition ! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 12:11:24'!
updateDiagramContents: aBrowserWindow

	|diagramMorph|
	
	 diagramMorph _ fsmClass diagram imageMorph.
						
	diagramMorph morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: diagramMorph! !

!FSMBrowsableObject methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:55:15'!
updateSourceContents: aBrowserWindow

	|editor|
	
	 editor _ TextModelMorph 
		textProvider: self 
		textGetter: #source
		textSetter: #source:notifying:.
						
	editor morphExtent: 1500 @ 1000.
		
	aBrowserWindow setContent: editor! !

!FSMBrowsableObject class methodsFor: 'as yet unclassified' stamp: 'MM 1/7/2021 10:17:50'!
on: anFSMClass
	^ self new initialize: anFSMClass ! !

!FSM class methodsFor: '*MetaBrowserFSM' stamp: 'MM 1/7/2021 10:16:26'!
asBrowsableObject
	^ (self isSubclassOf: FSM) 
		ifTrue: [ FSMBrowsableObject on: self]
		ifFalse: [super asBrowsableObject]! !
