'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 29 April 2016 at 1:48:29.608891 pm'!
!classDefinition: #PluggableFilteringDropDownListMorph category: #'StyledText-Morphic-Windows'!
PluggableDropDownListMorph subclass: #PluggableFilteringDropDownListMorph
	instanceVariableNames: 'editorMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!PluggableFilteringDropDownListMorph commentStamp: '<historical>' prior: 0!
A DropDownList that allows typing in, to filter visible items in the list.!


!PluggableFilteringDropDownListMorph methodsFor: 'private' stamp: 'jmv 12/20/2012 13:22'!
closeList
	"Also clear the selection in the entry field"
	(listMorph notNil and: [ listMorph hasKeyboardFocus ]) ifTrue: [
		self world activeHand newKeyboardFocus: editorMorph ].
	super closeList! !

!PluggableFilteringDropDownListMorph methodsFor: 'private' stamp: 'jmv 11/4/2010 15:42'!
openList

	super openList.
	editorMorph selectAll! !


!PluggableFilteringDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 9/10/2010 08:57'!
drawLabelOn: aCanvas
	"Not needed. Our label is a submorph"! !


!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/20/2010 15:00'!
editorClass
	^TextEditor! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/13/2012 10:41'!
keyStrokeInList: aKeyboardEvent
	"Send to the text anything but Up, Down, Return and Escape"
	(aKeyboardEvent isReturnKey or: [
		(#(27 30 31) includes: aKeyboardEvent keyValue)]) ifFalse: [
			editorMorph keyStroke: aKeyboardEvent ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/13/2012 10:59'!
keyStrokeInText: aKeyboardEvent

	"Handle Return and Escape separatedly"
	aKeyboardEvent isReturnKey ifTrue: [ ^self returnInEditor ].
	aKeyboardEvent keyValue = 27 ifTrue: [ ^self closeList ].

	"Send to the list only Up and Down,"
	self shouldOpenList
		ifTrue: [ self basicOpenList ]
		ifFalse: [
			self shouldCloseList ifTrue: [ self closeList ]].
	listMorph ifNotNil: [
		(#(30 31 ) includes: aKeyboardEvent keyValue)
			ifTrue: [ listMorph keyStroke: aKeyboardEvent ].
		listMorph verifyContents ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/29/2011 14:50'!
lostFocus: aMorph
	(self isListOpen and: [ listMorph hasKeyboardFocus not ]) ifTrue: [
		self closeList ]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/16/2011 09:27'!
shouldCloseList
	"True if no matches to show, or if there is no  typed text"
	^self isListOpen and: [
		"Filter empty does not mean we shouldn't show the list. Show it without filtering!!"
		"self filter isEmpty or: [ "
			self getList isEmpty 
		"]"
	]! !

!PluggableFilteringDropDownListMorph methodsFor: 'events' stamp: 'jmv 3/16/2011 09:24'!
shouldOpenList
	"True if list not open, but it makes sense to show it, because there is more than one entry that matches the typed text. "
	^self isListOpen not and: [
		"Open full list if filter empty. Why not???"
		"self filter notEmpty and: [ "
			self getList notEmpty "
		]" 
	]! !


!PluggableFilteringDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 7/5/2011 09:02'!
filter
	^editorMorph contents withBlanksTrimmed! !

!PluggableFilteringDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 3/16/2011 17:28'!
getLabel
	super getLabel.
	editorMorph ifNotNil: [ editorMorph contents: label ]! !


!PluggableFilteringDropDownListMorph methodsFor: 'filtering' stamp: 'jmv 9/10/2010 15:56'!
filter: entry with: filter

	^filter, '*' match: entry! !


!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 4/6/2011 19:03'!
getIndex
	"answer the index in the filtered list"
	| i filter |
	i _ super getIndex.
	i = 0 ifFalse: [
		filter _ self filter.
		(filter notEmpty and: [ filter ~= '-none-' ]) ifTrue: [
			i _ self getList indexOf: ((model perform: getListSelector) at: i) ]].
	^i! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 8/10/2011 15:17'!
getList
	| answer filter |
	answer _ super getList.
	filter _ self filter.
	(filter notEmpty and: [ filter ~= '-none-' ]) ifTrue: [ | filtered |
		filtered _ (answer select: [ :str | self filter: str with: filter ]).
		(filtered includes: filter) ifFalse: [
			answer _ filtered ]].
	^answer! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 3/13/2012 10:26'!
returnInEditor
	| filter all selected |
	self isListOpen ifTrue: [
		^ listMorph returnPressed ].

	filter _ self filter.
	filter isEmpty ifTrue: [ ^ self ].
	all _ model perform: getListSelector.
	selected _ all
		detect: [ :any | self filter: any with: filter ]
		ifNone: [ ^ self ].
	^ super setIndex: (super getList indexOf: selected)! !

!PluggableFilteringDropDownListMorph methodsFor: 'model' stamp: 'jmv 3/16/2011 09:10'!
setIndex: index
	| i filter |
	i _ index.
	filter _ self filter.
	filter notEmpty ifTrue: [
		i _ (model perform: getListSelector) indexOf: ( self getList at: i ) ].
	super setIndex: i! !


!PluggableFilteringDropDownListMorph methodsFor: 'initialization' stamp: 'jmv 12/27/2012 15:06'!
initialize
	| labelFont |
	super initialize.
	labelFont _ AbstractFont familyName: 'DejaVu' aroundPointSize: 10.
	editorMorph _ FilteringDDLEditorMorph contents: self label font: labelFont emphasis: 1.
	editorMorph keyboardFocusWatcher: self.
	self addMorph: editorMorph! !


!PluggableFilteringDropDownListMorph methodsFor: 'layout' stamp: 'jmv 12/20/2012 13:11'!
layoutSubmorphs
	| b innerBounds |
	super layoutSubmorphs.
	"innerBounds _ self innerBounds".
	innerBounds _ self morphBoundsInWorld insetBy: borderWidth.
	b _ innerBounds insetBy: 8@4.
	editorMorph morphBoundsInWorld: (b topLeft extent: b extent - (downButton morphWidth@0))! !
