'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 29 April 2016 at 1:48:15.763253 pm'!
!classDefinition: #PluggableDropDownListMorph category: #'StyledText-Morphic-Windows'!
PluggableMorph subclass: #PluggableDropDownListMorph
	instanceVariableNames: 'listMorph getListSelector getIndexSelector setIndexSelector label downButton'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StyledText-Morphic-Windows'!
!PluggableDropDownListMorph commentStamp: '<historical>' prior: 0!
A widget that shows the current value, and can open the full list for user selection.!


!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 2/25/2013 15:21'!
basicOpenList
	| xtraWidth xtraHeight bounds |
	bounds _ self morphBoundsInWorld.
	listMorph _ PluggableActOnReturnKeyListMorph
		model: self
		listGetter: #getList
		indexGetter: #getIndex
		indexSetter: #setIndex:.
	listMorph
		color: Color white;
		morphWidth: self morphWidth;
		morphHeight: 4;
		borderWidth: 1;
		borderColor: (Color black alpha: 0.3);
		morphPosition: bounds bottomLeft;
		autoDeselect: false.
	self world addMorph: listMorph.
	listMorph updateList.
	xtraWidth _ listMorph hLeftoverScrollRange + 4.
	xtraWidth > 0 ifTrue: [
		listMorph morphWidth: listMorph morphWidth + xtraWidth ].
	xtraHeight _ listMorph vLeftoverScrollRange + 4.
	xtraHeight > 0 ifTrue: [
		listMorph morphHeight: (listMorph morphHeight + xtraHeight min: 100) ]! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 9/10/2010 15:31'!
closeList
	listMorph ifNotNil: [
		listMorph delete.
		listMorph _ nil ]! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 2/14/2013 12:41'!
openList
	self basicOpenList.
	self world activeHand newKeyboardFocus: listMorph! !

!PluggableDropDownListMorph methodsFor: 'private' stamp: 'jmv 8/16/2010 16:01'!
openOrCloseList
	self isListOpen
		ifFalse: [ self openList ]
		ifTrue: [ self closeList ]! !


!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:29'!
drawBasicLookOn: aCanvas

	aCanvas
		fillRectangle: (0@0 extent: extent)
		color: color
		borderWidth: borderWidth
		borderStyleSymbol: #simple
		baseColorForBorder: borderColor.
	self drawLabelOn: aCanvas ! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:33'!
drawLabelOn: aCanvas 

	| f |
	f _ Preferences standardButtonFont.
	aCanvas drawString: label at: 0@(extent y // 2) + (8@ f height negated // 2) font: f color: Color black! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 4/12/2012 22:19'!
drawOn: aCanvas

	"Theme current steButtons"
	true
		ifTrue: [ self drawSTELookOn: aCanvas ]
		ifFalse: [ self drawBasicLookOn: aCanvas ]! !

!PluggableDropDownListMorph methodsFor: 'drawing' stamp: 'jmv 2/14/2013 13:31'!
drawSTELookOn: aCanvas
	| gh |
"sin gradiente el borde!!"
	gh _ extent y-8 max: extent y//2.
	aCanvas
		roundRect: (0@0 extent: extent)
		color: (Color gray: 0.4)
		radius: 4
		gradientTop: 0.9
		gradientBottom: 0.6
		gradientHeight: gh.
	aCanvas
		roundRect: (1@1 extent: extent-2)
		color: (Color gray: 0.95)
		radius: 4
		gradientTop: 0.99
		gradientBottom: 0.96
		gradientHeight: gh.
	self drawLabelOn: aCanvas ! !


!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 9/9/2010 15:12'!
getIndex
	^model ifNil: [ 0 ] ifNotNil: [ model perform: getIndexSelector ]! !

!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 9/16/2009 13:45'!
getList
	^model perform: getListSelector! !

!PluggableDropDownListMorph methodsFor: 'model' stamp: 'jmv 6/3/2011 14:43'!
modelChanged
	self getLabel.
	self changed: self! !


!PluggableDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 4/6/2011 19:03'!
getLabel
	| i |
	i _ self getIndex.
	label _ i = 0
		ifTrue: [ '-none-' ]
		ifFalse: [ self getList at: i ].! !

!PluggableDropDownListMorph methodsFor: 'accessing' stamp: 'jmv 9/10/2010 08:37'!
label
	^ label! !


!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 9/9/2010 14:25'!
handlesMouseDown: evt
	"So our #mouseDown: method is called"
	^ true! !

!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 9/9/2010 14:25'!
handlesMouseOver: anEvent
	"So our #mouseLeave: method is called"
	^ true! !

!PluggableDropDownListMorph methodsFor: 'testing' stamp: 'jmv 1/27/2011 16:11'!
isListOpen
	^ listMorph notNil! !


!PluggableDropDownListMorph methodsFor: 'initialization' stamp: 'KenD 12/4/2015 21:21'!
initialize
	| icon |
	super initialize.
	self color: Color white.
	self borderColor: Color black.
	self getLabel.
"	self extent: 120 @ 20."
	icon _ "Theme current steButtons" true
		ifFalse: [ BitBltCanvas arrowOfDirection: #down size: ScrollBar scrollbarThickness ]
		ifTrue: [ BitBltCanvas buildArrowOfDirection: #down size: ScrollBar scrollbarThickness].
			 "@@FIXME@@ arrowWithGradientOfDirection: #down"
	downButton _ FancyButtonMorph new.
	downButton
		model: self;
		roundButtonStyle: false;
		icon: icon;
		actWhen: #buttonDown;
		action: #openOrCloseList.
	self addMorph: downButton.! !

!PluggableDropDownListMorph methodsFor: 'initialization' stamp: 'jmv 9/16/2009 11:29'!
model: anObject listGetter: getListSel indexGetter: getSelectionSel indexSetter: setSelectionSel

	self model: anObject.
	getListSelector _ getListSel.
	getIndexSelector _ getSelectionSel.
	setIndexSelector _ setSelectionSel.! !


!PluggableDropDownListMorph methodsFor: 'layout' stamp: 'jmv 12/20/2012 12:57'!
layoutSubmorphs
	| e innerBounds |
	"innerBounds _ self innerBounds".
	innerBounds _ self morphBoundsInWorld insetBy: borderWidth.
	e _ innerBounds height.
	downButton morphBoundsInWorld: (innerBounds bottomRight - e extent: e)! !


!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/20/2012 13:24'!
mouseLeave: evt
	super mouseLeave: evt.
	(listMorph isNil or: [ (listMorph morphBoundsInWorld containsPoint: evt eventPosition) not])
		ifTrue: [
			"Do the call even if the list is not there, as this also clears selection in subclass with entry field"
			self closeList ]! !

!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 12/27/2012 15:02'!
mouseLeaveList: evt
	(self morphBoundsInWorld containsPoint: evt eventPosition)
		ifFalse: [ self closeList ]! !

!PluggableDropDownListMorph methodsFor: 'events' stamp: 'jmv 6/3/2011 14:44'!
setIndex: index
	model perform: setIndexSelector with: index.
	"self changed: #getIndex." "No chance to actually see it, it is closed too quickly"
	self getLabel.
	self changed: self.
	self closeList! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PluggableDropDownListMorph class' category: #'StyledText-Morphic-Windows'!
PluggableDropDownListMorph class
	instanceVariableNames: ''!

!PluggableDropDownListMorph class methodsFor: 'instance creation' stamp: 'jmv 9/16/2009 11:30'!
model: anObject listGetter: getListSel indexGetter: getSelectionSel indexSetter: setSelectionSel

	^self new
		model: anObject
		listGetter: getListSel
		indexGetter: getSelectionSel
		indexSetter: setSelectionSel! !
