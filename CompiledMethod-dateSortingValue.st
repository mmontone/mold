'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 3 October 2018 at 9:13:51.559486 am'!

!CompiledMethod methodsFor: 'time stamp' stamp: 'MM 10/3/2018 09:13'!
dateSortingValue
	"Answer an integer that is suitable for chronologically sorting methods.
	It is the number of whole minutes since 'The dawn of Squeak history'
	"
	"
	(CompiledMethod compiledMethodAt: #dateAndTime) dateSortingValue
	"

	self timeStamp ifEmpty: [^0].
	
	^self class timeStamp: self timeStamp partsDo: [ :authorInitials :dateAndTime |
		dateAndTime
			ifNil: [ 0 ]
			ifNotNil: [ (dateAndTime - (DateAndTime fromString: '01/01/1996 00:00')) totalMinutes max: 0 ]]! !
