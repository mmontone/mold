'From Cuis 6.0 [latest update: #5234] on 12 June 2022 at 7:02:20 pm'!
'Description '!
!provides: 'PlantUML' 1 12!
!requires: 'OSProcess' 1 23 nil!
SystemOrganization addCategory: 'PlantUML'!


!classDefinition: #PlantUMLWriter category: 'PlantUML'!
Object subclass: #PlantUMLWriter
	instanceVariableNames: 'stream outputFile inputFile'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PlantUML'!
!classDefinition: 'PlantUMLWriter class' category: 'PlantUML'!
PlantUMLWriter class
	instanceVariableNames: ''!


!PlantUMLWriter commentStamp: 'MM 9/8/2021 11:14:03' prior: 0!
|uml form|

uml _ PlantUMLWriter startuml.

uml class: 'MyClass' with: [
	uml field: 'name' type: 'String'.
	uml field: 'age' type: 'int'].

form _ uml enduml.

(ImageMorph new image: form) openInWorld!

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:55:30'!
aggregate: aClass to: otherClass

	self relation: self aggregation from: aClass  to: otherClass ! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:54:55'!
aggregation
	^ ' --o '! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:01:47'!
association
	^ ' -- '! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:01:38'!
association: aClass to: otherClass

	self relation: self association from: aClass  to: otherClass ! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:06:25'!
beginClass: className

	stream nextPutAll: 'class '.
	self className: className.
	stream 	nextPutAll: ' {'; newLine! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:06:00'!
class: className

	self className: className.
	stream newLine.
! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:51:21'!
class: aClass extends: otherClass

	self relation: ' --|> ' from: aClass to: otherClass! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:04:20'!
class: className with: aBlock

	self beginClass: className.
	aBlock value.
	self endClass
	
! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:08:26'!
className: aClassName
	stream nextPut: $"; nextPutAll: aClassName; nextPut: $"! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:55:42'!
compose: aClass to: otherClass

	self relation: self composition from: aClass to: otherClass.! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:55:04'!
composition
	^ ' --* '! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:01:52'!
endClass
	stream nextPutAll: '}';
			newLine! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:31:06'!
endState

	^ '[*]'! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 10:35:12'!
enduml
	stream nextPutAll: '@enduml'; newLine.
	^ self generateDiagram! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:55:12'!
extension
	^ ' --|> '! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:13:32'!
field: fieldName type: fieldType

	stream nextPutAll: fieldType;
		nextPutAll: ' ';
		nextPutAll: fieldName;
		newLine! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:27:06'!
generateDiagram

	| commandStatus command |
	
	stream close.
	
	command _ self plantUMLCommand , ' ', inputFile asString.	
	
	commandStatus _ OSProcess waitForCommand: command.
	commandStatus succeeded ifFalse: [
		self error: 'Error running PlantUML command'].	
	
	^ Form fromFileNamed: outputFile! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:57:28'!
getTemporalFileName
	
	|randomStr|
	
	randomStr _ String streamContents: [:s |
				15 timesRepeat: [		s nextPut: ($a asInteger + ($z asInteger - $a asInteger) atRandom) asCharacter]].
	^ '/tmp/plantuml-', randomStr! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:56:46'!
getTemporalFileName: extension

	|randomStr|
	
	randomStr _ String streamContents: [:s |
				15 timesRepeat: [		s nextPut: ($a asInteger + ($z asInteger - $a asInteger) atRandom) asCharacter]].
	^ '/tmp/plantuml-', randomStr, '.', extension! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 10:36:04'!
initialize

	|tmp|
	
	tmp _ self getTemporalFileName.
	inputFile _ tmp,  '.plantuml'.
	outputFile _ tmp, '.png'.
	stream _ inputFile asFileEntry forceWriteStream.
	! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:29:57'!
newLine

	stream newLine! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:28:58'!
nextPut: aCharacter

	stream nextPut: aCharacter! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:28:42'!
nextPutAll: aString

	stream nextPutAll: aString! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:34:00'!
plantUMLCommand

	^ 'plantuml'
	! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 11:49:16'!
relation: relation from: fromClass to: toClass 

	self relation: relation from: fromClass  to: toClass  c1: nil  c2: nil  label: nil! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 14:07:02'!
relation: relation from: fromClass to: toClass c1: fromCardinality c2: toCardinality label: aString

	self className: fromClass. stream space.
	fromCardinality ifNotNil: [
		stream nextPut: $";
				nextPutAll: fromCardinality;
				nextPut: $";
				space].
	stream nextPutAll: relation;			space.
	toCardinality ifNotNil: [
		stream nextPut: $";
				nextPutAll: toCardinality;
				nextPut: $";
				space].
	self className: toClass. stream space.
	aString ifNotNil: [
		stream nextPutAll: ' : ';
			nextPutAll: aString ].
	stream newLine! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:30:05'!
space
	stream space! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:31:02'!
startState

	^ '[*]'! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:31:19'!
startUML

	stream nextPutAll: '@startuml'; newLine.! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:30:10'!
tab
	stream tab! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:47:51'!
transitionFrom: aState to: otherState
	
	stream nextPutAll: aState;
		nextPutAll: ' --> ';
		nextPutAll: otherState;
		newLine.! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:46:10'!
transitionFromStartTo: someState

	self transitionFrom: self startState to: someState! !

!PlantUMLWriter methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:46:28'!
transitionToEnd: someState

	self transitionFrom: someState to: self endState! !

!PlantUMLWriter class methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:31:19'!
example1

	"self example1"
	
	|uml form|

	uml _ PlantUMLWriter startUML.

	uml class: 'MyClass' with: [
		uml field: 'name' type: 'String'.
		uml field: 'age' type: 'int'].

	form _ uml enduml.

	(ImageMorph new image: form) openInWorld! !

!PlantUMLWriter class methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:39:13'!
example2

	"self example2"
	
	|uml form|

	uml _ PlantUMLWriter startUML.

	uml nextPutAll: '
class Student {
  Name
}
Student "0..*" -- "1..*" Course
(Student, Course) . Enrollment

class Enrollment {
  drop()
  cancel()
}
'.
	form _ uml enduml.

	(ImageMorph new image: form) openInWorld! !

!PlantUMLWriter class methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:40:46'!
example3

	"self example3"
	
	|uml form|

	uml _ PlantUMLWriter startUML.

	uml nextPutAll: '
skinparam groupInheritance 2

A1 <|-- B1

A2 <|-- B2
A2 <|-- C2

A3 <|-- B3
A3 <|-- C3
A3 <|-- D3

A4 <|-- B4
A4 <|-- C4
A4 <|-- D4
A4 <|-- E4
'.
	form _ uml enduml.

	(ImageMorph new image: form) openInWorld! !

!PlantUMLWriter class methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 19:00:59'!
example4

	"self example4"
	
	|uml form|

	uml _ PlantUMLWriter startUML.

	uml nextPutAll: 'scale 350 width
[*] --> NotShooting

state NotShooting {
  [*] --> Idle
  Idle --> Configuring : EvConfig
  Configuring --> Idle : EvConfig
}

state Configuring {
  [*] --> NewValueSelection
  NewValueSelection --> NewValuePreview : EvNewValue
  NewValuePreview --> NewValueSelection : EvNewValueRejected
  NewValuePreview --> NewValueSelection : EvNewValueSaved

  state NewValuePreview {
     State1 -> State2
  }

}
'.
	form _ uml enduml.

	(ImageMorph new image: form) openInWorld! !

!PlantUMLWriter class methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2022 18:31:19'!
startUML

	^ self new startUML! !
