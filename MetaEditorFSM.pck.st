'From Cuis 5.0 [latest update: #4743] on 9 September 2021 at 3:49:17 pm'!
'Description '!
!provides: 'MetaEditorFSM' 1 4!
SystemOrganization addCategory: #MetaEditorFSM!


!classDefinition: #MEFSMDiagramAspect category: #MetaEditorFSM!
MEModelAspect subclass: #MEFSMDiagramAspect
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaEditorFSM'!
!classDefinition: 'MEFSMDiagramAspect class' category: #MetaEditorFSM!
MEFSMDiagramAspect class
	instanceVariableNames: ''!

!classDefinition: #MetaEditorFSM category: #MetaEditorFSM!
FSM subclass: #MetaEditorFSM
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MetaEditorFSM'!
!classDefinition: 'MetaEditorFSM class' category: #MetaEditorFSM!
MetaEditorFSM class
	instanceVariableNames: ''!


!MEFSMDiagramAspect methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:32:55'!
buildAspectMorph

	^ (FSMDiagramGenerator on: model) generate! !

!MEFSMDiagramAspect methodsFor: 'as yet unclassified' stamp: 'MM 9/5/2021 11:33:10'!
label
	^ 'diagram'! !

!MetaEditorFSM methodsFor: 'as yet unclassified' stamp: 'MM 8/31/2021 16:31:43'!
initFSM
	! !

!MetaEditorFSM methodsFor: 'as yet unclassified' stamp: 'MM 9/2/2021 19:52:01'!
initialize
	super initialize.
	name _ ''.
! !

!MetaEditorFSM methodsFor: 'as yet unclassified' stamp: 'MM 8/31/2021 16:31:33'!
startState
	^ startState! !

!MetaEditorFSM class methodsFor: 'as yet unclassified' stamp: 'MM 9/5/2021 11:34:51'!
metamodel

	^ MEMetaModel new
		name: 'FSM';
		description: 'A finite state machine';
		attribute: #name type: #string properties: [:attr |
			attr description: 'The FSM name.'];
		attribute: #startState type: #choice
			properties: [:attr |
				attr type choices: [:anFSM | anFSM states]];
		attribute: #states type: #composition properties: [:attr |
			attr type associatedModel: FSMState;
					multiplicity: #many];
		attribute: #transitions type: #composition properties: [:attr |
			attr type associatedModel: FSMTransition;
					multiplicity: #many.
			attr adder: #addTransition:;
				remover: #removeTransition:				];
		action: 'compile' selector: #compile description: 'Compile the FSM to a Smalltalk class';
		aspect: MEFSMDiagramAspect;
		instanceClass: MetaEditorFSM;
		yourself! !

!FSM methodsFor: '*MetaEditorFSM' stamp: 'MM 8/31/2021 09:09:27'!
metamodel

	^ self class metamodel! !

!FSMState methodsFor: '*MetaEditorFSM' stamp: 'MM 8/31/2021 16:36:14'!
metamodel
	^ self class metamodel! !

!FSMState class methodsFor: '*MetaEditorFSM' stamp: 'MM 9/3/2021 12:10:02'!
metamodel

	^ MEMetaModel new
		name: 'FSM state';
		description: 'FSM state';
		attribute: #name type: #string;
		attribute: #action type: #string;
		attribute: #description type: #text;
		instanceClass: FSMState;
		createInstance: [:metamodel :cls | cls new
									name: 'new state';
									description: '';
									action: '';
									yourself];
		yourself! !

!FSMTransition methodsFor: '*MetaEditorFSM' stamp: 'MM 9/3/2021 12:51:14'!
metamodel

	^ self class metamodel! !

!FSMTransition class methodsFor: '*MetaEditorFSM' stamp: 'MM 9/3/2021 14:25:52'!
metamodel

	^ MEMetaModel new
		name: 'FSM state';
		description: 'A finite state machine state';
		attribute: #name type: #string;
		attribute: #action type: #string;
		attribute: #event type: #string;
		attribute: #description type: #text;
		attribute: #sourceState type: #aggregation
			properties: [:attr |
				attr type associatedModel: FSMState;
					source: [:aTransition | aTransition fsm states];
					multiplicity: #one];
		attribute: #targetState type: #aggregation
			properties: [:attr |
				attr type associatedModel: FSMState;
					source: [:aTransition | aTransition fsm states];
					multiplicity: #one];
		instanceClass: FSMTransition;
		createInstance: [:metamodel :cls | cls new
									name: 'new transition';
									description: '';
									event: '';
									sourceState: (FSMState new
												name: 'empty';
												yourself);
									targetState: (FSMState new
												name: 'empty'; yourself);
									action: ''];
		yourself! !
