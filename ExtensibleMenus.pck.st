'From Cuis 6.0 [latest update: #5045] on 22 January 2022 at 4:31:15 pm'!
'Description Cuis menus are already extensible. This packages makes more menus extensible, like inspectors, explorers, and others.'!
!provides: 'ExtensibleMenus' 1 4!
SystemOrganization addCategory: 'ExtensibleMenus'!



!FileListWindow methodsFor: '*ExtensibleMenus' stamp: 'MM 9/22/2021 18:33:31'!
fileSelectedMenu

	| itemsPart1 itemsPart2 itemsPart3 itemsPart4 n1 n2 n3 services aMenu |
	aMenu _ DynamicMenuBuilder buildTargeting: model 
				collectingMenuOptionsWith: #fileListSelectedMenuOptions.
	itemsPart1 _ model itemsForAnyFile1.
	itemsPart2 _ model itemsForFileEntry: model selectedFileEntry.
	itemsPart3 _ model itemsForAnyFile2.
	itemsPart4 _ model itemsForNoFile.
	n1 _ itemsPart1 size.
	n2 _ n1 + itemsPart2 size.
	n3 _ n2 + itemsPart3 size.
	services _ itemsPart1, itemsPart2, itemsPart3, itemsPart4.
	services do: [ :svc | svc when: #fileListChanged send: #updateFileList to: model ].
	^ aMenu 
		addServices: services 
		for: model
		extraLines:{ n1 . n2 . n3 }
! !

!FileListWindow methodsFor: '*ExtensibleMenus' stamp: 'MM 9/22/2021 18:33:16'!
noFileSelectedMenu

	| aMenu |
	
	aMenu _ DynamicMenuBuilder buildTargeting: model 
				collectingMenuOptionsWith: #fileListNoSelectedMenuOptions.
	
	^ aMenu
		addServices: model itemsForNoFile 
		for: model
		extraLines: #()! !

!InspectorWindow methodsFor: '*ExtensibleMenus' stamp: 'MM 9/22/2021 18:23:16'!
fieldListMenu
	"Arm the supplied menu with items for the field-list of the receiver"
	
	"This method has been overwritten by ExtensibleMenus package in order to make the InspectorWindow menu extensible."

	| aMenu |
	
	"Use a DynamicMenuBuilder for dynamically building the inspector menu."
	aMenu _ DynamicMenuBuilder buildTargeting: self collectingMenuOptionsWith: #smalltalkInspectorMenuOptions.
	aMenu addItemsFromDictionaries: self basicMenuOptions.
	self model suggestObjectSpecificMenuItemsFor: aMenu from: self.
	aMenu addItemsFromDictionaries: self menuOptionsForBrowsing.
	^ aMenu! !

!ObjectExplorerWindow methodsFor: '*ExtensibleMenus' stamp: 'MM 1/22/2022 16:30:29'!
genericMenu
	"Borrow a menu from my inspector"
	
	| aMenu |
	aMenu _ DynamicMenuBuilder buildTargeting: self 
				collectingMenuOptionsWith: #objectExplorerMenuOptions.
	model getCurrentSelection
		ifNil: [
			aMenu
				add: '*nothing selected*'
				target: self
				action: #yourself]
		ifNotNil: [
			aMenu
				addItemsFromDictionaries: `{
					{
						#label 			-> 		'inspect (i)'.
						#selector 			-> 		#inspectSelection.
						#icon 			-> 		#inspectIcon
					} asDictionary.
					{
						#label 			-> 		'explore (I)'.
						#selector 			-> 		#exploreSelection.
						#icon 			-> 		#exploreIcon
					} asDictionary.
					{
						#label 			-> 		'copy to clipboard (c)'.
						#selector 			-> 		#copySelectionToClipboard.
						#icon 			-> 		#copyIcon
					} asDictionary.
					{
						#label 			-> 		'basic inspect'.
						#selector 			-> 		#inspectBasic.
						#icon 			-> 		#inspectIcon
					} asDictionary.
					{
						#label 			-> 		'references finder'.
						#selector 			-> 		#openReferencesFinder.
						#icon 			-> 		#exploreIcon
					} asDictionary.
					{
						#label 			-> 		'weight explorer'.
						#selector 			-> 		#openWeightExplorer.
						#icon 			-> 		#exploreIcon
					} asDictionary.
					nil.
					{
						#label 			-> 		'browse full (b)'.
						#selector 			-> 		#browseMethodFull.
						#icon 			-> 		#editFindReplaceIcon
					} asDictionary.
					{
						#label 			-> 		'browse hierarchy (h)'.
						#selector 			-> 		#browseHierarchy.
						#icon 			-> 		#goTopIcon
					} asDictionary.
					{
						#label 			-> 		'browse protocol (p)'.
						#selector 			-> 		#browseFullProtocol.
						#icon 			-> 		#spreadsheetIcon
					} asDictionary.
				}`.
			model getCurrentSelection ifNotNil: [ :currSel |
				(currSel item is: #Morph) ifTrue: [
					aMenu addLine.
					aMenu add: 'show morph halo' target: currSel item action: #addHalo]].
			aMenu addLine;
				add: 'monitor changes'
				target: self
				action: #monitor:
				argument: model getCurrentSelection.
			model class == ReferencesExplorer ifTrue: [
				aMenu addLine;
					add: 'rescan'
					target: self
					action: #rescan ]].
	model basicMonitorList isEmptyOrNil
		ifFalse: [
			aMenu addLine;
				add: 'stop monitoring all'
				target: self
				action: #stopMonitoring ].
	^ aMenu! !
