#('Mold' 'Erudite' 'GML' 'MetaBrowser' 'MetaBrowserErudite' 'Meld' 'Dia' 'Presentations' 'Commander' 'WorldCommands'
  'FSM' 'MetaBrowserFSM' 'Props' 'ObjV' 'BrowserPlus' 'IMGUI' 'StructurEdSmalltalk')
	do: [:system |
		(self confirm: 'Install ', system, '?') ifTrue: [
			Feature require: system]].