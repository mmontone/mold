'From Cuis 5.0 [latest update: #4241] on 13 July 2020 at 11:21:50 am'!
'Description Flow Based Programming Framework
Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'Flow' 1 0!
!requires: 'Dia' 1 223 nil!
SystemOrganization addCategory: #Flow!


!classDefinition: #FlowNode category: #Flow!
DiaInputOutputNode subclass: #FlowNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Flow'!
!classDefinition: 'FlowNode class' category: #Flow!
FlowNode class
	instanceVariableNames: ''!

