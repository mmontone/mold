(ql:quickload '(:cl-cairo2 :cl-colors))

(defpackage :pngicon
  (:use :cl)
  (:export :write-png-icon))

(in-package :pngicon)

(defun generate-icon (type &rest args)
  (apply #'generate-icon-type type args))

(defgeneric generate-icon-type (type &rest args))

(defmethod generate-icon-type ((type (eql :squared)) &rest args)
  (let* ((width (or (getf args :width)
                    30))
         (height (or (getf args :height)
                     30))
         (surface (cairo:create-image-surface
                   :argb32 width height))
         (ctx (cairo:create-context surface))
         (border-size (or (getf args :border-size) 0))
         (border-color (getf args :border-color))
         (font-size (or (getf args :font-size) 16))
         (font-color (or (getf args :font-color)
                         cl-colors:+black+))
         (padding (or (getf args :padding) 0))
         (background-color (or (getf args :background-color)
                               cl-colors:+white+))
         (text (getf args :text)))
    
    (cairo:with-context (ctx)
      (cairo:set-source-color background-color)
      (cairo:rectangle 0 0 width height)
      (cairo:fill-path)
      
      (cairo:set-font-size font-size)
      (cairo:set-source-color font-color)
      (cairo:select-font-face (or (getf args :font) "sans")
                              (or (getf args :font-slant)
                                  :normal)
                              (or (getf args :font-weight)
                                  :normal))
      (multiple-value-bind (x-bearing y-bearing text-width text-height
                                      x-advance y-advance)
        (cairo:text-extents text)
        (let ((x (/ (- width text-width) 2))
              (y (+ (/ (- height text-height) 2) text-height)))
          (cairo:move-to x y)))
      
      (cairo:show-text text)
      (cairo:stroke))
    surface))

(defun write-png-icon (filename icon-type &rest args)
  (let ((surface (apply #'generate-icon-type icon-type args)))
    (cairo:surface-write-to-png surface filename)))

(defun generate-alphabet-icons (&optional (path #p"/home/marian/src/rilke/icons/"))
  (ensure-directories-exist path)
  (let ((characters "abcdefghijklmnñopqrstuvwxyz")
        (colors (list cl-colors:+red+
                      cl-colors:+blue+
                      cl-colors:+darkgreen+
                      cl-colors:+orange+
                      cl-colors:+black+
                      cl-colors:+violet+)))
    
    (loop
       for char across characters
       for filepath = (merge-pathnames (pathname (format nil "~A.png" char))
                                       path)
       for color = (nth (random (length colors)) colors)
       do
         (pngicon:write-png-icon filepath :squared
                                 :text (princ-to-string char)
                                 :font-size 10 :width 12 :height 12
                                 :background-color color
                                 :font-color cl-colors:+white+
                                 :font-weight :bold))))
