'From Cuis 5.0 [latest update: #4619] on 30 June 2021 at 11:51:16 am'!
'Description '!
!provides: 'MethodDispatcher' 1 4!
SystemOrganization addCategory: 'MethodDispatcher'!


!classDefinition: #MethodDispatchError category: 'MethodDispatcher'!
Error subclass: #MethodDispatchError
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MethodDispatcher'!
!classDefinition: 'MethodDispatchError class' category: 'MethodDispatcher'!
MethodDispatchError class
	instanceVariableNames: ''!

!classDefinition: #MethodDispatcherCompiler category: 'MethodDispatcher'!
Compiler subclass: #MethodDispatcherCompiler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MethodDispatcher'!
!classDefinition: 'MethodDispatcherCompiler class' category: 'MethodDispatcher'!
MethodDispatcherCompiler class
	instanceVariableNames: ''!

!classDefinition: #MethodDispatcherTest category: 'MethodDispatcher'!
TestCase subclass: #MethodDispatcherTest
	instanceVariableNames: 'methodDispatcher'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MethodDispatcher'!
!classDefinition: 'MethodDispatcherTest class' category: 'MethodDispatcher'!
MethodDispatcherTest class
	instanceVariableNames: ''!

!classDefinition: #MethodDispatcher category: 'MethodDispatcher'!
Object subclass: #MethodDispatcher
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MethodDispatcher'!
!classDefinition: 'MethodDispatcher class' category: 'MethodDispatcher'!
MethodDispatcher class
	instanceVariableNames: 'orderedMethods dispatching'!

!classDefinition: #MethodDispatcherExample category: 'MethodDispatcher'!
MethodDispatcher subclass: #MethodDispatcherExample
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MethodDispatcher'!
!classDefinition: 'MethodDispatcherExample class' category: 'MethodDispatcher'!
MethodDispatcherExample class
	instanceVariableNames: ''!


!MethodDispatcherTest commentStamp: '<historical>' prior: 0!
MethodDispatcher tests.!

!MethodDispatcher commentStamp: 'MM 6/30/2021 10:16:07' prior: 0!
Dispatches a methods looking at the type of the argument.

How to use:

Subclass MethodDispatcher.

On the class side of the subclass define a #selectorPrefix method that returns a selector. Class methods that start with that prefix are dispatched looking at the type of the first method argument. For example, if #selectorPrefix returns #propertiesOf, then all methods defined in the method dispatcher class whose name begins with 'propertiesOf' are considered methods that dispatch on the type of their first argument. After the prefix, the class name that the method dispatches on should come. So, the syntax for method names is <selectorPrefix><className>. For example, #propertiesOfObject:, #propertiesOfString:, #propertiesOfNumber:, etc. 

Methods can receive any number of arguments, but the dispatcher looks at the type of the first argument only for the dispatch.

To dispatch a method, get an instance of a MethodDispatcher via #instance, then use #dispatch: passing the list of arguments (or better, wrap #dispatch: like in #getPropertiesOf:).

Have a look at MethodDispatcherExample for an example of how this works.!

!MethodDispatcherExample commentStamp: '<historical>' prior: 0!
An example of a MethodDispatcher.!

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:12:01'!
compilerClass
	^ MethodDispatcherCompiler! !

!MethodDispatcherCompiler methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:47:24'!
compile: textOrStream in: aClass classified: aCategory notifying: aRequestor ifFail: failBlock 
	
	|methodNode|
	
	methodNode _ super compile: textOrStream in: aClass classified: aCategory notifying: aRequestor ifFail: failBlock. 
	Transcript show: aClass className, 'needs recompilation';newLine.
	aClass needsRecompilation.
	^ methodNode! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:13:34'!
setUp
	methodDispatcher _ MethodDispatcherExample instance.! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 09:37:12'!
tearDown
	methodDispatcher _ nil! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:14:37'!
testDispatch1
	self assert: ((methodDispatcher getPropertiesOf: Object new) = 'properties of object')! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:20:52'!
testDispatchError
	self should: [methodDispatcher getPropertiesOf: ProtoObject new] raise: MethodDispatchError! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 09:43:05'!
testDispatchObjects
	self assert: ((methodDispatcher getPropertiesOf: Object new) = 'properties of object').
	self assert: ((methodDispatcher getPropertiesOf: Morph new) = 'properties of morph'). 
	self assert: ((methodDispatcher getPropertiesOf: Point new) = 'properties of object').
	self assert: ((methodDispatcher getPropertiesOf: 22) = 'properties of a number').
	self assert:((methodDispatcher getPropertiesOf: 'asdf') = 'properties of object and a string' ).
	self assert: ((methodDispatcher getPropertiesOf: SystemWindow topWindow) =  'properties of morph (a system window)')! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 09:49:51'!
testDispatchTypeChain
	
	
	self assert: ((methodDispatcher getPropertiesOf: 22) = 'properties of a number').
	self assert: ((methodDispatcher getPropertiesOf: 0.22) = 'properties of a number').
	
	self assert: ((methodDispatcher getPropertiesOf: Point new) = 'properties of object').
	
	self assert: ((methodDispatcher getPropertiesOf: BoxedMorph new) = 'properties of morph').
	self assert: ((methodDispatcher getPropertiesOf: MenuMorph new) = 'properties of morph').
	self assert: ((methodDispatcher getPropertiesOf: LayoutMorph newRow) = 'properties of morph').
	
	! !

!MethodDispatcherTest methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:30:12'!
testSuperDispatch

	self assert:((methodDispatcher getPropertiesOf: 'asdf') = 'properties of object and a string' ).
	self assert: ((methodDispatcher getPropertiesOf: SystemWindow topWindow) =  'properties of morph (a system window)')! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 11:48:11'!
collectMethods
	
	|methods|
	
	methods _ OrderedCollection new.
	
	(self selectorsInClassHierarchy: self class) do: [:selector |
		(selector asString beginsWith: self selectorPrefix asString)
			ifTrue: [|methodClassName|
				"extract the class the method matches from the selector name: <selectorPrefix><className>"
				methodClassName _ selector asString 
									copyFrom: self selectorPrefix size + 1
									to: (selector asString indexOf: $:) - 1.
				"collect methods as an association Class -> selector"					
			  	methods add: (Smalltalk at: methodClassName asSymbol) -> selector]].
			
	^ methods! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:44:03'!
compile

	"self compile"
	
	orderedMethods _ self collectMethods.
	orderedMethods _ self sortMethods: orderedMethods.! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/29/2021 21:21:05'!
dispatch: arguments

	"Dispatches one of the methods depending of type of the first argument in arguments."
	
	^ self dispatch: arguments from: arguments first class! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:20:37'!
dispatch: arguments from: aClass

	"Dispatches one of the methods depending of type of the first argument in arguments."
	
	[orderedMethods do: [:method |
		((aClass == method key) or: [aClass inheritsFrom: method key])
			ifTrue: [
				dispatching _ method.
				^ self perform: method value withArguments: arguments]]]
	ensure: [dispatching _ nil].
		
	^ MethodDispatchError new
		messageText: 'Could not dispatch for argument of type: ', aClass className;
		signal! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:44:46'!
instance
	"Get an instance of the MethodDispatcher."
	"orderedMethods ifNil: [
		self compile]."
	self compile.
	^ self! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 10:07:37'!
needsRecompilation
	orderedMethods _ nil! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/29/2021 19:49:03'!
orderedMethods
	^ orderedMethods! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/29/2021 19:50:57'!
selectorPrefix
	^ self subclassResponsibility ! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 11:49:14'!
selectorsInClassHierarchy: aClass
	^ ((aClass allSuperclasses collect: [:cls | cls selectors]) flatten, aClass selectors) asSet
	! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/29/2021 20:18:19'!
sortMethods: anOrderedCollection
	
	"Sorts the Class -> #selector associations in anOrderedCollection"
	
	|sorted|
	
	sorted _ SortedCollection new: anOrderedCollection size
				sortBlock: [:method1 :method2 |
					method1 key inheritsFrom: method2 key].
	sorted addAll: anOrderedCollection.
	
	^ sorted ! !

!MethodDispatcher class methodsFor: 'as yet unclassified' stamp: 'MM 6/30/2021 09:05:30'!
superDispatch: arguments

	"Dispatches one of the methods depending of type of the first argument in arguments."
	
	^ self dispatch: arguments from: dispatching key superclass! !

!MethodDispatcherExample class methodsFor: 'methods' stamp: 'MM 6/29/2021 20:43:00'!
propertiesOfObject: anObject
	
	"this method is dispatched when anObject is an object"
	
	^ 'properties of object'! !

!MethodDispatcherExample class methodsFor: 'methods' stamp: 'MM 6/29/2021 21:28:32'!
propertiesOfString: aString

	"we perform a 'super' dispatch here. it dispatches to Object method."
	
	^ (self superDispatch: {aString}), ' and a string'.! !

!MethodDispatcherExample class methodsFor: 'dispatcher' stamp: 'MM 6/29/2021 20:44:22'!
getPropertiesOf: anObject

	"This is the top level wrapping function that dispatches one of the defined methods depending on the type of anObject."
	
	^ self dispatch: {anObject}! !

!MethodDispatcherExample class methodsFor: 'dispatcher' stamp: 'MM 6/29/2021 20:46:56'!
selectorPrefix
	"Dispatch on methods that start with #propertiesOf"
	^ #propertiesOf! !

!MethodDispatcherExample class methodsFor: 'example' stamp: 'MM 6/29/2021 21:29:21'!
example

	"initialize the dispatcher so that methods are collected and sorted by argument type"
	self initialize.

	"print each line below to see how methods are dispatched"
	self getPropertiesOf: Object new.
	self getPropertiesOf: Morph new.
	self getPropertiesOf: 22.
	"there's no method defined for Point, so the Object method is dispatched"
	self getPropertiesOf: Point new.
	"method for string performs a 'super' dispatch:"
	self getPropertiesOf: 'asdf'! !
