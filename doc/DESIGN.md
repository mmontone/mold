# Design #

## Core ##

Concepts are objects with attributes, which are concepts in themselves too. So, a concept is an object aggregate.

A concept in itself doesn't have any meaning. All of a concept's structural meaning and semantics is defined at the metalevel. The metalevel is accessed via some type of attribute too, like "concept".

After a first basic bootstrapping, concepts form a graph, as attributes which are relationships have their meaning now.

There's complete control over a concept via its meta concept.

Subtype or extension versus meta type:

Such an architecture can be seen as a validation of the
nowadays  well-known  distinction  between  two  conceptu-
ally different kinds of instance-of relationships: (i) a tradi-
tional and implementation driven one where an instance is
an instance of its type, and (ii) a representation one where
an instance is described by another entity [5]. Atkinson and
Kühne [1,2] named these two kinds: form vs. contents or lin-
guistic vs. ontological.

## View modes ##

### Text view ###

The text view provides a text editor for editing the concept. Each concept is responsible for parsing its instances.

What we want to try:

* An extensible parser based on PEG. Each concept adds its PEG rules to the language parser.

Rules can be added first/last/before/after some other existent rule:
Example:
`ConditionConcept>>grammarDefinition: aGrammar
    aGrammar addRule: self grammarRule to: 'expression' before: 'Number'
`
To define grammar rules we define the PEG parsing rule referencing other concepts.

`ConditionConcept>>grammarRule
    ^ ParseRule new
        concept: self;
        text: 'if'; 
        rule: RLKExpressionConcept it;
        text: 'then';
        rule: RLKExpressionConcept it;
        text: 'else';
        rule: RLKExpressionConcept it;
        text: 'endif';
        or: [:rule |
              text: 'if'; 
              rule: RLKExpressionConcept it;
              text: 'then';
              rule: RLKExpressionConcept it;
              text: 'endif'].
`

A language contains the complete grammar (that's one of the purpose of having a separated language concept).

A language/concept can have different parsers, serializers and formatters. We can start with the simplest possible. S-Expressions and PEG based.
![Parser](parser.png)

Snake language is a Python like language, based on PEGs but more structured. Maybe more similar to Dylan, which is more structured.

Packages imported/exported symbols or concepts play a role when parsing concepts. Languages can be disabled/enabled for parsing a determinate concept method. The activation can be done at different granularity levels. At package level, concept level, method level, etc. 

Example:

`MyMethod use RegexLang:
    return /w+/;
`

### Diagram view ###

Concepts can be directly edited from their diagram representation. A diagram take any visual form. 

### Structured text view ###

The structured text view provides a structured editor. The syntax is defined via a template in the (meta)concept.

### Form view ###

The form view provides a form dialog to edit the concept.


## Projects ##

Projects are similar to Java Eclipse IDE projects. Among other things, they specify the set of dependencies and provide project specific tasks to run. 

A project task is some command that can be run via a wizard, and they can be accessed from the project menu (for instance).

Projects have natures attached. Project natures define the set of tasks and other properties. The project structure and behaviour is augmented via project natures.

Projects contain artifacts. Those can be languages, packages, or maybe other concepts.
