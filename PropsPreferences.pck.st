'From Cuis 5.0 [latest update: #4494] on 23 December 2020 at 4:30:06 pm'!
'Description Inspect Cuis preferences using a Properties browser.'!
!provides: 'PropsPreferences' 1 3!
!requires: 'Props' 1 51 nil!



!Preference methodsFor: '*PropsPreferences' stamp: 'MM 9/22/2019 17:28'!
asProperty
	^ PreferenceProperty on: self! !

!Preference methodsFor: '*PropsPreferences' stamp: 'MM 10/4/2019 13:04'!
categoryList
	^ categoryList ! !

!Preference methodsFor: '*PropsPreferences' stamp: 'MM 8/16/2020 17:50:46'!
helpString
	^ helpString! !

!Preferences class methodsFor: '*PropsPreferences' stamp: 'MM 12/23/2020 16:20:21'!
collectPreferencesAsProperties
	^ self preferencesDictionary values collect: [:pref | pref asProperty]! !

!Preferences class methodsFor: '*PropsPreferences' stamp: 'MM 12/23/2020 16:30:04'!
customPreferencesAsProperties
	^ {(PluggableProperty 
		getBlock: ['Set'] 
		setBlock: [:val |			Preferences perform: ('defaultFont', val) asSymbol	])
		name: 'Default Font';
		category: 'fonts';
		type: (ChoicePropertyType choices:  {'05'. '06'. '07'. '08'. '09'. '10'});
		description: 'Default system font sizes';
		yourself}! !

!Preferences class methodsFor: '*PropsPreferences' stamp: 'MM 8/16/2020 17:42:58'!
initializePreferences
	"self initializePreferences"
	self addPreference: #allowBlockArgumentAssignment category: #compiler default: false balloonHelp: ''. 
	self addPreference: #alternativeBrowseIt category: #ui default: false balloonHelp: ''. 
	self addPreference: #assignmentGlyphSelector category: #'code editing' default: #useLeftArrow balloonHelp: ''.
	self addPreference: #autoNumberUserChanges category: #system default: true balloonHelp: ''.! !

!Preferences class methodsFor: '*PropsPreferences' stamp: 'MM 2/10/2020 10:24:34'!
openPreferencesInspector
	"Open a window on the current set of preferences choices, allowing the user to view and change their settings"
	
	PropertiesBrowser openOn: Preferences! !

!Preferences class methodsFor: '*PropsPreferences' stamp: 'MM 12/23/2020 16:20:39'!
properties
	^ self collectPreferencesAsProperties, self customPreferencesAsProperties! !
