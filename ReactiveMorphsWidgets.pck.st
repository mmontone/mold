'From Cuis 6.0 [latest update: #5840] on 12 June 2023 at 4:10:55 pm'!
'Description '!
!provides: 'ReactiveMorphsWidgets' 1 3!
!requires: 'Morphic-Widgets-Extras2' 1 115 nil!
!requires: 'ReactiveMorphs' 1 85 nil!
SystemOrganization addCategory: 'ReactiveMorphsWidgets'!


!classDefinition: #RxLabelMorph category: 'ReactiveMorphsWidgets'!
LabelMorph subclass: #RxLabelMorph
	instanceVariableNames: 'contentsModel'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxLabelMorph class' category: 'ReactiveMorphsWidgets'!
RxLabelMorph class
	instanceVariableNames: ''!

!classDefinition: #RxButtonMorph category: 'ReactiveMorphsWidgets'!
PluggableButtonMorph subclass: #RxButtonMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxButtonMorph class' category: 'ReactiveMorphsWidgets'!
RxButtonMorph class
	instanceVariableNames: ''!

!classDefinition: #RxTreeMorph category: 'ReactiveMorphsWidgets'!
RxContainerMorph subclass: #RxTreeMorph
	instanceVariableNames: 'model treeAccessor selectedNode'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxTreeMorph class' category: 'ReactiveMorphsWidgets'!
RxTreeMorph class
	instanceVariableNames: ''!

!classDefinition: #RxMorphTreeMorph category: 'ReactiveMorphsWidgets'!
RxTreeMorph subclass: #RxMorphTreeMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxMorphTreeMorph class' category: 'ReactiveMorphsWidgets'!
RxMorphTreeMorph class
	instanceVariableNames: ''!

!classDefinition: #RxIndentedColLayoutMorph category: 'ReactiveMorphsWidgets'!
ColLayoutMorph subclass: #RxIndentedColLayoutMorph
	instanceVariableNames: 'indentation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxIndentedColLayoutMorph class' category: 'ReactiveMorphsWidgets'!
RxIndentedColLayoutMorph class
	instanceVariableNames: ''!

!classDefinition: #RxInstVarValue category: 'ReactiveMorphsWidgets'!
Object subclass: #RxInstVarValue
	instanceVariableNames: 'name value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxInstVarValue class' category: 'ReactiveMorphsWidgets'!
RxInstVarValue class
	instanceVariableNames: ''!

!classDefinition: #RxTreeAccessor category: 'ReactiveMorphsWidgets'!
Object subclass: #RxTreeAccessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxTreeAccessor class' category: 'ReactiveMorphsWidgets'!
RxTreeAccessor class
	instanceVariableNames: ''!

!classDefinition: #RxDefaultTreeAccessor category: 'ReactiveMorphsWidgets'!
RxTreeAccessor subclass: #RxDefaultTreeAccessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxDefaultTreeAccessor class' category: 'ReactiveMorphsWidgets'!
RxDefaultTreeAccessor class
	instanceVariableNames: ''!

!classDefinition: #RxExplorerTreeAccessor category: 'ReactiveMorphsWidgets'!
RxTreeAccessor subclass: #RxExplorerTreeAccessor
	instanceVariableNames: 'action'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxExplorerTreeAccessor class' category: 'ReactiveMorphsWidgets'!
RxExplorerTreeAccessor class
	instanceVariableNames: ''!

!classDefinition: #RxMorphTreeAccessor category: 'ReactiveMorphsWidgets'!
RxTreeAccessor subclass: #RxMorphTreeAccessor
	instanceVariableNames: 'action'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsWidgets'!
!classDefinition: 'RxMorphTreeAccessor class' category: 'ReactiveMorphsWidgets'!
RxMorphTreeAccessor class
	instanceVariableNames: ''!


!RxTreeMorph commentStamp: 'MM 6/4/2023 23:41:20' prior: 0!
RxTreeMorph on: Morph new :: openInWindow.

RxTreeMorph on: self runningWorld :: openInWindow.

PluggableScrollPane new
	scroller: (RxTreeMorph on: self runningWorld);
	openInWindow.
	
PluggableScrollPane new
	scroller: (RxTreeMorph on: self runningWorld treeAccessor: (RxExplorerTreeAccessor new action: [:ev :itemWrapper | itemWrapper item inspect]));
	openInWindow.
	
PluggableScrollPane new
	scroller: (RxTreeMorph on: self runningWorld treeAccessor: (RxMorphTreeAccessor new action: [:ev :morph | morph highlighted: morph isHighlighted not]));
	openInWindow.
	!

!RxLabelMorph methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 17:08:16'!
contents: newContents
	contentsModel := newContents.
	newContents
		when: #changed:
		send: #update:
		to: self.
	super contents: newContents value.! !

!RxLabelMorph methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 17:13:13'!
update: anObject
	super contents: anObject! !

!RxLabelMorph class methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 14:32:21'!
example1

	|text lblMorph|
	
	text := 'Hello world' asValue.

	lblMorph := RxLabelMorph contents: text.

	lblMorph openInWorld.
	
	"Then update the value of text and see the morph change:"
	"text value: 'Bye world'"! !

!RxButtonMorph methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 15:37:28'!
label: aString
	"Label this button with the given string."
	
	(aString isKindOf: ValueModel) ifTrue: [
		aString when: #changed: send: #update: to: self].
	

	^ super label: aString! !

!RxTreeMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 00:00:48'!
buildNodeFor: anObject

	| hasChildren showChildren |
	
	hasChildren := RxFormula with: anObject do: [:obj | treeAccessor hasChildren: obj].
	showChildren := RxValue with: false.
	
	^ hasChildren 
		ifIsTrue: [
			ColLayoutMorph new children: {
				RowLayoutMorph new children: {
					PluggableButtonMorph label: '+' action: [showChildren value: showChildren value not].
					self labelMorphFor: anObject.
				}.
				showChildren ifIsTrue: [ |children|
					"This RxCollection is not enough. We need a collection that listen changed events on anObject."
					children := RxCollection with: (treeAccessor childrenOf: anObject).
					RxIndentedColLayoutMorph new rxChildren: children from: [:node | self buildNodeFor: node]]
			}]
		ifIsFalse: [self labelMorphFor: anObject]! !

!RxTreeMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 00:01:28'!
initialize: anObject treeAccessor: aTreeAccessor

	model := anObject.
	treeAccessor := aTreeAccessor.
	selectedNode := RxValue with: nil.
	
	self color: Color white.
	
	self setMorph: (self buildNodeFor: model).! !

!RxTreeMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 11:18:29'!
labelMorphFor: anObject

	| labelMorph selectionEmphasis |
	
	labelMorph := (RxLabelMorph contents: (treeAccessor nameOf: anObject))
		onClick: [:ev | 
			selectedNode value: anObject.
			treeAccessor action ifNotNil: [:action | action value: ev value: anObject]];
		yourself.
	
	selectionEmphasis := RxFormula with: selectedNode do: [:node | node == anObject ifTrue: [1] ifFalse: [0]].
	labelMorph rxProp: #emphasis: is: selectionEmphasis.
	
	^ labelMorph! !

!RxTreeMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:26:14'!
on: anObject
	^ self on: anObject treeAccessor: RxExplorerTreeAccessor new

	! !

!RxTreeMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 18:26:43'!
on: anObject treeAccessor: aTreeAccessor

	^ self new initialize: anObject treeAccessor: aTreeAccessor! !

!RxMorphTreeMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:08:42'!
buildNodeFor: aMorph

	| isHighlighted |
	
	isHighlighted := RxFormula with: selectedNode do: [:node | node == aMorph].
	aMorph rxProp: #highlighted: is: isHighlighted.
	
	^ super buildNodeFor: aMorph! !

!RxMorphTreeMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 11:26:23'!
on: aMorph
	^ self on: aMorph treeAccessor: RxMorphTreeAccessor new! !

!RxIndentedColLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 20:02:22'!
initialize

	super initialize.
	
	indentation := 30.! !

!RxIndentedColLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 20:10:59'!
layoutSubmorphs

	| submorphsToLayout nextMorph currentPosition nextY currentCol colWidth |
	
	currentPosition := indentation@0.
	
	submorphsToLayout := self submorphsToLayout.	
	
	currentCol := OrderedCollection new.
	
	submorphsToLayout size to: 1 by: -1 do: [ :index |
		
		nextMorph := submorphsToLayout at: index.
		
		nextMorph morphPosition: currentPosition.
		
		currentCol add: nextMorph.
		
		nextY := (currentPosition y) + separation + nextMorph morphExtent y.
		
		currentPosition := (currentPosition x) @ nextY].
	
	colWidth := self morphWidth.
	
	currentCol do: [:m | 		m morphExtent: colWidth  @ m morphExtent y].
	
	self layoutNeeded: false! !

!RxIndentedColLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 20:05:23'!
minimumExtent

	^ super minimumExtent + (30 @ 0).! !

!RxInstVarValue methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 17:50:57'!
initialize: aString value: anObject

	name := aString.
	value := anObject.! !

!RxInstVarValue class methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 17:50:37'!
named: aString value: anObject

	^ self new initialize: aString value: anObject! !

!RxTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:31:51'!
actionFor: anObject

	^ nil! !

!RxTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 17:55:21'!
childrenOf: anObject

	^ self subclassResponsibility ! !

!RxTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:53:42'!
hasChildren: anObject

	^ self childrenOf: anObject :: isEmpty not! !

!RxTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:54:07'!
nameOf: anObject

	^ anObject printString! !

!RxDefaultTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 17:49:04'!
childrenOf: anObject

	anObject respondsTo: #treeChildren :: ifTrue: [
		^ anObject treeChildren].
	
	anObject isCollection ifTrue: [
		^ anObject].
	
	^(anObject class allInstVarNames asOrderedCollection withIndexCollect: [:each :index |
		RxInstVarValue named: each value: (anObject instVarAt: index)]),
		((1 to: anObject basicSize) collect: [:index |
			RxInstVarValue named: index printString value: (anObject basicAt: index)])! !

!RxExplorerTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:32:40'!
actionFor: anObject

	^ action! !

!RxExplorerTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 17:54:08'!
childrenOf: anObject

	anObject customizeExplorerContents :: ifTrue: [^anObject explorerContents].
	
	anObject isKindOf: ObjectExplorerWrapper :: ifTrue: [^ anObject contents].
	
	"For all others, show named vars first, then indexed vars"
	^(anObject class allInstVarNames asOrderedCollection withIndexCollect: [:each :index |
		ObjectExplorerWrapper
			with: (anObject instVarAt: index)
			name: each
			model: anObject
			parent: self]) ,
	((1 to: anObject basicSize) collect: [:index |
		ObjectExplorerWrapper
			with: (anObject basicAt: index)
			name: index printString
			model: anObject
			parent: self])! !

!RxExplorerTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:32:27'!
initialize

	super initialize.
	
	action := nil! !

!RxExplorerTreeAccessor methodsFor: 'accessing' stamp: 'MM 6/4/2023 22:32:47'!
action
	"Answer the value of action"

	^ action! !

!RxExplorerTreeAccessor methodsFor: 'accessing' stamp: 'MM 6/4/2023 22:32:47'!
action: anObject
	"Set the value of action"

	action := anObject! !

!RxMorphTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:52:55'!
actionFor: aMorph

	^ action! !

!RxMorphTreeAccessor methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 22:53:14'!
childrenOf: aMorph

	^ aMorph submorphs! !

!RxMorphTreeAccessor methodsFor: 'accessing' stamp: 'MM 6/4/2023 22:55:55'!
action
	"Answer the value of action"

	^ action! !

!RxMorphTreeAccessor methodsFor: 'accessing' stamp: 'MM 6/4/2023 22:55:55'!
action: anObject
	"Set the value of action"

	action := anObject! !
