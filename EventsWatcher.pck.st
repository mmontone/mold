'From Cuis 6.0 [latest update: #5067] on 11 February 2022 at 10:12:42 pm'!
'Description Tools for watching events on any object using a drag and drop user interface.
Inspect those events and attach handlers for user level programmability.

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'EventsWatcher' 1 29!
!requires: 'ObjectGrabber' 1 2 nil!
!requires: 'AXAnnouncements' 1 2 nil!
!requires: 'Morphic-Widgets-Extras2' 1 98 nil!
!requires: 'Utils' 1 4 nil!
SystemOrganization addCategory: 'EventsWatcher'!


!classDefinition: #EventHandlerEditorMorph category: 'EventsWatcher'!
LayoutMorph subclass: #EventHandlerEditorMorph
	instanceVariableNames: 'eventHandler handlerSource'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventHandlerEditorMorph class' category: 'EventsWatcher'!
EventHandlerEditorMorph class
	instanceVariableNames: ''!

!classDefinition: #EventHandlersListMorph category: 'EventsWatcher'!
LayoutMorph subclass: #EventHandlersListMorph
	instanceVariableNames: 'model listModel'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventHandlersListMorph class' category: 'EventsWatcher'!
EventHandlersListMorph class
	instanceVariableNames: ''!

!classDefinition: #EventsWatcherListMorph category: 'EventsWatcher'!
LayoutMorph subclass: #EventsWatcherListMorph
	instanceVariableNames: 'model eventIndex'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventsWatcherListMorph class' category: 'EventsWatcher'!
EventsWatcherListMorph class
	instanceVariableNames: ''!

!classDefinition: #EventsWatcherMorph category: 'EventsWatcher'!
LayoutMorph subclass: #EventsWatcherMorph
	instanceVariableNames: 'model eventIndex watchedObjectIndex'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventsWatcherMorph class' category: 'EventsWatcher'!
EventsWatcherMorph class
	instanceVariableNames: ''!

!classDefinition: #TimerMorph category: 'EventsWatcher'!
LabelMorph subclass: #TimerMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'TimerMorph class' category: 'EventsWatcher'!
TimerMorph class
	instanceVariableNames: ''!

!classDefinition: #WatchedObjectsListMorph category: 'EventsWatcher'!
PluggableListMorph subclass: #WatchedObjectsListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'WatchedObjectsListMorph class' category: 'EventsWatcher'!
WatchedObjectsListMorph class
	instanceVariableNames: ''!

!classDefinition: #TimerEvent category: 'EventsWatcher'!
AXAnnouncement subclass: #TimerEvent
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'TimerEvent class' category: 'EventsWatcher'!
TimerEvent class
	instanceVariableNames: ''!

!classDefinition: #EventHandler category: 'EventsWatcher'!
Object subclass: #EventHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventHandler class' category: 'EventsWatcher'!
EventHandler class
	instanceVariableNames: ''!

!classDefinition: #PluggableEventHandler category: 'EventsWatcher'!
EventHandler subclass: #PluggableEventHandler
	instanceVariableNames: 'name description handler subject eventClass handlerSource'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'PluggableEventHandler class' category: 'EventsWatcher'!
PluggableEventHandler class
	instanceVariableNames: ''!

!classDefinition: #EventHandlersModel category: 'EventsWatcher'!
Object subclass: #EventHandlersModel
	instanceVariableNames: 'eventHandlers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventHandlersModel class' category: 'EventsWatcher'!
EventHandlersModel class
	instanceVariableNames: ''!

!classDefinition: #EventsWatcher category: 'EventsWatcher'!
Object subclass: #EventsWatcher
	instanceVariableNames: 'subscriptions events scripts'
	classVariableNames: 'GlobalInstance'
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'EventsWatcher class' category: 'EventsWatcher'!
EventsWatcher class
	instanceVariableNames: ''!

!classDefinition: #SelectorEvent category: 'EventsWatcher'!
Object subclass: #SelectorEvent
	instanceVariableNames: 'selector announcer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'SelectorEvent class' category: 'EventsWatcher'!
SelectorEvent class
	instanceVariableNames: ''!

!classDefinition: #Timer category: 'EventsWatcher'!
Object subclass: #Timer
	instanceVariableNames: 'name description interval announcer running startedTime'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'EventsWatcher'!
!classDefinition: 'Timer class' category: 'EventsWatcher'!
Timer class
	instanceVariableNames: ''!


!EventsWatcher commentStamp: '<historical>' prior: 0!
The model for a EventsWatcherMorph.

Structure:

  subscriptions <AXSubscriptionCollection> -- The collection of active subscriptions.
  events <OrderedCollection> -- The collection of events triggered.
  scripts <Dictionary> -- A dictionary with subscriptions as keys, and a collection of scripts as values.!

!SelectorEvent commentStamp: 'MM 2/7/2022 12:51:20' prior: 0!
An event triggered by default Smalltalk's event system, using #when:send:to: and family.!

!PluggableEventHandler methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 17:07:48'!
name
	^ name! !

!EventHandlerEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 1/29/2022 20:21:14'!
buildMorph

	| form actionsRow |
	
	form _ FormLayoutMorph new.
	
	form addRowWithLabel: 'name' inputMorph: (StringInputMorph on: (AspectAdaptor on: eventHandler aspect: #name)).
	form addRowWithLabel: 'subject' inputMorph: (ObjectDraggerMorph on: (AspectAdaptor on: eventHandler aspect: #subject)).
	form addRowWithLabel: 'event class' inputMorph: (SelectionButtonMorph on: (AspectAdaptor on: eventHandler aspect: #eventClass) elems: AXAnnouncement allSubclasses ). 
	form addRowWithLabel: 'description' inputMorph: (TextAreaMorph on: (AspectAdaptor on: eventHandler aspect: #description)).
	form addRowWithLabel: 'handler' inputMorph: (TextModelMorph textProvider: self textGetter: #handlerSource  textSetter: #handlerSource:).
	
	form layoutRows.
	
	self addMorphUseAll: form.
	
	actionsRow _ RowLayoutMorph new.
	actionsRow addMorph: (PluggableButtonMorph model: self action: #saveEventHandler label: 'save').
	self addMorph: actionsRow fixedHeight: 30. ! !

!EventHandlerEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 17:46:39'!
initialize: anEventHandler

	eventHandler _ anEventHandler.
	handlerSource _ ''.
	
	self buildMorph.! !

!EventHandlerEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 22:40:41'!
saveEventHandler

	eventHandler subscribe! !

!EventHandlerEditorMorph methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:46:48'!
eventHandler
	"Answer the value of eventHandler"

	^ eventHandler! !

!EventHandlerEditorMorph methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:46:48'!
eventHandler: anObject
	"Set the value of eventHandler"

	eventHandler _ anObject! !

!EventHandlerEditorMorph methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:46:48'!
handlerSource
	"Answer the value of handlerSource"

	^ handlerSource! !

!EventHandlerEditorMorph methodsFor: 'accessing' stamp: 'MM 9/20/2021 19:36:28'!
handlerSource: anObject
	"Set the value of handlerSource"
	
	|handler|

	handlerSource _ anObject.
	
	handler _ Compiler evaluate: handlerSource.
	self assert: handler isClosure description: 'Handler should evaluate to a closure with one argument'. 
	eventHandler handler: handler
	! !

!EventHandlerEditorMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 17:14:19'!
on: anEventHandler

	^ self newColumn initialize: anEventHandler! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:09:01'!
addEventHandler
	
	| eventHandler |
	
	eventHandler _ PluggableEventHandler new.
	
	model add: eventHandler.
	
	listModel listChanged.
	
	(EventHandlerEditorMorph on: eventHandler) openInWindowTitled: 'Event handler'.! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:11:00'!
buildMorph

	| eventsList buttonsRow |
	
	eventsList _ PluggableListMorph new.
	listModel _ ListValueModel with: (AspectAdaptor on: self aspect: #eventHandlers)
					for: eventsList.						
	self addMorphUseAll: eventsList.
	
	buttonsRow _ RowLayoutMorph new.
		
	buttonsRow addMorph: (PluggableButtonMorph model: self action: #editSelectedEventHandler  label: 'edit').
	buttonsRow addMorph: (PluggableButtonMorph model: self action: #addEventHandler label: 'add').
	buttonsRow addMorph: (PluggableButtonMorph model: self action: #removeEventHandler label: 'remove').
	self addMorph: buttonsRow fixedHeight: 30. 
		! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:08:27'!
editSelectedEventHandler

	listModel getCurrentSelection ifNotNil: [:eventHandler |
		(EventHandlerEditorMorph on: eventHandler) openInWindowTitled: 'Event handler editor'].! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 22:10:13'!
eventHandlers

	^ model eventHandlers! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 20:38:15'!
initialize

	super initialize.
	
	model _ EventHandlersModel new.
	
	self buildMorph.! !

!EventHandlersListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:11:41'!
removeEventHandler
	
	listModel getCurrentSelection ifNotNil: [:eventHandler |
		model remove: eventHandler.
		listModel listChanged]! !

!EventHandlersListMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 23:55:13'!
new
	^ self newColumn! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:36:08'!
buildMorph

	| eventsList actionsRow |
	
	eventsList _ PluggableListMorph model: self listGetter: #events indexGetter: #eventIndex indexSetter: #eventIndex:.
	
	self addMorphUseAll: eventsList.
	
	actionsRow _ RowLayoutMorph new.
	
	actionsRow addMorph: (PluggableButtonMorph model: self action: #inspectSelectedEvent  label: 'inspect' ).
	
	self addMorph: actionsRow fixedHeight: 30.
	
	 
	
	! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:23:44'!
eventIndex
	^ eventIndex ifNil: [0]! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:23:59'!
eventIndex: anIndex
	eventIndex _ anIndex! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 13:04:06'!
events

	^ model events! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 19:56:11'!
initialize

	super initialize.
	
	model _ EventsWatcher new.
	
	model addDependent: self.
	
	self buildMorph.! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:36:44'!
inspectSelectedEvent

	eventIndex ifNotNil: [
		(model events at: eventIndex) inspect]! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 13:10:04'!
update: aSymbol
	self changed: aSymbol! !

!EventsWatcherListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:06:29'!
watch: anObjectOrCollection
	anObjectOrCollection isCollection ifTrue: [
		anObjectOrCollection do: [:el |
			model watch: el]]
		ifFalse: [model watch: anObjectOrCollection]! !

!EventsWatcherListMorph class methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:15:23'!
new
	^ self newColumn! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 18:44:17'!
buildMorph

	| eventsList eventActionsRow watchingList eventsCol watchingCol watchingActionsRow |
	
	watchingCol _ LayoutMorph newColumn.
	eventsCol _ LayoutMorph newColumn.
	
	watchingList _ WatchedObjectsListMorph model: self 
					listGetter: #watchedObjects  
					indexGetter: #watchedObjectIndex  
					indexSetter: #watchedObjectIndex:.
	
	watchingCol addMorph: (LabelMorph contents: 'Watching:') fixedHeight: 30. 				
	watchingCol addMorphUseAll: watchingList.
	
	watchingActionsRow _ LayoutMorph newRow.
	watchingActionsRow addMorph: (PluggableButtonMorph model: self 
								stateGetter: #selectedWatchedObjectState 
								action: #toggleStartStop label: 'Stop').
	watchingActionsRow addMorph: (PluggableButtonMorph model: self
								action: #addScript
								label: 'Add script').
	
	watchingCol addMorph: watchingActionsRow fixedHeight: 30.
	
	self addMorph: watchingCol proportionalWidth: 0.4.
	
	eventsList _ PluggableListMorphPlus model: self 
				listGetter: #events 
				indexGetter: #eventIndex 
				indexSetter: #eventIndex:.
	
	eventsCol addMorph: (LabelMorph contents: 'Events:') fixedHeight: 30. 
	eventsCol addMorphUseAll: eventsList.
	
	eventActionsRow _ RowLayoutMorph new.
	
	eventActionsRow addMorph: (PluggableButtonMorph model: self 
							action: #inspectSelectedEvent  
							label: 'inspect' ).
	
	eventsCol addMorph: eventActionsRow fixedHeight: 30.
	
	self addAdjusterAndMorph: eventsCol layoutSpec: LayoutSpec useAll. ! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 17:51:36'!
dropToWatchList: anObject

	model watch: anObject.
	self changed: #watchedObjects! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:24:51'!
eventIndex
	^ eventIndex ifNil: [0]! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:24:51'!
eventIndex: anIndex
	eventIndex _ anIndex! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:24:51'!
events

	^ model events! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:32:03'!
initialize: aModel

	model _ aModel.
	
	model addDependent: self.
	
	self buildMorph.! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:24:51'!
inspectSelectedEvent

	eventIndex ifNotNil: [
		(model events at: eventIndex) inspect]! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 18:30:47'!
selectedWatchedObject

	^ self watchedObjectIndex isZero ifFalse: [
		self watchedObjects at: self watchedObjectIndex]
		ifTrue: [nil]! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 18:32:01'!
selectedWatchedObjectState

	^ self selectedWatchedObject ifNil: [false] ifNotNil: [:wo | wo state] ! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:40:31'!
update: aSymbol
	self changed: aSymbol! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:24:51'!
watch: anObjectOrCollection
	anObjectOrCollection isCollection ifTrue: [
		anObjectOrCollection do: [:el |
			model watch: el]]
		ifFalse: [model watch: anObjectOrCollection]! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:46:53'!
watchedObjectIndex
	^ watchedObjectIndex ifNil: [0]! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:47:08'!
watchedObjectIndex: anIndex
	watchedObjectIndex _ anIndex! !

!EventsWatcherMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 16:46:13'!
watchedObjects
	^ model watchedObjects! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 18:00:35'!
initialize
	"self initialize"
	self installWatchHalo.! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 18:21:03'!
installWatchHalo

	|spec|
	
	spec _ 
	HaloSpec new 
		horizontalPlacement: #rightCenter 
		verticalPlacement: #bottom 
		color: Color pink
		iconSymbol: #findIcon
		addHandleSelector: #addWatchHandle: 
		hoverHelp: 'Watch'.
	Preferences haloSpecifications.	
	Preferences parameters at: #HaloSpecs
		put: ((Preferences parameters at: #HaloSpecs), {spec}).! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:31:45'!
new
	^ self on: EventsWatcher globalInstance! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:31:33'!
on: aModel
	^ self newRow initialize: aModel! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 18:26:12'!
open

	^ self new openInWindowLabeled: 'Watcher'! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:31:10'!
openOn: aModel

	^ (self on: aModel) openInWindowLabeled: 'Watcher'! !

!EventsWatcherMorph class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 17:57:46'!
worldMenuForOpenGroup
	^ `{{
			#itemGroup 		-> 		20.
			#itemOrder 		-> 		10.
			#label 			->	'Events Watcher'.
			#selector 		-> 		#open.
			#object -> EventsWatcherMorph.
			#icon 			-> 		#findIcon.
			#balloonText 	-> 		'A tool for watching objects events and status'.
		} asDictionary}`! !

!WatchedObjectsListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 17:49:39'!
initialize

	super initialize.
	
	self setProperty: #allowsMorphDrop toValue: true.
	self setProperty: #dropOutsideListActionSelector toValue: #dropToWatchList:.! !

!WatchedObjectsListMorph methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 17:40:52'!
wantsDroppedMorph: aMorph event: evt

	"^(aMorph is: #DraggingGuideMorph)
		and: [ (aMorph valueOfProperty: #dragSource) = (self valueOfProperty: #acceptedDragSource) ]"
		
		^ true
		! !

!PluggableEventHandler methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 22:28:53'!
eventAnnounced: anAnnouncement
	handler value: anAnnouncement! !

!PluggableEventHandler methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 17:45:02'!
initialize

	name _ ''.
	description _ ''.
	handlerSource _ ''! !

!PluggableEventHandler methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 22:28:33'!
subscribe
	subject announcer when: eventClass send: #eventAnnounced: to:  self.! !

!PluggableEventHandler methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:12:12'!
unsubscribe
	subject ifNotNil: [subject announcer unsubscribe: self]! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:07:55'!
description
	^ description! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:07:38'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:21:28'!
eventClass
	"Answer the value of eventClass"

	^ eventClass! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:21:28'!
eventClass: anObject
	"Set the value of eventClass"

	eventClass _ anObject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:07:38'!
handler
	"Answer the value of handler"

	^ handler! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:07:38'!
handler: anObject
	"Set the value of handler"

	handler _ anObject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:21:42'!
handlerSource
	"Answer the value of handlerSource"

	^ handlerSource! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:21:42'!
handlerSource: anObject
	"Set the value of handlerSource"

	handlerSource _ anObject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:07:38'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 17:21:28'!
subject
	"Answer the value of subject"

	^ subject! !

!PluggableEventHandler methodsFor: 'accessing' stamp: 'MM 9/20/2021 22:25:03'!
subject: anObject

	"Set the value of subject"
	
	subject ifNotNil: [
		subject announcer unsubscribe: self].

	subject _ anObject.! !

!EventHandlersModel methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:02:01'!
add: anEventHandler

	eventHandlers add: anEventHandler! !

!EventHandlersModel methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 22:10:26'!
eventHandlers

	^ eventHandlers! !

!EventHandlersModel methodsFor: 'as yet unclassified' stamp: 'MM 9/20/2021 20:36:18'!
initialize

	eventHandlers _ OrderedCollection new.! !

!EventHandlersModel methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 00:02:30'!
remove:  anEventHandler
	anEventHandler unsubscribe.
	eventHandlers remove: anEventHandler! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 13:09:13'!
eventTriggered: anEvent

	events add: anEvent.
	self changed: #events! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:18:59'!
events

	^ events! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 18:46:37'!
initialize

	subscriptions _ AXSubscriptionCollection new.
	events _ OrderedCollection new.
	scripts _ Dictionary new.! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 18:46:25'!
scriptsFor: aSubscription

	scripts at: aSubscription! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:20:16'!
selectorEventTriggered: aSymbol announcer: anObject

	events add: (SelectorEvent selector: aSymbol announcer: anObject ).
	self changed: #events! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:00:27'!
stopWatching: anObject

	subscriptions removeAllSuchThat: [:sub | sub announcer == anObject].
	
	anObject announcer unsubscribe: self.! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 9/17/2021 12:02:45'!
stopWatching: anObject event: anEvent

	subscriptions removeAllSuchThat: [:sub | sub announcer == anObject and: [sub announcementClass = anEvent]].
	
	anObject announcer unsubscribe: self from: anEvent! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:47:03'!
watch: anObject

	(anObject respondsTo: #announcer) ifTrue: [ |subs|
		subs _ anObject announcer when: AXAnnouncement send: #eventTriggered: to: self.
		subscriptions addAll: subs.
		self changed: #watchedObjects]
	ifFalse: [
		anObject eventsTriggered ifEmpty: [
			self inform: 'There are no events to watch for this object'.
			^ self].
		anObject eventsTriggered do: [:eventSymbol |
			anObject when: eventSymbol 
				send: #selectorEventTriggered:announcer: to: self 
				withArguments: {eventSymbol. anObject}.
			subscriptions add: (AXStrongSubscription new
							announcer: anObject;
							subscriber:  self;
							selector: eventSymbol;
							yourself).
			self changed: #watchedObjects]]! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:41:46'!
watch: anObject event: anEvent

	anEvent isSymbol ifTrue: [
		anObject when: anEvent send: #selectorEventTriggered:announcer: 
			to: self withArguments: {anEvent. anObject}.
		subscriptions add: (AXStrongSubscription new
							announcer: anObject;
							selector: anEvent;
							subscriber: self;
							yourself).
		self changed: #watchedObjects]
		ifFalse: [ |subs|
			subs _ anObject announcer when: anEvent send: #eventTriggered: to: self.
			subscriptions addAll: subs.
			self changed: #watchedObjects]! !

!EventsWatcher methodsFor: 'as yet unclassified' stamp: 'MM 9/18/2021 18:05:43'!
watchedObjects

	^ (subscriptions collect: [:sub | sub announcer subject]) asIdentitySet asOrderedCollection! !

!EventsWatcher class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 19:57:46'!
globalInstance

	^ GlobalInstance ifNil: [GlobalInstance _ self new]! !

!EventsWatcher class methodsFor: 'as yet unclassified' stamp: 'MM 2/6/2022 20:00:46'!
smalltalkEditorMenuOptions
	
	^`{
			{
				#itemGroup 		-> 		20.
				#itemOrder 		-> 		70.
				#label 			-> 	'Watch it'.
				#selector 		-> 		#watchIt.
				#icon 			-> 		#findIcon
			} asDictionary.
		}`! !

!SelectorEvent methodsFor: 'accessing' stamp: 'MM 2/7/2022 13:25:42'!
announcer
	"Answer the value of announcer"

	^ announcer! !

!SelectorEvent methodsFor: 'accessing' stamp: 'MM 2/7/2022 13:25:42'!
announcer: anObject
	"Set the value of announcer"

	announcer _ anObject! !

!SelectorEvent methodsFor: 'accessing' stamp: 'MM 2/7/2022 13:25:42'!
selector
	"Answer the value of selector"

	^ selector! !

!SelectorEvent methodsFor: 'accessing' stamp: 'MM 2/7/2022 13:25:42'!
selector: anObject
	"Set the value of selector"

	selector _ anObject! !

!SelectorEvent class methodsFor: 'as yet unclassified' stamp: 'MM 2/7/2022 13:26:07'!
selector: aSymbol announcer: anObject
	^ self new
		selector: aSymbol;
		announcer: anObject;
		yourself! !

!Timer methodsFor: 'accessing' stamp: 'MM 9/19/2021 10:31:10'!
description
	"Answer the value of description"

	^ description! !

!Timer methodsFor: 'accessing' stamp: 'MM 9/19/2021 10:31:10'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!Timer methodsFor: 'accessing' stamp: 'MM 9/19/2021 10:31:10'!
interval
	"Answer the value of interval"

	^ interval! !

!Timer methodsFor: 'accessing' stamp: 'MM 9/19/2021 10:31:10'!
interval: anObject
	"Set the value of interval"

	interval _ anObject! !

!Timer methodsFor: 'accessing' stamp: 'MM 9/19/2021 10:31:10'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:42:29'!
announcer
	^ announcer! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:43:46'!
initialize

	announcer _ AXPluggableAnnouncer on: self.
	running _ false! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:26:48'!
running
	^ running! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:40:23'!
shouldGetStepsFrom: aWorld
	^ true! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:33:02'!
start
	startedTime _ Time localMillisecondClock.
	
	self runningWorld
		startStepping: self
		at: startedTime
		selector: #step
		stepTime: nil.
			
	running _ true! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:37:02'!
step

	self running ifFalse: [^self].
	
	(Time localMillisecondClock >= (startedTime + interval)) ifTrue: [
		announcer announce: TimerEvent new].! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:41:31'!
stepTime
	^ interval! !

!Timer methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:37:36'!
stop
	running _ false.
	self runningWorld stopSteppingMorph: self! !

!Timer class methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:35:34'!
interval: aNumber
	^ self new
		interval: aNumber;
		yourself! !

!Timer class methodsFor: 'as yet unclassified' stamp: 'MM 9/19/2021 11:35:11'!
name: aString interval: aNumber
	^ self new name: aString;
		interval: aNumber;
		yourself! !

!Object methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 18:44:10'!
eventsTriggered

	"The collection of events triggered by this object."
	
	^ OrderedCollection new! !

!Object methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 18:44:42'!
statusSelector

	"Selector to be used to extract the status of this object."
	
	^ nil! !

!Object methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 19:58:15'!
watch

	EventsWatcher globalInstance watch: self! !

!Object methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 18:42:59'!
watchSpec

	"A spec of the status and events triggered by this object.
	
	Used by EventsWatcher.	"
	
	^ nil! !

!SmalltalkEditor methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 19:59:27'!
watchIt
	
	self evaluateSelectionAndDo: [ :result | result watch] ifFail: [] profiled: false.! !

!HaloMorph methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 18:01:33'!
addWatchHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #openWatchMenu! !

!HaloMorph methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 21:45:24'!
openWatchMenu

	| watcherMorph |
	
	watcherMorph _ self activeHand world
		findAWindowSatisfying: [ :aWindow |
			aWindow model class == EventsWatcher ]
		orMakeOneUsing: [
			EventsWatcherMorph open ].
		
	watcherMorph watch: target! !

!HaloMorph class methodsFor: '*EventsWatcher' stamp: 'MM 2/6/2022 18:16:43'!
haloWatchIcon
	^ self icons
		at: #haloWatchIcon
		ifAbsentPut: [ Theme current findIcon ]! !
EventsWatcherMorph initialize!
