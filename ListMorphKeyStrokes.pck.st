'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 15 August 2019 at 12:07:04.039005 pm'!
'Description Please enter a description for this package'!
!provides: 'ListMorphKeyStrokes' 1 0!

!PluggableListMorph methodsFor: '*ListMorphKeyStrokes' stamp: 'MM 8/15/2019 10:58'!
keyStroke: aKeyboardEvent 
	"Process keys"
	
	| aCharacter |
	(Theme current keyStroke: aKeyboardEvent morph: self)
		ifTrue: [^ self].
	(self focusKeyboardFor: aKeyboardEvent)
		ifTrue: [ ^ self ].
	(self scrollByKeyboard: aKeyboardEvent) 
		ifTrue: [ ^self ].
	aCharacter _ aKeyboardEvent keyCharacter.
	(self arrowKey: aCharacter)
		ifTrue: [ ^self ].
	aCharacter asciiValue = 27 ifTrue: [	" escape key"
		^ self mouseButton2Activity].
	(Preferences valueOfFlag: #listsKeystrokesWithModifierKey)
		ifTrue: [
			aKeyboardEvent anyModifierKeyPressed
				ifTrue: [
					(self keystrokeAction: aCharacter)
						ifTrue: [ ^self ]].
				^ self keyboardSearch: aCharacter]
		ifFalse: [
			aKeyboardEvent anyModifierKeyPressed
				ifFalse: [
					(self keystrokeAction: aCharacter)
						ifTrue: [ ^self ]]
				ifTrue: [^self keyboardSearch: aCharacter]]! !

!PluggableListMorph class methodsFor: '*ListMorphKeyStrokes' stamp: 'MM 8/15/2019 12:05'!
initialize
	Preferences addPreference: #listsKeystrokesWithModifierKey category: #input default: true  balloonHelp: 'Toggle keystrokes to work with modifier key or not'.! !
