'From Cuis 5.0 [latest update: #4030] on 17 February 2020 at 2:38:53 pm'!
'Description '!
!provides: 'ClassCompilerTests' 1 0!
!requires: 'ClassCompiler' 1 4 nil!
SystemOrganization addCategory: #ClassCompilerTests!


!classDefinition: #ClassCompilerTest category: #ClassCompilerTests!
Object subclass: #ClassCompilerTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompilerTests'!
!classDefinition: 'ClassCompilerTest class' category: #ClassCompilerTests!
ClassCompilerTest class
	instanceVariableNames: ''!

!classDefinition: #ClassCompilerTestSubclass category: #ClassCompilerTests!
ClassCompilerTest subclass: #ClassCompilerTestSubclass
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ClassCompilerTests'!
!classDefinition: 'ClassCompilerTestSubclass class' category: #ClassCompilerTests!
ClassCompilerTestSubclass class
	instanceVariableNames: ''!

!classDefinition: #MyParser category: #ClassCompilerTests!
CCParser named: #MyParser 
	extends: CCParser 
	start: #foo 
	category: 'ClassCompilerTests'!
!classDefinition: 'MyParser class' category: #ClassCompilerTests!
MyParser class
	instanceVariableNames: ''!


!ClassCompilerTest class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 14:31:05'!
compilerClass
	^ ClassCompiler ! !

!MyParser class methodsFor: 'initialization' stamp: 'MM 2/17/2020 14:37:53'!
start
		   ^ #foo! !
