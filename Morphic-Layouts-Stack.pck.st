'From Cuis 5.0 [latest update: #4256] on 15 August 2020 at 10:17:11 pm'!
'Description '!
!provides: 'Morphic-Layouts-Stack' 1 4!
SystemOrganization addCategory: #'Morphic-Layouts-Stack'!


!classDefinition: #StackLayoutMorph category: #'Morphic-Layouts-Stack'!
BoxedMorph subclass: #StackLayoutMorph
	instanceVariableNames: 'separation orientation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Morphic-Layouts-Stack'!
!classDefinition: 'StackLayoutMorph class' category: #'Morphic-Layouts-Stack'!
StackLayoutMorph class
	instanceVariableNames: ''!


!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:39:59'!
addMorph: aMorph layoutSpec: aLayoutSpec

	"Add a submorph, at the bottom or right, with aLayoutSpec"
	aMorph layoutSpec: aLayoutSpec.
	self addMorphFront: aMorph.! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:39:59'!
addMorph: aMorph proportionalHeight: aNumber
	"Convenience method.
	Add others as necessary."
	self addMorph: aMorph layoutSpec: (LayoutSpec proportionalHeight: aNumber)! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:39:59'!
addMorph: aMorph proportionalWidth: aNumber
	"Convenience method.
	Add others as necessary."
	self addMorph: aMorph layoutSpec: (LayoutSpec proportionalWidth: aNumber)! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:39:59'!
fitContents
	"Measures contents later at #minimumExtent"
	self morphExtent: 0@0! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:39:59'!
initialize
	super initialize.
	separation  := 0.! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:43:56'!
layoutSubmorphs

	orientation = #row ifTrue: [^self layoutSubmorphsInARow].
	orientation = #column ifTrue: [^self layoutSubmorphsInAColumn].
	self error: 'Invalid orientation'
		! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:42:18'!
layoutSubmorphsInAColumn

	| submorphsToLayout nextMorph currentPosition nextY currentCol |
	
	super layoutSubmorphs.
	
	currentPosition := 0@0.
	
	submorphsToLayout := self submorphsToLayout.
	
	"Warning: this is CPU intensive:"
	"submorphsToLayout do: [:m | m fitContents]."
	
	currentCol := OrderedCollection new.
	
	submorphsToLayout size to: 1 by: -1 do: [ :index | |mHeight|
		
		nextMorph := submorphsToLayout at: index.
		
		mHeight _ nextMorph layoutSpec fixedHeight ifNil: [nextMorph morphHeight].
		
		nextMorph morphExtent: self morphExtent x @ mHeight.
		
		nextMorph morphPosition: currentPosition.
		
		currentCol add: nextMorph.
		
		nextY := (currentPosition y) + separation + mHeight.
		
		"nextY > height ifTrue: [
			colWidth := currentCol inject: 0 into: [:w :m | m morphExtent x max: w].
			currentPosition := (currentPosition x + colWidth) @ 0.
			currentCol := OrderedCollection new.] 
		ifFalse: ["
			currentPosition := currentPosition x @ nextY"]"
		].
	self morphHeight: 0.! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:15:33'!
layoutSubmorphsInARow

	| submorphsToLayout nextMorph currentPosition nextX currentRow |
	
	super layoutSubmorphs.
	
	currentPosition := 0@0.
	
	submorphsToLayout := self submorphsToLayout.
	
	"Warning: this is CPU intensive:"
	"submorphsToLayout do: [:m | m fitContents]."
	
	currentRow := OrderedCollection new.
	
	submorphsToLayout size to: 1 by: -1 do: [ :index | |mWidth|
		
		nextMorph := submorphsToLayout at: index.
		
		mWidth _ nextMorph layoutSpec fixedWidth ifNil: [nextMorph morphWidth].
		
		nextMorph morphExtent: mWidth  @ self morphExtent y.
		
		nextMorph morphPosition: currentPosition.
		
		currentRow add: nextMorph.
		
		nextX := (currentPosition x) + separation + mWidth.
		
		"nextY > height ifTrue: [
			colWidth := currentCol inject: 0 into: [:w :m | m morphExtent x max: w].
			currentPosition := (currentPosition x + colWidth) @ 0.
			currentCol := OrderedCollection new.] 
		ifFalse: ["
			currentPosition := nextX @ currentPosition y"]"
		].
	self morphWidth: 0.! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:10:52'!
minimumColumnExtent
	
	"Answer size sufficient to frame my submorphs."
	
	| width height |
	
	height := separation.
	width := 0.
	
	self submorphsDo: [ :sm | | smExtent |
				smExtent := sm morphExtent.
				"use maximum width across submorphs"
				width := width max: smExtent x. 
				"sum up submorph heights"
				height := height + smExtent y + self separation. 
			].
		   
	^ (width @ height) + self extentBorder.! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:51:12'!
minimumExtent

	orientation = #row ifTrue: [^self minimumRowExtent].
	orientation = #column ifTrue: [^self minimumColumnExtent].
	self error: 'Invalid orientation'! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:16:33'!
minimumRowExtent

	"Answer size sufficient to frame my submorphs."
	
	| width height |
	
	height := 0.
	width := self separation.
	
	self submorphsDo: [ :sm | | smExtent |
				smExtent := sm morphExtent.
				"use maximum width across submorphs"
				height := height max: smExtent y. 
				"sum up submorph heights"
				width := width + smExtent x + self separation. 
			].
		
	^ (width @ height) "+ self extentBorder".! !

!StackLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 21:40:00'!
submorphsToLayout
	"Select those that will be layout"

	^submorphs select: [ :m | m visible ]! !

!StackLayoutMorph methodsFor: 'accessing' stamp: 'MM 8/15/2020 21:44:04'!
orientation
	"Answer the value of orientation"

	^ orientation! !

!StackLayoutMorph methodsFor: 'accessing' stamp: 'MM 8/15/2020 21:44:04'!
orientation: anObject
	"Set the value of orientation"

	orientation _ anObject! !

!StackLayoutMorph methodsFor: 'accessing' stamp: 'MM 8/15/2020 21:40:00'!
separation
	"Answer the value of separation"

	^ separation! !

!StackLayoutMorph methodsFor: 'accessing' stamp: 'MM 8/15/2020 21:40:00'!
separation: anObject
	"Set the value of separation"

	separation _ anObject! !

!StackLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:09:36'!
columnExample
	"self  columnExample"
	
	|col|
	
	col _ StackLayoutMorph newColumn.
	col separation: 2.
	col addMorph: (LabelMorph contents: 'Hello world').
	col addMorph: (PluggableButtonMorph model: [self inform: 'Hello world'] action: #value label: 'Hello world!!!!').
	col addMorph: (LabelMorph contents: 'Bye bye').
	col openInWorld! !

!StackLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:02:00'!
new
	^ self error: 'Use newRow or newColumn'! !

!StackLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:02:31'!
newColumn
	^ super new 
		orientation: #column;
		yourself! !

!StackLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:02:21'!
newRow
	^ super new 
		orientation: #row;
		yourself! !

!StackLayoutMorph class methodsFor: 'as yet unclassified' stamp: 'MM 8/15/2020 22:16:45'!
rowExample
	"self rowExample"
	|row|
	
	row _ StackLayoutMorph newRow.
	row separation: 5.
	row addMorph: (LabelMorph contents: 'Hello world').
	row addMorph: (PluggableButtonMorph model: [self inform: 'Hello world'] action: #value label: 'Hello world!!!!').
	row openInWorld! !
