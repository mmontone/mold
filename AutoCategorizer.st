'From Cuis 5.0 [latest update: #3946] on 7 November 2019 at 9:18:50 am'!
!classDefinition: #AutoCategorizer category: #'Goodies-AutoCategorizer' stamp: 'MM 11/7/2019 09:18:50'!
Object subclass: #AutoCategorizer
	instanceVariableNames: 'rules class'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Goodies-AutoCategorizer'!
!AutoCategorizer commentStamp: '<historical>' prior: 0!
Categorizes the given class methods automatically.

Usage:

AutoCategorizer categorize: MyClass

How it works:

It goes through methods not categorized and tries to find a category for them (methods already categorized are untouched.)

First, sees if the method is categorized up the hierarchy chain, and uses that category.
Then, tries to categorize the methods applying the first matching rule in rules instVar.
Finally, if no category could be found, looks at the caller methods and extracts the category from them.!


!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:15:15'!
categorize
	self categorizeByInheritance.
	self categorizeByRules.
	self categorizeByCalls.! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:55:02'!
categorizeByCalls
	self uncategorizedClassMethods do: [:m | |calls cm|
		calls _ self localCallsOn: m.
		cm _ calls detect: [:im | self isClassified: im] ifNone: [nil].
		cm ifNotNil: [
			self classify: m under: cm category]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:34:39'!
categorizeByInheritance
	self uncategorizedClassMethods do: [:m | |chain cm|
		chain _ self methodHierarchyOf: m.
		cm _ chain detect: [:im | self isClassified: im] ifNone: [nil].
		cm ifNotNil: [
			self classify: m under: cm category]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 19:00:32'!
categorizeByRules
	self uncategorizedClassMethods do: [:m | |rule|
		rule _ self rules detect: [:r | r key value: m] ifNone: [nil].
		rule ifNotNil: [
			self classify: m under: rule value]]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 17:30:04'!
classMethods
	^ class organization allMethodSelectors collect: [:selector |
		MethodReference class: class selector: selector] ! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/7/2019 09:17:38'!
defaultRules
	^ OrderedCollection new
		add: [:m | m selector asString beginsWith: 'initialize'] -> #initialization;
		add: [:m | m selector asString beginsWith: 'print'] -> #printing;
		add: [:m | m selector asString beginsWith: 'draw'] -> #drawing;
		add: [:m | m selector asString beginsWith: 'is'] -> #testing;
		add: [:m | #(mouseButton1Activity. mouseButton2Activity) includes: m selector] -> #'event handling';
		yourself! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:55:13'!
localCallsOn: aMethod
	^ class allLocalCallsOn: aMethod selector! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:35:03'!
methodHierarchyOf: aMethod

	|list|
	
	list _ OrderedCollection new.
	
	class allSuperclasses reverseDo: [ :cl |
		(cl includesSelector: aMethod selector) ifTrue: [
			list addLast: (MethodReference class: cl selector: aMethod selector)]].
	
	^ list! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:08:12'!
rules
	^ rules ifNil: [rules _ self defaultRules]! !

!AutoCategorizer methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 18:30:08'!
uncategorizedClassMethods
	^ self classMethods select: [:m | |cat|
		cat _ m category.
		cat isNil or: [cat = #'as yet unclassified']]! !


!AutoCategorizer methodsFor: 'initialization' stamp: 'MM 11/6/2019 17:55:09'!
initialize: aClass
	class _ aClass! !


!AutoCategorizer methodsFor: 'testing' stamp: 'MM 11/6/2019 18:32:01'!
isClassified: aMethod
	^ (aMethod category = nil or: [aMethod category = #'as yet unclassified']) not! !


!AutoCategorizer methodsFor: 'accessing method dictionary' stamp: 'MM 11/6/2019 18:47:58'!
classify: aMethod under: aCategory
	aMethod actualClass organization classify: aMethod selector under: aCategory.
	Transcript show: 'Classified '; show: aMethod; show: ' under: '; show: aCategory; newLine.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AutoCategorizer class' category: #'Goodies-AutoCategorizer' stamp: 'MM 11/7/2019 09:18:50'!
AutoCategorizer class
	instanceVariableNames: ''!

!AutoCategorizer class methodsFor: 'as yet unclassified' stamp: 'MM 11/6/2019 17:06:48'!
categorize: aClass
	^ (self new initialize: aClass) categorize! !
