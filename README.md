![logo](logo.png "Logo")

Mold Domain Modeling Workbench
========================

Repository
----------

This repository contains several packages, most of them work in progress, that are going to be needed by Mold.

![deps](deps.png "Dependencies")

### Mold

The meta-modeling workbench.

### Cairo

Vector graphics via Cairo FFI.

### IMGUI

[Immediate mode GUI](http://www.johno.se/book/imgui.html) inside Morphic. Used by StructurEd and Dia.

### Dia

A diagrams editing library. Uses IMGUI and Cairo for the UI. Follows a design similar to that of http://www.draw2d.org.

![Dia](doc/Dia.png "Dia")

### StructurEd 

An experiment in semi-structured editing. Based on IMGUI.

![SEDSmalltalkEditor](doc/SEDSmalltalkEditor.gif "SEDSmalltalkEditor")


### StructurEdSmalltalk

Semi-structured editing for Smalltalk language, on top of StructurEd.

### Props

Generic properties editor. Edit properties of any object that returns a list of #properties.

![Props](doc/Props.png "Props")

### Meld

Smalltalk extensions for meta-programming.

### MetaBrowser

A browser for browsing and editing models, based on the models model (metamodels). A domain-specific browser.

![MetaBrowser](doc/MetaBrowser.png "MetaBrowser")

### Meta editing tools

Several different generic editing tools based on the metamodel + some aspect spec.

Diagram editor, tree editor, properties editor, form editor.

A diagram editor that works from a metamodel, so a diagram can be used to edit virtually any object. Same for the other editors.

![MetaEditor](doc/MetaEditor.png "MetaEditor")

### BrowserPlus

An extensible Smalltalk browser via plugins, with superior navigation and domain specific menus. 
Some of the plugins include auto categorization of methods, source code services like browsing of undefined references, and runnable example methods.

![BrowserPlus](doc/BrowserPlus.png "BrowserPlus")

### Commander (WIP)

Global commands manager and runner. Inspired in Emacs and Lisp Clim interfaces. Smalltalk looks like a good candidate for a protype implementation.

It supports [Direct Combination](https://www.researchgate.net/publication/221515443_Direct_Combination).

#### References

* The Humane Interface: New Directions for Designing Interactive Systems
* Emacs
* CLIM: Common Lisp Interface Manager
* Presentation Based User Interfaces.
* https://www.nngroup.com/articles/anti-mac-interface/
* https://www.semanticscholar.org/paper/Reusable-hierarchical-command-objects-Myers-Kosbie/950f6e996d7debd64d2baa8b043827f1e2355c73
* http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.31.755
* [Direct Combination](https://www.researchgate.net/publication/221515443_Direct_Combination)

### Presentations (WIP)

Presentations based user interface.

![Presentations](https://pbs.twimg.com/media/EphvXxxW8AM_JJl?format=jpg&name=medium)

- An information presentation system
- Presentation Based User Interfaces
- https://dspace.mit.edu/handle/1721.1/41161
- https://dspace.mit.edu/handle/1721.1/6946
- A Guided Tour of CLIM , Common Lisp Interface Manager
- An Implementation of CLIM Presentation Types

### GML

Genenric Modeling Language. A language for describing models.

## NOT DONE AT ALL YET:

### ObjV

An experiment on a multi-level metamodeling Kernel.
https://files.pharo.org/books-pdfs/booklet-Reflective/2017-11-20-ReflectiveKernel.pdf

### Document composer

WYSIWYM (What You See Is What You Mean) document editor with the ability to compose almost any Smalltalk object into a document.

Add objects to document using drag and drop. Objects are invoked with #addToText: aText . 
Object to be included uses Memento pattern to externalize its state for inclusion into the document.

### Flow

Flow Based Programming framework based on Dia for composing nodes visually (ala Node-RED).

Resources
---------

* https://www.jetbrains.com/mps/
* https://www.youtube.com/watch?v=3ka4KY7TMTU
* https://www.youtube.com/watch?v=ifW0qEJ7OTM
* http://www.newspeaklanguage.org/
* https://www.youtube.com/watch?v=L4FLWSt9Px4
* https://hal.inria.fr/inria-00636785/file/Casa11a-Smalltalks-BootstrappingASmalltalk.pdf
* http://scg.unibe.ch/archive/papers/Nier16a-death-of-oop.pdf
* Microsoft Intentional Programming: https://www.youtube.com/watch?v=tSnnfUj1XCQ
* Intentional Domain Workbench: https://www.youtube.com/watch?v=UBI33yXJZxg
* http://voelter.de/data/articles/FromProgrammingToModeling-1.2-final.pdf
* http://cedalion.sourceforge.net/cedalion-plde.pdf
* A Foundation for Multi-Level Modeling http://shura.shu.ac.uk/12074/
* [Explicit Metaclasses](https://www.researchgate.net/publication/221320901_Metaclasses_are_First_Class_the_ObjVlisp_Model)

### Dependencies graph:

```
digraph G {
  "StructurEd" -> "PetitParser"
  "StructurEd" -> "IMGUI"
  "StructurEdSmalltalk" -> "StructurEd"
  "MetaBrowser" -> "Meld"
  "Meld" -> "PetitParser"
  "Meld" -> "Props"
  "Dia" -> "IMGUI"
  "Dia" -> "Props"
  "Dia" -> "MultiPanelWindow"
  "Meld" -> "GML"
  "Meld" -> "DSLs"
  "DSLs" -> "PetitParser"
  "Meld" -> "Dia"
  "Meld" -> "FSM"
  "GML" -> "PetitParser"
  "Mold" -> "ObjVLisp"
  "Mold" -> "MetaBrowser"
  "MetaBrowser" -> "Props"
  "Dia" -> "Cairo"
  "Cairo" -> "FFIGen"
  "MetaTreeEditor"
  "BrowserPlus" -> "Props"
  "BrowserPlus" -> "SourceCodeServices"
  "BrowserPlus" -> "AutoCategorizer"
  "Mold" -> "MetaTreeEditor"
  "Mold" -> "StructurEd"
  "Erudite"
}
```
