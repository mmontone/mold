'From Cuis 6.0 [latest update: #5045] on 22 January 2022 at 6:33:42 pm'!
'Description Quick views for data.

Consider rename to DataViews.

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'QuickViews' 1 31!
!requires: 'Morphic-Widgets-Extras2' 1 19 nil!
SystemOrganization addCategory: 'QuickViews'!


!classDefinition: #FilterableListMorph category: 'QuickViews'!
PluggableListMorph subclass: #FilterableListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'FilterableListMorph class' category: 'QuickViews'!
FilterableListMorph class
	instanceVariableNames: ''!

!classDefinition: #QuickForm category: 'QuickViews'!
SystemWindow subclass: #QuickForm
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'QuickForm class' category: 'QuickViews'!
QuickForm class
	instanceVariableNames: ''!

!classDefinition: #QuickView category: 'QuickViews'!
SystemWindow subclass: #QuickView
	instanceVariableNames: 'actions detail filter action filterString filterInput printer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'QuickView class' category: 'QuickViews'!
QuickView class
	instanceVariableNames: ''!

!classDefinition: #QuickList category: 'QuickViews'!
QuickView subclass: #QuickList
	instanceVariableNames: 'currentIndex sortableBy sortByKey'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'QuickList class' category: 'QuickViews'!
QuickList class
	instanceVariableNames: ''!

!classDefinition: #QuickTable category: 'QuickViews'!
QuickView subclass: #QuickTable
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'QuickTable class' category: 'QuickViews'!
QuickTable class
	instanceVariableNames: ''!

!classDefinition: #QuickTree category: 'QuickViews'!
QuickView subclass: #QuickTree
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'QuickViews'!
!classDefinition: 'QuickTree class' category: 'QuickViews'!
QuickTree class
	instanceVariableNames: ''!


!QuickList commentStamp: '<historical>' prior: 0!
Quickly displays aCollection using a ListMorph.

Basic example:

(QuickList on: {'foo'.'bar'.'baz'})
	actions: {'Inspect' -> #inspect};
	openTitled: 'A collection'.
	
See class side for more examples.

Please consider using ExamplesBrowser for browsing the examples more conviniently. Feature require: 'ExamplesBrowser'.!

!QuickTable commentStamp: '<historical>' prior: 0!
SpaceTally new systemWideSpaceTally!

!QuickTree commentStamp: '<historical>' prior: 0!
TODO: tree of directories and files!

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/29/2020 09:43:47'!
buildActionsBar

	| actionsBar | 
	
	actionsBar _ LayoutMorph newRow.
	
	actions keysDo: [:actionTitle | | button |
		button _ PluggableButtonMorphPlus model: self action: [self performAction: actionTitle].
		button label: actionTitle.
		actionsBar addMorph: button].
	
	filter ifNotNil: [ 
		filterInput _ TextModelMorph textProvider: self textGetter: #filterString textSetter: #filterString:.
		filterInput acceptOnCR: true;
				askBeforeDiscardingEdits: false.
		actionsBar addMorph: filterInput layoutSpec: (LayoutSpec fixedHeight: 30)].
	
	^ actionsBar! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 16:03:43'!
buildDataWidget
	^ self subclassResponsibility ! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 18:12:38'!
buildDetailPane
	^ (detail isKindOf:Morph) 
		ifTrue: [detail]
		ifFalse: [TextModelMorph textProvider:  self textGetter: #detailString] ! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 20:36:38'!
buildMorphicWindow

	detail 
		ifNotNil: [
			|layout|
		
			layout _ LayoutMorph newRow.
			layout addMorph: self buildDataWidget layoutSpec: (LayoutSpec proportionalWidth: 0.5).
			layout addMorph: self buildDetailPane layoutSpec: (LayoutSpec proportionalWidth: 0.5).
			self addMorph: layout layoutSpec: (LayoutSpec proportionalWidth: 1)]
		ifNil: [ 
			self addMorph: self buildDataWidget layoutSpec: (LayoutSpec proportionalWidth: 1)].
	
	
	self addMorph: self buildActionsBar layoutSpec: (LayoutSpec new fixedHeight: 30; proportionalWidth: 1; yourself). 
	! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 19:59:40'!
detailString
	|item|
	
	item _ self selectedItem.
	
	item ifNil: [^''].
	
	^ detail isSymbol 
		ifTrue: [item perform: detail]
		ifFalse: [detail value: item]! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/29/2020 09:54:02'!
getFilter
	^ (filter = true) 
		ifTrue: [	[:str :el | el printString includesSubstring: str caseSensitive: false]]
		ifFalse: [filter]! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 23:41:10'!
initialize: aModel
	self model: aModel.
	filterString _ ''.
	actions _ Dictionary new! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 16:36:17'!
openTitled: aString
	self buildMorphicWindow.
	labelString _ aString.
	self openInWorld! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 17:16:03'!
performAction: actionTitle
	|item act |
	
	item _ self selectedItem.
	item ifNil: [^self].
	act _ actions at: actionTitle.
	act isSymbol 
		ifTrue: [item perform: act]
		ifFalse: [act value: item]
	
	! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 17:14:47'!
performSelectedAction

	|item|
	
	action ifNil: [^self].
	
	item _ self selectedItem.
	item ifNil: [^self].
	
	action isSymbol 
		ifTrue: [item perform: action]
		ifFalse: [action value: item]
	
	! !

!QuickView methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 17:15:05'!
selectedItem
	^ self subclassResponsibility ! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 17:09:32'!
action
	"Answer the value of action"

	^ action! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 17:09:32'!
action: anObject
	"Set the value of action"

	action _ anObject! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
actions
	"Answer the value of actions"

	^ actions! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:16:01'!
actions: anObject
	"Set the value of actions"

	actions _ anObject asDictionary! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
detail
	"Answer the value of detail"

	^ detail! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
detail: anObject
	"Set the value of detail"

	detail _ anObject! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
filter
	"Answer the value of filter"

	^ filter! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
filter: anObject
	"Set the value of filter"

	filter _ anObject! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/29/2020 09:30:39'!
filterInput
	"Answer the value of filterInput"

	^ filterInput! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/29/2020 09:30:39'!
filterInput: anObject
	"Set the value of filterInput"

	filterInput _ anObject! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 23:11:12'!
filterString
	"Answer the value of filterString"

	^ filterString! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 23:13:04'!
filterString: anObject
	"Set the value of filterString"

	filterString _ anObject.
	model changed! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
model
	"Answer the value of model"

	^ model! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/28/2020 16:04:18'!
model: anObject
	"Set the value of model"

	model _ anObject! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/29/2020 09:30:39'!
printer
	"Answer the value of printer"

	^ printer! !

!QuickView methodsFor: 'accessing' stamp: 'MM 11/29/2020 09:30:39'!
printer: anObject
	"Set the value of printer"

	printer _ anObject! !

!QuickView class methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 16:18:11'!
on: aModel
	^ self new initialize: aModel! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2020 11:41:48'!
buildActionsBar
	
	|actionsBar sortButton|
	
	actionsBar _ super buildActionsBar.
	sortableBy ifNil: [^actionsBar].
	
	sortButton _ SelectionButtonMorph 
				on: self 
				getter: #sortByKey
				setter: #sortByKey:
				elems:  sortableBy keys
				printer: #asString. 
	actionsBar addMorph: sortButton.
	^ actionsBar! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/29/2020 09:32:06'!
buildDataWidget
	|listMorph|
	
	listMorph _ PluggableListMorph model: self listGetter: #getListStrings indexGetter: #currentIndex indexSetter: #currentIndex:.
	
	listMorph doubleClickSelector: #performSelectedAction.
	
	^ listMorph! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 16:35:35'!
currentIndex
	^ currentIndex ifNil: [0]! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 20:37:35'!
currentIndex: index
	currentIndex _ index.
	self changed: #acceptedContents! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/29/2020 09:32:25'!
filterString: anObject
	"Set the value of filterString"

	filterString _ anObject.
	filterInput hasUnacceptedEdits: false.
	self changed: #getListStrings! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2020 11:03:34'!
getFilteredList
	^ (filter isNil or: [filterString isEmpty]) 
		ifTrue: [model]
		ifFalse: [model select: [:el | self getFilter value: filterString value: el]]! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2020 11:38:40'!
getList
	^ self getSortedList! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/29/2020 09:31:53'!
getListStrings
	^ printer ifNil: [self getList]
		ifNotNil: [self getList collect: [:x | printer isSymbol ifTrue: [x perform: printer]
									ifFalse: [printer value: x]]]! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/30/2020 11:40:58'!
getSortedList
	^ sortByKey 
		ifNil: [self getFilteredList]
		ifNotNil: [self getFilteredList asSortedCollection
					sortBlock: (sortableBy at: sortByKey);
					yourself]! !

!QuickList methodsFor: 'as yet unclassified' stamp: 'MM 11/28/2020 23:20:34'!
selectedItem
	currentIndex ifNil: [^nil].
	currentIndex isZero ifTrue: [^nil].
	^ self getList at: currentIndex.! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:07:04'!
currentSort
	"Answer the value of currentSort"

	^ currentSort! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:07:04'!
currentSort: anObject
	"Set the value of currentSort"

	currentSort _ anObject! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:37:49'!
sortByKey
	"Answer the value of sortByKey"

	^ sortByKey! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:41:18'!
sortByKey: anObject
	"Set the value of sortByKey"

	sortByKey _ anObject.
	self changed: #getListStrings! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:40:21'!
sortableBy
	"Answer the value of sortableBy"

	^ sortableBy! !

!QuickList methodsFor: 'accessing' stamp: 'MM 11/30/2020 11:40:28'!
sortableBy: anObject
	"Set the value of sortableBy"

	sortableBy _ anObject asDictionary! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:17:06'!
basicExample

	(QuickList on: {'foo'.'bar'.'baz'})
		actions: {'Inspect' -> #inspect};
		openTitled: 'A collection'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:17:43'!
classesListExample

	(QuickList on: Object allSubclasses)
		actions: {'Browse' -> [:class | BrowserWindow fullOnClass: class]};
		openTitled: 'Classes'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:23:34'!
contributorsExample

	(QuickList on: Smalltalk allContributors asOrderedCollection )
		actions: {'Browse contributions' -> [:user | (QuickList on: (Smalltalk contributionsOf: user))
										filter: true;
										actions: {'Browse' -> [:el | BrowserWindow fullOnClass: el first selector: el second]};
										openTitled: 'Contributions of ', user]};
		filter: true;
		openTitled: 'Contributors'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:18:20'!
detailExample

	(QuickList on: Object allSubclasses)
		actions: {'Browse' -> [:class | BrowserWindow fullOnClass: class]};
		detail: #comment;
		openTitled: 'Classes'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:19:12'!
filterExample

	(QuickList on: Object allSubclasses)
		actions: {'Browse' -> [:class | BrowserWindow fullOnClass: class]};
		detail: #comment;
		filter: [:str :class | class name includesSubstring: str caseSensitive: false];
		openTitled: 'Classes'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:33:13'!
packagesExample
	
	"self packagesExample"
	
	(QuickList on: (CodePackage installedPackages values sort: [:x :y | x packageName < y packageName]))
		printer: [:p | p packageName];
		actions: {
			'Inspect' -> #inspect};
		detail: #description;
		openTitled: 'Packages'! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:26:32'!
preferencesExample

	(QuickList on: Preferences preferencesDictionary values )
		printer: [:pref | pref name];
		actions: {
			'Inspect' -> #inspect.
			'Set' -> [:pref | self request: (pref name, ' (', pref preferenceValue asString, ')') do: [:str | pref preferenceValue: (Compiler evaluate: str) ]]};
		detail: [:pref | String streamContents: [:s |
				s nextPutAll: pref preferenceValue printString;
					newLine; newLine;
			 		nextPutAll: (pref instVarNamed: 'helpString')]];
		filter: true;
		openTitled: 'Preferences'.! !

!QuickList class methodsFor: 'examples' stamp: 'MM 1/22/2022 18:27:37'!
processesExample

	(QuickList on: (Process allSubInstances select: [:p | p isTerminated not]))
		actions: {
			'Run' -> #run.
			'Resume' -> #resume.
			'Terminate' -> #terminate.
			'Suspend' -> #suspend};
		printer: #name;
		detail: #browserPrintString;
		openTitled: 'Processes'! !

!Object methodsFor: '*QuickViews' stamp: 'MM 12/19/2020 15:50:39'!
chooseFrom: aCollection
	"Asks the user to choose one of the elements of the collection"! !

!Object methodsFor: '*QuickViews' stamp: 'MM 12/19/2020 15:51:08'!
chooseFrom: aCollection default: anObject
	"Asks the user to choose one of the elements of the collection"! !
