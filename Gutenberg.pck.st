'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 13 October 2019 at 12:05:30.430331 pm'!
'Description Pretty Printer'!
!provides: 'Gutenberg' 1 0!
!classDefinition: #GutBreakable category: #Gutenberg!
Object subclass: #GutBreakable
	instanceVariableNames: 'separator width pretty indent group'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg'!
!classDefinition: 'GutBreakable class' category: #Gutenberg!
GutBreakable class
	instanceVariableNames: ''!

!classDefinition: #GutGroup category: #Gutenberg!
Object subclass: #GutGroup
	instanceVariableNames: 'depth breakables broken'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg'!
!classDefinition: 'GutGroup class' category: #Gutenberg!
GutGroup class
	instanceVariableNames: ''!

!classDefinition: #GutGroupQueue category: #Gutenberg!
Object subclass: #GutGroupQueue
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg'!
!classDefinition: 'GutGroupQueue class' category: #Gutenberg!
GutGroupQueue class
	instanceVariableNames: 'contents'!

!classDefinition: #GutPrettyPrint category: #Gutenberg!
Object subclass: #GutPrettyPrint
	instanceVariableNames: 'output maxWidth newline spaces buffer outputWidth bufferWidth groupQueue indent currentGroupStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg'!
!classDefinition: 'GutPrettyPrint class' category: #Gutenberg!
GutPrettyPrint class
	instanceVariableNames: ''!

!classDefinition: #GutText category: #Gutenberg!
Object subclass: #GutText
	instanceVariableNames: 'width contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg'!
!classDefinition: 'GutText class' category: #Gutenberg!
GutText class
	instanceVariableNames: ''!

!classDefinition: #GutBreakableTest category: #'Gutenberg-Tests'!
TestCase subclass: #GutBreakableTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg-Tests'!
!classDefinition: 'GutBreakableTest class' category: #'Gutenberg-Tests'!
GutBreakableTest class
	instanceVariableNames: ''!

!classDefinition: #GutGroupQueueTest category: #'Gutenberg-Tests'!
TestCase subclass: #GutGroupQueueTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg-Tests'!
!classDefinition: 'GutGroupQueueTest class' category: #'Gutenberg-Tests'!
GutGroupQueueTest class
	instanceVariableNames: ''!

!classDefinition: #GutGroupTest category: #'Gutenberg-Tests'!
TestCase subclass: #GutGroupTest
	instanceVariableNames: 'group'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg-Tests'!
!classDefinition: 'GutGroupTest class' category: #'Gutenberg-Tests'!
GutGroupTest class
	instanceVariableNames: ''!

!classDefinition: #GutPrettyPrintTest category: #'Gutenberg-Tests'!
TestCase subclass: #GutPrettyPrintTest
	instanceVariableNames: 'tree'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg-Tests'!
!classDefinition: 'GutPrettyPrintTest class' category: #'Gutenberg-Tests'!
GutPrettyPrintTest class
	instanceVariableNames: ''!

!classDefinition: #GutTextTest category: #'Gutenberg-Tests'!
TestCase subclass: #GutTextTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Gutenberg-Tests'!
!classDefinition: 'GutTextTest class' category: #'Gutenberg-Tests'!
GutTextTest class
	instanceVariableNames: ''!


!SequenceableCollection methodsFor: '*Gutenberg' stamp: 'MM 10/9/2016 21:18'!
lastIfEmpty: aBlock 	

	^self isEmpty ifTrue: [aBlock value] ifFalse: [self last]! !

!SequenceableCollection methodsFor: '*Gutenberg' stamp: 'MM 10/9/2016 21:16'!
reverseKeysAndValuesDo: aBlock 	
	self size to: 1 by: -1 do: [:i | aBlock value: i value: (self at: i)]! !

!SequenceableCollection methodsFor: '*Gutenberg' stamp: 'MM 10/9/2016 21:17'!
with: aSequenceableCollection do: aBlock separatedBy: separatorBlock

	| otherCollection |	
	self size ~= aSequenceableCollection size ifTrue: [^self noMatchError].
	self isEmpty ifTrue: [^self].	
	otherCollection := ReadStream on: aSequenceableCollection.	
	aBlock		value: (self at: 1)		value: otherCollection next.	
	2 to: self size do:	[:index | 		
		separatorBlock value.		
		aBlock			value: (self at: index)			value: otherCollection next]! !

!GutBreakable methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
indent    ^ indent! !

!GutBreakable methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:11'!
initializeInPretty: aPretty separator: anObject width: aWidth    separator := anObject.    width := aWidth.    pretty := aPretty.    indent := pretty indent.    group := pretty currentGroup.    group pushBreakable: self! !

!GutBreakable methodsFor: 'testing' stamp: 'cdlm 7/15/2006 01:11'!
isText    ^ false! !

!GutBreakable methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:11'!
outputOn: anObject width: aWidth    group popBreakable.    group isBroken        ifTrue: [            anObject nextPutAll: pretty newline;                nextPutAll: (pretty spaces value: indent).            ^ indent.]        ifFalse: [            group hasBreakables ifFalse: [pretty dropGroup: group.].            anObject nextPutAll: separator.            ^ width + aWidth.]! !

!GutBreakable methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:12'!
separator    ^ separator! !

!GutBreakable methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:12'!
width    ^ width! !

!GutBreakable class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
separator: anObject width: aWidth inPretty: aPretty 	^(self new)		initializeInPretty: aPretty separator: anObject width: aWidth! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
break    broken := true! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
depth    ^ depth! !

!GutGroup methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:12'!
depth: aNumber    depth := aNumber! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
hasBreakables    ^ breakables notEmpty! !

!GutGroup methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:12'!
initialize    breakables := OrderedCollection new. broken := false. depth := 1. ^ self! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
isBroken    ^ broken! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
popBreakable    ^ breakables removeLast! !

!GutGroup methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:11'!
pushBreakable: aBreakable    breakables addLast: aBreakable! !

!GutGroup class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
atDepth: depth 	^(self new)		depth: depth;		yourself! !

!GutGroup class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
new	^super new initialize! !

!GutGroupQueue methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:10'!
dequeue    ^ self dequeueIfNone: [nil.]! !

!GutGroupQueue methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:10'!
dequeueIfNone: aBlock    contents        do: [:groups |            groups                reverseKeysAndValuesDo: [:index :group |                    group hasBreakables                        ifTrue: [                            groups removeAt: index. group break. ^ group.].].            groups do: [:group | group break.].            groups removeAllSuchThat: [:each | true.].].    ^ aBlock value! !

!GutGroupQueue methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:10'!
enqueue: aGroup    | index |    index := aGroup depth.    [index <= contents size.]        whileFalse: [contents addLast: OrderedCollection new.].    (contents at: index) addLast: aGroup! !

!GutGroupQueue methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:10'!
initialize    contents := OrderedCollection new. ^ self! !

!GutGroupQueue methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:10'!
remove: aGroup    (contents at: aGroup depth)        removeAllSuchThat: [:each |            each = aGroup "do I just need to remove one?   yes probably".]! !

!GutGroupQueue class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
new	^super new initialize! !

!GutGroupQueue class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
with: anObject 	^self new enqueue: anObject! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 10/28/2006 12:56'!
atIndent: aNumber open: opener close: closer group: aBlock 	self text: opener width: opener size.	self groupSub: [self indentingBy: aNumber nest: aBlock].	self text: closer width: closer size! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:13'!
breakOutmostGroups    | group |    [            outputWidth + bufferWidth > maxWidth                and: [group := groupQueue dequeue. group notNil.].]        whileTrue: [            [group hasBreakables.] whileTrue: [self outputOne.].            [buffer notEmpty and: [buffer first isText.].]                whileTrue: [self outputOne.].]! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
breakable    self breakable: '' width: 0! !

!GutPrettyPrint methodsFor: 'formatting-kernel' stamp: 'cdlm 7/15/2006 01:13'!
breakable: anObject width: separatorWidth    | group |    group := currentGroupStack last.    group isBroken        ifTrue: [self lineBreak.]        ifFalse: [            buffer                addLast: (GutBreakable                     separator: anObject                     width: separatorWidth                     inPretty: self).            bufferWidth := bufferWidth + separatorWidth.            self breakOutmostGroups.]! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
breakableSpace    self breakable: ' ' width: 1! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
currentColumn    ^ self indent + bufferWidth! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:14'!
currentGroup    ^ currentGroupStack last! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:14'!
dropGroup: aGroup    groupQueue remove: aGroup! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
fillBreakable: anObject width: aWidth    self group: [self breakable: anObject width: aWidth.]! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
fillBreakableSpace    self fillBreakable: ' ' width: 1! !

!GutPrettyPrint methodsFor: 'formatting-kernel' stamp: 'cdlm 7/15/2006 01:13'!
flush    buffer        do: [:token |            outputWidth := token outputOn: output width: outputWidth.].    buffer := OrderedCollection new.    bufferWidth := 0! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
group: aBlock    self atIndent: 0 open: '' close: '' group: aBlock! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:14'!
groupSub: aBlock    | group |    group := GutGroup atDepth: currentGroupStack last depth + 1.    currentGroupStack addLast: group.    groupQueue enqueue: group.    aBlock value.    currentGroupStack removeLast.    group hasBreakables ifFalse: [groupQueue remove: group.]! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 10/28/2006 12:51'!
hangingBy: aNumber nest: aBlock	|oldIndent|	oldIndent := indent.    indent := self currentColumn + aNumber. aBlock value. indent := oldIndent! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
indent    ^ indent! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 10/28/2006 12:49'!
indentingBy: aNumber nest: aBlock    indent := indent + aNumber. aBlock value. indent := indent - aNumber.! !

!GutPrettyPrint methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:15'!
initialize    | rootGroup |    output := self class defaultOutput.    maxWidth := self class defaultMaxWidth.    newline := self class defaultNewline.    spaces := self class defaultSpaces.    buffer := OrderedCollection new.    outputWidth := 0.    bufferWidth := 0.    rootGroup := GutGroup new.    currentGroupStack := OrderedCollection with: rootGroup.    groupQueue := GutGroupQueue with: rootGroup.    indent := 0.    ^ self! !

!GutPrettyPrint methodsFor: 'formatting-kernel' stamp: 'cdlm 7/15/2006 01:13'!
lineBreak    self flush.    output nextPutAll: newline.    output nextPutAll: (spaces value: indent).    outputWidth := indent.    bufferWidth := 0! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
maxWidth    ^ maxWidth! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
maxWidth: aWidth    maxWidth := aWidth! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
newline    ^ newline! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
newline: anObject    newline := anObject! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
output    ^ output! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
output: anObject    output := anObject! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:14'!
outputOne    | token |    token := buffer removeFirst.    outputWidth := token outputOn: output width: outputWidth.    bufferWidth := bufferWidth - token width! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
space    self text: ' '! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
spaces    ^ spaces! !

!GutPrettyPrint methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:14'!
spaces: aBlock    spaces := aBlock.! !

!GutPrettyPrint methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:13'!
text: anObject "use (obj,width) pairs (tokens?), and define String#asToken"    self text: anObject width: anObject size! !

!GutPrettyPrint methodsFor: 'formatting-kernel' stamp: 'cdlm 7/15/2006 01:13'!
text: anObject width: aWidth    self textAtEnd addLast: anObject width: aWidth.    bufferWidth := bufferWidth + aWidth.    self breakOutmostGroups! !

!GutPrettyPrint methodsFor: 'private' stamp: 'cdlm 7/15/2006 01:14'!
textAtEnd    | text |    (buffer isEmpty or: [text := buffer last. text isText not.])        ifTrue: [text := GutText new. buffer addLast: text.].    ^ text! !

!GutPrettyPrint class methodsFor: 'default values' stamp: 'dp 6/27/2006 23:05'!
defaultMaxWidth	^ 79! !

!GutPrettyPrint class methodsFor: 'default values' stamp: 'dp 6/27/2006 23:05'!
defaultNewline	^''! !

!GutPrettyPrint class methodsFor: 'default values' stamp: 'dp 6/27/2006 23:06'!
defaultOutput	^ WriteStream on: String new! !

!GutPrettyPrint class methodsFor: 'default values' stamp: 'dp 6/27/2006 23:05'!
defaultSpaces	^ [:width | String new: width withAll: Character space]! !

!GutPrettyPrint class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
format: aBlock 	| pretty |	pretty := self new.	aBlock value: pretty.	pretty flush.	^pretty output contents! !

!GutPrettyPrint class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
new	^super new initialize! !

!GutPrettyPrint class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
within: width format: aBlock 	| pretty |	pretty := self new.	pretty maxWidth: width.	aBlock value: pretty.	pretty flush.	^pretty output contents! !

!GutText methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 00:56'!
addLast: anObject width: aWidth    contents addLast: anObject. width := width + aWidth.! !

!GutText methodsFor: 'initialize-release' stamp: 'cdlm 7/15/2006 01:15'!
initialize    width := 0. contents := OrderedCollection new. ^ self! !

!GutText methodsFor: 'testing' stamp: 'cdlm 7/15/2006 01:15'!
isText    ^ true! !

!GutText methodsFor: 'formatting' stamp: 'cdlm 7/15/2006 01:15'!
outputOn: anObject width: aWidth    contents do: [:each | anObject nextPutAll: each.]. ^ width + aWidth! !

!GutText methodsFor: 'accessing' stamp: 'cdlm 7/15/2006 01:16'!
width    ^ width! !

!GutText class methodsFor: 'instance creation' stamp: ' 23/6/06 13:51'!
new	"Answer a newly created and initialized instance."	^super new initialize! !

!GutBreakableTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testCreation	| breakable pretty |	pretty := GutPrettyPrint new.	breakable := GutBreakable 				separator: #separator				width: #width				inPretty: pretty.	self assert: breakable separator = #separator.	self assert: breakable width = #width.	self assert: breakable indent = pretty indent! !

!GutGroupTest methodsFor: 'initialize-release' stamp: ' 23/6/06 13:51'!
setUp	group := GutGroup atDepth: 42! !

!GutGroupTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testBreakables	self assert: group hasBreakables not.	group pushBreakable: #b0rked.	self assert: group hasBreakables.	self assert: group popBreakable = #b0rked.	self assert: group hasBreakables not! !

!GutGroupTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testCreation	self assert: group depth = 42.	self assert: group hasBreakables not.	self assert: group isBroken not.	group break.	self assert: group isBroken! !

!GutPrettyPrintTest methodsFor: 'private' stamp: ' 23/6/06 13:51'!
alternateShowTree: anArray on: aPretty 	| text children |	text := anArray first.	children := anArray allButFirst: 1.	aPretty group: 			[aPretty text: text.			self alternateShowTreeChildren: children on: aPretty]! !

!GutPrettyPrintTest methodsFor: 'private' stamp: 'cdlm 10/28/2006 12:57'!
alternateShowTreeChildren: children on: aPretty 	children isEmpty 		ifFalse: 			[aPretty text: '['.			aPretty indentingBy: 2				nest: 					[aPretty breakableSpace.					children do: [:each | self alternateShowTree: each on: aPretty]						separatedBy: 							[aPretty text: ','.							aPretty breakableSpace]].			aPretty breakableSpace.			aPretty text: ']']! !

!GutPrettyPrintTest methodsFor: 'private' stamp: 'nlm 4/11/2007 17:00'!
hello: width    ^ GutPrettyPrint        within: width        format: [:pretty |            pretty                group: [                    pretty                        group: [                            pretty                                group: [                                    pretty                                        group: [                                            pretty text: 'hello'.                                            pretty breakableSpace.                                            pretty text: 'a'].                                    pretty breakableSpace.                                    pretty text: 'b'].                            pretty breakableSpace.                            pretty text: 'c'].                    pretty breakableSpace.                    pretty text: 'd']]! !

!GutPrettyPrintTest methodsFor: 'private' stamp: ' 23/6/06 13:51'!
helloFill: width    ^ GutPrettyPrint        within: width        format: [:pretty |            pretty text: 'hello';                fillBreakableSpace;                text: 'a';                fillBreakableSpace;                text: 'b';                fillBreakableSpace;                text: 'c';                fillBreakableSpace;                text: 'd']! !

!GutPrettyPrintTest methodsFor: 'initialize-release' stamp: ' 23/6/06 13:51'!
setUp	tree := #('aaaa' #('bbbbb' #('ccc') #('dd')) #('eee') #('ffff' #('gg') #('hhh') #('ii')))! !

!GutPrettyPrintTest methodsFor: 'private' stamp: 'cdlm 10/28/2006 12:58'!
showTree: anArray on: aPretty 	| text children |	text := anArray first.	children := anArray allButFirst: 1.	aPretty group: 			[aPretty text: text.			aPretty indentingBy: text size				nest: [self showTreeChildren: children on: aPretty]]! !

!GutPrettyPrintTest methodsFor: 'private' stamp: 'cdlm 10/28/2006 12:58'!
showTreeChildren: children on: aPretty 	children isEmpty 		ifFalse: 			[aPretty text: '['.			aPretty indentingBy: 1				nest: 					[children do: [:each | self showTree: each on: aPretty]						separatedBy: 							[aPretty text: ','.							aPretty breakableSpace]].			aPretty text: ']']! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:07'!
testAlternateTree0to18	| expected result |	expected := 'aaaa[  bbbbb[    ccc,    dd  ],  eee,  ffff[    gg,    hhh,    ii  ]]'.	result := self treeAlternate: 0.	self assert: expected = result.	result := self treeAlternate: 18.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:08'!
testAlternateTree19to20	| expected result |	expected := 'aaaa[  bbbbb[ ccc, dd ],  eee,  ffff[    gg,    hhh,    ii  ]]'.	result := self treeAlternate: 19.	self assert: expected = result.	result := self treeAlternate: 20.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:08'!
testAlternateTree21to49	| expected result |	expected := 'aaaa[  bbbbb[ ccc, dd ],  eee,  ffff[ gg, hhh, ii ]]'.	result := self treeAlternate: 21.	self assert: expected = result.	result := self treeAlternate: 49.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testAlternateTree50	| expected result |	expected := 'aaaa[ bbbbb[ ccc, dd ], eee, ffff[ gg, hhh, ii ] ]'.	result := self treeAlternate: 50.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'cdlm 10/28/2006 12:58'!
testHangIndent	| expected actual |	expected := 'abcdefGHIJ      KLMN'.	actual := GutPrettyPrint within: 10				format: 					[:pretty | 					pretty						text: 'abc';						text: 'def';						indentingBy: pretty currentColumn							nest: 								[pretty									text: 'GHIJ';									breakableSpace;									text: 'KLMN']].	self assert: actual = expected! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'nlm 4/11/2007 16:48'!
testHello0to6	| expected hello |	expected := 'helloabcd '.	hello := self hello: 0.	self assert: expected = hello.	hello := self hello: 6.	self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:09'!
testHello11to12	| expected hello |	expected := 'hello a b cd'.	hello := self hello: 11.	self assert: expected = hello.	hello := self hello: 12.	self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testHello13	| expected hello |	expected := 'hello a b c d'.	hello := self hello: 13.	self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:09'!
testHello7to8	| expected hello |	expected := 'hello abcd'.	hello := self hello: 7.	self assert: expected = hello.	hello := self hello: 8.	self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:09'!
testHello9to10	| expected hello |	expected := 'hello a bcd'.	hello := self hello: 9.	self assert: expected = hello.	hello := self hello: 10.	self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:10'!
testHelloFill0to2	| expected hello |	expected := 'helloabcd'.	#(0 2 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:10'!
testHelloFill11to12	| expected hello |	expected := 'hello a b cd'.	#(11 12 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testHelloFill13    | expected hello |    expected := 'hello a b c d'.    hello := self helloFill: 13.    self assert: expected = hello! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:10'!
testHelloFill3to4	| expected hello |	expected := 'helloa bc d'.	#(3 4 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:14'!
testHelloFill5to6	| expected hello |	expected := 'helloa b cd'.	#(5 6 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:14'!
testHelloFill7to8	| expected hello |	expected := 'hello ab c d'.	#(7 8 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:14'!
testHelloFill9to10	| expected hello |	expected := 'hello a bc d'.	#(9 10 )		do: [:each | 			hello := self helloFill: each.			self assert: expected = hello]! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testQuickCreation	| pretty |	pretty := GutPrettyPrint new.	self assert: pretty output contents = GutPrettyPrint defaultOutput contents.	self assert: pretty maxWidth = GutPrettyPrint defaultMaxWidth.	self assert: pretty newline = GutPrettyPrint defaultNewline.	self 		assert: (pretty spaces value: 5) = (GutPrettyPrint defaultSpaces value: 5)! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:15'!
testTree0to19	| expected result |	expected := 'aaaa[bbbbb[ccc,           dd],     eee,     ffff[gg,          hhh,          ii]]'.	result := self tree: 0.	self assert: expected = result.	result := self tree: 19.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:16'!
testTree20to22	| expected result |	expected := 'aaaa[bbbbb[ccc, dd],     eee,     ffff[gg,          hhh,          ii]]'.	result := self tree: 20.	self assert: expected = result.	result := self tree: 22.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: 'dp 6/27/2006 23:16'!
testTree23to43	| expected result |	expected := 'aaaa[bbbbb[ccc, dd],     eee,     ffff[gg, hhh, ii]]'.	result := self tree: 23.	self assert: expected = result.	result := self tree: 43.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testTree44	| expected result |	expected := 'aaaa[bbbbb[ccc, dd], eee, ffff[gg, hhh, ii]]'.	result := self tree: 44.	self assert: expected = result! !

!GutPrettyPrintTest methodsFor: 'private' stamp: ' 23/6/06 13:51'!
tree: aNumber 	^GutPrettyPrint within: aNumber format: [:pretty | self showTree: tree on: pretty]! !

!GutPrettyPrintTest methodsFor: 'private' stamp: ' 23/6/06 13:51'!
treeAlternate: aNumber 	^GutPrettyPrint within: aNumber		format: [:pretty | self alternateShowTree: tree on: pretty]! !

!GutTextTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testAddLast	| text output outputWidth |	text := GutText new.	output := ReadWriteStream on: String new.	text		addLast: 'foo' width: 3;		addLast: 'bar' width: 39.	self assert: text width = 42.	outputWidth := text outputOn: output width: 51.	self assert: outputWidth = (51 + 42).	self assert: output contents = 'foobar'! !

!GutTextTest methodsFor: 'testing' stamp: ' 23/6/06 13:51'!
testCreation	| text |	text := GutText new.	self assert: text width = 0! !
