fileIn := [:fileName | |fileEntry|
		fileEntry _ fileName asFileEntry.
		fileEntry readStream: [ :stream |
			CodeFile new fullName: fileEntry pathName; buildFrom: stream; fileIn ]].
	
fileIn value: 'CompiledMethod-dateSortingValue.st'.
fileIn value: 'ImageMorph class-defaultForm.st'.

Feature require: 'Mold'.