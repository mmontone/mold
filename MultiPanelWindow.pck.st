'From Cuis 5.0 [latest update: #4619] on 28 June 2021 at 6:47:27 pm'!
'Description A SystemWindow subclass that manages window panels.

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'MultiPanelWindow' 1 17!
SystemOrganization addCategory: 'MultiPanelWindow'!
SystemOrganization addCategory: 'MultiPanelWindow-Tests'!


!classDefinition: #MPWindowArea category: 'MultiPanelWindow'!
LayoutMorph subclass: #MPWindowArea
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowArea class' category: 'MultiPanelWindow'!
MPWindowArea class
	instanceVariableNames: ''!

!classDefinition: #MPWindowPanel category: 'MultiPanelWindow'!
LayoutMorph subclass: #MPWindowPanel
	instanceVariableNames: 'title status body header'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowPanel class' category: 'MultiPanelWindow'!
MPWindowPanel class
	instanceVariableNames: ''!

!classDefinition: #MPPluggableWindowPanel category: 'MultiPanelWindow'!
MPWindowPanel subclass: #MPPluggableWindowPanel
	instanceVariableNames: 'model buildSelector'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPPluggableWindowPanel class' category: 'MultiPanelWindow'!
MPPluggableWindowPanel class
	instanceVariableNames: ''!

!classDefinition: #MPSwitchingWindowPanel category: 'MultiPanelWindow'!
MPPluggableWindowPanel subclass: #MPSwitchingWindowPanel
	instanceVariableNames: 'currentPanel panelSwitcher'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPSwitchingWindowPanel class' category: 'MultiPanelWindow'!
MPSwitchingWindowPanel class
	instanceVariableNames: ''!

!classDefinition: #MPWindowToolBar category: 'MultiPanelWindow'!
LayoutMorph subclass: #MPWindowToolBar
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowToolBar class' category: 'MultiPanelWindow'!
MPWindowToolBar class
	instanceVariableNames: ''!

!classDefinition: #LimitedLabelMorph category: 'MultiPanelWindow'!
LabelMorph subclass: #LimitedLabelMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'LimitedLabelMorph class' category: 'MultiPanelWindow'!
LimitedLabelMorph class
	instanceVariableNames: ''!

!classDefinition: #MultiPanelWindow category: 'MultiPanelWindow'!
SystemWindow subclass: #MultiPanelWindow
	instanceVariableNames: 'configuration configurations mainMorph areas'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MultiPanelWindow class' category: 'MultiPanelWindow'!
MultiPanelWindow class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest1 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest1
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest1 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest1 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest2 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest2
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest2 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest2 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest3 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest3
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest3 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest3 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest4 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest4
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest4 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest4 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest5 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest5
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest5 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest5 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest6 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest6
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest6 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest6 class
	instanceVariableNames: ''!

!classDefinition: #MPWindowTest7 category: 'MultiPanelWindow-Tests'!
MultiPanelWindow subclass: #MPWindowTest7
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow-Tests'!
!classDefinition: 'MPWindowTest7 class' category: 'MultiPanelWindow-Tests'!
MPWindowTest7 class
	instanceVariableNames: ''!

!classDefinition: #MPConfiguration category: 'MultiPanelWindow'!
Object subclass: #MPConfiguration
	instanceVariableNames: 'configurations'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPConfiguration class' category: 'MultiPanelWindow'!
MPConfiguration class
	instanceVariableNames: ''!

!classDefinition: #MPPluggableConfiguration category: 'MultiPanelWindow'!
MPConfiguration subclass: #MPPluggableConfiguration
	instanceVariableNames: 'model selector'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPPluggableConfiguration class' category: 'MultiPanelWindow'!
MPPluggableConfiguration class
	instanceVariableNames: ''!

!classDefinition: #MPWindowAreaConfiguration category: 'MultiPanelWindow'!
MPConfiguration subclass: #MPWindowAreaConfiguration
	instanceVariableNames: 'name layoutSpec orientation visible'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowAreaConfiguration class' category: 'MultiPanelWindow'!
MPWindowAreaConfiguration class
	instanceVariableNames: ''!

!classDefinition: #MPWindowConfiguration category: 'MultiPanelWindow'!
MPConfiguration subclass: #MPWindowConfiguration
	instanceVariableNames: 'orientation'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowConfiguration class' category: 'MultiPanelWindow'!
MPWindowConfiguration class
	instanceVariableNames: ''!

!classDefinition: #MPWindowPanelConfiguration category: 'MultiPanelWindow'!
MPConfiguration subclass: #MPWindowPanelConfiguration
	instanceVariableNames: 'class layoutSpec properties'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MultiPanelWindow'!
!classDefinition: 'MPWindowPanelConfiguration class' category: 'MultiPanelWindow'!
MPWindowPanelConfiguration class
	instanceVariableNames: ''!


!MPWindowPanel commentStamp: '<historical>' prior: 0!
A MPWindowPanel is a panel of a MultiPanelWindow.

Instance variables:
- title: The title of the panel
- status: The status (collapsed, etc)

A panel has a header with the title and a set of buttons:
- close: to close the panel.
- collapse/uncollapse: to collapse the panel (hide panel body).
- arrow down: panel menu.
- split?!

!MPPluggableWindowPanel commentStamp: '<historical>' prior: 0!
Use this for giving the responsibility of building the panel to model.!

!MPSwitchingWindowPanel commentStamp: '<historical>' prior: 0!
A window panel with a button to switch its panel body with a button, that replaces the panel title in header.!

!MultiPanelWindow commentStamp: '<historical>' prior: 0!
A MultiPanelWindow is a kind of SystemWindow that manages a configuration of panels. Their layout, status, splitting, etc.

Instance variables:

- configuration <MPWindowConfiguration> : The current window configuration.
- configurations <Collection of MPWindowConfiguration> : A collection of window configurations to the user can select from.
 
Ideas:

A MultiPanelWindow has a window configuration. The current configuration of the panels is held there.

Window menu:
- configuration -> select configuration from list
- panels -> toggle left/right/bottom/top
- save configuration -> saves the current configuration in a Smalltalk method. asks for a name first.!

!MPWindowArea methodsFor: 'as yet unclassified' stamp: 'MM 11/11/2019 23:22:55'!
configuration
	^ submorphs collect: [:m |
		m configuration]! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:37:15'!
build
	self buildPanelHeader.
	body _ self buildPanelBody.
	self addMorphUseAll: body ! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:51:00'!
buildPanelBody
	^ LayoutMorph newColumn! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 6/14/2020 19:57:32'!
buildPanelHeader
	header _ LayoutMorph newRow 
					axisEdgeWeight: #right;
					separation: 2@0;
					color: Color lightGray;
					yourself.
					
	title ifNotNil: [header addMorphUseAll: (LimitedLabelMorph contents: title)].
	
	header addMorph: self createCollapseButton.
	header addMorph: self createMenuButton.
	header addMorph: self createCloseButton.
	
	
	self addMorph: header fixedHeight: 20.! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:12:18'!
buttonsExtent
	^ 12@12! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:17:28'!
calculateMinimumExtent
	"Answer size sufficient to frame my submorphs."
	
	| width height |
	width := self ySeparation.
	height := self xSeparation.
	(self direction =  #vertical)
		ifTrue: [
			self submorphsDo: [ :sm | | smMinExtent |
				sm visible ifTrue: [smMinExtent := sm minimumExtent.
				"use maximum width across submorphs"
				width := width max: smMinExtent x. 
				"sum up submorph heights"
				height := height + smMinExtent y + self ySeparation. ]
			].
		     width := width + self xSeparation.
		]
		ifFalse: [
			self submorphsDo: [ :sm | | smMinExtent |
				sm visible ifTrue: [smMinExtent := sm minimumExtent.
				"sum up submorphs width"
				width := width + smMinExtent x + self xSeparation. 
				"use maximum height across submorph"
				height := height max: smMinExtent y. ]
			].
			height := height + self xSeparation.
		].

	^ (width @ height) + self extentBorder.! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:39:23'!
close
	self owner removeMorph: self! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:27:57'!
collapse
	body visible: false.
	cachedMinExtent _ nil.
	self morphExtent: header morphExtent.
	self layoutSpec: (LayoutSpec fixedHeight: header morphHeight).
	owner ifNotNil: [ owner someSubmorphPositionOrExtentChanged ].! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:12:09'!
createCloseButton
	^ (PluggableButtonMorph model: self action: #close)
		icon: Theme current closeIcon;
		iconName: #drawCloseIcon;
		setBalloonText: 'close this panel';
		morphExtent: self buttonsExtent! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:28:37'!
createCollapseButton
	^(PluggableButtonMorph model: self action: #toggleCollapse)
		icon: Theme current collapseIcon;
		iconName: #drawCollapseIcon;
		setBalloonText: 'collapse/uncollapse this panel';
		morphExtent: self buttonsExtent! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:12:26'!
createMenuButton
	^ (PluggableButtonMorph model: self action: #openMenu)
		icon: Theme current windowMenuIcon;
		iconName: #drawMenuIcon;
		setBalloonText: 'panel menu';
		morphExtent: self buttonsExtent! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:52:23'!
openMenu
	|menu|
	
	menu _ MenuMorph new defaultTarget: self.
	
	menu add: 'close' action: #close.
	menu add: 'un/collapse' action: #toggleCollapse.
	
	menu invokeModal! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:04:54'!
title
	^ title! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:49:36'!
title: aString
	title _ aString! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:29:34'!
toggleCollapse
	body visible 
		ifTrue: [self collapse]
		ifFalse: [self uncollapse]
	! !

!MPWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:28:30'!
uncollapse
	body visible: true.
	cachedMinExtent _ nil.
	self layoutSpec: LayoutSpec useAll.
	owner ifNotNil: [ owner someSubmorphPositionOrExtentChanged ].! !

!MPWindowPanel class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:53:23'!
new
	^ self newColumn! !

!MPPluggableWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:54:18'!
buildPanelBody
	^ model perform: buildSelector ! !

!MPPluggableWindowPanel methodsFor: 'accessing' stamp: 'MM 11/12/2019 15:56:44'!
buildSelector
	"Answer the value of buildSelector"

	^ buildSelector! !

!MPPluggableWindowPanel methodsFor: 'accessing' stamp: 'MM 11/12/2019 15:56:44'!
buildSelector: anObject
	"Set the value of buildSelector"

	buildSelector _ anObject! !

!MPPluggableWindowPanel methodsFor: 'accessing' stamp: 'MM 11/12/2019 15:56:44'!
model
	"Answer the value of model"

	^ model! !

!MPPluggableWindowPanel methodsFor: 'accessing' stamp: 'MM 11/12/2019 15:56:44'!
model: anObject
	"Set the value of model"

	model _ anObject! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:35:20'!
buildPanelBody
	^ model perform: self buildSelector! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 6/14/2020 19:58:18'!
buildPanelHeader
	header _ LayoutMorph newRow 
					axisEdgeWeight: #rowRight;
					separation: 2@0;
					color: Color lightGray;
					yourself.
					
	panelSwitcher _ SelectionButtonMorph 
					on: self 
					getter: #currentPanel 
					setter: #currentPanel: 
					elems: buildSelector printer: #asString.
	
	header addMorphUseAll: panelSwitcher.			
					
	header addMorph: self createCollapseButton.
	header addMorph: self createMenuButton.
	header addMorph: self createCloseButton.
	
	
	self addMorph: header fixedHeight: 20! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:42:03'!
buildSelector
	^  ('build', currentPanel asString capitalized, 'Panel') asSymbol! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:44:37'!
currentPanel
	^ currentPanel! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2019 20:24:25'!
currentPanel: aSymbol
	aSymbol = currentPanel ifFalse: [
		currentPanel _ aSymbol.
		self removeMorph: body.
		body _ self buildPanelBody.
		self addMorphUseAll: body.
		self ownerChain do: [:morph | (morph isKindOf: SystemWindow) ifTrue: [
				morph adoptWidgetsColor: morph widgetsColor]]]! !

!MPSwitchingWindowPanel methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:50:39'!
panels: symbols
	buildSelector _ symbols.
	currentPanel _ symbols first! !

!LimitedLabelMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:59:51'!
drawOn: aCanvas
	
	"hack to prevent the string being drawn outside of morph extent."
	
	|c|
	
	c _ BitBltCanvas withExtent: self morphExtent depth: 32.
	
	super drawOn: c.
	aCanvas image: c form at: 0@0 
	! !

!LimitedLabelMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:59:51'!
minimumExtent
	^ 0@0! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 13:08:08'!
addArea: aMorph named: aSymbol
	areas at: aSymbol put: aMorph! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 13:11:52'!
buildAreasMenu: aMenu
	
	|areasMenu|
	
	areasMenu _ MenuMorph new defaultTarget: self.
	
	areas keysDo: [:aname |
		areasMenu add: aname asString
					target: self
					action: #toggleArea:
					argument: aname].
				
	aMenu add: 'toggle area' subMenu:  areasMenu! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:29:05'!
buildMorphicWindow
	self buildWindowAreas.! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:23:42'!
buildWindowAreas
	self layoutMorph removeAllMorphs.
	configuration orientation = #horizontal
		ifTrue: [self layoutMorph beRow]
		ifFalse: [self layoutMorph beColumn].
	configuration recreateOn: self.! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:51:13'!
buildWindowMenu

	| aMenu |

	aMenu _ MenuMorph new defaultTarget: self.

	aMenu 
		add: 'change title...' 			action: #relabel 						icon: #saveAsIcon;
		add: 'window color...' 			action: #setWindowColor 			icon: #graphicsIcon.
	
	self hasSaveAs
		ifTrue: [ aMenu add: 'Save as ...' action: #saveContents icon: #saveAsIcon ].
		
	aMenu addLine.
	
	self buildAreasMenu: aMenu.
			
	aMenu
		addLine.
		
	self addWindowControlTo: aMenu.
	self addTileResizerMenuTo: aMenu.

	^ aMenu! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:20:36'!
configuration
	^ configuration! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:15:04'!
defaultConfiguration
	^ MPWindowConfiguration new! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 13:08:14'!
initialize
	configuration _ self defaultConfiguration.
	areas _ Dictionary new.
	super initialize.! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:54:47'!
loadConfiguration: aConfiguration
	
	|as|
	
	as _ aConfiguration recreateWith: model.
	
	layoutMorph removeAllMorphs.
	
	as do: [:area |
		layoutMorph addMorph: area].! !

!MultiPanelWindow methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 13:12:30'!
toggleArea: aSymbol
	|amorph|
	amorph _ areas at: aSymbol.
	amorph visible: amorph visible not! !

!MPWindowTest1 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:50:29'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color blue};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			name: #main;
			layoutSpec: (LayoutSpec useAll);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest1 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:00:37'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest2 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:12:57'!
defaultConfiguration
	^ MPWindowConfiguration columnWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedHeight: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color blue};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			name: #main;
			layoutSpec: (LayoutSpec useAll);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedHeight: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest2 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:12:29'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest3 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:16:24'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color blue};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			name: #main;
			orientation: #vertical;
			layoutSpec: (LayoutSpec useAll);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll.
					  MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color yellow};
						layoutSpec: (LayoutSpec fixedHeight: 100)}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest3 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:13:28'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest4 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:42:14'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color blue};
						layoutSpec: LayoutSpec useAll}.
		  MPWindowAreaConfiguration new
			orientation: #vertical;
			layoutSpec: (LayoutSpec useAll);
			configurations: 
				{MPWindowAreaConfiguration new
					name: #main;
					layoutSpec: LayoutSpec useAll;
					configurations: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
					MPWindowAreaConfiguration new
						name: #bottomBar;  
						layoutSpec: (LayoutSpec fixedHeight: 100);
						configurations: {MPWindowPanelConfiguration new
							class: BoxedMorph;
							properties: {#color: -> Color yellow};
							layoutSpec: LayoutSpec useAll}}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest4 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:35:15'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest5 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:10:10'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: MPWindowPanel;
						properties: {#title: -> 'panel1'}.
					MPWindowPanelConfiguration new
						class: MPWindowPanel;
						properties: {#title: -> 'panel2'}}.
		  MPWindowAreaConfiguration new
			orientation: #vertical;
			layoutSpec: (LayoutSpec useAll);
			configurations: 
				{MPWindowAreaConfiguration new
					name: #main;
					layoutSpec: LayoutSpec useAll;
					configurations: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
					MPWindowAreaConfiguration new
						name: #bottomBar;  
						layoutSpec: (LayoutSpec fixedHeight: 100);
						configurations: {MPWindowPanelConfiguration new
							class: BoxedMorph;
							properties: {#color: -> Color yellow};
							layoutSpec: LayoutSpec useAll}}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest5 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:51:53'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest6 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:57:26'!
buildPropertiesPanelBody
	^ PropertiesEditorPanel on: Preferences! !

!MPWindowTest6 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:56:24'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: MPPluggableWindowPanel;
						properties: {#model: -> self. #buildSelector: -> #buildPropertiesPanelBody. #title: -> 'properties'. }.
					MPWindowPanelConfiguration new
						class: MPWindowPanel;
						properties: {#title: -> 'panel2'}}.
		  MPWindowAreaConfiguration new
			orientation: #vertical;
			layoutSpec: (LayoutSpec useAll);
			configurations: 
				{MPWindowAreaConfiguration new
					name: #main;
					layoutSpec: LayoutSpec useAll;
					configurations: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
					MPWindowAreaConfiguration new
						name: #bottomBar;  
						layoutSpec: (LayoutSpec fixedHeight: 100);
						configurations: {MPWindowPanelConfiguration new
							class: BoxedMorph;
							properties: {#color: -> Color yellow};
							layoutSpec: LayoutSpec useAll}}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest6 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 15:54:48'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPWindowTest7 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:54:59'!
buildExplorerPanel
	^ BoxedMorph new
		color: Color violet;
		yourself! !

!MPWindowTest7 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:46:07'!
buildPropertiesPanel
	^ PropertiesEditorPanel on: Preferences! !

!MPWindowTest7 methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:48:40'!
defaultConfiguration
	^ MPWindowConfiguration rowWith:
		{MPWindowAreaConfiguration new
			name: #leftSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: MPSwitchingWindowPanel;
						properties: {#model: -> self. #panels: -> #(properties explorer) }.
					MPWindowPanelConfiguration new
						class: MPWindowPanel;
						properties: {#title: -> 'panel2'}}.
		  MPWindowAreaConfiguration new
			orientation: #vertical;
			layoutSpec: (LayoutSpec useAll);
			configurations: 
				{MPWindowAreaConfiguration new
					name: #main;
					layoutSpec: LayoutSpec useAll;
					configurations: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color red};
						layoutSpec: LayoutSpec useAll}.
					MPWindowAreaConfiguration new
						name: #bottomBar;  
						layoutSpec: (LayoutSpec fixedHeight: 100);
						configurations: {MPWindowPanelConfiguration new
							class: BoxedMorph;
							properties: {#color: -> Color yellow};
							layoutSpec: LayoutSpec useAll}}.
		  MPWindowAreaConfiguration new
			name: #rightSideBar;
			layoutSpec: (LayoutSpec fixedWidth: 100);
			panels: {MPWindowPanelConfiguration new
						class: BoxedMorph;
						properties: {#color: -> Color green};
						layoutSpec: LayoutSpec useAll}
		}! !

!MPWindowTest7 class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:33:00'!
example1
	|w|
	
	w _ MultiPanelWindow new.
	w openInWorld! !

!MPConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:08:59'!
configurations
	"Answer the value of configurations"

	^ configurations! !

!MPConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:08:59'!
configurations: anObject
	"Set the value of configurations"

	configurations _ anObject! !

!MPConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:49:34'!
collect: aBlock
	|configs|
	configs _ OrderedCollection new.
	
	configs add: (aBlock value: self).
	
	configurations do: [:config |
		configs _ configs, (config collect: aBlock)].
	
	^ configs! !

!MPConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:47:17'!
do: aBlock
	aBlock value: self.
	configurations do: [:config |
		config do: aBlock]! !

!MPConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:17:30'!
initialize
	configurations _ OrderedCollection new! !

!MPConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:49:09'!
select: aBlock
	|configs|
	configs _ OrderedCollection new.
	
	(aBlock value:self) ifTrue: [
		configs add: self].
	configurations do: [:config |
		configs _ configs, (config select: aBlock)].
	
	^ configs! !

!MPConfiguration class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:18:20'!
with: aCollection
	^ self new configurations: aCollection; yourself! !

!MPPluggableConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 18:31:44'!
model
	"Answer the value of model"

	^ model! !

!MPPluggableConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 18:31:44'!
model: anObject
	"Set the value of model"

	model _ anObject! !

!MPPluggableConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 18:31:44'!
selector
	"Answer the value of selector"

	^ selector! !

!MPPluggableConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 18:31:44'!
selector: anObject
	"Set the value of selector"

	selector _ anObject! !

!MPPluggableConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 18:38:45'!
recreateWith: aWindow
	^ model perform: selector! !

!MPWindowAreaConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2019 20:03:48'!
initialize
	super initialize.
	orientation _ #vertical.
	visible _ true! !

!MPWindowAreaConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 12:16:49'!
panels
	^ configurations! !

!MPWindowAreaConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2019 20:03:30'!
recreateWith: aWindow
	|area p |
	
	orientation = #horizontal 
		ifTrue: [area _ MPWindowArea newRow]
		ifFalse: [area _ MPWindowArea newColumn].
	area layoutSpec: layoutSpec; yourself.
	p _ self panels first recreateWith: aWindow.
	area addMorph: p.
	self panels allButFirst do: [:config | |panel|
		panel _ config recreateWith: aWindow.
		area addAdjusterMorph;
				addMorph: panel].
	visible ifFalse: [area visible: false].
	name ifNotNil: [
		aWindow addArea: area named: name].
	^ area! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:41:38'!
layoutSpec
	"Answer the value of layoutSpec"

	^ layoutSpec! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:41:38'!
layoutSpec: anObject
	"Set the value of layoutSpec"

	layoutSpec _ anObject! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:41:38'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:29:58'!
orientation
	"Answer the value of orientation"

	^ orientation! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:29:58'!
orientation: anObject
	"Set the value of orientation"

	orientation _ anObject! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:44:35'!
panels: aCollection
	configurations _ aCollection! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/14/2019 20:02:37'!
visible
	"Answer the value of visible"

	^ visible! !

!MPWindowAreaConfiguration methodsFor: 'accessing' stamp: 'MM 11/14/2019 20:02:37'!
visible: anObject
	"Set the value of visible"

	visible _ anObject! !

!MPWindowConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:22:21'!
areas
	^ configurations! !

!MPWindowConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 13:05:55'!
recreateOn: aWindow
	|a|
	a _ self areas first recreateWith: aWindow.
	aWindow layoutMorph addMorph: a.
	self areas allButFirst do: [:config | |area|
		area _ config recreateWith: aWindow.
		aWindow layoutMorph 
			addAdjusterMorph;
			addMorph: area]! !

!MPWindowConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:43:31'!
orientation
	"Answer the value of orientation"

	^ orientation! !

!MPWindowConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 09:43:31'!
orientation: anObject
	"Set the value of orientation"

	orientation _ anObject! !

!MPWindowConfiguration class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:44:58'!
columnWith: configurations
	^ self new
		orientation: #vertical;
		configurations: configurations;
		yourself! !

!MPWindowConfiguration class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 09:45:07'!
rowWith: configurations
	^ self new
		orientation: #horizontal;
		configurations: configurations;
		yourself! !

!MPWindowPanelConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:49:01'!
class: aClass
	class _ aClass
	! !

!MPWindowPanelConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 14:54:11'!
initialize
	super initialize.
	layoutSpec _ LayoutSpec useAll.
	properties _ OrderedCollection new! !

!MPWindowPanelConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 11:46:42'!
properties: anArray
	properties _ anArray asDictionary! !

!MPWindowPanelConfiguration methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2019 16:50:20'!
recreateWith: aModel

	|panel|
	
	panel _ class new.
	panel layoutSpec: layoutSpec.
	properties keysAndValuesDo: [:prop :value |
		panel perform: prop with: value].
	
	(panel respondsTo: #build) ifTrue: [panel build].
	
	^ panel! !

!MPWindowPanelConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:49:13'!
layoutSpec
	"Answer the value of layoutSpec"

	^ layoutSpec! !

!MPWindowPanelConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:49:13'!
layoutSpec: anObject
	"Set the value of layoutSpec"

	layoutSpec _ anObject! !

!MPWindowPanelConfiguration methodsFor: 'accessing' stamp: 'MM 11/12/2019 11:49:13'!
properties
	"Answer the value of properties"

	^ properties! !
