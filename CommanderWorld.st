!Browser methodsFor: '*CommanderWorld' stamp: 'MM 2/21/2021 21:06:45'!
classListPresentedObject
	^ self defaultClassList collect: [:className | Smalltalk at: className]! !

!Browser methodsFor: '*CommanderWorld' stamp: 'MM 2/21/2021 21:05:32'!
messageCategoryListPresentedObject
	^ self messageCategoryList collect: [:selector | MethodReference class: self selectedClass selector: selector] ! !

!FileList methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:49:18'!
currentDirectorySelectedPresentedObject
	^ self currentDirectorySelected item! !

!FileList methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:15:48'!
fileListForSelectingPatterns: patternsThatSelect rejectingPatterns: patternsThatReject
	"Make the list be those file names which match the patterns."

	| selected newList |
	directory ifNil: [^#()].
	selected _ Set new.
	patternsThatSelect do: [ :pat |
		directory childrenDo: [ :entry |
			(entry isDirectory
				ifTrue: [ showDirsInFileList ]
				ifFalse: [ self doesPattern: pat allow: entry])
					ifTrue: [ selected add: entry ]]].
	newList _ selected copy.
	patternsThatReject do: [ :pat |
		selected do: [ :entry |
			(entry isDirectory not and: [ pat match: entry name]) ifTrue: [
				newList remove: entry ]]].
		
	newList _ newList asArray sort: self sortBlock.
	
	^ newList! !

!FileList methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:15:59'!
fileListPresentedObject

	| patterns patternsThatReject patternsThatSelect |
	patterns _ OrderedCollection new.
	(pattern findTokens: (String with: Character cr with: Character lf with: $;))
		do: [ :each |
			(each includes: $*) | (each includes: $?)
				ifTrue: [ patterns add: (each copyReplaceAll: '?' with: '#')]
				ifFalse: [
					each isEmpty
						ifTrue: [ patterns add: '*']
						ifFalse: [ patterns add: '*' , each , '*']]].
	"A pattern that starts with $/ is used to reject entries
	Rejecting patterns are applied after selecting patterns."
	patternsThatSelect _ patterns reject: [ :any | any first = $/ ].
	patternsThatSelect isEmpty ifTrue: [ patternsThatSelect add: '*' ]. 
	patternsThatReject _ patterns select: [ :any | any first = $/ ] thenCollect: [ :each | each copyFrom: 2 to: each size ].
	^ self fileListForSelectingPatterns: patternsThatSelect rejectingPatterns: patternsThatReject! !

!CodePackageList methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:55:08'!
packageFullNamesPresentedObject

	^ packages collect: [ :each | each fullFileName ifNotNil:[:f | f asFileEntry] ]! !

!CodePackageList methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 00:57:38'!
packageNamesPresentedObject
	"Return the real packages as presented objects"
	^ packages! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 14:59:53'!
aboutToBeGrabbedBy: aHand event: dragEvent
	^ (self possiblyGrabForCommand: dragEvent) ifNil: [self aboutToBeGrabbedBy: aHand]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 12:03:53'!
aboutToGrab: submorph event: dragEvent
	"submorph is being grabbed by a hand.
	Perform necessary adjustments (if any) and return the actual morph
	that should be added to the hand.
	Answer nil to reject the drag."

	^(submorph commandInteractionEnabled: dragEvent) 
		ifTrue: [	DefaultCommandArgumentMorph on: submorph presentedObject]
		ifFalse: [self aboutToGrab: submorph] "Grab it"! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 13:08:29'!
allowsMorphDrop
	"Answer whether we accept dropping morphs. By default answer false."

	"Use a property test to allow individual instances to specify this."
	^ true! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 20:08:28'!
allowsSubmorphDrag: aMorph event: aMouseEvent
	^ self allowsSubmorphDrag or: [self commandInteractionEnabled: aMouseEvent]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 01:07:31'!
commandInteractionEnabled: aMouseEvent
	^ aMouseEvent shiftPressed and: [self presentedObject isNil not]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 22:29:25'!
commanderEvaluate
	
	self request: ('Evaluate on: ', self presentedObject printString)  
			do: [:str | |result morph|
				result _ Compiler evaluate:  str for: self presentedObject logged: false.
				morph _  DefaultCommandArgumentMorph on: result.
				self runningWorld addMorph: morph]  ! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 14:33:33'!
dragEvent: aMouseEvent localPosition: aPoint

	aMouseEvent hand grabMorph: self event: aMouseEvent! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/14/2021 14:05:53'!
grabForCommand: aHand
	"Return the morph to be dragged"
	 ^ DefaultCommandArgumentMorph on: self presentedObject! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 19:36:58'!
handlesMouseHover
	"Do I want to receive unhandled mouseMove events when the button is up and the hand is empty?  The default response is false."
	"Use a property test to allow individual instances to specify this."
	^ (self hasProperty: #handlesMouseHover)
		or: [Smalltalk includes: #CommandAreaMorph]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 11:47:27'!
mouseButton1Up: aMouseButtonEvent localPosition: localEventPosition
	"Handle a mouse button 1 up event.
	This message will only be sent to Morphs that answer true to #handlesMouseDown:"
	
	"Allow instances to dynamically use properties for handling common events."
	
	self whenCommandInteractionEnabled: aMouseButtonEvent do: [ |commandArea|
		commandArea _ CommandAreaMorph runningInstanceOrNew.
		commandArea addArgumentFromMorph: (self grabForCommand: self runningWorld activeHand)].
	self 
		valueOfProperty: #'mouseButton1Up:localPosition:' 
		ifPresentDo: [ :handler | handler value: aMouseButtonEvent value: localEventPosition ]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 20:07:27'!
mouseEnter: evt
	"Handle a mouseEnter event, meaning the mouse just entered my bounds with no button pressed."
	
	"Allow instances to dynamically use properties for handling common events."
	self 
		valueOfProperty: #mouseEnter: 
		ifPresentDo: [ :handler | handler value: evt ].
	
	self whenCommandInteractionEnabled: evt do: [ 
		self highlighted: true]! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 20:07:45'!
mouseLeave: evt
	"Handle a mouseEnter event, meaning the mouse just entered my bounds with no button pressed."
	
	"Allow instances to dynamically use properties for handling common events."
	self 
		valueOfProperty: #mouseEnter: 
		ifPresentDo: [ :handler | handler value: evt ].
	
	self whenCommandInteractionEnabled: evt do: [
		self highlighted: false].! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 22:26:04'!
openCommandsMenu

	| commandsSet menu |
	
	commandsSet _ self runningWorld commandsSet collectCommandInstancesForArguments: {self presentedObject}.
	
	menu _ commandsSet menuWithCommands.
	menu addTitle: self presentedObject printString.
	menu add: 'Add to command area' 
			target: [CommandAreaMorph runningInstanceOrNew addArgument: self presentedObject]
			action: #value argumentList: {}.
	menu add: 'Evaluate ...'
		target: self
		action: #commanderEvaluate.
				
	menu popUpInWorld ! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/2/2021 06:56:55'!
possiblyGrabForCommand: aDragEvent
	"If command mode is on (for example, a CommandAreaMorph is opened), and we can infer a model from the morph, grab it."
	(self commandInteractionEnabled: aDragEvent) ifFalse: [^nil].
	CommandAreaMorph runningInstanceOrNew.	
	^ self grabForCommand: aDragEvent
		! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 13:47:09'!
presentedObject
	^ nil! !

!Morph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 20:06:13'!
whenCommandInteractionEnabled: aMouseEvent do: aBlock
	^ (self commandInteractionEnabled: aMouseEvent)
		ifTrue: [aBlock value]! !

!HandMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/14/2021 15:20:50'!
grabMorph: aMorph event: aDragEvent
	"Grab the given morph (i.e., add it to this hand and remove it from its current owner) without changing its position. This is used to pick up a morph under the hand's current position, versus attachMorph: which is used to pick up a morph that may not be near this hand."

	^self grabMorph: aMorph event: aDragEvent moveUnderHand: (aMorph isKindOf: SystemWindow) not! !

!HandMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 14:59:21'!
grabMorph: aMorph event: dragEvent moveUnderHand: moveUnderHand
	"Grab the given morph (i.e., add it to this hand and remove it from its current owner).
	If moveUnderHand is requested or it seems neccesary anyway, move the grabbed morph under the hand."

	| grabbed positionInHandCoordinates tx |
	self releaseMouseFocus.	"Break focus"
	grabbed _ aMorph.
	aMorph owner ifNotNil: [ :o | grabbed _ o aboutToGrab: aMorph event: dragEvent ].
	grabbed ifNil: [ ^ self ].
	grabbed _ grabbed aboutToBeGrabbedBy: self event: dragEvent.
	grabbed ifNil: [ ^ self ].

	moveUnderHand
		ifTrue: [
			"We can possibly do better, especially for non BoxedMorphs"
			positionInHandCoordinates _ -30 @ -10.
			grabbed isInWorld ifTrue: [
				grabbed displayBounds ifNotNil: [ :r |
					positionInHandCoordinates _ (r extent // 2) negated ]].
			self
				grabMorph: grabbed
				delta: positionInHandCoordinates.
			^self ].

	positionInHandCoordinates _ (grabbed isInWorld ifTrue: [grabbed] ifFalse: [aMorph])
		morphPositionInWorld - self morphPositionInWorld.

	tx _ GeometryTransformation identity.
	aMorph withAllOwnersDo: [ :o |
		tx _ o location composedWith: tx ].
	self withAllOwnersReverseDo: [ :o |
		tx _ o location inverseTransformation composedWith: tx ].

	self
		grabMorph: grabbed
		delta: positionInHandCoordinates.

	grabbed location: tx.! !

!WorldMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 01:08:24'!
commandsSet
	"TODO: change this"
	^ CommandsSet allRegisteredAsComposite ! !

!WorldMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 01:13:32'!
presentedObject
	^ self runningWorld! !

!InnerHierarchicalListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/2/2021 07:05:19'!
presentedObject
	^ self owner presentedObject! !

!InnerListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 15:58:45'!
presentedObject
	^ self owner presentedObject! !

!InnerTextMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 11:23:19'!
presentedObject
	^ editor hasSelection 
		ifTrue: [editor selectedString]
		ifFalse: [editor actualContents]! !

!HierarchicalListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:44:47'!
getSelectedModel
	
	"Get the model wrapper. We play with selectors a bit, because HierarchicalListMorphs model does not work with the real domain models, but with string representations of them. 
	And that is useless for commands filtering, as we want commands that work on real domain models, not strings.
	So, if the HierarchicalListMorph responds to some #<getListSelector>PresentedObject message, use that. Otherwise, just use #<getListSelector>."
	
	| selectedObjectSelector selectedModel |
	
	selectedObjectSelector _ (getSelectionSelector asString, 'PresentedObject') asSymbol.
	
	selectedModel _ model perform: ((model respondsTo: selectedObjectSelector)
							ifTrue: [selectedObjectSelector]
							ifFalse: [getSelectionSelector]).
	
	^ selectedModel! !

!HierarchicalListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 11:44:57'!
presentedObject
	^ self getSelectedModel! !

!PluggableListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/15/2021 00:51:04'!
getModelList
	
	"Get the model list. We play with selectors a bit, because PluggableListMorphs model does not work with the real domain models, but with string representations of them. 
	And that is useless for commands filtering, as we want commands that work on real domain models, not strings.
	So, if the PluggableListMorph responds to some #<getListSelector>PresentedObject message, use that. Otherwise, just use #<getListSelector>."
	
	| presentedObjectSelector modelList |
	
	presentedObjectSelector _ (getListSelector asString, 'PresentedObject') asSymbol.
	
	modelList _ model perform: ((model respondsTo: presentedObjectSelector)
							ifTrue: [presentedObjectSelector]
							ifFalse: [getListSelector]).
	
	^ modelList 	ifNil: [#()]! !

!PluggableListMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/16/2021 15:36:41'!
presentedObject
	|index|
	index _ self getCurrentSelectionIndex .
	^ index isZero 
		ifTrue: [self getModelList]
		ifFalse: [self getModelList ifNotEmpty: [:lst | lst at: index]]! !

!TextModelMorph methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 13:57:02'!
presentedObject
	^ self text! !

!MouseButtonEvent methodsFor: '*CommanderWorld' stamp: 'MM 2/1/2021 15:08:38'!
dispatchWith: aMorph
	"Find the appropriate receiver for the event and let it handle it. Default rules:
	* The top-most chain of visible, unlocked morphs containing the event position will get a chance to handle the event.
	* When travelling down the hierarchy a prospective handler for the event is installed. This prospective handler can be used by submorphs wishing to handle the mouse down for negotiating who the receiver is.
	* When travelling up, the prospective handler is always executed. The handler needs to check if the event was handled before as well as checking if somebody else's handler has been installed.
	* If another handler has been installed but the event was not handled it means that somebody up in the hierarchy wants to handle the event.
	"
	| aMorphHandlesIt grabAMorph handledByInner |
	"Only for MouseDown"
	self isMouseDown ifFalse: [
		^super dispatchWith: aMorph ].

	"Try to get out quickly"
	(aMorph fullContainsGlobalPoint: position)
		ifFalse: [ ^#rejected ].

	"Install the prospective handler for the receiver"
	aMorphHandlesIt _ false.
	grabAMorph _ false.
	self mouseButton3Pressed
		ifTrue: [
			(eventHandler isNil or: [ eventHandler isWorldMorph or: [
					self shiftPressed or: [ aMorph is: #HaloMorph ]]])
				ifTrue: [
					eventHandler _ aMorph.
					aMorphHandlesIt _ true ]]
		ifFalse: [
			(aMorph handlesMouseDown: self) ifTrue: [
				eventHandler _ aMorph.
				aMorphHandlesIt _ true ].
			"If button 1, and both aMorph and the owner allows grabbing with the hand (to initiate drag & drop), so be it."
			self mouseButton1Pressed ifTrue: [
				aMorph owner ifNotNil: [ :o |
					((o allowsSubmorphDrag: aMorph event: self) and: [ aMorph isSticky not ]) ifTrue: [
						grabAMorph _ true ]]]].

	"Now give submorphs a chance to handle the event"
	handledByInner _ false.
	aMorph submorphsDo: [ :eachChild |
		handledByInner ifFalse: [
			(eachChild dispatchEvent: self) == #rejected ifFalse: [
				"Some child did contain the point so aMorph is part of the top-most chain."
				handledByInner _ true ]]].

	(handledByInner or: [ (aMorph rejectsEvent: self) not and: [aMorph fullContainsGlobalPoint: position] ]) ifTrue: [
		"aMorph is in the top-most unlocked, visible morph in the chain."
		aMorphHandlesIt
			ifTrue: [ ^self sendEventTo: aMorph ]
			ifFalse: [
				(grabAMorph and: [ handledByInner not ]) ifTrue: [
					self hand
						waitForClicksOrDrag: aMorph event: self
						dragSel: (Preferences clickGrabsMorphs ifFalse: [#dragEvent:localPosition:])
						clkSel: (Preferences clickGrabsMorphs ifTrue: [#dragEvent:localPosition:]).
					"false ifTrue: [ self hand grabMorph: aMorph ]."
					Preferences clickGrabsMorphs ifFalse: [aMorph activateWindow].
					self wasHandled: true.
					^self ]]].

	handledByInner ifTrue: [ ^self ].
	"Mouse was not on aMorph nor any of its children"
	^ #rejected! !

!MouseButtonEvent methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 01:03:05'!
isCommandsMenuEvent
	^ self shiftPressed and: [self mouseButton2Pressed]! !

!MouseButtonEvent methodsFor: '*CommanderWorld' stamp: 'MM 2/9/2021 10:25:21'!
sendEventTo: aMorph
	"Dispatch the receiver into anObject"
	
	(self isCommandsMenuEvent and: [aMorph presentedObject isNil not]) ifTrue: [
		self wasHandled not ifTrue: [
			self wasHandled: true.
			^ aMorph openCommandsMenu]].

	type == #mouseDown ifTrue: [
		^aMorph processMouseDown: self localPosition: (aMorph internalizeFromWorld: position) ].
	type == #mouseUp ifTrue: [
		^aMorph processMouseUp: self localPosition: (aMorph internalizeFromWorld: position) ].
	^super sendEventTo: aMorph! !