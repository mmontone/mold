'From Cuis 6.0 [latest update: #5045] on 18 January 2022 at 7:30:18 pm'!
'Description '!
!provides: 'TaskManager' 1 0!
SystemOrganization addCategory: 'TaskManager'!


!classDefinition: #TaskManagerWindow category: 'TaskManager'!
SystemWindow subclass: #TaskManagerWindow
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TaskManager'!
!classDefinition: 'TaskManagerWindow class' category: 'TaskManager'!
TaskManagerWindow class
	instanceVariableNames: ''!

!classDefinition: #Task category: 'TaskManager'!
Object subclass: #Task
	instanceVariableNames: 'status'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TaskManager'!
!classDefinition: 'Task class' category: 'TaskManager'!
Task class
	instanceVariableNames: ''!

!classDefinition: #TaskExecution category: 'TaskManager'!
Object subclass: #TaskExecution
	instanceVariableNames: 'task startTime endTime output'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TaskManager'!
!classDefinition: 'TaskExecution class' category: 'TaskManager'!
TaskExecution class
	instanceVariableNames: ''!

!classDefinition: #TaskManager category: 'TaskManager'!
Object subclass: #TaskManager
	instanceVariableNames: 'taskExecutions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TaskManager'!
!classDefinition: 'TaskManager class' category: 'TaskManager'!
TaskManager class
	instanceVariableNames: ''!


!Task methodsFor: 'as yet unclassified' stamp: 'MM 1/18/2022 19:24:20'!
description

	^ ''! !

!Task methodsFor: 'as yet unclassified' stamp: 'MM 1/18/2022 19:24:08'!
taskName

	^ self subclassResponsibility ! !

!Task methodsFor: 'accessing' stamp: 'MM 1/18/2022 19:26:01'!
status
	"Answer the value of status"

	^ status! !

!Task methodsFor: 'accessing' stamp: 'MM 1/18/2022 19:26:01'!
status: anObject
	"Set the value of status"

	status _ anObject! !
