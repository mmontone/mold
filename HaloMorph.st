'From Cuis 5.0 [latest update: #4241] on 27 June 2020 at 8:22:34 pm'!
!classDefinition: #HaloMorph category: #'Morphic-Halos'!
BoxedMorph subclass: #HaloMorph
	instanceVariableNames: 'target positionOffset angleOffset growingOrRotating haloBox'
	classVariableNames: 'HandleSize Icons'
	poolDictionaries: ''
	category: 'Morphic-Halos'!
!HaloMorph commentStamp: '<historical>' prior: 0!
This morph provides a halo of handles for its target morph. Dragging, duplicating, rotating, and resizing to be done by mousing down on the appropriate handle. There are also handles for help and for a menu of infrequently used operations.!


!HaloMorph methodsFor: 'accessing' stamp: 'jm 7/16/97 06:51'!
target

	^ target
! !

!HaloMorph methodsFor: 'accessing' stamp: 'jmv 6/8/2014 18:37'!
target: aMorph

	target _ aMorph.
	target ifNotNil: [ self addHandles ]! !



!HaloMorph methodsFor: 'dropping/grabbing' stamp: 'jmv 9/22/2012 15:02'!
startDrag: evt with: dragHandle
	"Drag my target without removing it from its owner."

	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	positionOffset _ dragHandle referencePosition - target morphPositionInWorld! !


!HaloMorph methodsFor: 'events' stamp: 'jmv 1/14/2013 22:03'!
mouseButton3Down: aMouseButtonEvent localPosition: localEventPosition
	"Transfer the halo to the next likely recipient"
	target ifNil:[^self delete].
	aMouseButtonEvent hand obtainHalo: self.
	positionOffset _ aMouseButtonEvent eventPosition - target morphPositionInWorld.
	"wait for click to transfer halo"
	aMouseButtonEvent hand 
		waitForClicksOrDrag: self 
		event: aMouseButtonEvent
		clkSel: #transferHalo:localPosition:
		dblClkSel: nil! !


!HaloMorph methodsFor: 'event handling' stamp: 'jmv 6/30/2015 09:38'!
popUpFor: aMorph event: aMorphicEvent
	"This message is sent by morphs that explicitly request the halo on a button click. Note: anEvent is in aMorphs coordinate frame."

	| hand anEvent |
	self flag: #workAround.	"We should really have some event/hand here..."
	anEvent _ aMorphicEvent
				ifNil: [
					hand _ aMorph world activeHand.
					hand ifNil: [ hand _ aMorph world firstHand ]. 
					hand lastMouseEvent ]
				ifNotNil: [
					hand _ aMorphicEvent hand.
					aMorphicEvent ].
	hand halo: self.
	hand world addMorphFront: self.
	self target: aMorph.
	positionOffset _ anEvent eventPosition - aMorph morphPositionInWorld! !

!HaloMorph methodsFor: 'event handling' stamp: 'jmv 8/20/2012 18:13'!
staysUpWhenMouseIsDownIn: aMorph
	^ ((aMorph == target) or: [aMorph hasOwner: self])! !

!HaloMorph methodsFor: 'event handling' stamp: 'jmv 1/27/2013 00:19'!
transferHalo: event localPosition: localEventPosition
	"Transfer the halo to the next likely recipient"
	target ifNil: [ ^self delete ].
	target transferHalo: event from: target.! !


!HaloMorph methodsFor: 'events-processing' stamp: 'jmv 12/20/2014 15:07'!
containsPoint: aLocalPoint event: aMorphicEvent

	self visible ifFalse: [ ^false ].

	"mouseButton3 events are handled by the halo"
	(aMorphicEvent isMouse and: [
		aMorphicEvent isMouseDown and: [ aMorphicEvent mouseButton3Pressed ]])
	ifTrue: [
		^ self morphLocalBounds containsPoint: aLocalPoint ].

	^false! !

!HaloMorph methodsFor: 'events-processing' stamp: 'jmv 9/19/2012 23:43'!
rejectsEvent: anEvent
	"Return true to reject the given event. Rejecting an event means neither the receiver nor any of it's submorphs will be given any chance to handle it."
	(super rejectsEvent: anEvent) ifTrue: [^true].
	anEvent isDropEvent ifTrue: [^true]. "never attempt to drop on halos"
	^false! !


!HaloMorph methodsFor: 'geometry testing' stamp: 'jmv 10/8/2009 09:02'!
isOrthoRectangularMorph
	^false! !

!HaloMorph methodsFor: 'geometry testing' stamp: 'jmv 12/20/2014 14:54'!
morphContainsPoint: aLocalPoint

	"If not visible, won't contain any point at all."
	self visible ifFalse: [ ^false ].

	"We behave as if we were a rectangle. I.e., we want (specifically mouse button) events that happen inside our bounds"
	^ self morphLocalBounds containsPoint: aLocalPoint! !


!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:40'!
addCollapseHandle: handleSpec
	"Add the collapse handle, with all of its event handlers set up, unless the target's owner is not the world or the hand."

	target owner
		ifNil: [ ^self ]	"nil happens, amazingly"
		ifNotNil: [ :to |
			(to isWorldMorph or: [ to is: #HandMorph ])
				ifFalse: [ ^self ]].
		
	(self addHandle: handleSpec)
		mouseDownSelector: #mouseDownInCollapseHandle:with:;
		mouseMoveSelector: #setDismissColor:with:;
		mouseUpSelector: #maybeCollapse:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:37'!
addDebugHandle: handleSpec

	Preferences debugHaloHandle ifTrue: [
		(self addHandle: handleSpec)
			mouseDownSelector: #doDebug:with: ]
! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:31'!
addDismissHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #setDismissColor:with:;
		mouseMoveSelector: #setDismissColor:with:;
		mouseUpSelector: #maybeDismiss:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:32'!
addDragHandle: haloSpec

	(self addHandle: haloSpec)
		mouseDownSelector: #startDrag:with:;
		mouseMoveSelector: #doDrag:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:37'!
addDupHandle: haloSpec
	(self addHandle: haloSpec) mouseDownSelector:#doDup:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:39'!
addFontEmphHandle: haloSpec

	(target is: #InnerTextMorph) ifTrue: [
		(self addHandle: haloSpec) mouseDownSelector: #chooseEmphasisOrAlignment ]! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:38'!
addFontSizeHandle: haloSpec

	(target is: #InnerTextMorph) ifTrue: [
		(self addHandle: haloSpec) mouseDownSelector: #chooseFont]! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:36'!
addGrabHandle: haloSpec

	(self addHandle: haloSpec)
		mouseDownSelector: #doGrab:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:23'!
addGrowHandle: haloSpec

	(self addHandle: haloSpec)
		mouseDownSelector: #startGrow:with:;
		mouseMoveSelector: #doGrow:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:29'!
addHelpHandle: haloSpec
	(self addHandle: haloSpec)
		mouseDownSelector: #mouseDownOnHelpHandle:;
		mouseMoveSelector: #deleteBalloon! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:36'!
addMenuHandle: haloSpec

	(self addHandle: haloSpec) mouseDownSelector: #doMenu:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:35'!
addRecolorHandle: haloSpec
	"Add a recolor handle to the receiver, if appropriate"

	(self addHandle: haloSpec) mouseUpSelector: #doRecolor:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'jmv 8/18/2012 17:35'!
addRotateHandle: haloSpec

	(self addHandle: haloSpec)
		mouseDownSelector: #startRot:with:;
		mouseMoveSelector: #doRot:with:! !

!HaloMorph methodsFor: 'handles' stamp: 'ar 1/30/2001 23:32'!
positionIn: aBox horizontalPlacement: horiz verticalPlacement: vert
	| xCoord yCoord |

	horiz == #left
		ifTrue:	[xCoord _ aBox left].
	horiz == #leftCenter
		ifTrue:	[xCoord _ aBox left + (aBox width // 4)].
	horiz == #center
		ifTrue:	[xCoord _ (aBox left + aBox right) // 2].
	horiz == #rightCenter
		ifTrue:	[xCoord _ aBox left + ((3 * aBox width) // 4)].
	horiz == #right
		ifTrue:	[xCoord _ aBox right].

	vert == #top
		ifTrue:	[yCoord _ aBox top].
	vert == #topCenter
		ifTrue:	[yCoord _ aBox top + (aBox height // 4)].
	vert == #center
		ifTrue:	[yCoord _ (aBox top + aBox bottom) // 2].
	vert == #bottomCenter
		ifTrue:	[yCoord _ aBox top + ((3 * aBox height) // 4)].
	vert == #bottom
		ifTrue:	[yCoord _ aBox bottom].

	^ xCoord asInteger @ yCoord asInteger! !


!HaloMorph methodsFor: 'initialization' stamp: 'jmv 3/10/2018 20:58:21'!
defaultColor
	"answer the default color/fill style for the receiver"
	^ `Color
		r: 0.6
		g: 0.8
		b: 1.0`! !

!HaloMorph methodsFor: 'initialization' stamp: 'jmv 10/11/2010 23:24'!
initialize
	"initialize the state of the receiver"
	super initialize.
	""
	growingOrRotating _ false! !


!HaloMorph methodsFor: 'testing' stamp: 'jmv 3/17/2013 22:55'!
is: aSymbol
	^ aSymbol == #HaloMorph or: [ super is: aSymbol ]! !

!HaloMorph methodsFor: 'testing' stamp: 'jmv 7/11/2015 16:53'!
wantsHalo
	^false! !


!HaloMorph methodsFor: 'private' stamp: 'jmv 3/12/2018 15:51:29'!
addHandle: handleSpec
	"Add a handle within the halo box as per the haloSpec, and set it up to respond to the given event by sending the given selector to the given recipient.  Return the handle."

	| handle aPoint colorToUse form icon |
	aPoint _ self 
				positionIn: haloBox
				horizontalPlacement: handleSpec horizontalPlacement
				verticalPlacement: handleSpec verticalPlacement.
	colorToUse _ Color colorFrom: handleSpec color.
	handle _ HaloHandleMorph new color: colorToUse.
	self addMorph: handle.
	handle morphBoundsInWorld: (Rectangle center: aPoint extent: self class handleSize asPoint).
	handleSpec iconSymbol ifNotNil: [ :iconName |
			form _ self class icons at: iconName ifAbsent: [self class perform: iconName].
			form ifNotNil: [
				icon _ ImageMorph new
					image: form;
					color: colorToUse makeForegroundColor;
					lock.
				handle addMorphFront: icon position: `0@0` ]].
	handle mouseUpSelector: #endInteraction.
	handle setBalloonText: handleSpec hoverHelp.
	^ handle! !

!HaloMorph methodsFor: 'private' stamp: 'MM 6/27/2020 20:21:17'!
addHandles

	self removeAllMorphs.  "remove old handles, if any"
	self morphBoundsInWorld: target worldBoundsForHalo.  "update my size"
	haloBox _ self basicBox.
	target addHandlesTo: self box: haloBox.
	self addNameString: ((target printStringLimitedTo: 40), '[', target morphExtent rounded asString, ']').
	growingOrRotating _ false.
	self redrawNeeded! !

!HaloMorph methodsFor: 'private' stamp: 'len 6/3/2020 13:07:09'!
addNameString: aString 
	"Add a name display centered beneath the bottom of the outer rectangle. Return the handle."

	| nameMorph verticalNamePosition namePosition nameBackground |
	nameBackground _ BoxedMorph new
		color: ((target is: #SystemWindow) ifTrue: [target windowColor] ifFalse: [`Color lightBlue alpha: 0.9`]).
	nameMorph _ LabelMorph contents: aString.
	nameMorph color: `Color black`.
	nameBackground morphExtent: nameMorph morphExtent + 4.
	verticalNamePosition _ haloBox bottom + self class handleSize.
	self world ifNotNil: [:w| verticalNamePosition + nameMorph morphHeight > w morphHeight ifTrue: [verticalNamePosition _ haloBox bottom - nameMorph morphHeight - self class handleSize]].
	namePosition _ haloBox width - nameMorph morphWidth // 2 + haloBox left @ verticalNamePosition.
	self addMorph: nameBackground.
	nameBackground morphPositionInWorld: namePosition - 2.
	self addMorph: nameMorph.
	nameMorph morphPositionInWorld: namePosition.
	^nameMorph! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 3/12/2018 15:51:35'!
basicBox
	| aBox minSide anExtent w |
	minSide _ 4 * self class handleSize.
	anExtent _ ((extent x + self class handleSize + 8) max: minSide) @
				((extent y + self class handleSize + 8) max: minSide).
	aBox _ Rectangle center: self morphBoundsInWorld center extent: anExtent.
	w _ self world ifNil: [ target world ].
	^ w
		ifNil:
			[ aBox ]
		ifNotNil:
			[ aBox intersect: (w viewBox insetBy: `8@8`) ]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 8/18/2012 16:21'!
doDebug: evt with: menuHandle
	"Ask hand to invoke the a debugging menu for my inner target.  If shift key is down, immediately put up an inspector on the inner target"

	| menu |
	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	self world displayWorld.
	evt shiftPressed ifTrue: [
		self delete.
		^ target inspect].

	menu _ target buildDebugMenu: evt hand.
	menu addTitle: (target printStringLimitedTo: 40).
	menu popUpInWorld: self world! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 6/30/2015 09:33'!
doDrag: evt with: dragHandle
	| thePoint |
	evt hand obtainHalo: self.
	thePoint _ evt eventPosition - positionOffset.
	target morphPositionInWorld: thePoint.
	self morphPositionInWorld: thePoint.! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 6/30/2015 09:42'!
doDup: evt with: dupHandle 
	"Ask hand to duplicate my target."

	target _ target duplicateMorph: evt.
	evt hand
		obtainHalo: self;
		grabMorph: target! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 6/30/2015 09:35'!
doGrab: evt with: grabHandle
	"Ask hand to grab my target."

	evt hand
		obtainHalo: self;
		grabMorph: target! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 7/8/2014 11:41'!
doGrow: evt with: growHandle
	"Called while the mouse is down in the grow handle"

	| newExtent |
self revisar.
	self flag: #jmvVer2.
	evt hand obtainHalo: self.
"Como podria andar el grow de un morph embebido en otro? andara ahora?"
newExtent _ evt eventPosition - positionOffset - target morphPositionInWorld.
	evt shiftPressed ifTrue: [newExtent _ (newExtent x max: newExtent y) asPoint].
	(newExtent x = 0 or: [newExtent y = 0]) ifTrue: [^ self].
	target morphExtentInWorld: newExtent.
	growHandle morphPositionInWorld: evt eventPosition - (growHandle morphExtent // 2)! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 8/18/2012 16:21'!
doMenu: evt with: menuHandle
	"Ask hand to invoke the halo menu for my inner target."

	| menu |
	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	self world displayWorld.
	menu _ target buildHandleMenu: evt hand.
	target addTitleForHaloMenu: menu.
	menu popUpInWorld: self world.
! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 9/20/2012 00:00'!
doRecolor: evt with: aHandle
	"The mouse went down in the 'recolor' halo handle.  Allow the user to change the color of the innerTarget"

	evt hand obtainHalo: self.
	(aHandle morphContainsPoint: (aHandle internalizeFromWorld: evt eventPosition))
		ifFalse: [  "only do it if mouse still in handle on mouse up"
			self delete.
			target addHalo: evt]
		ifTrue: [
			target changeColor]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 3/10/2018 21:38:00'!
doRot: evt with: rotHandle
	"Update the rotation of my target if it is rotatable.  Keep the relevant command object up to date."

	| degrees |
self revisar.
	self flag: #jmvVer2.
	evt hand obtainHalo: self.
	degrees _ (evt eventPosition - target referencePosition) degrees.
	degrees _ degrees - angleOffset degrees.
	degrees _ degrees detentBy: 10.0 atMultiplesOf: 90.0 snap: false.
	degrees = 0.0
		ifTrue: [rotHandle color: `Color lightBlue`]
		ifFalse: [rotHandle color: `Color blue`].
	rotHandle submorphsDo:
		[:m | m color: rotHandle color makeForegroundColor].
	self removeAllHandlesBut: rotHandle.

	target rotationDegrees: degrees.

	rotHandle morphPositionInWorld: evt eventPosition - (rotHandle morphExtent // 2)! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 9/22/2012 14:47'!
endInteraction
	"Clean up after a user interaction with the a halo control"

	(target isInWorld not or: [owner isNil]) ifTrue: [^self].
	self isInWorld 
		ifTrue: [
			"make sure handles show in front"
			self comeToFront.
			self addHandles]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 9/20/2012 00:01'!
maybeCollapse: evt with: collapseHandle 
	"Ask hand to collapse my target if mouse comes up in it."

	evt hand obtainHalo: self.
	self delete.
	(collapseHandle morphContainsPoint: (collapseHandle internalizeFromWorld: evt eventPosition)) 
		ifFalse: [
			target addHalo: evt ]
		ifTrue: [
			target collapse ]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 9/20/2012 00:01'!
maybeDismiss: evt with: dismissHandle
	"Ask hand to dismiss my target if mouse comes up in it."

	evt hand obtainHalo: self.
	(dismissHandle morphContainsPoint: (dismissHandle internalizeFromWorld: evt eventPosition))
		ifFalse: [
			self delete.
			target addHalo: evt]
		ifTrue: [
			target resistsRemoval ifTrue: [
				(PopUpMenu
					confirm: 'Really throw this away'
					trueChoice: 'Yes'
					falseChoice: 'Um, no, let me reconsider') ifFalse: [^ self]].

			self delete.
			target dismissViaHalo]! !

!HaloMorph methodsFor: 'private' stamp: 'ar 10/24/2000 18:42'!
maybeDoDup: evt with: dupHandle
	evt hand obtainHalo: self.
	^ target okayToDuplicate ifTrue:
		[self doDup: evt with: dupHandle]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 8/8/2012 23:39'!
mouseDownInCollapseHandle: evt with: collapseHandle
	"The mouse went down in the collapse handle; collapse the morph"

	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	self setDismissColor: evt with: collapseHandle! !

!HaloMorph methodsFor: 'private' stamp: 'di 9/18/97 08:20'!
removeAllHandlesBut: h
	"Remove all handles except h."
	submorphs copy do:
		[:m | m == h ifFalse: [m delete]].
! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 3/10/2018 21:53:51'!
setDismissColor: evt with: dismissHandle
	"Called on mouseStillDown in the dismiss handle; set the color appropriately."

	| colorToUse |
	evt hand obtainHalo: self.
	colorToUse _  (dismissHandle morphContainsPoint:  (dismissHandle internalizeFromWorld: evt eventPosition))
		ifFalse: [ `Color red muchLighter` ]
		ifTrue: [ `Color lightGray` ].
	dismissHandle color: colorToUse! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 3/12/2018 15:51:38'!
startGrow: evt with: growHandle
	"Initialize resizing of my target.  Launch a command representing it, to support Undo"

	| botRt |
	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	self removeAllHandlesBut: growHandle.  "remove all other handles"
	botRt _ target morphPositionInWorld + target morphExtentInWorld.
	positionOffset _ (self world viewBox containsPoint: botRt)
		ifTrue: [evt eventPosition - botRt]
		ifFalse: [`0@0`]! !

!HaloMorph methodsFor: 'private' stamp: 'jmv 8/8/2012 23:39'!
startRot: evt with: rotHandle
	"Initialize rotation of my target if it is rotatable.  Launch a command object to represent the action"

	evt hand obtainHalo: self.	"Make sure the event's hand correlates with the receiver"
	growingOrRotating _ true.

	self removeAllHandlesBut: rotHandle.  "remove all other handles"
	angleOffset _ evt eventPosition - target referencePosition.
	angleOffset _ Point
			r: angleOffset r
			degrees: angleOffset degrees - target rotationDegrees

! !


!HaloMorph methodsFor: 'forward to target' stamp: 'jmv 8/18/2012 16:33'!
chooseEmphasisOrAlignment
	target chooseEmphasisOrAlignment! !

!HaloMorph methodsFor: 'forward to target' stamp: 'jmv 8/18/2012 16:34'!
chooseFont
	target chooseFont! !

!HaloMorph methodsFor: 'forward to target' stamp: 'jmv 8/18/2012 16:25'!
deleteBalloon
	target deleteBalloon! !

!HaloMorph methodsFor: 'forward to target' stamp: 'jmv 8/18/2012 16:25'!
mouseDownOnHelpHandle: anEvent
	target mouseDownOnHelpHandle: anEvent! !


!HaloMorph methodsFor: 'stepping' stamp: 'len 6/3/2020 13:09:52'!
step
	self comeToFront.
	(target isNil or: [target isInWorld not]) ifTrue: [self delete]! !

!HaloMorph methodsFor: 'stepping' stamp: 'len 7/25/2016 21:41'!
stepTime
	^ 100! !

!HaloMorph methodsFor: 'stepping' stamp: 'len 7/25/2016 21:38'!
wantsSteps
	^ true! !


!HaloMorph methodsFor: '*Props' stamp: 'MM 1/12/2020 18:51:07'!
addPropertiesHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #editProperties! !

!HaloMorph methodsFor: '*Props' stamp: 'MM 1/12/2020 18:54:11'!
editProperties
	^ PropertiesBrowser openOn: target! !


!HaloMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/23/2020 12:16:26'!
color
	^ Color blue alpha: 0.3! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'HaloMorph class' category: #'Morphic-Halos'!
HaloMorph class
	instanceVariableNames: ''!

!HaloMorph class methodsFor: 'class initialization' stamp: 'jmv 4/20/2015 15:54'!
initialize
	"HaloMorph initialize"
	
	Preferences preferenceAt: #haloEnclosesFullBounds ifAbsent: [
		Preferences addPreference: #haloEnclosesFullBounds
			category: #halos
			default: false
			balloonHelp: 'if true, halos will enclose the full bounds of the target Morph, rather than just the bounds'
	].
 
	Preferences resetHaloSpecifications! !


!HaloMorph class methodsFor: 'accessing - icons' stamp: 'cbr 5/16/2011 14:30'!
haloCollapseIcon

	^ Theme current haloCollapseIcon! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:52'!
haloColorIcon

	^ self icons
		at: #haloColorIcon
		ifAbsentPut: [ Theme current haloColorIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloDebugIcon

	^ self icons
		at: #haloDebugIcon
		ifAbsentPut: [ Theme current haloDebugIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloDismissIcon

	^ self icons
		at: #haloDismissIcon
		ifAbsentPut: [ Theme current haloDismissIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloDragIcon

	^ self icons
		at: #haloDragIcon
		ifAbsentPut: [ Theme current haloDragIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloDuplicateIcon

	^ self icons
		at: #haloDuplicateIcon
		ifAbsentPut: [ Theme current haloDuplicateIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloFontEmphasisIcon

	^ self icons
		at: #haloFontEmphasisIcon
		ifAbsentPut: [ Theme current haloFontEmphasisIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloFontSizeIcon

	^ self icons
		at: #haloFontSizeIcon
		ifAbsentPut: [ Theme current haloFontSizeIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloGrabIcon

	^ self icons
		at: #haloGrabIcon
		ifAbsentPut: [ Theme current haloGrabIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloHelpIcon

	^ self icons
		at: #haloHelpIcon
		ifAbsentPut: [ Theme current haloHelpIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloMenuIcon

	^ self icons
		at: #haloMenuIcon
		ifAbsentPut: [ Theme current haloMenuIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloRotateIcon

	^ self icons
		at: #haloRotateIcon
		ifAbsentPut: [ Theme current haloRotateIcon ]! !

!HaloMorph class methodsFor: 'accessing - icons' stamp: 'jmv 4/20/2015 15:53'!
haloScaleIcon

	^ self icons
		at: #haloScaleIcon
		ifAbsentPut: [ Theme current haloScaleIcon ]! !


!HaloMorph class methodsFor: 'cached state access' stamp: 'jmv 4/20/2015 15:50'!
handleSize
	HandleSize ifNil: [
		HandleSize _ 16 ].
	^ HandleSize! !

!HaloMorph class methodsFor: 'cached state access' stamp: 'jmv 4/20/2015 15:52'!
icons
	Icons ifNil: [
		Icons _ Dictionary new ].
	^ Icons! !

!HaloMorph class methodsFor: 'cached state access' stamp: 'jmv 4/20/2015 15:52'!
releaseClassCachedState

	HandleSize _ nil.
	Icons _ nil! !


!HaloMorph class methodsFor: 'new-morph participation' stamp: 'pb 6/8/2017 23:53:26'!
categoryInNewMorphMenu
	^ 'Halos'! !


!HaloMorph class methodsFor: '*Props' stamp: 'MM 1/12/2020 18:53:32'!
haloPropertiesIcon
	^ self icons
		at: #haloPropertiesIcon
		ifAbsentPut: [ Theme current haloMenuIcon ]! !


HaloMorph initialize!
