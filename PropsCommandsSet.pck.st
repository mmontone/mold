'From Cuis 5.0 [latest update: #4528] on 15 February 2021 at 3:23:41 pm'!
'Description '!
!provides: 'PropsCommandsSet' 1 0!
!requires: 'Props' 1 62 nil!
!requires: 'Commander' 1 92 nil!
SystemOrganization addCategory: 'PropsCommandsSet'!


!classDefinition: #PropsCommandsSet category: 'PropsCommandsSet'!
MethodsCommandsSet subclass: #PropsCommandsSet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PropsCommandsSet'!
!classDefinition: 'PropsCommandsSet class' category: 'PropsCommandsSet'!
PropsCommandsSet class
	instanceVariableNames: ''!


!PropsCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/15/2021 11:29:54'!
commandsSetName
	^ 'Props commands'! !

!PropsCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/15/2021 11:30:45'!
editPropertiesCommand
	<command>
	^ PluggableCommand new
		commandName: 'edit properties';
		commandCategory: 'properties';
		arguments: {CommandArgument new name: #object;
									type: Object;
									yourself 	};
			description: 'Edit the properties of object';
			runBlock: [:obj | obj editProperties]! !
