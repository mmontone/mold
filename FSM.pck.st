'From Cuis 5.0 [latest update: #4743] on 8 September 2021 at 6:48:48 pm'!
'Description '!
!provides: 'FSM' 1 4!
!requires: 'YAXO' 1 17 nil!
!requires: 'PlantUML' 1 4 nil!
!requires: 'OSProcess' 1 14 nil!
SystemOrganization addCategory: #FSM!
SystemOrganization addCategory: #'FSM-Calculator'!


!classDefinition: #CalculatorMorph category: #'FSM-Calculator'!
LayoutMorph subclass: #CalculatorMorph
	instanceVariableNames: 'model display'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorMorph class' category: #'FSM-Calculator'!
CalculatorMorph class
	instanceVariableNames: ''!

!classDefinition: #FSM category: #FSM!
Object subclass: #FSM
	instanceVariableNames: 'name states transitions startState'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSM class' category: #FSM!
FSM class
	instanceVariableNames: ''!

!classDefinition: #FSMSource category: #FSM!
FSM subclass: #FSMSource
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMSource class' category: #FSM!
FSMSource class
	instanceVariableNames: ''!

!classDefinition: #OnOffFSM category: #FSM!
FSM subclass: #OnOffFSM
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'OnOffFSM class' category: #FSM!
OnOffFSM class
	instanceVariableNames: ''!

!classDefinition: #CalculatorFSM category: #'FSM-Calculator'!
FSM subclass: #CalculatorFSM
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorFSM class' category: #'FSM-Calculator'!
CalculatorFSM class
	instanceVariableNames: ''!

!classDefinition: #FSMAction category: #FSM!
Object subclass: #FSMAction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMAction class' category: #FSM!
FSMAction class
	instanceVariableNames: ''!

!classDefinition: #FSMBlockAction category: #FSM!
FSMAction subclass: #FSMBlockAction
	instanceVariableNames: 'block'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMBlockAction class' category: #FSM!
FSMBlockAction class
	instanceVariableNames: ''!

!classDefinition: #FSMDiagramGenerator category: #FSM!
Object subclass: #FSMDiagramGenerator
	instanceVariableNames: 'fsm'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMDiagramGenerator class' category: #FSM!
FSMDiagramGenerator class
	instanceVariableNames: ''!

!classDefinition: #FSMEvent category: #FSM!
Object subclass: #FSMEvent
	instanceVariableNames: 'name'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMEvent class' category: #FSM!
FSMEvent class
	instanceVariableNames: ''!

!classDefinition: #FSMRunner category: #FSM!
Object subclass: #FSMRunner
	instanceVariableNames: 'fsm state properties trace'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMRunner class' category: #FSM!
FSMRunner class
	instanceVariableNames: ''!

!classDefinition: #FSMState category: #FSM!
Object subclass: #FSMState
	instanceVariableNames: 'name action description'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMState class' category: #FSM!
FSMState class
	instanceVariableNames: ''!

!classDefinition: #CalculatorAccumulatorDecimalState category: #'FSM-Calculator'!
FSMState subclass: #CalculatorAccumulatorDecimalState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorAccumulatorDecimalState class' category: #'FSM-Calculator'!
CalculatorAccumulatorDecimalState class
	instanceVariableNames: ''!

!classDefinition: #CalculatorAccumulatorState category: #'FSM-Calculator'!
FSMState subclass: #CalculatorAccumulatorState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorAccumulatorState class' category: #'FSM-Calculator'!
CalculatorAccumulatorState class
	instanceVariableNames: ''!

!classDefinition: #CalculatorComputedState category: #'FSM-Calculator'!
FSMState subclass: #CalculatorComputedState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorComputedState class' category: #'FSM-Calculator'!
CalculatorComputedState class
	instanceVariableNames: ''!

!classDefinition: #CalculatorErrorState category: #'FSM-Calculator'!
FSMState subclass: #CalculatorErrorState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorErrorState class' category: #'FSM-Calculator'!
CalculatorErrorState class
	instanceVariableNames: ''!

!classDefinition: #CalculatorZeroState category: #'FSM-Calculator'!
FSMState subclass: #CalculatorZeroState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorZeroState class' category: #'FSM-Calculator'!
CalculatorZeroState class
	instanceVariableNames: ''!

!classDefinition: #FSMTransition category: #FSM!
Object subclass: #FSMTransition
	instanceVariableNames: 'name sourceState targetState event action description label fsm'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM'!
!classDefinition: 'FSMTransition class' category: #FSM!
FSMTransition class
	instanceVariableNames: ''!

!classDefinition: #CalculatorFSMModel category: #'FSM-Calculator'!
Object subclass: #CalculatorFSMModel
	instanceVariableNames: 'fsmRunner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'FSM-Calculator'!
!classDefinition: 'CalculatorFSMModel class' category: #'FSM-Calculator'!
CalculatorFSMModel class
	instanceVariableNames: ''!


!CalculatorMorph commentStamp: '<historical>' prior: 0!
Calculator morph. Works on a calculator state machine as model.!

!CalculatorFSM commentStamp: '<historical>' prior: 0!
A state machine for a simple calculator.!

!FSMDiagramGenerator commentStamp: '<historical>' prior: 0!
Sate machine diagram generator using PlantUML

(FSMDiagramGenerator on: anFSM) generate imageMorph openInWorld!

!FSM methodsFor: 'accessing' stamp: 'MM 8/31/2021 09:11:15'!
name

	^ name! !

!FSMEvent methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
name

	^ name! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:07'!
name

	^ name! !

!FSMState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 10:45'!
printOn: aStream

	super printOn: aStream.
	aStream nextPut: $[;
			nextPutAll: self name;
			nextPut: $]
	! !

!FSMTransition methodsFor: 'as yet unclassified' stamp: 'MM 9/3/2021 12:52:22'!
printOn: aStream

	super printOn: aStream.
	aStream nextPut: $[;
			nextPutAll: self event;
			nextPut: Character space.
	self sourceState name printOn: aStream.
	aStream 	nextPutAll: '->'.
	self targetState name printOn: aStream.
	aStream 	nextPut: $]
		! !

!CalculatorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:05'!
build

	| r |
	
	self color: Color white.
	
	display _ LabelMorph contents: '0'.
		
	self addMorphUseAll: display.
	
	0 to: 2 do: [:row | |buttons|
		buttons _ LayoutMorph newRow.
		1 to: 3 do: [:digit |
			buttons addMorphUseAll: (self buttonForDigit: digit  + (row * 3))].
		self addMorph: buttons].
	
	r _ LayoutMorph newRow.
	
	r addMorphUseAll: (PluggableButtonMorph
							model: model
							action: #plus
							label: '+').
							
	r addMorphUseAll: (PluggableButtonMorph
							model: model
							action: #minus
							label: '-').
							
	r addMorphUseAll: (PluggableButtonMorph
							model: model
							action: #multiply
							label: '*').
							
	r addMorphUseAll: (PluggableButtonMorph
							model: model
							action: #divide
							label: '%').
							
	self addMorphUseAll: r.
	
	r _ LayoutMorph newRow.
	
	r addMorphUseAll:( PluggableButtonMorph
						model: model
						action: #zero
						label: '0').
						
	r addMorphUseAll: (PluggableButtonMorph
						model: model
						action: #equals
						label: '=').
		
	r addMorphUseAll: (PluggableButtonMorph
						model: model
						action: #clear
						label: 'Clear').
						
	self addMorph: r! !

!CalculatorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:38'!
buttonForDigit: aNumber

	|actionSelector|
	
	actionSelector _ ('digit', aNumber asString) asSymbol.

	^ PluggableButtonMorph
		model: model
		action: actionSelector
		label: aNumber asString ! !

!CalculatorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:40'!
initialize: aModel

	model _ aModel.
	
	self build.
	
	model when: #updateDisplay send: #updateDisplay: to: self! !

!CalculatorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:41'!
updateDisplay: anObject

	display contents: anObject! !

!CalculatorMorph class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:56'!
openOn: aModel

	^ (self newColumn initialize: aModel)
		morphExtent: 200@300;
		 openInWorld! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:46'!
addState: aState

	|state|
	
	state _ aState.
	
	aState isString ifTrue: [
		state _ FSMState named: state].
	
	(aState isKindOf: Class) ifTrue: [
		state _ aState new].
	
	states add: state! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 9/3/2021 14:19:44'!
addTransition: anFSMTransition

	transitions add: anFSMTransition.
	anFSMTransition fsm: self.
	
	^ anFSMTransition ! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:22'!
addTransition: anEvent from: sourceStateName to: targetStateName

	^ self addTransition: (FSMTransition event: anEvent from: sourceStateName to: targetStateName)! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2018 19:12'!
diagramImageMorph

	|gen|
	
	gen _ FSMDiagramGenerator on: self.
	gen generate.
	
	^ gen imageMorph! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2018 22:05'!
hasState: aString

	states detect: [:state | state name = aString] ifNone: [^false].
	^ true 

	! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:00'!
initFSM

	^ self subclassResponsibility ! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:00'!
initialize

	states _ OrderedCollection new.
	transitions _ OrderedCollection new.
	self initFSM.! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2018 19:31'!
removeState: aState

	"Remove a state and all transitions that go from and to it."

	|state|
	
	state _ aState.
	
	aState isString ifTrue: [
		state _ self stateNamed: state].
	
	states remove: state.! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2018 19:31'!
removeStateAndTransitions: aState

	"Remove a state and all transitions that go from and to it."

	|state|
	
	state _ aState.
	
	aState isString ifTrue: [
		state _ self stateNamed: state].
	
	self removeState: state.
	self removeTransitionToState: state.
	self removeTransitionFromState: state.! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 9/3/2021 14:20:33'!
removeTransition: aTransition
	transitions remove: aTransition! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2018 19:30'!
removeTransitionFromState: aState

	transitions _ transitions removeAllSuchThat: [:transition | transition sourceState = aState name].! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/16/2018 19:30'!
removeTransitionToState: aState

	transitions _ transitions removeAllSuchThat: [:transition | transition targetState = aState name].! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:32'!
start: anFSMRunner! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 09:24'!
stateNamed: aString

	^ states detect: [:state | state name = aString] ifNone: [self error: 'State not found: ', aString]! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2018 20:16'!
transitionsFrom: aState

	^ transitions select: [:transition | transition sourceState = aState name]! !

!FSM methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2018 20:17'!
transitionsTo: aState

	^ transitions select: [:transition | transition targetState = aState name]! !

!FSM methodsFor: 'accessing' stamp: 'MM 8/31/2021 09:11:01'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 10:44'!
startState

	^ self stateNamed: startState! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:16'!
startState: anObject
	"Set the value of startState"

	startState _ anObject! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:16'!
states
	"Answer the value of states"

	^ states! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:03'!
states: aCollection

	states _ OrderedCollection new.
	aCollection do: [:state | self addState: state]! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:16'!
transitions
	"Answer the value of transitions"

	^ transitions! !

!FSM methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:16'!
transitions: anObject
	"Set the value of transitions"

	transitions _ anObject! !

!FSM class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:15'!
classMenu

	^ {{#label -> 'open diagram'. #action -> #openDiagram. #icon -> #worldIcon. #target -> self}}.! !

!FSM class methodsFor: 'as yet unclassified' stamp: 'MM 1/9/2021 18:53:27'!
diagram
	|gen|
	
	gen _ FSMDiagramGenerator on: self new.
	^ gen generate.! !

!FSM class methodsFor: 'as yet unclassified' stamp: 'MM 1/9/2021 18:51:04'!
openDiagram

	self diagram imageMorph openInWorld! !

!FSMSource methodsFor: 'as yet unclassified' stamp: 'MM 11/14/2018 21:24'!
initFSM! !

!OnOffFSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:52'!
initFSM

	self addState: (FSMState named: 'on').
	self addState: (FSMState named: 'off').
	
	self addTransition: #turnOff from: 'on' to: 'off'.
	self addTransition: #turnOn from: 'off' to: 'on'.
	
	self startState: 'off'! !

!CalculatorFSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 19:58'!
calculate: anOperation with: aNumber with: otherNumber

	^ aNumber perform: anOperation with: otherNumber! !

!CalculatorFSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:41'!
initFSM

	"https://fsharpforfunandprofit.com/posts/calculator-complete-v2/"

	self states: {CalculatorZeroState.  CalculatorAccumulatorState.
					CalculatorAccumulatorDecimalState.
					CalculatorComputedState. CalculatorErrorState}.
	
	"Transitions from ZeroState."
	self addTransition: 'zero' from: 'ZeroState' to: 'ZeroState'.
	self addTransition: ((FSMTransition event: #nonZeroDigit from: 'ZeroState' to: 'AccumulatorState')
								action: #nonZeroDigit:digit:; yourself).
	self addTransition: ((FSMTransition event: #decimalSeparator from: 'ZeroState' to: 'AccumulatorDecimalState')
							withActionFromEvent).
	self addTransition: ((FSMTransition event: #mathOp from: 'ZeroState' to: 'ComputedState')
							action: #mathOp:operator:; yourself).
	self addTransition: ((FSMTransition event: #equals from: 'ZeroState' to: 'ComputedState') withActionFromEvent).
	self addTransition: ((FSMTransition event: #clear from: 'ZeroState' to: 'ZeroState') withActionFromEvent).
	
	"Transitions from AccumulatorState."
	(self addTransition: #zero from: 'AccumulatorState' to: 'AccumulatorState') withActionFromEvent.
	(self addTransition: #nonZeroDigit from: 'AccumulatorState' to: 'AccumulatorState')
		action: #nonZeroDigit:digit:.
	(self addTransition: #decimalSeparator from: 'AccumulatorState' to: 'AccumulatorDecimalState') withActionFromEvent.
	(self addTransition: #mathOp from: 'AccumulatorState' to: 'ComputedState')
		action: #mathOp:operator:.
	self addTransition: ((FSMTransition event: #equals from: 'AccumulatorState' to: 'ComputedState') withActionFromEvent).
	(self addTransition: #clear from: 'AccumulatorState' to: 'ZeroState') withActionFromEvent.
	
	"Transitions from AccumulatorDecimalState."
	(self addTransition: #zero from: 'AccumulatorDecimalState' to: 'AccumulatorDecimalState') withActionFromEvent.
	(self addTransition: #nonZeroDigit from: 'AccumulatorDecimalState' to: 'AccumulatorDecimalState')
		action: #nonZeroDigit:digit:.
	(self addTransition: #decimalSeparator from: 'AccumulatorDecimalState' to: 'AccumulatorDecimalState') withActionFromEvent.
	(self addTransition: #mathOp from: 'AccumulatorDecimalState' to: 'ComputedState')
		action: #mathOp:operator:.
	self addTransition: ((FSMTransition event: #equals from: 'AccumulatorDecimalState' to: 'ComputedState') withActionFromEvent).
	(self addTransition: #clear from: 'AccumulatorDecimalState' to: 'ZeroState') withActionFromEvent.
	
	"Transitions from ComputedState."
	self addTransition: #zero from: 'ComputedState' to: 'ZeroState'.
	(self addTransition: #nonZeroDigit from: 'ComputedState' to: 'AccumulatorState')
		action: #nonZeroDigit:digit:.
	(self addTransition: #decimalSeparator from: 'ComputedState' to: 'AccumulatorDecimalState') withActionFromEvent.
	(self addTransition: #mathOp from: 'ComputedState' to: 'ComputedState')
		action: #mathOp:operator:.
	self addTransition: ((FSMTransition event: #equals from: 'ComputedState' to: 'ComputedState') withActionFromEvent).
	(self addTransition: #clear from: 'ComputedState' to: 'ZeroState') withActionFromEvent.
	
	"Transitions from ErrorState."
	self addTransition: #zero from: 'ErrorState' to: 'ErrorState'.
	self addTransition: #nonZeroDigit from: 'ErrorState' to: 'ErrorState'.
	self addTransition: #decimalSeparator from: 'ErrorState' to: 'ErrorState'.
	self addTransition: #mathOp from: 'ErrorState' to: 'ErrorState'.
	self addTransition: #equals from: 'ErrorState' to: 'ErrorState'.
	(self addTransition: #clear from: 'ErrorState' to: 'ZeroState') withActionFromEvent.
	
	self startState: 'ZeroState'! !

!CalculatorFSM methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:37'!
start: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: ''.! !

!CalculatorFSM class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:49'!
asModel
	^ CalculatorFSMModel on: self new! !

!CalculatorFSM class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:15'!
classMenu

	^ super classMenu, {{#label -> 'open calculator'. #action -> #openCalculator. #icon -> #displayIcon. #target -> self}}! !

!CalculatorFSM class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:48'!
openCalculator

	CalculatorMorph openOn: self asModel! !

!FSMAction methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 08:59'!
runOn: aStateMachine

	^ self subclassResponsibility ! !

!FSMBlockAction methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 09:00'!
runOn: aStateMachine

	^ block value: aStateMachine value: self! !

!FSMDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:40:14'!
generate

	| uml form |
	
	uml _ PlantUMLWriter startuml.
	
	uml transitionFromStartTo: fsm startState name.
	
	fsm transitions do: [:transition |
		uml transitionFrom: transition sourceState name to: transition targetState name].
	
	form _ uml enduml.
	
	^ ImageMorph new image: form! !

!FSMDiagramGenerator methodsFor: 'as yet unclassified' stamp: 'MM 9/8/2021 18:23:26'!
initialize: aFSM

	fsm _ aFSM! !

!FSMDiagramGenerator class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 22:00'!
on: aFSM

	^ self new initialize: aFSM! !

!FSMEvent methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 09:30'!
activeTransitions

	^ fsm transitions select: [:tran | tran sourceState = state name]! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 10:33'!
fsm

	^ fsm! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:37'!
goToState: anState

	|st|
	
	st _ anState.
	anState isString ifTrue: [st _ self fsm stateNamed: anState].
	(state == st) ifTrue: [^ self].
	
	state leave.
	state _ st.
	state enter.
	trace ifTrue: [
		Transcript show: 'Entered state: ', state printString.
		Transcript newLine].
	state action ifNotNil: [:anAction | self runAction: anAction withArguments: #()]! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:28'!
initialize: aFSM

	fsm _ aFSM.
	properties _ Dictionary new.
	trace _ false! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:30'!
propertyAt: aSymbol

	^ properties at: aSymbol! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:30'!
propertyAt: aSymbol put: anObject

	properties at: aSymbol put: anObject! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:37'!
runAction: anAction withArguments: aCollection

	|actionArgs|
	
	trace ifTrue: [
		Transcript show: 'Running action: ', anAction, ' args: ', aCollection printString.
		Transcript newLine].
	
	actionArgs _ {self}, aCollection.
	^ anAction isSymbol 
		ifTrue: [state perform: anAction withArguments: actionArgs]
		ifFalse: [anAction valueWithArguments: actionArgs]! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:35'!
start

	fsm start: self.
	state _ fsm startState.
	
	trace ifTrue: [
		Transcript show: 'FSM started: ', state printString.
		Transcript newLine].! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:28'!
startTracing

	trace _ true! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 10:33'!
state

	^ state! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:25'!
trigger: anEvent

	^ self trigger: anEvent withArguments: #()
	
	! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:25'!
trigger: anEvent with: anObject

	^ self trigger: anEvent withArguments: {anObject}
	
	
	
	! !

!FSMRunner methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 21:36'!
trigger: anEvent withArguments: aCollection

	| transition targetState |
	
	trace ifTrue: [
		Transcript show: 'Triggering event: ', anEvent, ' args: ', aCollection printString.
		Transcript newLine].
	
	transition _ self activeTransitions 
						detect: [:tran| tran event = anEvent]
						ifNone: [self error: 'Cannot trigger event: ', anEvent, ' in state: ', state printString].
					
	transition action ifNotNil: [:anAction | |res|
		res _ self runAction: anAction withArguments: aCollection.
		"If the action returns an state, then we go to that state and ignore the transition targetState"
		(res isKindOf: FSMState) ifTrue: [^ self goToState: res]].
		
	targetState _ fsm stateNamed: transition targetState.
	
	self goToState: targetState! !

!FSMRunner class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 10:31'!
on: aFSM

	^ self new initialize: aFSM! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
action
	"Answer the value of action"

	^ action! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
action: anObject
	"Set the value of action"

	action _ anObject! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:24'!
description
	"Answer the value of description"

	^ description! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:24'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!FSMState methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:07'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!FSMState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:31'!
enter! !

!FSMState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:31'!
leave! !

!FSMState class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 10:32'!
named: aString

	^ self new name: aString; yourself! !

!FSMState class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 07:17'!
parseFrom: aString notifying: aRequestor

	! !

!CalculatorAccumulatorDecimalState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:33'!
clear: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: '0'.
	anFSMRunner triggerEvent: #updateDisplay with: '0'.
	
	^ nil! !

!CalculatorAccumulatorDecimalState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:27'!
equals: anFSMRunner

	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp | |res|
		res _ [ |arg|
			arg _ (anFSMRunner propertyAt: #buffer) asNumber.
			anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: arg]
			on: Error do: [self halt. ^ anFSMRunner fsm stateNamed: 'ErrorState'].

		anFSMRunner triggerEvent: #updateDisplay with: res asString.
		anFSMRunner propertyAt: #pendingOp put: {pendingOp first. res}].

	anFSMRunner propertyAt: #buffer put: ''.
	
	^ nil! !

!CalculatorAccumulatorDecimalState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:27'!
mathOp: anFSMRunner operator: anOperator

	|res|
	
	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp |
		res _ [ |arg|
			arg _ (anFSMRunner propertyAt: #buffer) asNumber.
			anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: arg]
			on: Error do: [^ anFSMRunner fsm stateNamed: 'ErrorState']]
	ifNil: [
		res _ (anFSMRunner propertyAt: #buffer) asNumber].
	
	anFSMRunner triggerEvent: #updateDisplay with: res asString.
	anFSMRunner propertyAt: #pendingOp put: {anOperator.res}.
	anFSMRunner propertyAt: #buffer put: ''.
	
	^ nil! !

!CalculatorAccumulatorDecimalState methodsFor: 'actions' stamp: 'MM 11/12/2018 19:33'!
nonZeroDigit: anFSMRunner digit: aDigit

	|newBuffer|
	
	newBuffer _  (anFSMRunner propertyAt: #buffer), aDigit asString.
	anFSMRunner propertyAt: #buffer put: newBuffer.
	anFSMRunner triggerEvent: #updateDisplay with: newBuffer.
	^ nil! !

!CalculatorAccumulatorDecimalState methodsFor: 'actions' stamp: 'MM 11/12/2018 19:34'!
zero: anFSMRunner

	|newBuffer|
	
	newBuffer _ ((anFSMRunner propertyAt: #buffer), '0').
	anFSMRunner propertyAt: #buffer put: newBuffer.
	anFSMRunner triggerEvent: #updateDisplay with: newBuffer.
	^ nil! !

!CalculatorAccumulatorDecimalState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:42'!
initialize

	self name: 'AccumulatorDecimalState'! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:32'!
clear: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: '0'.
	anFSMRunner triggerEvent: #updateDisplay with: '0'.
	
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/12/2018 20:32'!
decimalSeparator: anFSMRunner

	anFSMRunner propertyAt: #buffer put: 
		(anFSMRunner propertyAt: #buffer), '.'.
	anFSMRunner triggerEvent: #updateDisplay with: (anFSMRunner propertyAt: #buffer).
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:26'!
equals: anFSMRunner

	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp | |res|
		res _ [ |arg|
			arg _ (anFSMRunner propertyAt: #buffer) asNumber.
			anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: arg]
			on: Error do: [self halt. ^ anFSMRunner fsm stateNamed: 'ErrorState'].

		anFSMRunner triggerEvent: #updateDisplay with: res asString.
		anFSMRunner propertyAt: #pendingOp put: {pendingOp first. res}].

	anFSMRunner propertyAt: #buffer put: ''.
	
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:18'!
mathOp: anFSMRunner operator: anOperator

	|res|
	
	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp |
		res _ [ |arg|
			arg _ (anFSMRunner propertyAt: #buffer) asNumber.
			anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: arg]
			on: Error do: [^ anFSMRunner fsm stateNamed: 'ErrorState']]
	ifNil: [
		res _ (anFSMRunner propertyAt: #buffer) asNumber].
	
	anFSMRunner triggerEvent: #updateDisplay with: res asString.
	anFSMRunner propertyAt: #pendingOp put: {anOperator.res}.
	anFSMRunner propertyAt: #buffer put: ''.
	
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/12/2018 20:41'!
nonZeroDigit: anFSMRunner digit: aDigit

	anFSMRunner propertyAt: #buffer put: (anFSMRunner propertyAt: #buffer), aDigit asString.
	anFSMRunner triggerEvent: #updateDisplay with: (anFSMRunner propertyAt: #buffer).
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'actions' stamp: 'MM 11/12/2018 20:41'!
zero: anFSMRunner

	anFSMRunner propertyAt: #buffer put: ((anFSMRunner propertyAt: #buffer), '0').
	anFSMRunner triggerEvent: #updateDisplay with: (anFSMRunner propertyAt: #buffer).
	^ nil! !

!CalculatorAccumulatorState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:42'!
initialize

	self name: 'AccumulatorState'! !

!CalculatorComputedState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:33'!
clear: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: '0'.
	anFSMRunner triggerEvent: #updateDisplay with: '0'.
	
	^ nil! !

!CalculatorComputedState methodsFor: 'actions' stamp: 'MM 11/12/2018 21:54'!
decimalSeparator: anFSMRunner

	anFSMRunner propertyAt: #buffer put: '0.'.
	anFSMRunner triggerEvent: #updateDisplay with: '0.'.
	^ nil! !

!CalculatorComputedState methodsFor: 'actions' stamp: 'MM 11/12/2018 21:54'!
equals: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	^ nil! !

!CalculatorComputedState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:30'!
mathOp: anFSMRunner operator: anOperator

	|res|
	
	res _ anFSMRunner fsm 
				calculate: anOperator 
				with: (anFSMRunner propertyAt: #pendingOp) second
				with: (anFSMRunner propertyAt: #buffer) asNumber.

	anFSMRunner propertyAt: #pendingOp put: {anOperator.res}.
	
	^ nil! !

!CalculatorComputedState methodsFor: 'actions' stamp: 'MM 11/12/2018 21:55'!
nonZeroDigit: anFSMRunner digit: aDigit

	|buffer|
	
	buffer _ (anFSMRunner propertyAt: #buffer), aDigit asString.
	anFSMRunner propertyAt: #buffer put: buffer.
	anFSMRunner triggerEvent: #updateDisplay with: buffer.
	
	^ nil! !

!CalculatorComputedState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:41'!
initialize

	self name: 'ComputedState'! !

!CalculatorErrorState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:33'!
clear: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: '0'.
	anFSMRunner triggerEvent: #updateDisplay with: '0'.
	
	^ nil! !

!CalculatorErrorState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:41'!
initialize

	self name: 'ErrorState'! !

!CalculatorZeroState methodsFor: 'actions' stamp: 'MM 11/13/2018 16:33'!
clear: anFSMRunner

	anFSMRunner propertyAt: #pendingOp put: nil.
	anFSMRunner propertyAt: #buffer put: '0'.
	anFSMRunner triggerEvent: #updateDisplay with: '0'.
	
	^ nil! !

!CalculatorZeroState methodsFor: 'actions' stamp: 'MM 11/12/2018 19:36'!
decimalSeparator: anFSMRunner

	anFSMRunner propertyAt: #buffer put: '0.'.
	anFSMRunner triggerEvent: #updateDisplay with: '0.'.
	^ nil! !

!CalculatorZeroState methodsFor: 'actions' stamp: 'MM 11/12/2018 20:27'!
equals: anFSMRunner

	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp | |res|
		res _ [anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: 0]
			on: Error do: [^ anFSMRunner fsm stateNamed: 'ErrorState'].

		anFSMRunner propertyAt: #buffer put: ''.
		anFSMRunner triggerEvent: #updateDisplay with: res asString.
		anFSMRunner propertyAt: #pendingOp put: nil].
	
	^ nil! !

!CalculatorZeroState methodsFor: 'actions' stamp: 'MM 11/12/2018 20:26'!
mathOp: anFSMRunner operator: anOperator

	(anFSMRunner propertyAt: #pendingOp) ifNotNil: [:pendingOp | |res|
		res _ [anFSMRunner fsm calculate: pendingOp first with: pendingOp second with: 0]
			on: Error do: [^ anFSMRunner fsm stateNamed: 'ErrorState'].

		anFSMRunner propertyAt: #buffer put: ''.
		anFSMRunner triggerEvent: #updateDisplay with: res asString.
		anFSMRunner propertyAt: #pendingOp put: {anOperator.0}].
	
	^ nil! !

!CalculatorZeroState methodsFor: 'actions' stamp: 'MM 11/12/2018 19:44'!
nonZeroDigit: anFSMRunner digit: aDigit

	anFSMRunner propertyAt: #buffer put: aDigit asString.
	anFSMRunner triggerEvent: #updateDisplay with: aDigit asString.
	^ nil! !

!CalculatorZeroState methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:41'!
initialize

	self name: 'ZeroState'! !

!FSMTransition methodsFor: 'as yet unclassified' stamp: 'MM 11/24/2018 21:03'!
descriptiveString

	^ String streamContents: [:aStream | 
			aStream 
			nextPutAll: self event;
			nextPut: Character space;
			nextPutAll: self sourceState;
			nextPutAll: '->';
			nextPutAll: self targetState]! !

!FSMTransition methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 10:37'!
label

	^ label ifNil: [self labelFromEvent]! !

!FSMTransition methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 11:48'!
labelFromEvent

	event ifNotNil: [
		^ event asString].
	
	^ nil
	
	! !

!FSMTransition methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 17:26'!
withActionFromEvent

	|actionSelector|
	
	actionSelector _ (event asString, ':') asSymbol.

	self action: actionSelector! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
action
	"Answer the value of action"

	^ action! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
action: anObject
	"Set the value of action"

	action _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:24'!
description
	"Answer the value of description"

	^ description! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:24'!
description: anObject
	"Set the value of description"

	description _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
event
	"Answer the value of event"

	^ event! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:15'!
event: anObject
	"Set the value of event"

	event _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 9/3/2021 14:19:57'!
fsm
	"Answer the value of fsm"

	^ fsm! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 9/3/2021 14:19:57'!
fsm: anObject
	"Set the value of fsm"

	fsm _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/13/2018 10:37'!
label: aString

	label _ aString! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 13:24'!
name: anObject
	"Set the value of name"

	name _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
sourceState
	"Answer the value of sourceState"

	^ sourceState! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
sourceState: anObject
	"Set the value of sourceState"

	sourceState _ anObject! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
targetState
	"Answer the value of targetState"

	^ targetState! !

!FSMTransition methodsFor: 'accessing' stamp: 'MM 11/12/2018 09:01'!
targetState: anObject
	"Set the value of targetState"

	targetState _ anObject! !

!FSMTransition class methodsFor: 'as yet unclassified' stamp: 'MM 11/12/2018 13:21'!
event: anEvent from: aStateName to: anotherStateName

	^ self new 
			event: anEvent;
			sourceState: aStateName;
			targetState: anotherStateName;
			yourself! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:47'!
clear

	fsmRunner trigger: #clear! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:47'!
digit0
	
	self zero! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit1
	
	self digit: 1! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit2
	
	self digit: 2! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit3
	
	self digit: 3! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit4
	
	self digit: 4! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit5
	
	self digit: 5! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit6
	
	self digit: 6! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit7
	
	self digit: 7! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit8
	
	self digit: 8! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:46'!
digit9
	
	self digit: 9! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:45'!
digit: aNumber

	fsmRunner trigger: #nonZeroDigit with: aNumber! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:02'!
divide

	fsmRunner trigger: #mathOp with: #/! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:47'!
equals

	fsmRunner trigger: #equals! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:59'!
initialize: aCalculatorFSM

	fsmRunner _ FSMRunner on: aCalculatorFSM.
	fsmRunner when: #updateDisplay send: #updateDisplay: to: self.
	fsmRunner start.! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:02'!
minus

	fsmRunner trigger: #mathOp with: #-! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:02'!
multiply

	fsmRunner trigger: #mathOp with: #*! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 15:02'!
plus

	fsmRunner trigger: #mathOp with: #+! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:44'!
updateDisplay: anObject

	self triggerEvent: #updateDisplay with: anObject! !

!CalculatorFSMModel methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:45'!
zero

	fsmRunner trigger: #zero! !

!CalculatorFSMModel class methodsFor: 'as yet unclassified' stamp: 'MM 11/13/2018 14:42'!
on: aCalculatorFSM

	^ self new initialize: aCalculatorFSM! !
