'From Cuis 5.0 [latest update: #3983] on 22 December 2019 at 8:01:48 pm'!
'Description '!
!provides: 'DevTools' 1 2!
SystemOrganization addCategory: #DevTools!


!classDefinition: #Logger category: #DevTools!
Object subclass: #Logger
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DevTools'!
!classDefinition: 'Logger class' category: #DevTools!
Logger class
	instanceVariableNames: ''!

!classDefinition: #Profiler category: #DevTools!
Object subclass: #Profiler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DevTools'!
!classDefinition: 'Profiler class' category: #DevTools!
Profiler class
	instanceVariableNames: ''!

!classDefinition: #Tracer category: #DevTools!
Object subclass: #Tracer
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DevTools'!
!classDefinition: 'Tracer class' category: #DevTools!
Tracer class
	instanceVariableNames: ''!

!classDefinition: #LogBrowser category: #DevTools!
SystemWindow subclass: #LogBrowser
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DevTools'!
!classDefinition: 'LogBrowser class' category: #DevTools!
LogBrowser class
	instanceVariableNames: ''!


!Logger commentStamp: '<historical>' prior: 0!
A simple logging tool.

Output goes to either Transcript, rich-text workspace (with clickable objects), or a LogBrowser.

Logger for: aCategory at: aLevel log: '%level: something %o: %o %o' % {#cool. anObject. otherObject}.!

!Tracer commentStamp: '<historical>' prior: 0!
Installs tracing code in objects and methods.

Installs:
At start of method: Tracer>>callMethod: (MethodReference class: aClass selector: aSelector) withArguments: aCollection
At end of method: Tracer>>methodReturns: (MethodReference class: aClass selector: aSelector) value: anObject.

See BreakPointManager to see how code is installed.

Output can go to either the Transcript, a simple plain text workspace, a rich-text workspace, or a LogBrowser.!

!LogBrowser commentStamp: '<historical>' prior: 0!
Displays logs in an interactive way.

Filters logs by tag or level.
Search logs.
Logs can be expanded and the objects involved inspected (similar like in web browser's console).

Logger for: aCategory at: aLevel log: '%level: something %o: %o %o' % {#cool. anObject. otherObject}.!
