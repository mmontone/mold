'From Cuis 5.0 [latest update: #4030] on 17 February 2020 at 5:06:46 pm'!
'Description '!
!provides: 'SmalltalkClassExamples' 1 0!
SystemOrganization addCategory: #SmalltalkClassExamples!


!classDefinition: #MyClass category: #SmalltalkClassExamples!
Object subclass: #MyClass
	instanceVariableNames: 'age style foo name color'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SmalltalkClassExamples'!
!classDefinition: 'MyClass class' category: #SmalltalkClassExamples!
Object class subclass: 'MyClass class'
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SmalltalkClassExamples'!

!classDefinition: #MySmalltalkClass category: #SmalltalkClassExamples!
SmalltalkClass named: #MySmalltalkClass
		          extends: Object
		          slots: #(#name -> #(#type -> String #public -> true #default -> nil) #age -> #(#type -> Integer #public -> false) #color -> #(#type -> Color #public -> true #default -> Color blue) #style -> #(#type -> String #setter -> #style: #getter -> #style #description -> 'Use this for styling') #foo -> #(#slotClass -> TracedSlot #description -> 'This slot value is traced'))
		          category: 'SmalltalkClassExamples'!
!classDefinition: 'MySmalltalkClass class' category: #SmalltalkClassExamples!
Object class subclass: 'MySmalltalkClass class'
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'SmalltalkClassExamples'!


!MySmalltalkClass class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 17:05:37'!
definition
	^ self definitionString ifNil: [super definition]! !

!MySmalltalkClass class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 17:05:37'!
slotsSpec
	^ #(#name -> #(#type -> String #public -> true #default -> nil) #age -> #(#type -> Integer #public -> false) #color -> #(#type -> Color #public -> true #default -> Color blue) #style -> #(#type -> String #setter -> #style: #getter -> #style #description -> 'Use this for styling') #foo -> #(#slotClass -> TracedSlot #description -> 'This slot value is traced'))! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
name
	^ name! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
color
	^ color! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
color: anObject
	color _ anObject! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
name: anObject
	name _ anObject! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
style
	^ style! !

!MySmalltalkClass methodsFor: 'accessing' stamp: 'MM 2/17/2020 17:05:37'!
style: anObject
	style _ anObject! !

!MySmalltalkClass class methodsFor: 'as yet unclassified' stamp: 'MM 2/17/2020 17:05:37'!
definitionString
		^ 'SmalltalkClass named: #MySmalltalkClass
		          extends: Object
		          slots: #(#name -> #(#type -> String #public -> true #default -> nil) #age -> #(#type -> Integer #public -> false) #color -> #(#type -> Color #public -> true #default -> Color blue) #style -> #(#type -> String #setter -> #style: #getter -> #style #description -> ''Use this for styling'') #foo -> #(#slotClass -> TracedSlot #description -> ''This slot value is traced''))
		          category: ''SmalltalkClassExamples'''! !
