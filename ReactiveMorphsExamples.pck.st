'From Cuis 6.0 [latest update: #5840] on 12 June 2023 at 4:10:45 pm'!
'Description '!
!provides: 'ReactiveMorphsExamples' 1 1!
!requires: 'ReactiveMorphsWidgets' 1 1 nil!
!requires: 'ReactiveMorphs' 1 85 nil!
SystemOrganization addCategory: 'ReactiveMorphsExamples'!


!classDefinition: #RxCounterExample category: 'ReactiveMorphsExamples'!
LayoutMorph subclass: #RxCounterExample
	instanceVariableNames: 'counter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsExamples'!
!classDefinition: 'RxCounterExample class' category: 'ReactiveMorphsExamples'!
RxCounterExample class
	instanceVariableNames: ''!

!classDefinition: #RxMorphExplorerMorph category: 'ReactiveMorphsExamples'!
LayoutMorph subclass: #RxMorphExplorerMorph
	instanceVariableNames: 'model selectedMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsExamples'!
!classDefinition: 'RxMorphExplorerMorph class' category: 'ReactiveMorphsExamples'!
RxMorphExplorerMorph class
	instanceVariableNames: ''!

!classDefinition: #ReactiveMorphsExamples category: 'ReactiveMorphsExamples'!
Object subclass: #ReactiveMorphsExamples
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsExamples'!
!classDefinition: 'ReactiveMorphsExamples class' category: 'ReactiveMorphsExamples'!
ReactiveMorphsExamples class
	instanceVariableNames: ''!

!classDefinition: #RxCounterComponent category: 'ReactiveMorphsExamples'!
RxComponent subclass: #RxCounterComponent
	instanceVariableNames: 'counter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsExamples'!
!classDefinition: 'RxCounterComponent class' category: 'ReactiveMorphsExamples'!
RxCounterComponent class
	instanceVariableNames: ''!

!classDefinition: #RxCounterEditorComponent category: 'ReactiveMorphsExamples'!
RxComponent subclass: #RxCounterEditorComponent
	instanceVariableNames: 'counter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsExamples'!
!classDefinition: 'RxCounterEditorComponent class' category: 'ReactiveMorphsExamples'!
RxCounterEditorComponent class
	instanceVariableNames: ''!


!RxCounterExample commentStamp: 'MM 5/30/2023 11:27:41' prior: 0!
RxCounterExample new openInWorld!

!RxMorphExplorerMorph commentStamp: 'MM 6/4/2023 23:17:01' prior: 0!
RxMorphExplorerMorph openOn: self runningWorld.!

!RxCounterComponent commentStamp: '<historical>' prior: 0!
(RxCounterComponent on: (RxValue with: 0)) openInWorld!

!RxCounterExample methodsFor: 'as yet unclassified' stamp: 'MM 5/31/2023 16:59:58'!
initialize

	super initialize.
	
	counter := RxValue with: 0.
	self addMorph: (RxLabelMorph contents: counter stringValue).
	self addMorph: (PluggableButtonMorph model: [counter value: counter value + 1]
									action: #value
									label: 'Increment')
	! !

!RxCounterExample class methodsFor: 'as yet unclassified' stamp: 'MM 5/29/2023 16:28:19'!
new
	^ self newColumn.! !

!RxMorphExplorerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:07:20'!
buildMorph
	
	|treeAccessor|
	
	treeAccessor := RxMorphTreeAccessor new 
				action: [:ev :aMorph | selectedMorph value: aMorph].
	
	self addMorph:
		(PluggableScrollPane new
			scroller: (RxMorphTreeMorph on: model treeAccessor: treeAccessor); yourself)
		proportionalHeight: 0.5.
	self addAdjusterAndMorph: (selectedMorph reactiveMorph: [:aMorph | self inspectorMorphOn: aMorph] :: fitContents: false; yourself)
		layoutSpec: LayoutSpec useAll.! !

!RxMorphExplorerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:05:20'!
delete

	selectedMorph value: nil.
	super delete.! !

!RxMorphExplorerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:05:01'!
initialize: aMorph

	model := aMorph.
	selectedMorph := RxValue with: model.
	
	self buildMorph.! !

!RxMorphExplorerMorph methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 16:52:29'!
inspectorMorphOn: aMorph

	|window|
	window := InspectorWindow new.
	window
		model: (Inspector inspect: aMorph);
		buildMorphicWindow.
	window layoutMorph adoptWidgetsColor: window windowColor.
	^window layoutMorph! !

!RxMorphExplorerMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 23:25:32'!
on: aMorph

	^ self newColumn initialize: aMorph! !

!RxMorphExplorerMorph class methodsFor: 'as yet unclassified' stamp: 'MM 6/5/2023 00:09:04'!
openOn: aMorph
	self on: aMorph :: openInWindowLabeled: 'Morph explorer'! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/2/2023 11:34:40'!
counterExample

	"self counterExample"
	
	| counter |
	
	counter := RxValue with: 0.
	
	LayoutMorph newColumn children: {
		RxLabelMorph contents: counter stringValue.
		PluggableButtonMorph label: 'Increment' action: [counter value: counter value + 1].
		PluggableButtonMorph label: 'Decrement' action: [counter value: counter value - 1]
	}; openInWorld! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/4/2023 23:38:13'!
createCounterWidget: counter

	| editing counterString textInput |
	
	editing := RxValue with: false.
	counterString := counter apply: #asString unapply: #asInteger.
	
	^ editing 
		ifIsTrue: [
			LayoutMorph newColumn children: {
				(textInput := TextModelMorph withText: counterString value)
					morphHeight: 30; yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Accept' action: [counterString value: textInput text asString. editing value: false].
					PluggableButtonMorph label: 'Cancel' action: [editing value: false]
				}
			}]
		ifIsFalse: [
			LayoutMorph newColumn children: {
				(RxLabelMorph contents: counterString)
						onClick: [:ev | editing value: true];
						yourself.
			LayoutMorph newRow children: {
				PluggableButtonMorph label: 'Increment' action: [counter value: counter value + 1].
				PluggableButtonMorph label: 'Decrement' action: [counter value: counter value - 1]
			}			
		}]! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/4/2023 23:38:24'!
editableCounterExample

	"self editableCounterExample"
	
	| counter editing counterString textInput |
	
	counter := RxValue with: 0.
	editing := RxValue with: false.
	counterString := counter apply: #asString unapply: #asInteger.
	
	editing 
		ifIsTrue: [
			LayoutMorph newColumn children: {
				(textInput := TextModelMorph withText: counterString value)
					morphHeight: 30; yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Accept' action: [counterString value: textInput text asString. editing value: false].
					PluggableButtonMorph label: 'Cancel' action: [editing value: false]
				}
			}]
		ifIsFalse: [
			LayoutMorph newColumn children: {
				(RxLabelMorph contents: counterString)
						onClick: [:ev | editing value: true];
						yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Increment' action: [counter value: counter value + 1].
					PluggableButtonMorph label: 'Decrement' action: [counter value: counter value - 1]
				}			
		}] :: openInWorld! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/4/2023 23:39:05'!
editableCounterExample2

	"self editableCounterExample2"
	
	"Use dynamic composition"
	
	| counter editing counterString textInput |
	
	counter := RxValue with: 0.
	editing := RxValue with: false.
	counterString := counter apply: #asString unapply: #asInteger.
	
	editing reactiveMorph: [:val |
		val ifTrue: [
			LayoutMorph newColumn children: {
				(textInput := TextModelMorph withText: counterString value)
					morphHeight: 30; yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Accept' action: [counterString value: textInput text asString. editing value: false].
					PluggableButtonMorph label: 'Cancel' action: [editing value: false]
				}
			}]
		ifFalse: [
			LayoutMorph newColumn children: {
				(RxLabelMorph contents: counterString)
						onClick: [:ev | editing value: true];
						yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Increment' action: [counter value: counter value + 1].
					PluggableButtonMorph label: 'Decrement' action: [counter value: counter value - 1]
				}			
		}]] :: openInWorld! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/5/2023 21:51:22'!
editableTODOItem: aTodoItem todos: todos

	|toggleEditing|
	
	toggleEditing := RxValue with: false.
				
	^toggleEditing 
		ifIsTrue: [ |todoEditor|
			todoEditor := TextModelMorph withText: aTodoItem.
			todoEditor morphHeight: 30.
			todoEditor innerTextMorph 
				onKeyStroke: [:ev | ev isReturnKey ifTrue: [
								todos replaceAll: aTodoItem with: todoEditor text asString. 
								toggleEditing value: false].
								ev isEsc ifTrue: [toggleEditing value: false] ].
			todoEditor] 
		ifIsFalse: [  
			LayoutMorph newRow children: {
				(LabelMorph contents: aTodoItem)
					onClick: [:ev | toggleEditing value: true];
					yourself.
				(PluggableButtonMorph label: 'x' action:[todos remove: aTodoItem])
					morphHeight: 30; yourself
				}]! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/6/2023 18:28:58'!
editableTODOListExample

	"self editableTODOListExample"

	| todos todoList todosMorph textInput |
	
	todos := RxCollection new.
	
	todosMorph := LayoutMorph newColumn.
	
	todoList := todos 
		ifEmpty: [LabelMorph contents: 'Nothing TODO. Add below.']
		ifNotEmpty: [|todoList2|
			todoList2 := LayoutMorph newColumn.
			todos addEach: [:todo |			|toggleEditing |
				toggleEditing := RxValue with: false.
				
				toggleEditing 
					ifIsTrue: [ |todoEditor|
						todoEditor := TextModelMorph withText: todo.
						todoEditor morphHeight: 30.
						todoEditor innerTextMorph 
							onKeyStroke: [:ev | ev isReturnKey ifTrue: [
											todos replaceAll: todo with: todoEditor text asString. 
											toggleEditing value: false].
											ev isEsc ifTrue: [toggleEditing value: false] ].
						todoEditor] 
					ifIsFalse: [ |todoLabel todoItem| 
						todoItem := LayoutMorph newRow.
						todoLabel := LabelMorph contents: todo.
						todoLabel onClick: [:ev | toggleEditing value: true]. 
						todoItem addMorph: todoLabel.
						todoItem addMorph: (PluggableButtonMorph model:[todos remove: todo]
												action: #value label: 'x').
						todoItem morphHeight: 30.
						todoItem]] 
			to: todoList2].
	todoList color: Color lightBlue.
	todosMorph addMorph: todoList.
	
	textInput := TextModelMorph withText: ''.
	textInput morphHeight: 30.
	textInput innerTextMorph onKeyStroke: [:ev | ev isReturnKey ifTrue: [todos add: textInput text asString]].
	todosMorph addMorph: textInput.
	
	todosMorph addMorph: (PluggableButtonMorph 
							model: [todos add: textInput text asString]
							action: #value
							label: 'Add todo').											
	todosMorph openInWorld.! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/4/2023 23:39:24'!
editableTODOListExample2

	"self editableTODOListExample2"

	| todos todoList todosMorph textInput |
	
	todos := RxCollection new.
	
	todosMorph := LayoutMorph newColumn.
	
	todoList := todos 
		ifEmpty: [LabelMorph contents: 'Nothing TODO. Add below.']
		ifNotEmpty: [|todoList2|
			todoList2 := LayoutMorph newColumn.
			todos add: [:todo |			|toggleEditing |
				toggleEditing := RxValue with: false.
				
				toggleEditing 
					ifIsTrue: [ |todoEditor|
						todoEditor := TextModelMorph withText: todo.
						todoEditor morphHeight: 30.
						todoEditor innerTextMorph 
							onKeyStroke: [:ev | ev isReturnKey ifTrue: [
											todos replaceAll: todo with: todoEditor text asString. 
											toggleEditing value: false].
											ev isEsc ifTrue: [toggleEditing value: false] ].
						todoEditor] 
					ifIsFalse: [ |todoLabel todoItem| 
						todoItem := LayoutMorph newRow.
						todoLabel := LabelMorph contents: todo.
						todoLabel onClick: [:ev | toggleEditing value: true]. 
						todoItem addMorph: todoLabel.
						todoItem addMorph: (PluggableButtonMorph model:[todos remove: todo]
												action: #value label: 'x').
						todoItem morphHeight: 30.
						todoItem]] 
			to: todoList2 key: #yourself].
	todoList color: Color lightBlue.
	todosMorph addMorph: todoList.
	
	textInput := TextModelMorph withText: ''.
	textInput morphHeight: 30.
	textInput innerTextMorph onKeyStroke: [:ev | ev isReturnKey ifTrue: [todos add: textInput text asString]].
	todosMorph addMorph: textInput.
	
	todosMorph addMorph: (PluggableButtonMorph 
							model: [todos add: textInput text asString]
							action: #value
							label: 'Add todo').											
	todosMorph openInWorld.! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/5/2023 21:51:22'!
editableTODOListExample3

	"self editableTODOListExample3"
	
	"This is an example of 'template-like syntax' using #children: to compose morphs."

	| todos textInput |
	
	todos := RxCollection new.
	
	LayoutMorph newColumn children: {
		(todos 
			ifEmpty: [LabelMorph contents: 'Nothing TODO. Add below.']
			ifNotEmpty: [LayoutMorph newColumn 
							rxChildren: todos 	from: [:todo |	 self editableTODOItem: todo todos: todos]])
			color: Color lightBlue; yourself.
	
		textInput := (TextModelMorph withText: '')
					morphHeight: 30; yourself.
	
		PluggableButtonMorph label: 'Add todo'
							action: [todos add: textInput text asString]
	}; openInWindow! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/6/2023 21:21:41'!
editableTODOListExample4

	"self editableTODOListExample4"
	
	"This is an example of 'template-like syntax' using #children: to compose morphs."

	| todos done textInput |
	
	todos := RxCollection new.
	done := OrderedCollection new.
	
	LayoutMorph newColumn children: {
		(todos 
			ifEmpty: [LabelMorph contents: 'Nothing TODO. Add below.']
			ifNotEmpty: [LayoutMorph newColumn 
							rxChildren: todos 	from: [:todo |	 self toggleableTODOItem: todo todos: todos done: done]])
			color: Color lightBlue; yourself.
	
		textInput := (TextModelMorph withText: '')
					morphHeight: 30; yourself.
	
		PluggableButtonMorph label: 'Add todo'
							action: [todos add: textInput text asString]
	}; openInWindow! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/2/2023 13:41:21'!
multiCounterExample

	"self multiCounterExample"

	| counters |
	
	counters := RxCollection new.
	
	LayoutMorph newColumn children: {
		LayoutMorph newColumn rxChildren: counters from: [:counter | self createCounterWidget: counter].
		PluggableButtonMorph label: 'Add counter' action: [counters add: (RxValue with: 0)]
	} :: openInWorld ! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/2/2023 18:38:23'!
tabsExample

	| tabs activeTab tabContents tabNumber |
	
	tabs := RxCollection new.
	activeTab := RxValue with: nil.
	tabContents := Dictionary new.
	tabNumber := 1.
	
	LayoutMorph newColumn children: {
		"Tabs row"
		LayoutMorph newRow children: {
			LayoutMorph newRow
				rxChildren: tabs from: [:tab | 
					LayoutMorph newRow children: {
						PluggableButtonMorph label: tab action: [activeTab value: tab].
						(PluggableButtonMorph label: 'x' action: [tabs remove: tab])
							roundButtonStyle: false; yourself.
					}].
			PluggableButtonMorph label: '+' action: [tabs add: (tabNumber := tabNumber + 1) asString]
		}.
		
		"Tab content"
	} :: openInWorld
		
		! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/6/2023 18:39:48'!
todoListExample

	"self todoListExample openInWorld"

	| todos textInput |
	
	todos := RxCollection new.
	
	^ LayoutMorph newColumn children: {
		todos 
			ifEmpty: [LabelMorph contents: 'Nothing TODO. Add below.']
			ifNotEmpty: [
				LayoutMorph newColumn 
					rxChildren: todos
					from: [:todo |			
						​ LayoutMorph newRow children: {
							LabelMorph contents: todo.
							PluggableButtonMorph label: 'x' action:[todos remove: todo]
								:: morphHeight: 30; yourself
							}
						]].	
	
		(textInput := TextModelMorph withText: '')
			morphHeight: 30;
			"textInput innerTextMorph onKeyStroke: [:ev | ev isReturnKey ifTrue: [todos add: textInput text asString]]."
			yourself.
	
		PluggableButtonMorph label: 'Add todo' action: [todos add: textInput text asString]
	}! !

!ReactiveMorphsExamples class methodsFor: 'examples' stamp: 'MM 6/6/2023 21:24:32'!
toggleableTODOItem: aTodoItem todos: todos done: done

	|toggleEditing doneSwitch |
	
	toggleEditing := RxValue with: false.
	doneSwitch := done includes: aTodoItem :: ifTrue: [Switch newOn] ifFalse: [Switch newOff].
					
	^toggleEditing 
		ifIsTrue: [ |todoEditor|
			todoEditor := TextModelMorph withText: aTodoItem.
			todoEditor morphHeight: 30.
			todoEditor innerTextMorph 
				onKeyStroke: [:ev | ev isReturnKey ifTrue: [
								todos replaceAll: aTodoItem with: todoEditor text asString.
								done replaceAll: aTodoItem with: todoEditor text asString. 
								toggleEditing value: false].
								ev isEsc ifTrue: [toggleEditing value: false] ].
			todoEditor] 
		ifIsFalse: [  
			LayoutMorph newRow children: {
				(ToggleButtonMorph1 on: doneSwitch)
					getStateSelector: #isOn;
					actionSelector: #switch;
					morphExtent: 30@16; yourself.
				(LabelMorph contents: aTodoItem)
					onClick: [:ev | toggleEditing value: true];
					yourself.
				(PluggableButtonMorph label: 'x' action:[todos remove: aTodoItem])
					morphHeight: 30; yourself
				}]! !

!ReactiveMorphsExamples class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 18:44:08'!
visibilityExample

	|message |
	
	LayoutMorph newColumn children: {
		message := LabelMorph contents: 'Hello!!!!'.
		PluggableButtonMorph label: 'Toggle visibility'
			action: [message visible: message visible not]
	} :: openInWorld
	! !

!ReactiveMorphsExamples class methodsFor: 'as yet unclassified' stamp: 'MM 6/2/2023 18:46:52'!
visibilityExample2

	| visible |
	
	visible := RxValue with: true.
	
	LayoutMorph newColumn children: {
		visible ifIsTrue: [LabelMorph contents: 'Hello!!!!'].
		PluggableButtonMorph label: 'Toggle visibility'
			action: [visible value: visible value not]
	} :: openInWorld
	! !

!ReactiveMorphsExamples class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 15:28:03'!
visibilityExample3

	| visible |
	
	visible := RxValue with: true.
	
	LayoutMorph newColumn children: {
		(LabelMorph contents: 'Hello!!!!')
			rxProp: #visible: is: visible;
			yourself.
		PluggableButtonMorph label: 'Toggle visibility'
			action: [visible value: visible value not]
	} :: openInWorld
	! !

!ReactiveMorphsExamples class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 19:32:53'!
visibilityExample4

	|label|
	
	LayoutMorph newColumn children: {
		label := (LabelMorph contents: 'Hello!!!!')
			yourself.
		[|visible|
		  visible := RxAspect on: label aspect: #visible.
		   PluggableButtonMorph label: 'Toggle visibility'
			action: [visible value: visible value not]] value.
	} :: openInWorld
	! !

!RxCounterComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/4/2023 23:39:37'!
buildMorph

	^ LayoutMorph newColumn children: { 
		(RxLabelMorph contents: counter stringValue)
			onClick: [:ev | self editCounter]; yourself.
		PluggableButtonMorph model: [counter value: counter value + 1]
									action: #value
									label: 'Increment'
		}! !

!RxCounterComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:46:04'!
editCounter

	self call: (RxCounterEditorComponent on: counter)! !

!RxCounterComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:36:32'!
initialize: aCounter

	counter := aCounter! !

!RxCounterComponent class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:40:47'!
on: aCounter

	^ self new initialize: aCounter! !

!RxCounterEditorComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:40:15'!
buildMorph
	
	| textInput counterString |
	
	counterString := counter apply: #asString unapply: #asInteger.
	
	^ LayoutMorph newColumn children: {
				(textInput := TextModelMorph withText: counterString value)
					morphHeight: 30; yourself.
				LayoutMorph newRow children: {
					PluggableButtonMorph label: 'Accept' action: [counterString value: textInput text asString. self return].
					PluggableButtonMorph label: 'Cancel' action: [self return]
				}
			}! !

!RxCounterEditorComponent methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:39:23'!
initialize: aCounter

	counter := aCounter.
	
	! !

!RxCounterEditorComponent class methodsFor: 'as yet unclassified' stamp: 'MM 6/3/2023 13:40:40'!
on: aCounter

	^ self new initialize: aCounter! !
