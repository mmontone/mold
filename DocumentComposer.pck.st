'From Cuis 5.0 [latest update: #4213] on 14 June 2020 at 1:12:47 pm'!
'Description Rich Documents Composer

Author: Mariano Montone <marianomontone@gmail.com>'!
!provides: 'DocumentComposer' 1 1!
!requires: 'StructurEd' 1 43 nil!
SystemOrganization addCategory: #DocumentComposer!


!classDefinition: #DocumentComposerMorph category: #DocumentComposer!
SEDEditorMorph subclass: #DocumentComposerMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DocumentComposerMorph class' category: #DocumentComposer!
DocumentComposerMorph class
	instanceVariableNames: ''!

!classDefinition: #DocumentComposer category: #DocumentComposer!
SEDEditor subclass: #DocumentComposer
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DocumentComposer class' category: #DocumentComposer!
DocumentComposer class
	instanceVariableNames: ''!

!classDefinition: #DCTextNode category: #DocumentComposer!
SEDTextNode subclass: #DCTextNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DCTextNode class' category: #DocumentComposer!
DCTextNode class
	instanceVariableNames: ''!

!classDefinition: #DCHeading category: #DocumentComposer!
SEDNode subclass: #DCHeading
	instanceVariableNames: 'level'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DCHeading class' category: #DocumentComposer!
DCHeading class
	instanceVariableNames: ''!

!classDefinition: #DCParagraphNode category: #DocumentComposer!
SEDNode subclass: #DCParagraphNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DCParagraphNode class' category: #DocumentComposer!
DCParagraphNode class
	instanceVariableNames: ''!

!classDefinition: #DocumentComposerNode category: #DocumentComposer!
SEDNode subclass: #DocumentComposerNode
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'DocumentComposer'!
!classDefinition: 'DocumentComposerNode class' category: #DocumentComposer!
DocumentComposerNode class
	instanceVariableNames: ''!

