'From Cuis 6.0 [latest update: #5090] on 28 February 2022 at 11:45:54 am'!
'Description '!
!provides: 'StandardCommandsSet' 1 12!
SystemOrganization addCategory: 'StandardCommandsSet'!


!classDefinition: #StandardCommandsSet category: 'StandardCommandsSet'!
MethodsCommandsSet subclass: #StandardCommandsSet
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'StandardCommandsSet'!
!classDefinition: 'StandardCommandsSet class' category: 'StandardCommandsSet'!
StandardCommandsSet class
	instanceVariableNames: ''!


!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:38:00'!
alphabetizeCollectionCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'alphabetize';
		commandCategory: 'collections';
		argument: #collection					type: SequenceableCollection;
			description: 'Alphabetize collection';
			runBlock: [:collection | collection sort: [:x :y | x asString < y asString]. collection changed];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:38:27'!
browseClassCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'browse';
		commandCategory: 'development';
		arguments: {CommandArgument new name: #class;
									predicate: [:x | x isKindOf: Class];
									yourself 	};
			description: 'Browse the class';
			runBlock: [:class | BrowserWindow fullOnClass: class];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:38:57'!
browseDirectoryEntryContentsCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'browse directory contents';
		commandCategory: 'uncategorized';
		argument: #directory					type: DirectoryEntry;
			description: 'Browse the directory';
			runBlock: [:directoryEntry | | win |
					win _ FileListWindow open: (FileList new directory: directoryEntry) label: nil.
					win model when: #updateButtonRow send: #updateButtonRow to: win];
			yourself! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:39:11'!
browseObjectClassCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'browse object class';
		commandCategory: 'development';
		argument: #object				type: Object;
			description: 'Browse the class of the object';
			runBlock: [:obj | BrowserWindow fullOnClass: obj class];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:30:35'!
commandsSetName
	^ 'Standard commands'! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/28/2022 11:41:15'!
copyFileCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'copy file';
		commandCategory: 'files';
		argument: #file type: FileEntry							description: 'File to copy';
		argument: #directory					type: DirectoryEntry					description: 'Destination directory';
		argument: #ifExistsAction type: [:type | type choices: {'overwrite'.'deny'.'question'}]
			properties: [:arg |
				arg description: 'What to do if destination file exists';
					optional: true;
					default: 'question'];
		description: 'Copy file to directory';
		runBlock: [:fileEntry :directoryEntry |
			self inform: 'Copy file to directory'];
		yourself! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:49:40'!
informProcessStatusCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'show process status';
		commandCategory: 'process';
		argument: #process				type: Process;
			description: 'Show process status';
			runBlock: [:process | self inform: process browserPrintString ];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/29/2022 13:35:51'!
moveCodePackageCommand

	<command>
	
	^ PluggableCommand new
		commandName: 'move code package';
		commandCategory: 'code package';
		argument: #codePackage type: CodePackage description: 'Code package to move';
		argument: #directory					type: DirectoryEntry					description: 'Destination directory';
		description: 'Change code package file name to directory';
		runBlock: [:codePackage :directoryEntry |
			codePackage fullFileName: (directoryEntry // codePackage fullFileName asFileEntry name) asString];
		yourself! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/28/2022 11:41:37'!
moveFileCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'move file';
		commandCategory: 'files';
		argument: #file type: FileEntry description: 'File to move';
		argument: #directory					type: DirectoryEntry					description: 'Destination directory';
		argument: #ifExistsAction type: [:type | type choices: {'overwrite'.'deny'.'question'}]
			properties: [:arg |
				arg description: 'What to do if destination file exists';
					optional: true;
					default: 'question'];
		description: 'Move file to directory';
		runBlock: [:fileEntry :directoryEntry |
			self inform: 'Move file to directory'];
		yourself! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:39:57'!
reinstallCommandsSets
	<command>
	
	^ PluggableCommand new
		commandName: 'reinstall commands sets';
		commandCategory: 'debugging';
		description: 'Reinstall commands sets';
			runBlock: [CommandsSet initialize];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:50:58'!
resumeProcessCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'resume process';
		commandCategory: 'process';
		argument: #process type: Process;
			description: 'Resume process';
			runBlock: [:process | process resume];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:52:06'!
runProcessCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'run process';
		commandCategory: 'process';
		argument: #process				type: Process;
			description: 'Run process';
			runBlock: [:process | process run];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:52:22'!
suspendProcessCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'suspend process';
		commandCategory: 'process';
		argument: #process				type: Process;
			description: 'Suspend process';
			runBlock: [:process | process suspend];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:52:43'!
terminateProcessCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'terminate process';
		commandCategory: 'process';
		argument:#process				type: Process;
			description: 'Terminate process';
			runBlock: [:process | process terminate];
			yourself.
		! !

!StandardCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 09:53:00'!
viewFileEntryContentsCommand
	<command>
	
	^ PluggableCommand new
		commandName: 'view file contents';
		commandCategory: 'uncategorized';
		argument: #fileEntry				type: FileEntry;
			description: 'View file contents';
			runBlock: [:fileEntry | fileEntry textContents edit];
			yourself! !

!StandardCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 1/22/2022 15:02:32'!
deleteFile: aFileEntry
	
	"Delete file entry"
	
	<interactive>
	
	(self confirm: ('Delete file?: ', aFileEntry printString))
		ifTrue: [aFileEntry delete]! !

!StandardCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 9/21/2021 21:51:57'!
initialize
	CommandsSet initialize! !
StandardCommandsSet initialize!
