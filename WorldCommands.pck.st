'From Cuis 5.0 [latest update: #4528] on 18 February 2021 at 1:46:09 pm'!
'Description Commander World commands set.'!
!provides: 'WorldCommands' 1 9!
!requires: 'Commander' 1 104 nil!
SystemOrganization addCategory: 'WorldCommands'!


!classDefinition: #WorldCommandsSet category: 'WorldCommands'!
CommandsSet subclass: #WorldCommandsSet
	instanceVariableNames: 'worldMenu'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'WorldCommands'!
!classDefinition: 'WorldCommandsSet class' category: 'WorldCommands'!
WorldCommandsSet class
	instanceVariableNames: ''!


!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/18/2021 13:45:20'!
buildCommandFromMenuSpec: aMenuSpec prefixed: aString
					
	^ aMenuSpec isSequenceable 
		ifTrue: [
			aMenuSpec collect: [:item | self buildCommandFromMenuSpec: item prefixed: aString]]
		ifFalse: [ |target|
			target _ aMenuSpec at: #object 
					ifPresent: [:obj | obj isSymbol ifTrue: [worldMenu perform: obj] ifFalse: [obj]] 
					ifAbsent: [worldMenu].
			PluggableCommand new
				commandName: (aString , ': ' , (aMenuSpec at: #label));
				target: target;
				selector: ((aMenuSpec at: #selector) numArgs isZero ifTrue: [aMenuSpec at: #selector]); 
				runBlock: (((aMenuSpec at: #selector) numArgs > 0) ifTrue: [[target perform: (aMenuSpec at: #selector) withArguments: {nil}]]); 
				description: (aMenuSpec at: #balloonText ifAbsent: ['']);
				yourself]! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:27:12'!
buildCommandsFromDictionaries: aCollection prefixed: aString
	^ aCollection collect: [:aDictionary |
		self buildCommandFromMenuSpec: aDictionary prefixed: aString]! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:27:40'!
changesCommands
	^ self buildCommandsFromDictionaries:
		`{
			{
				#label 			-> 		'Change Sorter'.
				#object 			-> 		ChangeSorterWindow.
				#selector 		-> 		#openChangeSorter.
				#icon 			-> 		#halfRefreshIcon.
				#balloonText 	-> 		'Open a 3-paned changed-set viewing tool'.
			} asDictionary.
			{
				#label 			-> 		'Install New Updates'.
				#object 			-> 		ChangeSet.
				#selector 		-> 		#installNewUpdates.
				#icon 			-> 		#updateIcon.
				#balloonText 	-> 		'Install in the current image the new updates available
in directory named ./CoreUpdates
or in directory named ../Cuis-Smalltalk-Dev/CoreUpdates'.
			} asDictionary.
			
			{
				#label 			-> 		'Browse my Changes'.
				#object 			-> 		Smalltalk.
				#selector 		-> 		#browseMyChanges.
				#icon 			-> 		#editFindReplaceIcon.
				#balloonText 	-> 		'Browse all of my changes since the last time #condenseSources was run.'.
			} asDictionary.
			{
				#label 			-> 		'Recently logged Changes...'.
				#object 			-> 		ChangeList.
				#selector 		-> 		#browseRecentLog.
				#icon 			-> 		#clockIcon.
				#balloonText 	-> 		'Open a change-list browser on the latter part of the changes log.  You can use this browser to recover logged changes which were not saved in your image, in the event of a crash or other interruption.'.
			} asDictionary.
			
			{
				#label 			-> 		'Save World as morph file'.
				#selector 		-> 		#saveWorldInFile.
				#icon 			-> 		#morphsIcon.
				#balloonText 	-> 		'Save a file that, when reloaded, reconstitutes the current World.'.
			} asDictionary.
		}`
		prefixed: 'Changes'! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:23:13'!
collectCommandsForSelector: aSelector
	^((((Smalltalk allClassesImplementing: aSelector) select: [ :aClass | aClass isMeta ])
		collect: [:aClass |
			aClass soleInstance perform: aSelector])
		collect: [:aMenuSpec |
			self buildCommandFromMenuSpec: aMenuSpec prefixed: 'World']) flatten asOrderedCollection! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 22:50:42'!
collectCommandsFromWorldMenu
	^ (self collectCommandsForSelector: #worldMenuOptions),
	  (self collectCommandsForSelector: #worldMenuForOpenGroup)! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 15:32:34'!
commands
	^ self collectCommandsFromWorldMenu , 
	self changesCommands,
	self helpCommands,
	self preferencesCommands,
	self windowsCommands,
	self debugCommands! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:30:35'!
commandsSetName
	^ 'World Commands'! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:28:43'!
debugCommands
	^ self buildCommandsFromDictionaries:
		 `{
			{
				#label 			-> 		'Inspect World'.
				#object 			-> 		#myWorld.
				#selector 		-> 		#inspect.
				#icon 			-> 		#inspectIcon.
			} asDictionary.
			{
				#label 			-> 		'Explore World'.
				#object 			-> 		#myWorld.
				#selector 		-> 		#explore.
				#icon 			-> 		#exploreIcon.
			} asDictionary.
			{
				#label 			-> 		'MessageTally all Processes'.
				#selector 		-> 		#startMessageTally.
				#icon 			-> 		#systemMonitorIcon.
			} asDictionary.
			
			{
				#label 			-> 		'Start drawing all again'.
				#object 			-> 		#myWorld.
				#selector 		-> 		#removeAllKnownFailing.
				#icon 			-> 		#graphicsIcon.
			} asDictionary.
			{
				#label 			-> 		'Start stepping again'.
				#object 			-> 		#myWorld.
				#selector 		-> 		#resumeAfterStepError.
				#icon 			-> 		#mediaPlaybackStartIcon.
			} asDictionary.
			{
				#label 			-> 		'Close all Debuggers'.
				#object 			-> 		DebuggerWindow.
				#selector 		-> 		#closeAllDebuggers.
				#icon 			-> 		#closeIcon.
			} asDictionary.
		}`
		prefixed: 'Debug'! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:28:35'!
helpCommands
	^ self buildCommandsFromDictionaries:
		 `{
			{
				#label 			-> 		'About this System...'.
				#object 			-> 		Smalltalk.
				#selector 		-> 		#aboutThisSystem.
				#icon 			->			#helpIcon.
				#balloonText 	-> 		'current version information.'
			} asDictionary.
			{
				#label 			-> 		'Terse Guide to Cuis'.
				#selector 		-> 		#openTerseGuide.
				#icon 			->			#helpIcon.
				#balloonText 	-> 		'explore Cuis Smalltalk'
			} asDictionary.
			{
				#label 			-> 		'Class Comment Browser'.
				#selector 		-> 		#openCommentGuide.
				#icon 			->			#helpIcon.
				#balloonText 	-> 		'search & explore Cuis Class Comments'
			} asDictionary.
			{
				#label 			-> 		'Code management in Cuis'.
				#object 			-> 		Utilities.
				#selector 		-> 		#openCodeManagementInCuis.
				#icon 			->			#helpIcon.
				#balloonText 	-> 		'Features are kept in Packages.'
			} asDictionary.
			{
				#label 			-> 		'Using GitHub to host Cuis packages'.
				#object 			-> 		Utilities.
				#selector 		-> 		#openCuisAndGitHub.
				#icon 			->			#helpIcon.
				#balloonText 	-> 		'GitHub usage pattern.'
			} asDictionary.
			
			{
				#label 			-> 		'Editor keyboard shortcuts'.
				#object 			-> 		SmalltalkEditor.
				#selector 		-> 		#openHelp.
				#icon 			-> 		#keyboardShortcutsIcon.
				#balloonText 	-> 		'summary of keyboard shortcuts in editors for Smalltalk code.'
			} asDictionary.
			{
				#label 			-> 		'Useful Expressions'.
				#object 			-> 		Utilities.
				#selector 		-> 		#openUsefulExpressions.
				#icon 			-> 		#chatIcon.
				#balloonText 	-> 		'a window full of useful expressions.'
			} asDictionary.
			
			{
				#label 			-> 		'VM Statistics'.
				#selector 		-> 		#vmStatistics.
				#icon 			-> 		#systemMonitorIcon.
				#balloonText 	-> 		'obtain some intriguing data about the vm.'
			} asDictionary.
			{
				#label 			-> 		'Space Left'.
				#selector 		-> 		#garbageCollect.
				#icon 			-> 		#removableMediaIcon.
				#balloonText 	-> 		'perform a full garbage-collection and report how many bytes of space remain in the image.'
			} asDictionary.
		}`
	prefixed: 'Help'! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/14/2021 21:43:49'!
initialize
	 worldMenu _ TheWorldMenu new 
		world: self runningWorld
		hand: self activeHand! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:28:03'!
preferencesCommands
	^ self buildCommandsFromDictionaries:
		{
			{
				#label 			-> 		'Focus follows mouse'.
				#object 			-> 		Preferences.
				#selector 			-> 		#enableFocusFollowsMouse.
				#icon 			-> 		#windowIcon.
				#balloonText 	-> 		'At all times, make the active window and widget the one on which the mouse is located.'
			} asDictionary.
			{
				#label 			-> 		'Click to focus'.
				#object 			-> 		Preferences.
				#selector 			-> 		#disableFocusFollowsMouse.
				#icon 			-> 		#windowIcon.
				#balloonText 	-> 		'At all times, make the active window and widget the one where the mouse was clicked.'
			} asDictionary.
			{
				#label 			-> 		'Font Sizes...'.
				#object 			-> 		Theme.
				#selector 			-> 		#changeFontSizes.
				#icon 			-> 		#preferencesDesktopFontIcon.
				#balloonText 	-> 		'use larger or smaller text and widgets'
			} asDictionary.
			{
				#label			->		'Set System Font...'.
				#object 			-> 		FontFamily.
				#selector 			-> 		#promptUserAndSetDefault.
				#icon 			-> 		#preferencesDesktopFontIcon.
				#balloonText 	-> 		'change the current system font family.'
			} asDictionary.
			{
				#label			->		'Load all TrueType Fonts'.
				#object 			-> 		FontFamily.
				#selector 			-> 		#readAdditionalTrueTypeFonts.
				#icon 			-> 		#preferencesDesktopFontIcon.
				#balloonText 	-> 		'Load additional TrueType fonts included with Cuis.'
			} asDictionary.
			{
				#label 			-> 		'Icons...'.
				#object 			-> 		Theme.
				#selector 			-> 		#changeIcons.
				#icon 			-> 		#worldIcon.
				#balloonText 	-> 		'show more or less icons.'
			} asDictionary.
			{
				#label 			-> 		'Themes...'.
				#object 			-> 		Theme.
				#selector 			-> 		#changeTheme.
				#icon 			-> 		#appearanceIcon.
				#balloonText 	-> 		'switch to another theme.'
			} asDictionary.
			
			{
				#label 			-> 		'Show taskbar'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#showTaskbar.
				#icon 			-> 		#expandIcon.
				#balloonText 	-> 		'show the taskbar'
			} asDictionary.
			{
				#label 			-> 		'Hide taskbar'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#hideTaskbar.
				#icon 			-> 		#collapseIcon.
				#balloonText 	-> 		'hide the taskbar'
			} asDictionary.
			
			{
				#label 			-> 		'Full screen on'.
				#selector 			-> 		#fullScreenOn.
				#icon 			-> 		#viewFullscreenIcon.
				#balloonText 	-> 		'puts you in full-screen mode, if not already there.'
			} asDictionary.
			{
				#label 			-> 		'Full screen off'.
				#selector 			-> 		#fullScreenOff.
				#icon 			-> 		#exitFullscreenIcon.
				#balloonText 	-> 		'if in full-screen mode, takes you out of it.'
			} asDictionary.
			
			{
				#label 			-> 		'Set Code Author...'.
				#object 			-> 		Utilities.
				#selector 			-> 		#setAuthor.
				#icon 			-> 		#usersIcon.
				#balloonText 	-> 		'supply initials to be used to identify the author of code and other content.'
			} asDictionary.
			{
				#label 			-> 		'All preferences...'.
				#object 			-> 		Preferences.
				#selector 			-> 		#openPreferencesInspector.
				#icon 			-> 		#preferencesIcon.
				#balloonText 	-> 		'view and change various options.'
			} asDictionary.
		}
	prefixed: 'Preferences'! !

!WorldCommandsSet methodsFor: 'as yet unclassified' stamp: 'MM 2/16/2021 13:28:21'!
windowsCommands
	^ self buildCommandsFromDictionaries:  
		 `{
			{
				#label 			-> 		'Find Window'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findWindow:.
				#icon 			-> 		#windowIcon.
				#balloonText 	-> 		'Presents a list of all windows; if you choose one from the list, it becomes the active window.'
			} asDictionary.
			{
				#label 			-> 		'Find changed Browsers...'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findDirtyBrowsers:.
				#icon 			-> 		#editFindReplaceIcon.
				#balloonText 	-> 		'Presents a list of browsers that have unsubmitted changes; if you choose one from the list, it becomes the active window.'
			} asDictionary.
			{
				#label 			-> 		'Find changed Windows...'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findDirtyWindows:.
				#icon 			-> 		#newWindowIcon.
				#balloonText 	-> 		'Presents a list of all windows that have unsubmitted changes; if you choose one from the list, it becomes the active window.'
			} asDictionary.
			
			{
				#label 			-> 		'Find a Transcript'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findATranscript:.
				#icon 			-> 		#printerIcon.
				#balloonText 	-> 		'Brings an open Transcript to the front, creating one if necessary, and makes it the active window'
			} asDictionary.
			{
				#label 			-> 		'Find a FileList'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findAFileList:.
				#icon 			-> 		#systemFileManagerIcon.
				#balloonText 	-> 		'Brings an open fileList  to the front, creating one if necessary, and makes it the active window'
			} asDictionary.
			{
				#label 			-> 		'Find a Change Sorter'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findAChangeSorter:.
				#icon 			-> 		#changesIcon.
				#balloonText 	-> 		'Brings an open change sorter to the front, creating one if necessary, and makes it the active window'
			} asDictionary.
			{
				#label 			-> 		'Find Message Names'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#findAMessageNamesWindow:.
				#icon 			-> 		#inspectIcon.
				#balloonText 	-> 		'Brings an open MessageNames window to the front, creating one if necessary, and makes it the active window'
			} asDictionary.
			
			{
				#label 			-> 		'Tile open windows'.
				#object          ->       					TileResizeMorph.
				#selector 			-> 		#tileOpenWindows.
				#icon 			-> 		#windowIcon.
				#balloonText 	-> 		'Tile open windows'.
			} asDictionary.
			{
				#label 			-> 		'Collapse all Windows'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#collapseAll.
				#icon 			-> 		#collapseIcon.
				#balloonText 	-> 		'Reduce all open windows to collapsed forms that only show titles.'
			} asDictionary.
			{
				#label 			-> 		'Restore all Windows'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#restoreAll.
				#icon 			-> 		#expandIcon.
				#balloonText 	-> 		'Restore all collapsed windows back to their previous forms.'
			} asDictionary.
			{
				#label 			-> 		'Close top Window'.
				#object 			-> 		SystemWindow.
				#selector 			-> 		#closeTopWindow.
				#icon 			-> 		#closeIcon.
				#balloonText 	-> 		'Close the topmost window if possible.'
			} asDictionary.
			{
				#label 			-> 		'Send top Window to back'.
				#object 			-> 		SystemWindow.
				#selector 			-> 		#sendTopWindowToBack.
				#icon 			-> 		#goBottomIcon.
				#balloonText 	-> 		'Make the topmost window become the backmost one, and activate the window just beneath it.'
			} asDictionary.
			{
				#label 			-> 		'Move Windows onscreen'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#bringWindowsFullOnscreen.
				#icon 			-> 		#displayIcon.
				#balloonText 	-> 		'Make all windows fully visible on the screen'
			} asDictionary.
			
			{
				#label 			-> 		'Delete unchanged Windows'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#closeUnchangedWindows.
				#icon 			-> 		#warningIcon.
				#balloonText 	-> 		'Deletes all windows that do not have unsaved text edits.'
			} asDictionary.
			{
				#label 			-> 		'Delete non Windows'.
				#object 			-> 		#myWorld.
				#selector 			-> 		#deleteNonWindows.
				#icon 			-> 		#warningIcon.
				#balloonText 	-> 		'Deletes all non-window morphs lying on the world.'
			} asDictionary.
			{
				#label 			-> 		'Delete Both of the Above'.
				#selector 			-> 		#cleanUpWorld.
				#icon 			-> 		#warningIcon.
				#balloonText 	-> 		'Deletes all unchanged windows and also all non-window morphs lying on the world, other than flaps.'
			} asDictionary.
		}`
		prefixed: 'Windows'! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 2/4/2021 00:29:50'!
initialize
	"self initialize"
	self new register! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 1/3/2021 14:58:27'!
menu: titleString
	"Create a menu with the given title, ready for filling"

	| menu |
	(menu _ MenuMorph entitled: titleString) 
		defaultTarget: self; 
		addStayUpIcons.
	^ menu! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 2/1/2021 20:18:26'!
openCommandArea
	CommandAreaMorph new openInWorld! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 1/3/2021 14:56:21'!
openWorldCommandsMenu
	"Build and show the preferences menu for the world."

	self worldCommandsMenu popUpInWorld: self runningWorld! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 2/8/2021 11:54:20'!
worldCommandsMenu

	"Build the World commands menu"

	^ (self menu: 'Commands...')
		addItemsFromDictionaries: `{
			{
				#label 			-> 		'Redo'.
				#selector 		-> 		#redoLastCommand.
				#icon 			-> 		#redoIcon.
				#balloonText 	-> 		'Redo last command.'
			} asDictionary.
			{
				#label 			-> 		'Undo'.
				#selector 		-> 		#undoLastCommand.
				#icon 			-> 		#undoIcon.
				#balloonText 	-> 		'Undo last command.'
			} asDictionary.
			{
				#label 			-> 		'Run...'.
				#selector 		-> 		#openCommandArea.
				#icon 			-> 		#terminalIcon.
				#balloonText 	-> 		'Run a command.'
			} asDictionary.
			{
				#label 			-> 		'History...'.
				#selector 		-> 		#showCommandsHistory.
				#icon 			-> 		#inspectIcon.
				#balloonText 	-> 		'Show commands history.'
			} asDictionary.			
		}`! !

!WorldCommandsSet class methodsFor: 'as yet unclassified' stamp: 'MM 1/3/2021 14:55:23'!
worldMenuOptions

	^`{{
		#itemGroup 		-> 		10.
		#itemOrder 		-> 		50.
		#label 			-> 		'Commands...'.
		#object             ->           WorldCommandsSet.
		#selector 		-> 		#openWorldCommandsMenu.
		#icon 			-> 		#terminalIcon.
		#balloonText 	-> 		'Submenu to manage world commands'.
	} asDictionary.
	}`! !
WorldCommandsSet initialize!
