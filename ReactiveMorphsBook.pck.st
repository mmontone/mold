'From Cuis 6.0 [latest update: #5840] on 12 June 2023 at 4:11:03 pm'!
'Description '!
!provides: 'ReactiveMorphsBook' 1 22!
!requires: 'Erudite' 1 237 nil!
!requires: 'ReactiveMorphsWidgets' 1 1 nil!
!requires: 'ReactiveMorphs' 1 83 nil!
!requires: 'ReactiveMorphsExamples' 1 0 nil!
SystemOrganization addCategory: 'ReactiveMorphsBook'!


!classDefinition: #ReactiveMorphsBook category: 'ReactiveMorphsBook'!
EruditeBook subclass: #ReactiveMorphsBook
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ReactiveMorphsBook'!
!classDefinition: 'ReactiveMorphsBook class' category: 'ReactiveMorphsBook'!
ReactiveMorphsBook class
	instanceVariableNames: ''!


!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
initialize
    super initialize.
    title _ 'Reactive Morphs Book'.
        self addSection: self section_Introduction.
        self addSection: self section_ReactiveProgramming.
        self addSection: self section_ReactiveUserInterfaces.
        self addSection: self section_ReactiveMorphs.
! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_Introduction
^(EruditeBookSection basicNew title: 'Introduction'; document: ((EruditeDocument contents: '!!!! Introduction

ReactiveMorphs is an experimental library for building Morphic interfaces in a declarative way, using fine-grained reactivity.') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs
^(EruditeBookSection basicNew title: 'ReactiveMorphs'; document: ((EruditeDocument contents: '!!!! ReactiveMorphs
') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self section_ReactiveMorphs_ReactiveValues;
        add: self section_ReactiveMorphs_ReactiveCollections;
        add: self section_ReactiveMorphs_ConditionalComposition;
        add: self section_ReactiveMorphs_ReactiveChildren;
        add: self section_ReactiveMorphs_DynamicComposition;
        add: self section_ReactiveMorphs_ReactiveMorphProperties;
        add: self section_ReactiveMorphs_Widgets;
        add: self section_ReactiveMorphs_Examples;
 yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_ConditionalComposition
^(EruditeBookSection basicNew title: 'Conditional composition'; document: ((EruditeDocument contents: '!!!! Conditional composition

The conditional composition of Morphs is done via the instantiation of an {RxMorph::class} (dynamic component), attached to a reactive boolean value, and two blocks, one for the truth branch, and the other for the false branch, both returning the Morph to show for each case.

 {/home/marian/Escritorio/conditionalmorph.png  ::image}

__Example__:

Create a conditional morph on a boolean condition, using {ifIsTrue:ifIsFalse: ::selector}:

[[[condition _ RxValue with: false.

condition 
	ifIsTrue: [BoxedMorph new color: Color green; yourself]
	ifIsFalse: [BoxedMorph new color: Color red; yourself] 
]]]

Resulting morph:

[[[condition _ RxValue with: false.

condition 
	ifIsTrue: [BoxedMorph new color: Color green; yourself]
	ifIsFalse: [BoxedMorph new color: Color red; yourself]
 
]]] embedIt

Then update the value of the reactive condition and observe how the conditional morph is updated:

[[[condition value: false]]] doIt

[[[condition value: true]]] doIt

__Example__:

[[[cond2 _ RxValue with: false.

cond2 
	ifIsTrue: [ImageMorph new]
	ifIsFalse: [LabelMorph contents: ''Hello''] 
]]]

Resulting Morph:

[[[cond2 _ RxValue with: false.

cond2 
	ifIsTrue: [ImageMorph new]
	ifIsFalse: [LabelMorph contents: ''Hello''] 
]]] embedIt

Then update the value of the reactive condition and observe how the conditional morph is updated:

[[[cond2 value: false]]] doIt

[[[cond2 value: true]]] doIt

__Example:__

{ReactiveMorphsExamples class>>visibilityExample2 ::method}[embed]

[[[ReactiveMorphsExamples visibilityExample2]]] doIt
') data: ((Dictionary new) add: ('/home/marian/Escritorio/conditionalmorph.png  '->(EruditeForm fromBase64String:'iVBORw0KGgoAAAANSUhEUgAAAU0AAADXCAYAAACeegNVAAAaNElEQVR4XuXdB3iNZ//A8fOc
LLFbSlW1Stv07dS33i57U3sEjVWlaiRGiJhNiL2FCCFkIULEiIrZ2kKorf72npEgss/5/c85
bdoEIbLOyfle9/V9+14XjZ7c9/1xnuc+J0clIirhV6A7d/NR7ao/BQk/0+qHCREH+evT/OJ/
E8ygbcduDdNvUuclf/AzofRzwl+ffDT58dHkx0eTjyY/Ppr8+Gjy46PJR5MfH01+fDT58dHk
o8mPjyY/Ppp8NPnx0eTHR5OPJn998tHkx0czz5q5do3Yuy2SNm6rZaDvYT6a/Pho8uOjmVnz
NgRI06G+sih8nIzwmSVNXJeLo89BPpr8+Gjy46OZPhf/Q+K93lsaOC+RnUeG6x5OL0OLN4yT
uoOCpNfcfXw0+fHR5MdHU9+oZftl8nJPaT5skZy9NugfMNP67fAIqT0gQH6Y8TsfTX58NPmx
0Ry/arsMmTdXuk+cJ9EP+z0FZlpnrjhLoyF+8v3EjXw0+fHR5MdEc+baddJtwnwZ7TtDkpJ7
ZwpmWvceOIrDWJ8Cd0DER5OPJj8+mjnOKzxImrn6Gu5ZarW9XghmWnpcXefPlu+GBYtTATkg
4qPJR5MfH81sN8TvsO7AZ740HLxYth8akWUs06dH1mfdeKmnOyD62WsfH01+fDT5mSeaI5ZG
yrQQT8MzzNOXnbMFZvq2RI3UHRAFSvcZO/ho8uOjyc+80PQI+V2GLZgjXcd7y50YxxyDmdap
S7oDosF+4jApgo8mPz6a/MwDzelrw3Wn4946NGdKYhYOfF62OzFO0nHMQmnrHiaDFh/mo8mP
jya/goumV/gyaTl8kcxfO/6lDnxetoSk3jLYy1OaGg6Iovho8uOjya9goWk48An3MbzDZ9OB
kXmG5ZMHRF5hE6W+c5D09trPR5MfH01+BQPN4UEHZPrKOdLExVdOXBicL2Cmb2PkqL8OiGbu
5KPJj48mP9NGc0zIDhm5cI44jJ0vt+875TuYaR07P0T3LNdfOk3exEeTHx9NfqaJ5tSwX+Wn
KfNkyLxZ8jixj9HATOtmtJO0d1sk7casNeoBER9NPpr8+Gg+1Zz1wdJ65EKZGzpRNNpeRgcz
LT3eA+fMkWbDV0j/hVF8NPnx0eRnXDQH6/IOXyiNhiyW8L2jTQbL9OkRn71yku6AaKn0mbef
jyY/Ppr8jIPmsMADMnu1/sBnsfxx1sUkwUyfHvU6ugOiH2fu4qPJj48mv/xF033FLnFb4ikd
3BfI9bv9TR7MtPS419cdEHWevJmPJj8+mnw08wfNqasjpPf0eTLQ01MeJ/QpMGCmpUe+7Whf
aTc2fw6I+Gjy0eQHRnNOeIgOnIUyM2SSaDQ/Fzgw/zkg0mHvOMtLmo8IyfMDIj6afDT5AdE0
HPhs8DUc+ITt+qXAYpnhgEiH/vQVU6TBYN0BkXckH01+fDT5aOZOrgEHxTNsrgHMQ2dczALM
9On/EtAfEPWYtZuPJj8+mnw0c5bb8j0y1t9T2o32kat3BpgdmGnp/zKoN0h3QDRlKx9Nfnw0
+Whmr0mhm6XfTC9xnDlHHj7ua7ZgpqX/S6HVSF9p77EuVw+I+Gjy0eQHQHP2ulDp4OYjk5dN
kdQCfODzsj2K72t4ZUCLkSEyYNEhPpr8+Gjy0Xxx3huWGF6wvvI3dwyWTx4QTVo6TRoOWSp9
c+GAiI8mH01+Zorm0IAo8VrrZTjw2XfSFQlm+lb97mY4IOo5ew8fTX58NPloZmz0sr0yPsjT
8EM3Lt4ciAczrQOnh0rdgQHSZeo2Ppr8zBfNSUGR8/QLld/L939XB/GxfKJZq334ayMXajVi
zTk+miYa9W/2nF6ed5ywReoNCjDKT1s3xVI1vcVj6VL5pm+wdJu+k/9Mk78v+Wjy0Xy6H2bs
kppOQbIxcjQazLgEJ+k1Y5VUdwqRn7328e9p8vclH00+mpmn/1CyOgOXyZzVk/P0EyRN9vWa
91yl1S+rpa7zanHyOcg/PefvSz6afDRfnB6LRkNXyqC5c/Pks8pNtcgzE6Te4FXy3YhwGeh7
mP86Tf6+5KPJRzPr6dFoMXqdtHf3lTsxjmYP5oqdXlLdcYXYe2zhv42Svy/5aPLRzH7tx22W
Bs4Bcvqys5m+mL23TAwONBz4dJ2+g//ec/6+5KPJRzPn6U+Pa/UPkq1RI80KzPgkJ+kze6VU
0z3D7DV3H/+nHPH3JR9NPpq5lx6V2gOWic/6iWZxQHQjeqi0cVstdQaFimMuHPjw0eSjyZ8c
PppPpceloUuIDJ0/R5JSCu4B0aFz46T+kFXSZPj6XDvw4aPJR5M/OXw0Mz0gaj5qrXw/dpFE
P+xX4MAM3TNXqjutkHYem/kfrMbfl3w09WWcHM4/8/sjfO3HbZKGQ/zl7LWC8dZLjfZnmboy
wHDg02XaDv6nUeZz5vZ94E8OH81s1VWHj/6A6Pcjw00azIQkR+k35+8Dnzl7+Z97zt+XfDT5
aBoHTcMBkQ6h2gOWyuJfx5skmLdiXMR+zGqprTvw6bfgQL5/f/ho8tHkTw4fzafSY9RgyAoZ
tWi2JKeYzk96P3JhrDRwWSWNh63T3Ys9ZJTvDR9NPpr8yeGj+cz0Hw/RbOQa6TJ+ocQ8Mv4B
0Zq9nlLDKUTajNlktO8JH00+mvzJ4aP5wtqN3ShNhvrLhRsDjXbgM3O1n3yrO/DpPPV3o38/
+Gjy0eRPDh/NF9Z5yu+6A6JA2Xt8WL6CmZjcTwbMC5Fq/VZIT8+9JvG94KPJR5M/OXw0s1RP
zz06OJdK0GaPfAHzTuwQ6eChO/AZuEr6zT9gMt8HPpp8NPmTw0cz6wdEOrzqDw4W9yWz8vSj
gI9fdpdGQ0OlkevaXPvoXT6a/H3JR5OPptEOiJoOXy3dJ/nIg8d9cx3M8MhZUrN/iLR2jzC5
x85Hk48mf3L4aGa7NmN+laaufnLl9oBcwVKry3PtEsOBT6cpv5ns4+ajyUeTPzl8NLNdp8m/
Se0BgRJ5emiOwExK6SeDF4TIt/2CpUcOP5ecjyZ/X/LR5KNp0umR0x8Qrdg+JltgRj8cLN+P
Xy21BqySvt6RJv94+Wjy0eRPDh/NHKfHrp5zsEwImiGalzggOnXVTZq4hkrDoaZ34MNHk48m
f3L4aOZp/RdFyXfDQqXXtPnyKP7FB0QRh2bonl2GSCu3jTJo8eEC8zj5aPLR5E8OH81cS49f
G/cN0mLEErl2p3+mBz7e4b6GAx+HSdsL1OPjo8lHkz85fDTzJD2GdXQHRIfPumQAMzm1rwxd
uOLvA5/dBfKx8dHko8mfHD6aedKPs3YbDojCdrkZwIx55CydJ62Wmv1XSp8CcODDR5OPJn9y
+Gjme3oc6w5aLi7zvaWJ7n5nA5c1hnufBfkx8dHko8mfHD6aeXtAtDDK8HbIlr8UrAMfPpp8
NPmTw0eTHx9N/r7ko8lHkx8fTf6+5KPJR1PX8cuxrfSP3f+3iyH8TCc+mnw0+ZNj4o+dn2nl
MHbDH3w0+WjyJ4cfvwKVqIz9Tz6aJhwfTX78+PuSjyYfTX78+PuSjyZ/cvjx46PJR5M/Ofz4
8eOjyZ8cfvz48fclH01+/Pjx9yUfTT6a/Pjx9yUfTT6a/Pjx9yUfTSNOjua89fRq1utUKmv5
Zto5D42Ikv7XU69HlHNv89/ACiVtoi0tbVNKf/H+aWuVSqvSfauynHX1xzMvaP7H3zBm3Euu
o7KdV4U9iA18vUUh1bWsrqNCzf23xYhYZe+/Uavc92vWxkal0tg0XLDrllYK8dHko5kHaMaq
Qx1Ke6lViqhL2j2o3bJteLPWddZ9aGf3p12GKp0pU1h5pNL9vsJlK9+1e/LXP3Y44ndN8wkf
FyqaT66jduHtPbZ5xMUGljWgqVhrX33b7upT6+aJPu29xv+hiCUfTT6apotmyiHbkR9a7lQp
JTX2wTG9tU88e3jW16gx66KbJrPfx4+JZmbrKDawtAFNy/eThkamNMnb/0Y+mnw08wPN5H2F
Xd6z2KeyeDu5/47k1ln5Gi+NpjZJ/SguqQgfHjNGM7N1xEeTj6b5oJmohHYs4q1k9b5SltDU
Knd9m9jbqNSa0j02hsSdX1HJscY7ESWsLRPK9d6yLEk0Fhdn1eiuv19aqNXSLY+eugzTKFfn
1uny7F/XKDHHgj9071rf59O3Sl0oam2ZaFP89Yd21ez3uSza1/d2qtjyYTMGmi9YR7GB5XKE
puaeZZT/yPad63228e3SRW/bWFolFypR7oHdN20iBy3Y65Rx3p+HplZ5eHKVnUf3ht6fv/Pa
2WI2lgkW1kWSS1f8/HLD7mMDV554WPXpq6zsrzk+mmaJZopyYtnoVsNcukz76hX1dZW6hKZq
J5dQV1fXSSP9Dv+UIGKRnWeaj5a1blZIpWiK2E/eMrlWsTW25T69Xathw61tpkS6JYtGnT00
U5QLwd1a2dkq91VKEW3Fam0O/Ninz4Ie9rVXvFtcfU/R3S97295v3bkUKcbHLb/RfME6ig0s
k200tTct1/R4b7qNotIotuUTvmhsv7XLD938Ozb7cl2FwsoD/X3Sd3uuW3b7HxwzR/Nx5Jj/
flVcualSCmvfrPrdkY4/9PTv2a29X4NPyx4yfP1iXz5w3x/X+N8/P2drjo8m//I8y2gmhNg3
LqRbtBblK979uOG0TUfi5LX0KGYHzdSzsyvXLqZcVywrJnUMPOuYHvSUq2Hle7xvfVBRimvq
z7s4gn+v1Xwuz5MjXT+3s1Q9VGy/ihtzKK5B+l+LPzb5o5pFlRuK1ScJv/yRUv/5aMaog9uV
8FGUQtqvPY5Mic/whOCR+uiMhr1KqZWkot/5/nbz738np2uOjyYfzSyjmbiqfSNbRZWqsvxP
4vADyY2fRPHl0UxW9g55d5SFSq0tZR+84Z5WbJ66j7Wy43el1apEy0/dTh5NkVf4wJkJmjcO
l4kIC24ZuDqqdbRWrDM+C71tubCRzXKVykbqz78x7K9L60zQTD1tM6GqVYRKXS6l95ak9k/9
WclXbaK276lx5Py995JE1Lmx5vho8tF8aTTVb/S+tjVJXs8xmqmn/lrwumcJzfyiBz7rdF97
P+C1FoWVKyrrunFe1zRV+MCZIJq6y9lX3qx8o3Llyucy7z/nOgXe7PvMV3Bok9Vx926+evXy
pbcuXbxY8eLFM+94N7Ndqqis5MvJZyakPg9NeaAOdSjlpVappVSNYfvXnI79PPV5VyS5sOb4
aPLRfGk0rapOOHIqVYrnGM2kiKI9X1Of1C/g9+p32dmtWze/p+rSKOiDwrp7TxaVkp13J7fg
A2eCaGbpxe3WUmfu1ZH/fl2tEnsk4CPXtl8trVjCKlp55r+jQ3PSnxOfj6bukvpC0NsOlQv9
YfgaurVU5qO6f3YcMN5r0fqoVtfjtRnXaS6sOT6afDRfGk3ratMPntdI0RyjmbimeKfiyrks
bTp1mdSeEUnf84Ezj8vz+IMeVb42HN7YaN+s+eP+0bN8hywNCe0QFhbWKiwspLVrdZsIJYto
Gp4dxhwrETK+5+BGH5c5XEhRUv9aN4pYlPwwts3YjRMuJ/+9XnNhzfHR5KOZb2hemVO7a8Zn
mpuL9npdfTzT+1H8zBNN7S3LJc2KBSgqC3nTISTshubJl/fEq0PsbX1eBs306yzu2uFyEQFT
HZztqy4ra608VilW2nf7bvKP0d87zYU1x0eTj2auoXnZs1Y3A4rNA7bFPvUe4xTliNunzpYZ
7mmesZn8ldWv+kulJr53Bmv5p+MMNP+Bq3xKn21J9k/fdzxmM6aK1VZVttDMuCbvbupT5339
KX2RlrGB0Vq73FhzfDT5aOYSmlol2q9pm0KKSmNdbcbBCxrJ+C4hzRUrr7q2K/X3nf59Jpqi
RLraDbdUKdrizZZsu/msjZB4yjYieFProzfjK/BRNRc0NxTtXkp9WmVROdl5T3LzJ9dR7Obe
tSpZqOL0aP5v4ulJz0NTG32i5MbAOR1nhJ7qkGI4HX/yz/q16I+l1adU1t/ETzun+To31hwf
TT6auYSm7ob8weFVPrBUPVCKNYpZcCX9qWOqcjWkc/N3ba1jFCU9mvrXzHlWrlNMuaayKJ/S
ct6xYRku65Mu26zt+8lEW0WdWrL5ki3XNfx3BpkFmpqz1tO+tV6v6C6bPxt50PPf11ZqlPsH
Zn7erNLHR2p+XeaAWnf5XqHf9oC/Xir0bDQ1l7zerldEua4uWeu+x777jTOu3VTlxprujfUA
q9/odW1TvJTPjTXHR5OPZq6hqX/N3IwaxdYouhvwReyanx84fpbb3NmTBrh2/sqvXNn6V6dM
aPdLMUWVUqhl0NYH/1y+pygXlnf9+90ZNtrXqzQ60bF7T/8fHZou/bJCkfP6r2X9TsdLQRdS
P+PjZi4HQRrlRnD7FuXUqgTF4tWUT5r32Ozk2HNuh7p2m0talY6vPTXK4+Liph2L6F/eVvyj
hy17D1q88GBizWdfnj9WR02s7vSKWpWkWJRIrfxN04Ptu3YP6t61g3/TbyvtKmGhJClWFZLs
Ay46/ftSpJytOT6afDQzfI0coalLc2d3ac+f6s/+6I3iV2wsLVNsX33n/pdtR6xdeSb+88TN
vWqU1W0Umya+O+6mf1GxaP56H3C3Bj6fvVXqQhFri0RL21cSKnxS56TDCP+pUfc05f/6ffzy
rHRrIH9+YMcj9fEg57bNqrwZWdzGMsGycOn4d7/tEDkq9ExnwzPPuKhis1p/4PeqjfXjIq9X
ue26PaFV5vc049XnNkyt36fFN6s+eKPkFVsri2T9e89LVfzsSr3OI4L9Iu/UevrSXZPtNcdH
04Tj/+R2fvz4+5KPJh9Nfvz4+5KPJn9y+PHjo8lHkz85/Pjx46PJnxx+/Pjx9yUfTX78+PH3
JR9NPpr8+PH3JR9NPpr8+PH3JR9N/uTw48dHk48mf3L48ePHR5M/Ofz48ePvSz6a/Pjx4+9L
Ppp8NPnx4+9LPpp8NPnx4+9LPpr8yeHHj48mH03+5PDjx4+PJn9y+PHjx9+XfDT58ePH35d8
NPlo8uPH35d8NPlo8uPH35d8NPmTw48fH00+mvzJ4cePHx9N/uTw48ePvy/5aPLjxy+fyrN9
aaSM9gfnReY2Ofz48fel6cWfHH78+PH3JR9Nfvz48fclH81/unrnUWX9RGSldqPXneYvXH78
8r5dR681y+q+dPHeEcpHM5/rMm7jwaxMzvw1R8fyFzQ/fnlfcorGus6AkJis7MtNBy515KOZ
z/lHnByalck5dy3mY/6C5scvf3JfvNfvRXuyWt/l8Y8TU4ry0cznrt999M6LJsf+l/Wn+AuZ
H7/8a/ex69+9aF8O9d65in9P00h1Hb/xwPMmZ8G6o+78hcyPX/5eotcdEHL/eftyy8FL7flo
GqnATaeGPG9yzl+P/Yi/kPnxy9/G+O1bnNmerN4v+HF8YkoRPppG6sa9uIr/6xWkfdbktHdb
f4K/gPnxy//2HL/eJDM0XefvXMl/yZGR6zZhY+SzJsdn3TE3/gLmxy//S0nVWNUdGBL9zEvz
qMv2fDSNfYm++dTgZ03OhRux/+EvYH78jNNYv32+T+7JGo7BcQlJKYX5aBq5m9Fxbz15id7B
Pfw4f+Hy42e89p640ehJNIfN3xnCf0eQifTDhIj96SdnYfix0fyFy4+fcS/R6w1ceS/9vtx2
6HJbPpom0tItpweln5yLNx98wF+4/PgZt3H++xem7cmajsGPzOHS3GzQvBX9uELaJfr3Y8KP
8hcsP37Gb9/JGw3T0BzusyvYXB6X2UxQ94kRe/WTsyj8+Cj+guXHz/ilarSW9QetvKvfl9sP
XWnDR9PEWrbl9ED95Fy+9eB9/oLlx89ELtED9vvUdFrxMDE51ZaPpol1+/7jNzt5bDjMX6j8
+JlOkadu1h+5cPcyc3pMqjiJe/Wu3K1kDm3984yDuTyWZEkuxN904KKjy8i1a5UKeqlXrr53
eO/JDubwWAxpNGrVT4HBf2T1B4fyy78au4bc4uPBzdVx3mX+PjC9HNzXHzP8KPpe/GFyg//R
HewM8//FF/xMLP288NHko8mPjyY/Ppp8NPnx0eTHR5OPJiDhZ4iPJh9N/uCjyY+PJh9N/uCj
yY+PJh9N/uCjyY+PJj8+mnw0+fHR5MdHk48mPz6a/Pho8gcfTX58NPlo8gcfTX58NPlo8gcf
TX58NPlo8gcfTX58NPnx0eSjyY+PJj8+mnw0+fHR5MdHkz/4aPLjo8lHkz/4aPLjo8lHkz/4
aPLjo8lHkz/4aPLjo8mPjyYfTX58NPnx0eSjyY+PJj8+mvzBR5MfH00+mvzBR5MfH00+mvzB
R5MfH00+mvzBR5MfH01+fDT5aPLjo8mPjyYfTX58NPnx0eQPPpr8+GiaZvp5MUxOr7wd/P/N
xtDPCx8PPpr8TCv9vPDR5KPJj48mPz6afDT58dHkx0eTjyY/Ppr8+GjyBx9Nfnw0+WjyBx9N
fnw0+WjyBx9Nfnw0+Wjy0eSjyY+PJj8+mnw0+fHR5MdHk48mPz6a/Pho8gcfTX58NPlo8gcf
TX58NPlo8gcfTX58NPlo8tHko8mPjyY/Ppp8NPnx0eTHR5OPJj8+mvz4aPIHH01+fDT5aPIH
H01+fDT5aPIHH01+fDT5aPLR5KPJj48mPz6afDT58dHkx0eTjyY/Ppr8+GjyBx9Nfnw0+Wjy
Bx9Nfnw0+WjyBx9NfjlI9TLx0eSjyR/CR5MfH00+mvzBR5MfH00+mvzBR5MfH01+fDT5aPLj
o8mPjyYfTX58NPnx0eQPPpp8NPlo8tHkDz6a/Pho8tHkDz6a/Pho8tHkDz6a/Pho8uOjyUeT
Hx9Nfnw0+Wjy46PJj48mf/DR5KPJR5OPJn/w0eTHR5OPJn/w0eTHR5OPJn/w0eTHR5MfH00+
mvz4aPLjo8lHkx8fTX75jOas0Khp+v/Dz7RqNWLNeT4e3Mb67vbn7wPTq/vEiH3/D9keTAWX
A4hHAAAAAElFTkSuQmCC')); yourself); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_DynamicComposition
^(EruditeBookSection basicNew title: 'Dynamic composition'; document: ((EruditeDocument contents: '!!!! Dynamic components

[[[labelSymbol _ RxValue with: #hello]]]

[[[labelSymbol reactiveMorph: [:lbl |
	lbl == #hello 
		ifTrue: [LabelMorph contents: ''hello world'']
		ifFalse: [LabelMorph contents: ''bye cruel world'']]]]]

[[[labelSymbol reactiveMorph: [:lbl |
	lbl == #hello 
		ifTrue: [LabelMorph contents: ''hello world'']
		ifFalse: [LabelMorph contents: ''bye cruel world'']]
]]] embedIt

[[[labelSymbol value: #bye]]] doIt
[[[labelSymbol value: #hello]]] doIt
') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_Examples
^(EruditeBookSection basicNew title: 'Examples'; document: ((EruditeDocument contents: '!!!! Reactive Morphs Examples

!!!!!! RxTreeMorph

[[[PluggableScrollPane new
	scroller: (RxTreeMorph on: self runningWorld);
	openInWindow]]] doIt

See: {RxTreeMorph ::class}') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new        add: self section_ReactiveMorphs_Examples_MorphExplorerTool;
 yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_Examples_MorphExplorerTool
^(EruditeBookSection basicNew title: 'Morph explorer tool'; document: ((EruditeDocument contents: '!!!! Morph explorer tool

{RxMorphExplorerMorph::class} uses an {RxTreeMorph::class} widget to build an explorer tool for Morphs.

[[[RxMorphExplorerMorph openOn: self runningWorld]]] doIt') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/6/2023 16:04:26'!
section_ReactiveMorphs_IterativeComposition
^(EruditeBookSection basicNew title: 'Iterative composition'; document: ((EruditeDocument contents: '') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_ReactiveChildren
^(EruditeBookSection basicNew title: 'Reactive children'; document: ((EruditeDocument contents: '!!!! Reactive children

The other gui composition construct is the reactive update of the children of a Morph.

{/home/marian/Escritorio/reactivechildren.png ::image}

The red squares represent a reactive collection.
The blue square is a Morph with its children (yellow) being updated from the reactive collection.

When the reactive collection changes, new Morphs are produced from it, and the list of children of the container Morph is updated to reflect those changes.

__Example:__

A TODO list Morph. The TODO items in the user interface are updated whenever a reactive collection with the TODO items changes.

{ReactiveMorphsExamples class>>todoListExample ::method}[embed]

[[[ReactiveMorphsExamples todoListExample]]] embedIt') data: ((Dictionary new) add: ('/home/marian/Escritorio/reactivechildren.png '->(EruditeForm fromBase64String:'iVBORw0KGgoAAAANSUhEUgAAAK4AAADbCAYAAAD046qcAAAKsUlEQVR4XuWdfUxWZRiHER2E
qPlGmKUEkt8VfeBqSR9gVDIt5DPU5UcofWgEOCX31jJ1iFY6YzFQBDMz0kClLZda5ChnC0Jy
Zpqf6JY2xZlaOj9OPm+DZcjL2zqv57nP9cc1cLLD/d7n4nl/53mf5xyfXUdODR8yeaXBh48U
8j78rthHfZNTVs+HjxiUs3xx+fDF5cOHLy4fPnxx+fDF5cOHLy4fPnxx+fDF5cOHLy4fPnxx
+fDhi8uHLy4fPnxx+fCxUtzR6RGGj48PHyhTCmtkiquKNww+RL78caLr/PPF5cMXly8uH764
fHH58MXlwxeXLy4fvrh8cfnwxeXDF5cvLh++uHxx+fDF5YvLF5cvLh++uHxx+fDF5YvLhy8u
H764fHH58MXli8uHLy4fvrh8cfnwxeWLy4cvLl9cPnxx+fDFvd7ihgwcwt+mDeahUS/ybwjC
hw9fXD58cd1R9EURHyh5FVVyxQ0L4+c8Muoahz+rwIc/q8AXlw9fXL64fPji8uGLyxeXD19c
vrh8+OLy4YvLF5cPX1y+uHz44vLF5YvLF5cPX1y+uHz44vLF5cMXlw9fXL64fPji8sXlwxeX
D19cvrh8+OLyxeXDF5cvLh++uHz44l5vcYNuCeLvduXv8uXfEIQPH764fPji8uFjK3Gjo/k5
j3+3Rv6sAh/+rAJfXD58cfni8uGLy4cvLl9cPnxx+eLy4YvLhy8uX1w+fHH54vLhi8sXly8u
X1w+fHH54vLhi8sXlw9fXD58cfni8uGLyxeXD19cPnxx+eLy4YvLF5cPX1y+uHxx+eLy4Yt7
/cVVL4APjyUb35cr7gMjJvK3aYOZUljDvyEIHz58cfmIRjnbWtyyetO+JqYG898ywaS+VmK6
UwrlrFfF5c8q8GcV+OLy4YvLF5cPX1y+uHz44vLhi8sXlw9fXL64fPji8uGLyxeXD19cvrh8
+OLyxeWLyxeXD19cvrh8+OLyxeXDF5cPX1y+uHz44vLF5cMXlw9fXL64fPji8sXlwxeXLy4f
8eIePj6ID5DKbblyxR3w4FP8bdpg4jMXyRSXDx++uHz42E3cl977SruaXi74Wste6VoXTty4
jLnGyClv69fcuPFGxsIvtKur/5BYvriWj7QF1UZAl+5G7DinXrWV/mB0DeppxIyZoVVd6raf
6qJpVNZ7fHGtZPDQEa4TEZX4slZ1jX5jhauuXv3u06quNOdyV109Qge5/rj44lpA0rTClmmX
+58cq1VtkcOfc9XVoYOvVnEhdvzrXp+q4ovrhleKtho3BvdqOQmDo0ZqFxOaa9MpLtz7eFpL
XcEh/cWOumLFbR7Rmgm7e6h2MaEZneJC7wGRV9Wm40WtbcUd++Yqo4Ov71UnoGf4Xdr+UekU
F24IvPGq2m7u3c/ILq3ji+ttspbVXrmwGNjqo8XuPXprExO6Bd3aqj4d4oL647nWx7IjXszn
i+ttHkl59ZrN9w/oomVM0CkuJOa8f83abrqtj7hRV5S4z8//zOjk59/mgo6sku+1iwk6xYVH
U7Pa7J36EEekuAurdtfqzLvrf67tf8/QU+5WIr31wbcNVtfo6HHb+bbqS5jsbLSyviEx8cfb
qi24V9if76z9qU53D1peixL3u59+jVXf6Eyfx15odwndncnzLa1xUPxbbuvr0nOApfV1Dgp1
W5/qse4eNJNdUF2l/VL6Y8eO9QgKCjrenrjV1dXRVtY5bdq0d9zV5+vre+nIkSO9rKjtwoUL
nfz9/c+5qy88PHyf+jkpWyy0L3DMmDEfebJouaKiItGqGi9fvtwhNDT0YHs1Ll68ONOK+nbu
3DnYkx4uXbp0El9ck0bbYcOGfRkQEPBHe01fsmTJZKvq3LZt24OeiBEVFfWNFfWVl5c/60l9
YWFhB86fP+/HF9ckVDNramoenj179htKZD8/v1YXQXl5eTN1jQlWxwWn0zm3rZrUoBAREdGQ
nJy8ZubMmXmHDx/uzRfXW0VfKXvLli2Pzpo1682YmJivVPNzcnLetaqedevWxRcXF2d4wr59
+8Kvd32JiYkVffv2/SUuLu7z7OzshYWFhS9t3rz58UOHDt2uYo5IByQW3dTU5Pjnv8+dO+ff
2NgYoludsbGxm3Sooy05/91Hvrh8Wt4Z+PXxxRXX7E2bNsXqXJ/D4WhSXyXi5j8NLZHcbD7m
wW+CF9El47rDiotFnLhHjx695ezZs53FNNfVXn6NeHHT09NL1FSOlHp1z7gK9VEvX1wvEx8f
v059fMmPIXz4TYBn3Nra2ki+uF5k9+7d/XX8kIGfcfniumXChAllRUVFL0hqroSMGxkZWcsX
14tkZmYu3rNnTz9+BOHDz7j8jCvmnUGkuGq9a11d3f3imiskP0rMuSKKHDdu3AdWLhS3+0gm
5Z1BnLiLFi3KOnjwYCg/fvDhZ1z+FXsL9fX19+7du/cOvrgmv91u2LBhuMhRQUh2nDFjxvx5
8+a9xhcXPn8r7VOp9evXP7NixYrn+OKaiNrPpfZG8aMHH37G5WfcVqiNnHxxTaKqqurpkpKS
dLGjgqD5UWlzufz1t/yM6yIjI6OYL66JJ17teuDHDj78jMvPuG2Sn5+fyxf3f7Jq1arRc+bM
eV30qCAtN0rK5Pz5W37GbSY3NzefL64JO3pPnjzZnR85+IgR99KlS778jMsfccWJu2zZsuen
Tp1aIH5U4GdclriTJk1aKnH9rfSMy59VMCEqSLkzNh++uC7ULZbOnDkTyM+4/JwrStzS0tKJ
6oElthgVJO7lkrJPTreC1O3xpc/fNiPxTohSVonx8xIffsb9v6hnEuzatWugXZor8U6IajXe
1q1bH+KL+x9Q20dSU1M/sc2oIDDjqqcXLViwYDpf3P+Yr1auXDnWLuJKzLgNDQ0R27dvv4cv
Lh8+dhZXLarZuHHjE3ZqrtS7faekpKzmi+shZWVlE9LS0j621agg9d6zEu7rq0sha9euHaU2
R9pJXKlPtFm9enUKX1w+fOwqrnpitx1Wg9kl465Zsya5oKBgKl9cD9bf2mV9gh0ybnl5+bMJ
CQmVfHHboaam5mH1GH+7iSv16eQnTpy4aceOHXfxxeXDx27iHjhwIMzpdM61Y3OlZlwJa4kt
L2D58uXj7TZ/Kz3jKnSv3ZMf8jEM731Vj4CS+GAST5A6j6vQfb+cjx2F4WN/LP3l6obNSUlJ
n9q1uQ6Ho0ly/TpndEt/ud3W37Zqrqu9/PptJ66a59y/f38fu4ordR5XQkbn5yU+/IxrpwzF
z7h/P9GzsrIygS+uzTKg3V/f9OnTF+j6/DN+BuS/vjY5ffp0l4sXL3bki8uHD19cPnxx/0HJ
yJGfqWxmBs9063bKjOMMvMLgzp3PmnGsUSa9NsXorl1/N+tYSSb2KjIw8IxZvQrs2PGiGcfq
GxLS6FVxXRcUJmHasSIiGkw7VnR0tY9ur4/SK764fHH54vLF5YvLF5cvLr9XfHH54vLF5YvL
F5cvLv9k8MXli8sXly8uX1y+uHxx+SeDLy5fXL64/F7xxeWLyxeXLy5fXL64fHH5veKLyxeX
Ly5fXL64fHH5J4MvLl9cvrh8cU0Td2xq6idmbUdxmLS1pZPaQhIQ8IcZx7rBxK073Uyqyexe
dTVp647qVUeTtu4EBwf/Zqa4fwFcAMm1s6Z7BAAAAABJRU5ErkJggg==')); yourself); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_ReactiveCollections
^(EruditeBookSection basicNew title: 'Reactive collections'; document: ((EruditeDocument contents: '!!!! Reactive collections') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_ReactiveMorphProperties
^(EruditeBookSection basicNew title: 'Reactive Morph properties'; document: ((EruditeDocument contents: '!!!! Reactive Morph properties

Properties of Morphs can be made reactive using {rxProp:is: ::selector}

__Example__:

{ReactiveMorphsExamples class>>visibilityExample3 ::method}[embed]

[[[ReactiveMorphsExamples visibilityExample3]]] doIt.

[[[| visible |
	
	visible := RxValue with: true.
	
	LayoutMorph newColumn children: {
		(LabelMorph contents: ''Hello!!!!'')
			rxProp: #visible: is: visible;
			yourself.
		PluggableButtonMorph label: ''Toggle visibility''
			action: [visible value: visible value not]
	} ]]] embedIt') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_ReactiveValues
^(EruditeBookSection basicNew title: 'Reactive values'; document: ((EruditeDocument contents: '!!!! Reactive values

!!!!!! Values

[[[val _ RxValue with: ''hello'']]]

[[[val value]]] printItHere

[[[val value: ''bye'']]]

[[[val value]]] printItHere

!!!!!! Formulas

[[[x _ RxValue with: 0]]]

[[[fr _ x formula: [x value * 100]]]]

[[[fr value]]] printItHere

!!!!!! Memos


!!!!!! Properties') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveMorphs_Widgets
^(EruditeBookSection basicNew title: 'Widgets'; document: ((EruditeDocument contents: '!!!! Reactive Widgets') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveProgramming
^(EruditeBookSection basicNew title: 'Reactive programming'; document: ((EruditeDocument contents: '!!!! Reactive programming

Reactive programming is a declarative programming paradigm concerned with data streams and the propagation of change. With this paradigm, it''s possible to express static (e.g., arrays) or dynamic (e.g., event emitters) data streams with ease, and also communicate that an inferred dependency within the associated execution model exists, which facilitates the automatic propagation of the changed data flow.

In Smalltalk and in Cuis in particular, this is realized via the Object events protocol:

{Object>>when:evaluate: ::method}
{Object>>when:send:to ::method}
{Object>>triggerEvent: ::method}

!!!!!! Similarities with observer pattern

Reactive programming has principal similarities with the observer pattern commonly used in object-oriented programming. However, integrating the data flow concepts into the programming language would make it easier to express them and could therefore increase the granularity of the data flow graph. For example, the observer pattern commonly describes data-flows between whole objects/classes, whereas object-oriented reactive programming could target the members of objects/classes. ') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !

!ReactiveMorphsBook methodsFor: 'as yet unclassified' stamp: 'MM 6/12/2023 16:10:59'!
section_ReactiveUserInterfaces
^(EruditeBookSection basicNew title: 'Reactive user interfaces'; document: ((EruditeDocument contents: '!!!! Reactive user interfaces

') data: ((Dictionary new)); yourself); subsections: (OrderedCollection new yourself); yourself)! !
