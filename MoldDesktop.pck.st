'From Cuis7.1 [latest update: #6409] on 27 May 2024 at 1:04:47 pm'!
'Description Cuis desktop with meta properties.'!
!provides: 'MoldDesktop' 1 40!
!requires: 'Protocols' 1 1 nil!
!requires: 'Morphic-Widgets-Extras2' 1 117 nil!
!requires: 'Morphic-Layouts-Table' 1 6 nil!
!requires: 'Commander' 1 133 nil!
!requires: 'Props' 1 73 nil!
SystemOrganization addCategory: #MoldDesktop!


!classDefinition: #MoldDesktopHierarchicalList category: #MoldDesktop!
AbstractHierarchicalList subclass: #MoldDesktopHierarchicalList
	instanceVariableNames: 'model'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopHierarchicalList class' category: #MoldDesktop!
MoldDesktopHierarchicalList class
	instanceVariableNames: ''!

!classDefinition: #MoldView category: #MoldDesktop!
LayoutMorph subclass: #MoldView
	instanceVariableNames: 'model viewManager'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldView class' category: #MoldDesktop!
MoldView class
	instanceVariableNames: ''!

!classDefinition: #ListMoldView category: #MoldDesktop!
MoldView subclass: #ListMoldView
	instanceVariableNames: 'listIndex listMorph'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'ListMoldView class' category: #MoldDesktop!
ListMoldView class
	instanceVariableNames: ''!

!classDefinition: #StandardMoldView category: #MoldDesktop!
MoldView subclass: #StandardMoldView
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'StandardMoldView class' category: #MoldDesktop!
StandardMoldView class
	instanceVariableNames: ''!

!classDefinition: #StringMoldView category: #MoldDesktop!
MoldView subclass: #StringMoldView
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'StringMoldView class' category: #MoldDesktop!
StringMoldView class
	instanceVariableNames: ''!

!classDefinition: #TableMoldView category: #MoldDesktop!
MoldView subclass: #TableMoldView
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'TableMoldView class' category: #MoldDesktop!
TableMoldView class
	instanceVariableNames: ''!

!classDefinition: #TreeMoldView category: #MoldDesktop!
MoldView subclass: #TreeMoldView
	instanceVariableNames: 'treeMorph selectedItem'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'TreeMoldView class' category: #MoldDesktop!
TreeMoldView class
	instanceVariableNames: ''!

!classDefinition: #MoldHaloMorph category: #MoldDesktop!
HaloMorph subclass: #MoldHaloMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldHaloMorph class' category: #MoldDesktop!
MoldHaloMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopInnerListMorph category: #MoldDesktop!
InnerListMorph subclass: #MoldDesktopInnerListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopInnerListMorph class' category: #MoldDesktop!
MoldDesktopInnerListMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopTreeItemMorph category: #MoldDesktop!
IndentingListItemMorph subclass: #MoldDesktopTreeItemMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopTreeItemMorph class' category: #MoldDesktop!
MoldDesktopTreeItemMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopDraggingMorph category: #MoldDesktop!
LabelMorph subclass: #MoldDesktopDraggingMorph
	instanceVariableNames: 'model'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopDraggingMorph class' category: #MoldDesktop!
MoldDesktopDraggingMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopTreeMorph category: #MoldDesktop!
HierarchicalListMorph subclass: #MoldDesktopTreeMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopTreeMorph class' category: #MoldDesktop!
MoldDesktopTreeMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopListMorph category: #MoldDesktop!
PluggableListMorph subclass: #MoldDesktopListMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopListMorph class' category: #MoldDesktop!
MoldDesktopListMorph class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopListItemWrapper category: #MoldDesktop!
ListItemWrapper subclass: #MoldDesktopListItemWrapper
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopListItemWrapper class' category: #MoldDesktop!
MoldDesktopListItemWrapper class
	instanceVariableNames: ''!

!classDefinition: #MoldDesktopServices category: #MoldDesktop!
Object subclass: #MoldDesktopServices
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldDesktopServices class' category: #MoldDesktop!
MoldDesktopServices class
	instanceVariableNames: ''!

!classDefinition: #MoldViewManager category: #MoldDesktop!
Object subclass: #MoldViewManager
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'MoldViewManager class' category: #MoldDesktop!
MoldViewManager class
	instanceVariableNames: ''!

!classDefinition: #WorldMoldViewManager category: #MoldDesktop!
MoldViewManager subclass: #WorldMoldViewManager
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MoldDesktop'!
!classDefinition: 'WorldMoldViewManager class' category: #MoldDesktop!
WorldMoldViewManager class
	instanceVariableNames: ''!


!MoldView commentStamp: 'MM 3/28/2024 20:43:07' prior: 0!
MoldView openForObject: 22.

MoldView openForObject: (OrderedCollection with: 1 with: 'foo' with: false).

Dictionary newFromPairs: {'Red' . Color red . 'Blue' . Color blue . 'Green' . Color green}
	:: openInWorld.

!

!TableMoldView commentStamp: 'MM 3/28/2024 16:20:10' prior: 0!
|data|

data _ Dictionary new.
data at: 'foo' put: Object.
data at: 22 put: (OrderedCollection with: 22).
data at: false put: $a.

(TableMoldView on: data) openInWorld!

!TreeMoldView commentStamp: 'MM 3/30/2024 21:11:22' prior: 0!
(TreeMoldView on: Collection allSubclasses) openInWorld.!

!WorldMoldViewManager commentStamp: '<historical>' prior: 0!
Open new objects in World!

!MoldDesktopHierarchicalList methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:02:17'!
getList

	^Array with: (MoldDesktopListItemWrapper with: model)! !

!MoldDesktopHierarchicalList methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:03:01'!
initialize: anObject

	model := anObject! !

!MoldDesktopHierarchicalList class methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:02:50'!
on: anObject

	^ self new initialize: anObject! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 11:01:43'!
acceptDroppingMorph: aMorph event: evt

	"A MoldDesktop object has been dropped.
	
	Display a Menu with a list of commands that can be applied to our model and the dropped model.
	We have alternatives:
	- Build the list from methods in our model protocol that can be potentially applied (no type information).
	- Build the list from methods in our model protocol that can be applied (requires LiveTyping Cuis distribution with type information).
	- A list of explicit Commands that can be applied to those models (Commander package).
	- A variation of all of the above.
	
	Current implementation lists methods in model protocol with correct arity, and without using type information.
	"
	
	| selected |
	
	aMorph hide; delete.
	self world ifNotNil: [ :w | w activeHand removeMorph: aMorph ].
	
	selected := MoldDesktopServices openMenuWithApplicableCommandsFor: self model with: aMorph model. 
	
	selected ifNotNil: [
		self model perform: selected with: aMorph model.
		
		"We don't have a universal change tracking events for every object. So we update the receiving morph just in case here for now"
		self updateView]! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 18:18:49'!
addHalo: aMorphicEventOrNil

	| hand position |
	aMorphicEventOrNil
		ifNil: [
			hand := self world activeHand.
			hand ifNil: [ hand := self world firstHand ]. 
			position := hand lastMouseEvent eventPosition ]
		ifNotNil: [
			hand := aMorphicEventOrNil hand.
			position := aMorphicEventOrNil eventPosition ].

	MoldHaloMorph new popUpFor: self handPosition: position hand: hand! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/29/2024 10:31:42'!
addHandlesTo: aHaloMorph box: box
	"Add halo handles to the halo.  Apply the halo filter if appropriate"

	self haloSpecifications do: [ :aSpec |
		(self
			wantsHaloHandleWithSelector: aSpec addHandleSelector
			inHalo: aHaloMorph) ifTrue: [
		aHaloMorph
			perform: aSpec addHandleSelector
			with: aSpec ]].
	aHaloMorph target
		addOptionalHandlesTo: aHaloMorph
		box: box! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 13:29:28'!
allowsMorphDrop
	^ true! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 12:50:31'!
haloSpecifications

	" the locations, colors, icons, and selectors of the halo handles "

	^ self haloSpecsFromArray:
	#(	"selector					horiz			vert			color info				icon key"
	(addDismissHandle:						left			top			(red)				haloDismissIcon 					'Remove')
	(addMenuHandle:						leftCenter			top			(blue lighter)				haloMenuIcon 					'Menu')
	(addMoldDragHandle:						rightCenter			top			(black)				haloGrabIcon 					'Drag')
	(addDragHandle:						center 			top			(brown)				haloDragIcon 					'Move')
	(addDupHandle:						right			top			(green)				haloDuplicateIcon     					'Duplicate')	
	"(addExploreHandle:						left			topCenter			(orange)				haloDebugIcon 					'Explore')"
	(addDebugHandle:						right			topCenter			(orange)				haloDebugIcon 					'Debug')
	"(addCollapseHandle:						left			center			(tan)				haloCollapseIcon 					'Collapse')"
	(addScaleHandle:						right			center			(blue)				haloScaleIcon 					'Change scale')
	(addRotateHandle:						left			bottom			(blue)				haloRotateIcon 					'Rotate')
	(addHelpHandle:						center			bottom			(lightBlue)				haloHelpIcon 					'Help')
	(addResizeHandle:						right			bottom			(yellow)				haloResizeIcon 					'Change size')
	(addMoldCommandsHandle: left     bottomCenter (green)         haloCommandsIcon 'Run command')
	(addMoldPropertiesHandle: right   bottomCenter  (green)         haloPropertiesIcon  'Edit properties'))! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/29/2024 10:34:38'!
haloSpecsFromArray: anArray

	| aColor |
	
	^ anArray collect: [ :each |
			aColor := Color.
			each fourth do: [ :sel | aColor := aColor perform: sel].
			HaloSpec new 
				horizontalPlacement: each second
				verticalPlacement: each third 
				color: aColor
				iconSymbol: each fifth
				addHandleSelector: each first
				hoverHelp: each sixth]! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 12:47:42'!
initialize: anObject

	model := anObject.
	viewManager := WorldMoldViewManager new.
	
	self initializeView! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 22:06:08'!
initializeView

	^ self subclassResponsibility ! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/29/2024 10:55:43'!
model

	^ model! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 12:55:06'!
open: anObject

	viewManager open: anObject! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 16:09:22'!
updateView

	^ nil! !

!MoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 13:24:54'!
wantsDroppedMorph: aMorph event: evt

	^ aMorph isKindOf: MoldDesktopDraggingMorph ! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:40:20'!
canDisplay: anObject

	^ anObject isKindOf: self modelClass! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 22:08:25'!
forObject: anObject

	|viewClass|
	
	viewClass := self viewClassForModel: anObject.
	
	^ viewClass on: anObject! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 21:58:04'!
modelClass
	^ self subclassResponsibility ! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 22:04:50'!
on: anObject

	^ self new initialize: anObject! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 22:08:57'!
openForObject: anObject

	(self forObject: anObject) openInWorld! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 22:05:15'!
openOn: anObject

	^ (self on: anObject) openInWorld! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:41:27'!
viewClassForModel: anObject

	|viewClasses|
	
	viewClasses := MoldView allSubclasses.
	
	viewClasses := viewClasses asSortedCollection: [:cls1 :cls2 |
		cls1 modelClass inheritsFrom:  cls2 modelClass].
		
	^ viewClasses detect: [:cls | cls canDisplay: anObject]  
			ifNone: [StandardMoldView]! !

!MoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:41:57'!
viewClassesForModel: anObject

	|viewClasses|
	
	viewClasses := MoldView allSubclasses.
	
	viewClasses := viewClasses asSortedCollection: [:cls1 :cls2 |
		cls1 modelClass inheritsFrom:  cls2 modelClass].
		
	^ viewClasses collect: [:cls | cls canDisplay: anObject]! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 10:59:16'!
getList

	^ model collect: [:elem | elem asString]! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 5/27/2024 13:04:04'!
initializeView

	listMorph := MoldDesktopListMorph withModel: self listGetter: #getList indexGetter: #listIndex indexSetter: #listIndex:.
	
	listMorph doubleClickSelector: #openListObject.
	
	self addMorphUseAll: listMorph! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 10:59:49'!
listIndex
	^ listIndex ifNil: [0] ifNotNil: [listIndex]! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 11:00:04'!
listIndex: anIndex

	listIndex := anIndex! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 12:55:24'!
openListObject

	self open: (model at: listIndex)! !

!ListMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 16:10:07'!
updateView

	listMorph updateList! !

!ListMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 10:52:53'!
modelClass

	^ Collection! !

!StandardMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 11:55:31'!
initializeView

	|label|
	
	label := LabelMorph contents: (model asString squeezedTo: 20).
	
	self addMorphUseAll: label! !

!StandardMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/27/2024 21:58:20'!
modelClass
	^ Object! !

!StringMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 18:21:12'!
initializeView

	|label|
	
	label := LabelMorph contents: (model squeezedTo: 20).
	
	self addMorphUseAll: label! !

!StringMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 18:21:00'!
modelClass

	^ String! !

!TableMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:50:21'!
initializeView

	|tableMorph|
	
	tableMorph := TableLayoutMorph new
					rows: model rows size;
					borderWidth: 0;
					yourself.
					
	model columns do: [:colName |
		tableMorph addCell: (TableCellMorph new
								with: (LabelMorph contents: colName);
								padding: 2;
								yourself)].
							
	model rows do: [:row |
		tableMorph addCell: (TableCellMorph new
								with: (LabelMorph contents: row first asString);
								padding: 2;
								yourself).
		tableMorph addCell: (TableCellMorph new
								with: (LabelMorph contents: row second asString);
								padding: 2;
								yourself)].
							
	self addMorphUseAll: tableMorph
					! !

!TableMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:48:31'!
canDisplay: anObject

	^ TableProtocol isImplementedBy: anObject! !

!TableMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 20:50:51'!
modelClass

	^ Dictionary! !

!TreeMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:51:51'!
getList

	^ Array with: (MoldDesktopListItemWrapper with: model model: self)! !

!TreeMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/31/2024 21:45:21'!
initializeView

	"hierarchicalList := MoldDesktopHierarchicalList on: model."
	treeMorph := MoldDesktopTreeMorph
		model: self
		listGetter: #getList
		indexGetter: #selectedItem
		indexSetter: #selectedItem:
		mainView: self
		menuGetter: #getMenu
		keystrokeAction: #keystroke:from: .
			
	self addMorphUseAll: treeMorph
	! !

!TreeMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:46:13'!
selectedItem

	^ selectedItem ! !

!TreeMoldView methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 20:56:32'!
selectedItem: aMorph

	^ true! !

!TreeMoldView class methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 16:13:11'!
modelClass
	^ AbstractHierarchicalList ! !

!MoldHaloMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 18:17:31'!
addHandles: aDisplayRectangle
	"update my size. owner is world, therefore owner coordinates are Display coordinates."

	self morphPosition: aDisplayRectangle topLeft extent: aDisplayRectangle extent.
	haloBox := self handlesBox.
	target addHandlesTo: self box: haloBox.
	self addNameString: target model shortPrintString.
	self redrawNeeded.! !

!MoldHaloMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 18:18:01'!
doDup: evt with: dupHandle 
	"Ask hand to duplicate my target."

	| hand positionInHandCoordinates |
	target okayToDuplicate ifFalse: [^ self].
	hand := evt hand.
	positionInHandCoordinates := target morphPositionInWorld - hand morphPositionInWorld.
	"Duplicate has no meaningful position, as it is not in the world. Grab position from original!!"
	target := target duplicateMorph: evt.
	self addNameString: target model shortPrintString.	
	hand
		obtainHalo: self;
		grabMorph: target delta: positionInHandCoordinates! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:49:59'!
addMoldCommandsHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #openMoldCommandsMenu! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:50:06'!
addMoldDragHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #startMoldDrag:with:! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:50:11'!
addMoldPropertiesHandle: handleSpec

	(self addHandle: handleSpec)
		mouseDownSelector: #editMoldProperties! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:50:16'!
editMoldProperties
	^ PropertiesBrowser openOn: target model! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:50:21'!
openMoldCommandsMenu

	CommanderUIServices openCommandsMenuFor: target model! !

!MoldHaloMorph methodsFor: 'handles' stamp: 'MM 3/30/2024 18:50:27'!
startMoldDrag: evt with: dragHandle

	"Start drag and dropping to perform an action"
	
	| dragMorph |
	
	dragMorph := MoldDesktopDraggingMorph on: target model.
	
	evt hand grabMorph: dragMorph moveUnderHand: true! !

!MoldDesktopTreeItemMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 11:00:44'!
acceptDroppingMorph: aMorph event: evt

	"A MoldDesktop object has been dropped.
	
	Display a Menu with a list of commands that can be applied to our model and the dropped model.
	We have alternatives:
	- Build the list from methods in our model protocol that can be potentially applied (no type information).
	- Build the list from methods in our model protocol that can be applied (requires LiveTyping Cuis distribution with type information).
	- A list of explicit Commands that can be applied to those models (Commander package).
	- A variation of all of the above.
	
	Current implementation lists methods in model protocol with correct arity, and without using type information.
	"
	
	| selected |
	
	aMorph hide; delete.
	self world ifNotNil: [ :w | w activeHand removeMorph: aMorph ].
	
	selected := MoldDesktopServices openMenuWithApplicableCommandsFor: complexContents item with: aMorph model. 
	
	selected ifNotNil: [
		complexContents item perform: selected with: aMorph model.
		
		"We don't have a universal change tracking events for every object. So we update the receiving morph just in case here for now"
		"self updateView"
		]! !

!MoldDesktopTreeItemMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/31/2024 21:53:04'!
allowsMorphDrop
	^ true! !

!MoldDesktopTreeItemMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/31/2024 21:53:19'!
wantsDroppedMorph: aMorph event: evt

	^ aMorph isKindOf: MoldDesktopDraggingMorph ! !

!MoldDesktopDraggingMorph methodsFor: 'accessing' stamp: 'MM 3/30/2024 11:21:08'!
model
	"Answer the value of model"

	^ model! !

!MoldDesktopDraggingMorph methodsFor: 'accessing' stamp: 'MM 3/30/2024 11:21:08'!
model: anObject
	"Set the value of model"

	model := anObject! !

!MoldDesktopDraggingMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 10:39:39'!
justDroppedInto: newOwnerMorph event: anEvent 
	
	self delete.
	anEvent hand redrawNeeded.! !

!MoldDesktopDraggingMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 13:03:53'!
rejectDropMorphEvent: evt
	"Rejected drop of me.  Remove me from the hand."
	
	self hide; delete.
	self world ifNotNil: [ :w | w activeHand removeMorph: self ]! !

!MoldDesktopDraggingMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 10:47:47'!
wantsToBeDroppedInto: aMorph
	"Return true if it's okay to drop the receiver into aMorph. This check is symmetric to #wantsDroppedMorph:event: to give both parties a chance of figuring out whether they like each other."

	^ (aMorph isKindOf: MoldView)
		or: [{MoldDesktopTreeItemMorph. MoldDesktopListMorph}
				includes: aMorph class]! !

!MoldDesktopDraggingMorph class methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 11:53:52'!
on: anObject

	^ (self contents: (anObject asString squeezedTo: 15))
		model: anObject;
		yourself! !

!MoldDesktopTreeMorph methodsFor: 'as yet unclassified' stamp: 'MM 3/31/2024 21:44:50'!
indentingItemClass
	
	^MoldDesktopTreeItemMorph! !

!MoldDesktopListMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 11:07:36'!
acceptDroppingMorph: aMorph event: dropEvent

	| localPosition row  selected target |
	
	aMorph hide; delete.
	self world ifNotNil: [ :w | w activeHand removeMorph: aMorph ].
	
	localPosition := self internalizeFromWorld: dropEvent eventPosition.
	row := self rowAtLocation: localPosition.
	target := self getList at: row.
	
	selected := MoldDesktopServices openMenuWithApplicableCommandsFor: target with: aMorph model. 
	
	selected ifNotNil: [
		target perform: selected with: aMorph model]
	
	! !

!MoldDesktopListMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 10:45:55'!
allowsMorphDrop
	^ true! !

!MoldDesktopListMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 10:19:34'!
innerMorphClass
	^MoldDesktopInnerListMorph! !

!MoldDesktopListMorph methodsFor: 'as yet unclassified' stamp: 'MM 4/1/2024 10:50:15'!
wantsDroppedMorph: aMorph event: evt

	^ (aMorph isKindOf: MoldDesktopDraggingMorph) and: [ 	| localPosition |
		localPosition := self internalizeFromWorld: evt eventPosition.
		(self rowAtLocation: localPosition ifNone: []) isNil not]! !

!MoldDesktopListItemWrapper methodsFor: 'as yet unclassified' stamp: 'MM 3/30/2024 21:08:00'!
contents

	^ item children collect: [:child | 
		self class with: child model: model]! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:10:27'!
applicableSelectorsFor: anObject with: anArgument

"A MoldDesktop object has been dropped.
	
	Display a Menu with a list of commands that can be applied to our model and the dropped model.
	We have alternatives:
	- Build the list from methods in our model protocol that can be potentially applied (no type information).
	- Build the list from methods in our model protocol that can be applied (requires LiveTyping Cuis distribution with type information).
	- A list of explicit Commands that can be applied to those models (Commander package).
	- A variation of all of the above.
	
	Current implementation lists methods in model protocol with correct arity, and without using type information.
	"
	
	| selectors |
	
	selectors := self protocolSelectorsFor: anObject class.
	^ selectors select: [:selector | self isApplicable: selector to: anObject with: anArgument]
	
	! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:51:44'!
categorizedProtocolSelectorsFor: anObject with: anArgument
	
	| categorizedSelectors categories |
	
	categories := self protocolCategoriesOf: anObject class.
	
	categorizedSelectors := Dictionary new.
	
	categories do: [:cat |
		|categorySelectors|
		categorySelectors := Set new.
		anObject class withAllSuperclassesDo: [:class | | catSelectors applicable|
			catSelectors := class organization listAtCategoryNamed: cat.
			applicable := catSelectors select: [:selector | self isApplicable: selector to: anObject with: anArgument].
			categorySelectors addAll: applicable].
		categorySelectors ifNotEmpty:  [
			categorizedSelectors at: cat put: categorySelectors]].
	
	^ categorizedSelectors! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:05:56'!
isApplicable: aSelector to: anObject with: anArgument

	^ (aSelector count: [:char | char == $:]) == 1! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:56:00'!
openCategorizedMenuWithApplicableCommandsFor: anObject with: argument

	|menu|
	
	menu := MVCMenuMorph new.
	
	(self categorizedProtocolSelectorsFor: anObject with: argument) 
		keysAndValuesDo: [:cat :selectors |
			| catSubMenu |
			catSubMenu := MenuMorph new.
			selectors do: [:selector |
				catSubMenu add: selector asString target: [
					menu selectMVCItem: selector] action: #value].
			menu add: cat
				   subMenu: catSubMenu].
			
	^ menu invokeAt: Sensor mousePoint allowKeyboard: (Preferences at: #menuKeyboardControl)! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:44:44'!
openFlatMenuWithApplicableCommandsFor: anObject with: anArgument

	| applicableSelectors menu |
	
	applicableSelectors := self applicableSelectorsFor: anObject with: anArgument.
	
	applicableSelectors ifEmpty: [^ self]. "If there are no applicable selectors, do nothing"
	
	menu := SelectionMenu fromArray: 
				(applicableSelectors collect: [:sel | Array with: sel asString with: sel]) asArray.
	 
	^ menu startUpMenu .! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:53:29'!
openMenuWithApplicableCommandsFor: anObject with: anArgument

	^ false ifTrue: [
		self openFlatMenuWithApplicableCommandsFor: anObject with: anArgument]
	ifFalse: [
		self openCategorizedMenuWithApplicableCommandsFor: anObject with: anArgument] 
	
	! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 11:07:01'!
protocolCategoriesOf: aClass

	| categories |
	
	categories := Set new.

	aClass withAllSuperclassesDo: [:class |
		categories addAll: class organization categories].
	
	^ categories! !

!MoldDesktopServices class methodsFor: 'as yet unclassified' stamp: 'MM 4/3/2024 09:39:28'!
protocolSelectorsFor: aClass
	
	| selectors |
	
	selectors := Set new.
	
	aClass withAllSuperclasses do: [ :each |
		selectors addAll: each selectors].
	
	 selectors := selectors asOrderedCollection sort: [:x :y | x asString < y asString].
	
	^ selectors! !

!MoldViewManager methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 12:48:08'!
open: anObject

	^ self subclassResponsibility ! !

!WorldMoldViewManager methodsFor: 'as yet unclassified' stamp: 'MM 3/28/2024 12:52:44'!
open: anObject

	(MoldView forObject: anObject) openInWorld! !

!Object methodsFor: '*MoldDesktop' stamp: 'MM 3/30/2024 21:03:08'!
children

	^ {}! !

!Object methodsFor: '*MoldDesktop' stamp: 'MM 3/28/2024 20:26:41'!
openInWorld
	MoldView openForObject: self! !

!Class methodsFor: '*MoldDesktop' stamp: 'MM 3/30/2024 21:10:04'!
children

	^ self subclasses! !

!Collection methodsFor: '*MoldDesktop' stamp: 'MM 3/30/2024 20:59:55'!
children
	^ self! !

!CharacterSequence methodsFor: '*MoldDesktop' stamp: 'MM 3/30/2024 21:00:29'!
children

	^ {}! !

!Dictionary methodsFor: '*MoldDesktop-protocol' stamp: 'MM 3/28/2024 20:50:04'!
columns
	^ {'key'. 'value'}! !

!Dictionary methodsFor: '*MoldDesktop-protocol' stamp: 'MM 3/28/2024 16:12:03'!
rows

	^ self keys collect: [:key | {key. self at: key}]! !
