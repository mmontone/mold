'From Cuis 5.0 [latest update: #3969] on 16 December 2019 at 2:02:28 pm'!
'Description PlantUML plugins for BrowserPlus.'!
!provides: 'BrowserPlusPlantUML' 1 0!
SystemOrganization addCategory: #BrowserPlusPlantUML!


!classDefinition: #PlantUMLBPPlugin category: #BrowserPlusPlantUML!
BrowserPlusPlugin subclass: #PlantUMLBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlusPlantUML'!
!classDefinition: 'PlantUMLBPPlugin class' category: #BrowserPlusPlantUML!
PlantUMLBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #ActivityDiagramUMLBPPlugin category: #BrowserPlusPlantUML!
PlantUMLBPPlugin subclass: #ActivityDiagramUMLBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlusPlantUML'!
!classDefinition: 'ActivityDiagramUMLBPPlugin class' category: #BrowserPlusPlantUML!
ActivityDiagramUMLBPPlugin class
	instanceVariableNames: ''!

!classDefinition: #ClassDiagramPlantUMLBPPlugin category: #BrowserPlusPlantUML!
PlantUMLBPPlugin subclass: #ClassDiagramPlantUMLBPPlugin
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'BrowserPlusPlantUML'!
!classDefinition: 'ClassDiagramPlantUMLBPPlugin class' category: #BrowserPlusPlantUML!
ClassDiagramPlantUMLBPPlugin class
	instanceVariableNames: ''!


!ActivityDiagramUMLBPPlugin commentStamp: '<historical>' prior: 0!
This plugin generates an UML activity diagram from the selected method source, and displays it.!

!ClassDiagramPlantUMLBPPlugin commentStamp: '<historical>' prior: 0!
This plugin shows an UML diagram of the selected Smalltalk class.!
