'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 22 October 2018 at 11:58:39.474286 pm'!
'Description Please enter a description for this package'!
!provides: 'MeldExamples' 1 4!
!classDefinition: #MeldExamples category: #MeldExamples!
Object subclass: #MeldExamples
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MeldExamples'!
!classDefinition: 'MeldExamples class' category: #MeldExamples!
MeldExamples class
	instanceVariableNames: ''!


!MeldExamples class methodsFor: 'as yet unclassified' meld: #gt stamp: 'MM 10/22/2018 23:57'!
genericTemplateEx1
	
	hello <%= (2 + 2) printString %> 
	<% true ifTrue: [%> 
	    world <% ]  
      ifFalse: [ %>   
	    earth <% ] %>! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #gt stamp: 'MM 10/22/2018 23:58'!
htmlHeading

	<head>
		<title>My web page</title>
	</head>! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #gt stamp: 'MM 10/22/2018 23:56'!
htmlTemplate
	
	<html>
	    <%= self htmlHeading %>
	    <body>
	        <%= self genericTemplateEx1 %>
	    </body>
	</html> ! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #json stamp: 'MM 10/22/2018 23:37'!
jsonExample
	
	{
	  "foo": "bar",
	  "lala": false,
	  "aaa":true
	}! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #gt stamp: 'MM 10/22/2018 23:56'!
templateWithArgs: foo

	Value of foo is: <%= foo printString %>. ! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #xml stamp: 'MM 10/19/2018 20:21'!
xmlExample1
	
	<element attr="val" attr2="true"> 
	   yes. allaal.
	</element>! !

!MeldExamples class methodsFor: 'as yet unclassified' meld: #xml stamp: 'MM 10/19/2018 18:52'!
xmlExample2
	
	<element attr="val" foo="true">
	</element>! !
